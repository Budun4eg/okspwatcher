﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;

namespace OKSPWatcher.RequestModel
{
    /// <summary>
    /// Данные о заявке на модели
    /// </summary>
    public class RequestModelItem : DBObject
    {
        /// <summary>
        /// Номер заявки
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if (name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        string pathToFolder;
        /// <summary>
        /// Путь до папки с заявкой
        /// </summary>
        public string PathToFolder
        {
            get
            {
                return pathToFolder;
            }
            set
            {
                if (pathToFolder != value)
                {
                    needUpdate = true;
                }
                pathToFolder = value;
                OnPropertyChanged("PathToFolder");
            }
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                PathToFolder = data.String("PathToFolder");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                pathToFolder = data.String("PathToFolder");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public RequestModelItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public RequestModelItem() : base()
        {

        }

        protected override string sqlPath
        {
            get
            {
                return "RequestModelItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO RequestModelItems (Name, PathToFolder, UpdateDate) VALUES ('{0}', '{1}', {2})", out id, name.Check(), pathToFolder, UpdateDate.Ticks);
            base.CreateNew();
            RequestModelManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE RequestModelItems SET Name = '{0}', PathToFolder = '{1}', UpdateDate = {2} WHERE Id = {3}", name.Check(), pathToFolder, UpdateDate.Ticks, Id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM RequestModelItems WHERE Id = {0}", id);
            RequestModelManager.Instance.Remove(this);
        }
    }
}
