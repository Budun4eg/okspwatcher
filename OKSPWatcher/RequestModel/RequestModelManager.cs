﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.RequestModel
{
    /// <summary>
    /// Менеджер заявок на модели
    /// </summary>
    public class RequestModelManager : ManagerObject<RequestModelItem>
    {
        static RequestModelManager instance;

        protected RequestModelManager() : base()
        {

        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static RequestModelManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if (instance == null)
                        {
                            instance = new RequestModelManager();
                        }
                    }
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("RequestModelItems").Execute(@"DROP TABLE RequestModelItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("RequestModelItems").Execute(@"CREATE TABLE IF NOT EXISTS RequestModelItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    PathToFolder TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override RequestModelItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM RequestModelItems WHERE Id = {0} ORDER BY Name", id);
                var data = SQL.Get("RequestModelItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new RequestModelItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }
    }
}
