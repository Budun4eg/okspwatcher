﻿using OKSPWatcher.Core;
using OKSPWatcher.DepPaths;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher.RequestModel
{
    /// <summary>
    /// Логика взаимодействия для GenerateModelRequestWindow.xaml
    /// </summary>
    public partial class GenerateModelRequestWindow : BasicWindow
    {
        public GenerateModelRequestWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        string searchString;
        /// <summary>
        /// Поисковая строка
        /// </summary>
        public string SearchString
        {
            get
            {
                return searchString;
            }
            set
            {
                searchString = value;
                OnPropertyChanged("SearchString");
            }
        }

        SafeObservableCollection<string> paths = new SafeObservableCollection<string>();
        /// <summary>
        /// Список путей
        /// </summary>
        public SafeObservableCollection<string> Paths
        {
            get
            {
                return paths;
            }
        }

        RequestModelItem selected;

        /// <summary>
        /// Получить заявку на модель
        /// </summary>
        public static RequestModelItem Get()
        {
            var window = new GenerateModelRequestWindow();
            window.ShowDialog();
            return window.selected;
        }

        private void ChoosePath(object sender, RoutedEventArgs e)
        {
            var path = (sender as Control).DataContext as string;
            var item = new RequestModelItem();
            item.Name = SearchString;
            item.PathToFolder = path;
            item.Save();
            selected = item;
            Close();
        }

        private void Search(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void SearchKey(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Enter)
            {
                Search();
            }
        }

        /// <summary>
        /// Строка для поиска
        /// </summary>
        string search;

        void Search()
        {
            if (SearchString == null || SearchString.Length == 0) return;
            Paths.Clear();
            search = SearchString.ToUpper();
            var depPaths = new DepPathItem(MainApp.Instance.CurrentUser.LinkedDep);
            foreach(var dir in depPaths.Parsed)
            {
                try
                {
                    Chk(dir);
                }catch(Exception ex)
                {
                    Output.Error(ex, "Ошибка при обработке пути {0}", dir);
                }                
            }
        }

        void Chk(string path)
        {
            var info = new DirectoryInfo(path);
            var chk = info.Name.ToUpper();
            if (chk.Contains(search))
            {
                Paths.Add(path);
            }
            foreach(var child in info.GetDirectories())
            {
                Chk(child.FullName);
            }
        }
    }
}
