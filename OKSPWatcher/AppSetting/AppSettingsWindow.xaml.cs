﻿using OKSPWatcher.Core;
using System;
using System.Windows;

namespace OKSPWatcher.AppSetting
{
    /// <summary>
    /// Логика взаимодействия для AppSettingsWindow.xaml
    /// </summary>
    public partial class AppSettingsWindow : BasicWindow
    {
        /// <summary>
        /// Путь до папки с базой
        /// </summary>
        public string PathToBase
        {
            get
            {
                return AppSettings.BaseFolder;
            }
            set
            {
                AppSettings.BaseFolder = value;
                OnPropertyChanged("PathToBase");
            }
        }

        public AppSettingsWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }
    }
}
