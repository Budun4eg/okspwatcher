﻿using System;
using System.Windows;

namespace OKSPWatcher.Secondary
{
    /// <summary>
    /// Логика взаимодействия для GetTextWindow.xaml
    /// </summary>
    public partial class GetTextWindow : BasicWindow
    {
        public GetTextWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        string windowName;
        /// <summary>
        /// Наименование окна
        /// </summary>
        public string WindowName
        {
            get
            {
                return windowName;
            }
            set
            {
                windowName = value;
                OnPropertyChanged("WindowName");
            }
        }

        string requestText;
        /// <summary>
        /// Текст запроса
        /// </summary>
        public string RequestText
        {
            get
            {
                return requestText;
            }
            set
            {
                requestText = value;
                OnPropertyChanged("RequestText");
            }
        }

        string returnString;
        /// <summary>
        /// Ответ
        /// </summary>
        public string ReturnString
        {
            get
            {
                return returnString;
            }
            set
            {
                returnString = value;
                OnPropertyChanged("ReturnString");
            }
        }

        /// <summary>
        /// Получить текст
        /// </summary>
        public static string Get(string name, string request, string baseString)
        {
            var window = new GetTextWindow();
            window.WindowName = name;
            window.RequestText = request;
            window.ReturnString = baseString;
            window.ShowDialog();
            return window.ReturnString;
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
