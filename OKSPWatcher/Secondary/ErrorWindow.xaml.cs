﻿using System;
using System.Windows;

namespace OKSPWatcher.Secondary
{
    /// <summary>
    /// Логика взаимодействия для ErrorWindow.xaml
    /// </summary>
    public partial class ErrorWindow : BasicWindow
    {
        public ErrorWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Показать окно с ошибкой
        /// </summary>
        public static void ShowError(string text)
        {
            var window = new ErrorWindow();
            window.Load(text);
            window.ShowDialog();
        }

        string errorText;
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorText
        {
            get
            {
                return errorText;
            }
            set
            {
                errorText = value;
                OnPropertyChanged("ErrorText");
            }
        }

        public void Load(string text)
        {
            ErrorText = text;
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        private void OkClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void KeyPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Enter)
            {
                OkClick(null, null);
            }
        }
    }
}
