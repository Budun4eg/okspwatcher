﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace OKSPWatcher.Secondary
{
    /// <summary>
    /// Логика взаимодействия для NotifyWindow.xaml
    /// </summary>
    public partial class NotifyWindow : BasicWindow
    {
        /// <summary>
        /// Таймер автоматического закрытия
        /// </summary>
        DispatcherTimer timer;

        /// <summary>
        /// Количество секунд до автоматического закрытия
        /// </summary>
        const int secondsToWait = 5;

        public NotifyWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, secondsToWait);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Close();
        }
        
        /// <summary>
        /// Показать окно информации
        /// </summary>
        public static void ShowNote(string text)
        {
            var window = new NotifyWindow();
            window.Load(text);
            window.Show();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if(timer != null)
            {
                timer.Tick += Timer_Tick;
                if (timer.IsEnabled)
                {
                    timer.Stop();
                }                
            }
        }

        string noteText;
        /// <summary>
        /// Текст информации
        /// </summary>
        public string NoteText
        {
            get
            {
                return noteText;
            }
            set
            {
                noteText = value;
                OnPropertyChanged("NoteText");
            }
        }

        public void Load(string text)
        {
            NoteText = text;
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        private void OkClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void WindowLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var workingArea = SystemParameters.WorkArea;
            Left = workingArea.Right - Width;
            Top = workingArea.Bottom - Height;
            Topmost = true;
        }
    }
}
