﻿using OKSPWatcher.Core;
using System;
using System.Windows;

namespace OKSPWatcher.Secondary
{
    /// <summary>
    /// Логика взаимодействия для RequestWindow.xaml
    /// </summary>
    public partial class RequestWindow : BasicWindow
    {
        bool yesToAllVis;
        /// <summary>
        /// Кнопка "Да для всех" видна?
        /// </summary>
        public bool YesToAllVis
        {
            get
            {
                return yesToAllVis;
            }
            set
            {
                yesToAllVis = value;
                OnPropertyChanged("YesToAllVis");
            }
        }

        /// <summary>
        /// Возможные варианты ответа
        /// </summary>
        public enum ResultType { No, Yes, YesToAll, None }

        static ResultType result;
        /// <summary>
        /// Полученный ответ
        /// </summary>
        public static ResultType Result
        {
            get
            {
                return result;
            }
        }

        public RequestWindow()
        {
            InitializeComponent();
        }

        string requestText;
        /// <summary>
        /// Текст вопроса
        /// </summary>
        public string RequestText
        {
            get
            {
                return requestText;
            }
        }

        /// <summary>
        /// Показать окно с вопросом
        /// </summary>
        public static void Show(string question, bool yesToAll = false)
        {
            try
            {
                result = ResultType.None;
                Application.Current.Dispatcher.Invoke(new Action(delegate ()
                {
                    var window = new RequestWindow();
                    window.requestText = question;
                    window.YesToAllVis = yesToAll;
                    window.DataContext = window;
                    window.ShowDialog();
                }));
            }catch(Exception ex)
            {
                Output.WriteError(ex);
                result = ResultType.None;
            }            
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if(result == ResultType.None)
            {
                result = ResultType.No;
            }
        }

        /// <summary>
        /// Показать окно с вопросом
        /// </summary>
        public static void Show(string question, params object[] vars)
        {
            Show(string.Format(question, vars));
        }

        private void YesClick(object sender, System.Windows.RoutedEventArgs e)
        {
            result = ResultType.Yes;
            Close();
        }

        private void NoClick(object sender, System.Windows.RoutedEventArgs e)
        {
            result = ResultType.No;
            Close();
        }

        private void YesToAllClick(object sender, System.Windows.RoutedEventArgs e)
        {
            result = ResultType.YesToAll;
            Close();
        }
    }
}
