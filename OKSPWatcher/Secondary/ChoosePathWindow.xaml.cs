﻿using OKSPWatcher.Core;
using System;
using System.ComponentModel;
using System.Windows.Forms;

/// <summary>
/// Optimized v1.0 @ Nikitin V.
/// Budun4eg@gmail.com
/// </summary>

namespace OKSPWatcher.Secondary
{
    /// <summary>
    /// Логика взаимодействия для ChoosePathWindow.xaml
    /// </summary>
    public partial class ChoosePathWindow : BasicWindow
    {
        string pathToBase = "";
        /// <summary>
        /// Путь до базы
        /// </summary>
        public string PathToBase {
            get
            {
                return pathToBase;
            }
            set
            {
                pathToBase = value;
                OnPropertyChanged("PathToBase");
            }
        }

        public ChoosePathWindow()
        {
            InitializeComponent();
            System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if(AppSettings.BaseFolder == null || Core.AppSettings.BaseFolder == "")
            {
                e.Cancel = true;
            }
        }

        private void SaveButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if(PathToBase != null && PathToBase != "")
            {
                Core.AppSettings.BaseFolder = PathToBase;
                Close();
            }
        }

        private void ChoosePathClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.Description = "Укажите путь до базы";
            folderDialog.ShowNewFolderButton = false;
            folderDialog.SelectedPath = PathToBase;

            try
            {                
                folderDialog.ShowDialog();
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
            PathToBase = folderDialog.SelectedPath;
        }
    }
}
