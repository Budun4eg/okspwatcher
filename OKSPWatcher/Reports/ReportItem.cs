﻿using OKSPWatcher.Core;
using OKSPWatcher.Products;
using System.Collections.Generic;

namespace OKSPWatcher.Reports
{
    /// <summary>
    /// Строка отчета
    /// </summary>
    public class ReportItem : NotifyObject
    {
        Product linkedProduct;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return linkedProduct;
            }
            set
            {
                linkedProduct = value;
                OnPropertyChanged("LinkedProduct");
            }
        }

        protected SafeObservableCollection<ReportCell> items = new SafeObservableCollection<ReportCell>();
        /// <summary>
        /// Список ячеек
        /// </summary>
        public SafeObservableCollection<ReportCell> Items
        {
            get
            {
                return items;
            }
        }

        /// <summary>
        /// Список интервалов
        /// </summary>
        protected List<long> intervals;
        
        public ReportItem(Product product, List<long> _intervals)
        {
            linkedProduct = product;
            for (int i = 0; i < _intervals.Count; i++)
            {
                items.Add(new ReportCell());
            }
            intervals = _intervals;
        }

        public ReportItem(List<long> intervals, string _name)
        {
            name = _name;
            for (int i = 0; i < intervals.Count; i++)
            {
                items.Add(new ReportCell());
            }
        }
    }
}
