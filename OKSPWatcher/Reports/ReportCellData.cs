﻿using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Reports
{
    /// <summary>
    /// Данные ячейки
    /// </summary>
    public class ReportCellData
    {
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        public ReportCellData(User _user, DateTime _date, string _name)
        {
            LinkedUser = _user;
            Date = _date;
            Name = _name;
        }
    }
}
