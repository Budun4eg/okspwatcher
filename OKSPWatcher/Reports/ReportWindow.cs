﻿using OKSPWatcher.Core;
using OKSPWatcher.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace OKSPWatcher.Reports
{
    /// <summary>
    /// Окно с отчетом
    /// </summary>
    public class ReportWindow<T> : BasicWindow where T : ReportItem
    {
        protected SafeObservableCollection<T> reportItems = new SafeObservableCollection<T>();
        /// <summary>
        /// Список элементов отчета
        /// </summary>
        public SafeObservableCollection<T> ReportItems
        {
            get
            {
                return reportItems;
            }
        }

        List<long> dateIntervals = new List<long>();

        /// <summary>
        /// Интервалы дат
        /// </summary>
        public List<long> DateIntervals {
            get
            {
                return dateIntervals;
            }
        }

        int total;
        /// <summary>
        /// Суммарное количество элементов
        /// </summary>
        public int Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
                OnPropertyChanged("Total");
            }
        }

        protected ScrollViewer header;

        protected void ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            header.ScrollToHorizontalOffset(e.HorizontalOffset);
        }

        protected void PanelInit(object sender, EventArgs e)
        {
            var panel = sender as StackPanel;
            var reportItem = panel.DataContext as T;

            StringBuilder tooltip = new StringBuilder();
            Insert(panel, null, reportItem.LinkedProduct != null ? reportItem.LinkedProduct.Name : reportItem.Name, "", true);
            bool mustHighlight = true;
            foreach (ReportCell count in reportItem.Items)
            {
                tooltip = new StringBuilder();
                int counter = 0;
                foreach (KeyValuePair<User, int> userPair in count.Users)
                {
                    tooltip.Append(string.Format("{0} - {1}", userPair.Value, userPair.Key.Name));
                    counter++;
                    if (counter < count.Users.Count)
                    {
                        tooltip.Append(Environment.NewLine);
                    }
                }
                Insert(panel, count, count.SummaryCount > 0 ? count.SummaryCount.ToString() : "", tooltip.ToString(), false, false, mustHighlight);
                mustHighlight = false;
            }
        }

        /// <summary>
        /// Стиль элемента отчета
        /// </summary>
        Style reportStyle;

        /// <summary>
        /// Цвет строки
        /// </summary>
        SolidColorBrush labelBrush;

        protected void Insert(StackPanel panel, ReportCell data, string text, string tooltip = "", bool isDouble = false, bool small = false, bool highlight = false)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate {
                    var label = new Label();
                    if(reportStyle == null)
                    {
                        reportStyle = FindResource("ReportItem") as Style;
                    }
                    label.Style = reportStyle;
                    if (isDouble)
                    {
                        label.Width *= 2f;
                    }
                    if (small)
                    {
                        label.FontSize -= 2;
                    }
                    if (tooltip != "")
                    {
                        label.ToolTip = tooltip;
                    }
                    if (highlight)
                    {
                        if(labelBrush == null)
                        {
                            labelBrush = Application.Current.FindResource("HighlightBrush") as SolidColorBrush;
                        }
                        label.Background = labelBrush;
                    }
                    if(data != null && data.SummaryCount > 0)
                    {
                        label.MouseLeftButtonDown += Label_MouseLeftButtonDown;
                        label.DataContext = data;
                        label.Cursor = System.Windows.Input.Cursors.Hand;
                    }
                    
                    label.Content = text;
                    panel.Children.Add(label);
                }), DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        private void Label_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var data = (sender as Label).DataContext as ReportCell;
            ReportCellDataWindow.Show(data);
        }
    }
}
