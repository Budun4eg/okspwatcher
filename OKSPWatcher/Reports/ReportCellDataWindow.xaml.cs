﻿namespace OKSPWatcher.Reports
{
    /// <summary>
    /// Логика взаимодействия для ReportCellDataWindow.xaml
    /// </summary>
    public partial class ReportCellDataWindow : BasicWindow
    {
        public ReportCellDataWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Показать данные о ячейке
        /// </summary>
        public static void Show(ReportCell cell)
        {
            var window = new ReportCellDataWindow();
            window.DataContext = cell;
            window.ShowDialog();
        }
    }
}
