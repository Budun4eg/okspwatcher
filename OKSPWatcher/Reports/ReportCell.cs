﻿using OKSPWatcher.Core;
using OKSPWatcher.Users;
using System.Collections.Generic;

namespace OKSPWatcher.Reports
{
    /// <summary>
    /// Ячейка отчета
    /// </summary>
    public class ReportCell : NotifyObject
    {
        int summaryCount;
        /// <summary>
        /// Суммарное количество элементов в ячейке
        /// </summary>
        public int SummaryCount
        {
            get
            {
                return summaryCount;
            }
            set
            {
                summaryCount = value;
                OnPropertyChanged("SummaryCount");
            }
        }

        SafeObservableCollection<ReportCellData> datas = new SafeObservableCollection<ReportCellData>();
        /// <summary>
        /// Список данных по пользователям
        /// </summary>
        public SafeObservableCollection<ReportCellData> Datas
        {
            get
            {
                return datas;
            }
        }

        Dictionary<User, int> users = new Dictionary<User, int>();
        /// <summary>
        /// Список пользователей в ячейке
        /// </summary>
        public Dictionary<User, int> Users
        {
            get
            {
                return users;
            }
        }
    }
}
