﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Texts
{
    /// <summary>
    /// Менеджер текстовых заметок
    /// </summary>
    public class TextItemManager : ManagerObject<TextItem>
    {
        static TextItemManager instance;

        protected TextItemManager() { }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static TextItemManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new TextItemManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override TextItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM TextNotes WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("TextNotes").ExecuteRead(request);

                foreach (SQLDataRow row in data)
                {
                    Add(new TextItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("TextNotes").Execute(@"DROP TABLE TextNotes");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("TextNotes").Execute(@"CREATE TABLE IF NOT EXISTS TextNotes (
                                                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                TextData TEXT,
                                                User INTEGER,
                                                Date INTEGER,
                                                UpdateDate INTEGER
                                                )");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
