﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Texts
{
    /// <summary>
    /// Набор текстовых заметок
    /// </summary>
    public class TextItemPack : ItemPack<TextItem>
    {

    }
}
