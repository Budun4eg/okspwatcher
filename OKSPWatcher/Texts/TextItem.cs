﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Texts
{
    /// <summary>
    /// Текстовая заметка
    /// </summary>
    public class TextItem : DBObject
    {
        string text;
        /// <summary>
        /// Текст заметки
        /// </summary>
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                if(text != value)
                {
                    needUpdate = true;
                }
                text = value;
                OnPropertyChanged("Text");
            }
        }

        public override string Name
        {
            get
            {
                return text != null ? text : name;
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(userId != toSet)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        long date;
        /// <summary>
        /// Дата заметки
        /// </summary>
        public DateTime Date
        {
            get
            {
                return new DateTime(date);
            }
            set
            {
                if(date != value.Ticks)
                {
                    needUpdate = true;
                }
                date = value.Ticks;
                OnPropertyChanged("Date");
            }
        }

        public TextItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public TextItem() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Text = data.String("TextData");
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                Date = new DateTime(data.Long("Date"));
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                text = data.String("TextData");
                userId = data.Long("User");
                date = data.Long("Date");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "TextNotes";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO TextNotes (TextData, User, Date, UpdateDate) VALUES ('{0}', {1}, {2}, {3})", out id,
                text.Check(), userId, date, UpdateDate.Ticks);
            base.CreateNew();
            TextItemManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE TextNotes SET TextData = '{0}', User = {1}, Date = {2}, UpdateDate = {3} WHERE Id = {4}",
                text.Check(), userId, date, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM TextNotes WHERE Id = {0}", id);
            TextItemManager.Instance.Remove(this);
        }
    }
}
