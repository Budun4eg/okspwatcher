﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.ImageNotation
{
    /// <summary>
    /// Менеджер якорей на изображениях
    /// </summary>
    public class ImageAnchorManager : ManagerObject<ImageAnchor>
    {
        static ImageAnchorManager instance;

        protected ImageAnchorManager() { }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ImageAnchorManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ImageAnchorManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("ImageAnchorItems").Execute(@"DROP TABLE ImageAnchorItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("ImageAnchorItems").Execute(@"CREATE TABLE IF NOT EXISTS ImageAnchorItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    X TEXT,
                                                    Y TEXT,
                                                    Width TEXT,
                                                    Height TEXT,
                                                    Code TEXT,
                                                    UserId INTEGER,
                                                    InsertDate INTEGER,
                                                    TextData TEXT,
                                                    UpdateDate INTEGER                       
                                                    )");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override ImageAnchor ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM ImageAnchorItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("ImageAnchorItems").ExecuteRead(request);

                foreach (SQLDataRow row in data)
                {
                    Add(new ImageAnchor(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }
    }
}
