﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Users;
using System;
using System.Text;
using System.Windows;

namespace OKSPWatcher.ImageNotation
{
    /// <summary>
    /// Якорь на изображении
    /// </summary>
    public class ImageAnchor : DBObject
    {
        Rect position;
        /// <summary>
        /// Положение якоря
        /// </summary>
        public Rect Position
        {
            get
            {
                return position;
            }
            set
            {
                if(position != value)
                {
                    needUpdate = true;
                }
                position = value;
                OnPropertyChanged("Position");
            }
        }

        string code;
        /// <summary>
        /// Код якоря
        /// </summary>
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                if(value != code)
                {
                    needUpdate = true;
                }
                code = value;
                OnPropertyChanged("Code");
            }
        }

        string text;
        /// <summary>
        /// Текст якоря
        /// </summary>
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                if (text != value) {
                    needUpdate = true;
                }
                text = value;
                OnPropertyChanged("Text");
                OnPropertyChanged("Tooltip");
            }
        }

        /// <summary>
        /// Строка для вывода
        /// </summary>
        public string Tooltip
        {
            get
            {
                StringBuilder builder = new StringBuilder();

                if(Text != null)
                {
                    builder.Append(Text);
                }
                if(LinkedUser != null)
                {
                    builder.AppendFormat("{0}{1}", Environment.NewLine, LinkedUser.ShortName);
                }
                if(Date.Ticks > 0)
                {
                    builder.AppendFormat("{0}{1}", Environment.NewLine, Date);
                }

                return builder.ToString();
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(userId != toSet)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
                OnPropertyChanged("Tooltip");
            }
        }

        long date;
        /// <summary>
        /// Дата установки якоря
        /// </summary>
        public DateTime Date
        {
            get
            {
                return new DateTime(date);
            }
            set
            {
                if(date != value.Ticks)
                {
                    needUpdate = true;
                }
                date = value.Ticks;
                OnPropertyChanged("Date");
                OnPropertyChanged("Tooltip");
            }
        }

        public ImageAnchor(SQLDataRow data) : base()
        {
            Load(data);
        }

        public ImageAnchor() : base()
        {
            needUpdate = true;
        }

        public ImageAnchor(Rect size, string _code) : base() {
            position = size;
            Code = _code;
            LinkedUser = MainApp.Instance.CurrentUser;
            Date = DateTime.Now;
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Position = new Rect(data.Double("X"), data.Double("Y"), data.Double("Width"), data.Double("Height"));
                Code = data.String("Code");
                userId = data.Long("UserId");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                Date = new DateTime(data.Long("InsertDate"));
                Text = data.String("TextData");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                position = new Rect(data.Double("X"), data.Double("Y"), data.Double("Width"), data.Double("Height"));
                code = data.String("Code");
                userId = data.Long("UserId");
                date = data.Long("InsertDate");
                text = data.String("TextData");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }            
        }

        protected override string sqlPath
        {
            get
            {
                return "ImageAnchorItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO ImageAnchorItems (X, Y, Width, Height, Code, UserId, InsertDate, TextData, UpdateDate) VALUES ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}', {8})", out id,
                position.X, position.Y, position.Width, position.Height, code, userId, date, text.Check(), UpdateDate.Ticks);
            base.CreateNew();
            ImageAnchorManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE ImageAnchorItems SET X = '{0}', Y = '{1}', Width = '{2}', Height = '{3}', Code = '{4}', UserId = {5}, InsertDate = {6}, TextData = '{7}', UpdateDate = {8} WHERE Id = {9}",
                position.X, position.Y, position.Width, position.Height, code, userId, date, text.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM ImageAnchorItems WHERE Id = {0}", id);
            ImageAnchorManager.Instance.Remove(this);
        }
    }
}
