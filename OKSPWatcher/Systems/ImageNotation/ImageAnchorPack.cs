﻿using OKSPWatcher.Core;

namespace OKSPWatcher.ImageNotation
{
    /// <summary>
    /// Набор якорей на изображении
    /// </summary>
    public class ImageAnchorPack : ItemPack<ImageAnchor>
    {
        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = ImageAnchorManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }

        /// <summary>
        /// Сохранить все якоря
        /// </summary>
        public new void Save()
        {
            foreach(var item in items)
            {
                item.Save();
            }
        }
    }
}
