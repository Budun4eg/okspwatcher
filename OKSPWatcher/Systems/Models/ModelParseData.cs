﻿using OKSPWatcher.Core;
using System.Text;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Данные разбора файла модели
    /// </summary>
    public class ModelParseData : DseParseData
    {
        bool wrongFolder;
        /// <summary>
        /// Модель в неправильной папке
        /// </summary>
        public bool WrongFolder
        {
            get
            {
                return wrongFolder;
            }
            set
            {
                wrongFolder = value;
            }
        }

        long modelVersion = -1;
        /// <summary>
        /// Версия модели
        /// </summary>
        public long ModelVersion
        {
            get
            {
                return modelVersion;
            }
            set
            {
                modelVersion = value;
            }
        }

        /// <summary>
        /// Данные модели полностью разобраны?
        /// </summary>
        public override bool IsParsed
        {
            get
            {
                var ret = base.IsParsed;
                if (!ret) return false;
                if (modelVersion == -1) return false;
                return true;
            }
        }

        /// <summary>
        /// Получить строку проверки модели
        /// </summary>
        public override string CheckString
        {
            get
            {
                builder = new StringBuilder(base.CheckString);
                if (modelVersion == -1) builder.Append("версия модели, ");

                return builder.ToString();
            }
        }
    }
}
