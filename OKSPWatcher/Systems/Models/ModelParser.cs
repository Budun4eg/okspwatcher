﻿using OKSPWatcher.Core;
using OKSPWatcher.Parsers;
using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Объект для разбора данных о модели
    /// </summary>
    public class ModelParser : ParseManager<ModelTemplateItem, ModelParseData>
    {
        static ModelParser instance;

        protected ModelParser()
        {
            pathToTemplates = AppSettings.TemplatesFolder + "/ModelParser.txt";
            UpdateTemplate();
        }

        public override void UpdateTemplate()
        {
            base.UpdateTemplate();
            translitedPrd = ProductManager.Instance.Translited;
        }

        Dictionary<string, ProductPack> translitedPrd;

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ModelParser Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ModelParser();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Разобрать данные
        /// </summary>
        public ModelParseData Parse(string pathToImage)
        {
            return Parse(new FileInfo(pathToImage), null);
        }

        /// <summary>
        /// Разобрать данные
        /// </summary>
        public ModelParseData Parse(FileInfo model, DirectoryInfo product)
        {
            var data = new ModelParseData();
            data.FilePath = model.FullName;

            if (model.Exists)
            {
                data.Date = model.LastWriteTime;
            }

            string productName = null;

            if (product != null)
            {
                string toCheck = product.Name.ToUpper();
                data.ProductItem = translitedPrd.ContainsKey(toCheck) ? translitedPrd[toCheck].Single : null;
                if(data.ProductItem != null)
                {
                    productName = toCheck;
                }
            }

            string name = model.Name.ToUpper();

            var splName = name.Split(new string[] { ".PRT.", ".ASM." }, StringSplitOptions.None);
            if (splName.Length != 2) return data;
            name = splName[0];
            data.ModelVersion = splName[1].ToLong();

            string ownerName = null;

            try
            {
                ownerName = model.GetAccessControl().GetOwner(typeof(NTAccount)).Value.Split(new char[] { '\\' })[1];
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }

            if (ownerName != null)
            {
                data.LinkedUser = UserManager.Instance.ByLogin(ownerName, true).Single;
            }

            if (data.ProductItem == null)
            {
                foreach (var prdPair in translitedPrd)
                {
                    if (name.StartsWith(prdPair.Key))
                    {
                        if (data.ProductItem == null || data.ProductItem.Name.Length < prdPair.Key.Length)
                        {
                            data.ProductItem = prdPair.Value.Single;
                            if(data.ProductItem != null)
                            {
                                productName = prdPair.Key;
                            }                            
                        }
                    }
                }
            }

            if (productName != null)
            {
                data.ProductName = productName;
                if (name.StartsWith(string.Format("{0}_", productName)))
                {
                    name = name.Substring(productName.Length + 1);
                }
                
                if (!data.WrongFolder)
                {
                    DirectoryInfo directory = model.Directory;
                    try
                    {
                        if (!directory.Name.ToUpper().StartsWith(data.ProductItem.Name.Translit()))
                        {
                            data.WrongFolder = true;
                        }
                    }catch(Exception ex)
                    {
                        Output.Error(ex, "Ошибка при определении требуемой папки на модель {0}", data.FilePath);
                    }                    
                }
            }

            foreach (var template in templates)
            {
                if (template.Parse(name, data))
                {
                    return data;
                }
            }
            return data;
        }
    }
}
