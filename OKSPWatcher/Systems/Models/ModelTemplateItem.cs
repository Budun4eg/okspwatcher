﻿using OKSPWatcher.Core;
using OKSPWatcher.Parsers;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Шаблон разбора
    /// </summary>
    public class ModelTemplateItem : ParseTemplate<ModelParseData>
    {
        public ModelTemplateItem() : base() { }

        /// <summary>
        /// Разобрать наименование в соответствии с шаблоном
        /// </summary>
        public override bool Parse(string name, ModelParseData data)
        {
            if (!Check(name)) return false;

            int leftBracket;
            int rightBracket;
            string parse = temp;
            string value = name;

            bool parseCompleted = true;

            while (true)
            {
                leftBracket = parse.IndexOf('{');
                if (leftBracket < 0)
                {
                    break;
                }
                else
                {
                    rightBracket = parse.IndexOf('}');
                    if (rightBracket < 0)
                    {
                        Output.ErrorFormat("Ошибка в шаблоне {0}", temp);
                        return false;
                    }
                    else
                    {
                        try
                        {
                            int leftOffset = leftBracket + 1;
                            int length = rightBracket - leftOffset;
                            string dat = parse.Substring(leftOffset, length);
                            string val = value.Substring(leftOffset - 1, length);
                            switch (dat[0])
                            {
                                case '0':
                                    for (int i = 0; i < val.Length; i++)
                                    {
                                        if (toCheck.ContainsKey('0') && toCheck['0'].ContainsKey(i))
                                        {
                                            if (val[i] != toCheck['0'][i])
                                            {
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            if (!char.IsDigit(val[i]))
                                            {
                                                return false;
                                            }
                                        }
                                    }
                                    data.Group = val;
                                    break;
                                case '1':
                                    foreach (var ch in val)
                                    {
                                        if (!char.IsDigit(ch))
                                        {
                                            return false;
                                        }
                                    }
                                    data.Name = val;
                                    break;
                                case '2':
                                    data.VariantIndex = val.ToInt(out parseCompleted) - 1;
                                    break;
                              }
                            parse = parse.Remove(rightBracket, 1).Remove(leftBracket, 1);
                        }
                        catch (Exception ex)
                        {
                            Output.Error(ex, "Ошибка при разборе шаблоном", temp);
                            return false;
                        }
                    }
                }
            }

            if (parseCompleted)
            {
                if (data.VariantIndex < 0)
                {
                    data.VariantIndex = 0;
                }
            }

            string nameTemp = nameTemplate;
            List<object> args = new List<object>();
            while (true)
            {
                int leftBrName = nameTemp.IndexOf('{');
                if (leftBrName < 0)
                {
                    break;
                }
                else
                {
                    switch (nameTemp[leftBrName + 1])
                    {
                        case '0':
                            nameTemplate = nameTemplate.Replace("{0}", "{" + args.Count + "}");
                            args.Add(data.Group);
                            break;
                        case '1':
                            nameTemplate = nameTemplate.Replace("{1}", "{" + args.Count + "}");
                            args.Add(data.Name);
                            break;
                        case '2':
                            nameTemplate = nameTemplate.Replace("{2}", "{" + args.Count + "}");
                            args.Add(data.VariantIndex+1);
                            break;
                    }
                }
                nameTemp = nameTemp.Remove(leftBrName, 1);
            }

            if (data.ProductItem == null)
            {
                data.Name = null;
            }
            else
            {
                data.Name = string.Format("{0}{1}{2}", data.ProductItem.Name, data.ProductSplitter, string.Format(nameTemplate, args.ToArray()).Replace('_','.'));
            }

#if IN_EDITOR && FULL_OUTPUT
            Output.LogFormat("{0} разобрано шаблоном {1}", name, fullTemplate);
#endif

            return true;
        }
    }
}
