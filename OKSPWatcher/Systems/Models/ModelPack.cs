﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Набор моделей
    /// </summary>
    public class ModelPack : ItemPack<ModelItem>
    {
        public ModelPack() : base()
        {
            name = "ModelPack";
        }

        /// <summary>
        /// Самый ранний элемент набора
        /// </summary>
        public ModelItem Top
        {
            get
            {
                if (needRefresh)
                {
                    Refresh();
                }
                if (Count == 0) return null;
                ModelItem top = items[0];
                for (int i = 1; i < Count - 1; i++)
                {
                    if (items[i].Date < top.Date)
                    {
                        top = items[i];
                    }
                }
                return top;
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = ModelManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
