﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System;
using System.Text;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// 3D-модель
    /// </summary>
    public class ModelItem : DBObject
    {
        /// <summary>
        /// Описание модели
        /// </summary>
        public string Tooltip
        {
            get
            {
                StringBuilder builder = new StringBuilder(name);

                if(LinkedUser != null)
                {
                    builder.AppendFormat("{0}{1}", Environment.NewLine, LinkedUser.Name);
                }
                builder.AppendFormat("{0}{1}", Environment.NewLine, Date.ToShortDateString());

                return builder.ToString();
            }
        }

        string pathToModel;
        /// <summary>
        /// Путь до модели
        /// </summary>
        public string PathToModel
        {
            get
            {
                return pathToModel;
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId);
            }
            set
            {
                var toSet = value != null ? value.Id : 0;
                if(userId != toSet)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        long productId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(productId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if (productId != toSet)
                {
                    needUpdate = true;
                }
                productId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        long date;
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime Date
        {
            get
            {
                return new DateTime(date);
            }
            set
            {
                if(date != value.Ticks)
                {
                    needUpdate = true;
                }
                date = value.Ticks;
                OnPropertyChanged("Date");
            }
        }

        bool parsed;
        /// <summary>
        /// Данные о модели полностью разобраны?
        /// </summary>
        public bool Parsed
        {
            get
            {
                return parsed;
            }
            set
            {
                if(parsed != value)
                {
                    needUpdate = true;
                }
                parsed = value;
                OnPropertyChanged("Parsed");
            }
        }

        long modelVersion;
        /// <summary>
        /// Версия модели
        /// </summary>
        public long ModelVersion
        {
            get
            {
                return modelVersion;
            }
            set
            {
                if(modelVersion != value)
                {
                    needUpdate = true;
                }
                modelVersion = value;
                OnPropertyChanged("ModelVersion");
            }
        }

        public ModelItem() : base() {
            needUpdate = true;
        }

        public ModelItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public ModelItem(ModelParseData data) : base()
        {
            name = data.Name;
            pathToModel = data.FilePath;
            LinkedUser = data.LinkedUser;
            Date = data.Date;
            LinkedProduct = data.ProductItem;
            ModelVersion = data.ModelVersion;
            Parsed = data.IsParsed;
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                pathToModel = data.String("PathToModel");
                OnPropertyChanged("PathToModel");
                Date = new DateTime(data.Long("ModelDate"));
                Parsed = data.Bool("Parsed");
                productId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                ModelVersion = data.Long("ModelVersion");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                userId = data.Long("User");
                pathToModel = data.String("PathToModel");
                date = data.Long("ModelDate");
                parsed = data.Bool("Parsed");
                productId = data.Long("Product");
                modelVersion = data.Long("ModelVersion");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "ModelItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO ModelItems (Name, User, PathToModel, ModelDate, Parsed, ModelVersion, Product, UpdateDate) VALUES ('{0}', {1}, '{2}', {3}, {4}, {5}, {6}, {7})", out id,
                name.Check(), userId, pathToModel, date, parsed.ToLong(), modelVersion, productId, UpdateDate.Ticks);
            base.CreateNew();
            ModelManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE ModelItems SET Name = '{0}', User = {1}, PathToModel = '{2}', ModelDate = {3}, Parsed = {4}, ModelVersion = {5}, Product = {6}, UpdateDate = {7} WHERE Id = {8}",
                name.Check(), userId, pathToModel, date, parsed.ToLong(), modelVersion, productId, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM ModelItems WHERE Id = {0}", id);
            ModelManager.Instance.Remove(this);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("LinkedUser", userId);
            AddStateItem("PathToModel", pathToModel);
            AddStateItem("Date", date);
            AddStateItem("Parsed", parsed);
            AddStateItem("ModelVersion", modelVersion);
            AddStateItem("LinkedProduct", productId);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                userId = (long)savedState["LinkedUser"];
                pathToModel = (string)savedState["PathToModel"];
                date = (long)savedState["Date"];
                parsed = (bool)savedState["Parsed"];
                modelVersion = (long)savedState["ModelVersion"];
                productId = (long)savedState["LinkedProduct"];
                base.LoadState();
            }
        }

        /// <summary>
        /// Синхронизировать состояние объекта с данными
        /// </summary>
        public void Sync(ModelParseData data)
        {
            if (data.IsParsed && !Parsed)
            {
                Parsed = data.IsParsed;
            }
            if(data.Name != null)
            {
                name = data.Name;
            }

            if (data.LinkedUser != null)
            {
                LinkedUser = data.LinkedUser;
            }

            if(data.ProductItem != null)
            {
                LinkedProduct = data.ProductItem;
            }

            if(data.ModelVersion != -1)
            {
                ModelVersion = data.ModelVersion;
            }
        }
    }
}
