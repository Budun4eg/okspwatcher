﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Менеджер 3D-моделей
    /// </summary>
    public class ModelManager : ManagerObject<ModelItem>
    {
        private static object root = new object();

        static ModelManager instance;

        protected ModelManager() : base()
        {
            pathToModel = AppSettings.GetString("PathToModel");
        }

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static ModelManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ModelManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Список моделей по наименованию
        /// </summary>
        Dictionary<string, ModelPack> byName = new Dictionary<string, ModelPack>();

        /// <summary>
        /// Список моделей отсортированных по наименованию
        /// </summary>
        public List<ModelPack> SortedByName
        {
            get
            {
                return byName.Values.ToList();
            }
        }

        /// <summary>
        /// Получить набор моделей
        /// </summary>
        public ModelPack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM ModelItems WHERE Name = '{0}' ORDER BY Id", key);
                    var data = SQL.Get("ModelItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new ModelItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new ModelPack();
                    byName.Add(key, item);
                    return item;
                }                
            }
        }

        /// <summary>
        /// Список моделей по изделию
        /// </summary>
        Dictionary<Product, ModelPack> byProduct = new Dictionary<Product, ModelPack>();

        /// <summary>
        /// Получить набор моделей
        /// </summary>
        public ModelPack ByProduct(Product key, bool load = false)
        {
            if (byProduct.ContainsKey(key))
            {
                return byProduct[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM ModelItems WHERE Product = {0} ORDER BY Id", key.Id);
                    var data = SQL.Get("ModelItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new ModelItem(row));
                    }
                    return ByProduct(key);
                }
                else
                {
                    var item = new ModelPack();
                    byProduct.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Получить модель
        /// </summary>
        public ModelPack ByNameCertain(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM ModelItems WHERE Name = '{0}' LIMIT 1", key);
                    var data = SQL.Get("ModelItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new ModelItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new ModelPack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Список отсортированных по наименованию и изделию наборов
        /// </summary>
        public Dictionary<Product, List<ModelPack>> Sorted
        {
            get
            {
                Dictionary<Product, List<ModelPack>> sorted = new Dictionary<Product, List<ModelPack>>();
                foreach (var item in byName.Values)
                {
                    var top = item.Top;
                    if (top == null) continue;
                    var prd = top.LinkedProduct != null ? top.LinkedProduct : ProductManager.Instance.NullItem;
                    if (sorted.ContainsKey(prd))
                    {
                        sorted[prd].Add(item);
                    }
                    else
                    {
                        sorted.Add(prd, new List<ModelPack>() { item });
                    }
                }
                var keys = sorted.Keys.OrderBy(o => o.Name);
                Dictionary<Product, List<ModelPack>> toReturn = new Dictionary<Product, List<ModelPack>>();
                foreach (var key in keys)
                {
                    toReturn.Add(key, sorted[key]);
                }
                return toReturn;
            }
        }

        string pathToModel;
        /// <summary>
        /// Путь до моделей
        /// </summary>
        public string PathToModel
        {
            get
            {
                return pathToModel;
            }
            set
            {
                if(pathToModel != value)
                {
                    AppSettings.SetString("PathToModel", value);
                    pathToModel = value;
                    OnPropertyChanged("PathToModel");
                }
            }
        }

        /// <summary>
        /// Получить модель по идентификатору
        /// </summary>
        public override ModelItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM ModelItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("ModelItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new ModelItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }



        /// <summary>
        /// Добавить модель
        /// </summary>
        public override bool Add(ModelItem item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new ModelPack() { item });
                    }
                }
                if (item.LinkedProduct != null)
                {
                    if (byProduct.ContainsKey(item.LinkedProduct))
                    {
                        byProduct[item.LinkedProduct].Add(item);
                    }
                    else
                    {
                        byProduct.Add(item.LinkedProduct, new ModelPack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить модель
        /// </summary>
        public override void Remove(ModelItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if (item.LinkedProduct != null && byProduct.ContainsKey(item.LinkedProduct) && byProduct[item.LinkedProduct].Contains(item))
                {
                    byProduct[item.LinkedProduct].Remove(item);
                    if (byProduct[item.LinkedProduct].Count == 0)
                    {
                        byProduct.Remove(item.LinkedProduct);
                    }
                }
            }            
        }

        List<string> _parsedPaths;

        /// <summary>
        /// Список добавленных моделей
        /// </summary>
        List<string> parsed
        {
            get
            {
                if(_parsedPaths == null)
                {
                    _parsedPaths = new List<string>();
                    var data = SQL.Get("ModelItems").ExecuteRead("SELECT PathToModel FROM ModelItems ORDER BY Id");
                    foreach (SQLDataRow row in data)
                    {
                        _parsedPaths.Add(row.String("PathToModel"));
                    }
                }
                return _parsedPaths;
            }
        }

        BasicWindow locked;

        int modelCount;

        int queueLength;

        public override bool CancelUpdate
        {
            set
            {
                base.CancelUpdate = value;
                queueLength = 0;
                CheckComplete();
            }
        }

        DateTime start;

        void CheckComplete()
        {
            lock (changeLock)
            {
                if (completed) return;
                if (queueLength == 0)
                {
                    completed = true;
                    DataIsUpdated(null, null);                    
                }
            }
        }

        /// <summary>
        /// Обновление завершено?
        /// </summary>
        bool completed;

        protected override void DataIsUpdated(object sender, RunWorkerCompletedEventArgs e)
        {
            if (completed)
            {
                base.DataIsUpdated(sender, e);
                DateTime end = DateTime.Now;
                Output.LogFormat("Данные о моделях обновлены [{0:0.00}с]", (end - start).TotalSeconds);

                mustUpdate = false;
                OnDataUpdated();
                BackgroundHandler.Unregister(worker);
            }
        }

        protected override void UpdateData(object sender, DoWorkEventArgs e)
        {
            base.UpdateData(sender, e);
            completed = false;
            start = DateTime.Now;
            locked = BasicWindow.Current;
            if (locked != null)
            {
                BasicWindow.Current.UpdateText = "3D-модели";
                Output.Log("Обновление 3D-моделей...");
                locked.Progress = 0f;
            }

            bool mode = AppSettings.GetBool("ThreadMode");
            
            int cnt = Directory.GetFiles(pathToModel, "*.*", SearchOption.AllDirectories).Length;
            modelCount = cnt != 0 ? cnt : 1;
            queueLength = 0;

            foreach (string depDirectoryPath in Directory.GetDirectories(pathToModel))
            {
                foreach (string productDirectoryPath in Directory.GetDirectories(depDirectoryPath))
                {
                    if (mode)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(ProductCheck), productDirectoryPath);
                        queueLength++;
                    }
                    else
                    {
                        ProductCheck(productDirectoryPath);
                    }                    
                }
            }
            if (!mode)
            {
                queueLength = 0;
                CheckComplete();
            }
        }

        void ProductCheck(object obj)
        {
            var productDirectoryPath = obj as string;
            DirectoryInfo productInfo = new DirectoryInfo(productDirectoryPath);
            string[] files = Directory.GetFiles(productDirectoryPath, "*.*", SearchOption.AllDirectories);
            foreach (string modelPath in files)
            {
                if (CancelUpdate)
                {
                    return;
                }
                checkedCount++;
                if (parsed.Contains(modelPath))
                {
                    continue;
                }
                FileInfo model = new FileInfo(modelPath);
                ParseModel(model, productInfo);
                if (locked != null)
                {
                    locked.Progress = (double)checkedCount / (double)modelCount;
                }
            }
            queueLength--;
            CheckComplete();
        }

        void ParseModel(FileInfo model, DirectoryInfo product)
        {
            string ext = model.Name.ToLower();
            if (ext.Contains(".prt.") || ext.Contains(".asm."))
            {
                var parse = ModelParser.Instance.Parse(model, product);
                ModelItem item = new ModelItem(parse);
                var check = SQL.Get("ModelItems").ExecuteScalar("SELECT Id FROM ModelItems WHERE PathToModel = '{0}' LIMIT 1", item.PathToModel);
                if (check != null)
                {
                    return;
                }
                item.Save();
                Output.LogFormat("Добавлена модель: {0}", parse.FilePath);
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("ModelItems").Execute(@"DROP TABLE ModelItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("ModelItems").Execute(@"CREATE TABLE IF NOT EXISTS ModelItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    User INTEGER,
                                                    PathToModel TEXT,
                                                    ModelDate INTEGER,
                                                    Parsed INTEGER,
                                                    ModelVersion INTEGER,
                                                    Product INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("ModelItems").Execute(@"CREATE INDEX IF NOT EXISTS ModelItems_Name ON ModelItems(Name)");
            SQL.Get("ModelItems").Execute(@"CREATE INDEX IF NOT EXISTS ModelItems_PathToModel ON ModelItems(PathToModel)");
            instance = null;
        }

        /// <summary>
        /// Загрузить все модели
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("ModelItems").ExecuteRead("SELECT * FROM ModelItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new ModelItem(row));
            }
        }
    }
}
