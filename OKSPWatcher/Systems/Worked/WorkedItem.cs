﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System;
using System.IO;
using System.Text;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Проработанное изображение
    /// </summary>
    public class WorkedItem : DBObject
    {

        string pathToImage;
        /// <summary>
        /// Путь до файла изображения
        /// </summary>
        public string PathToImage
        {
            get
            {
                return pathToImage;
            }
            set
            {
                if(pathToImage != value)
                {
                    needUpdate = true;
                }
                pathToImage = value;
                OnPropertyChanged("PathToImage");
            }
        }

        /// <summary>
        /// Переместить проработанное изображение
        /// </summary>
        public void Move(string newPath)
        {
            try
            {
                string oldName = pathToImage;
                if(oldName == newPath)
                {
                    Output.WriteError("Невозможно переместить: пути идентичны {0} -> {1}", oldName, newPath);
                    return;
                }
                if (oldName != newPath && File.Exists(newPath))
                {
                    Output.WriteError("Невозможно переместить: файл уже существует {0} -> {1}", oldName, newPath);
                    return;
                }
                new FileInfo(newPath).Directory.Create();
                File.Move(oldName, newPath);
                pathToImage = newPath;
                OnPropertyChanged("PathToImage");
                Output.LogFormat("Перенос изображения: {0} -> {1}", oldName, pathToImage);
                needUpdate = true;
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        /// <summary>
        /// Скопировать проработанное изображение
        /// </summary>
        public WorkedItem Copy(string newPath)
        {
            try
            {
                string oldName = pathToImage;
                if (oldName == newPath)
                {
                    Output.ErrorFormat("Невозможно скопировать: пути идентичны {0} -> {1}", oldName, newPath);
                    return null;
                }
                if (oldName != newPath && File.Exists(newPath))
                {
                    Output.ErrorFormat("Невозможно скопировать: файл уже существует {0} -> {1}", oldName, newPath);
                    return null;
                }
                new FileInfo(newPath).Directory.Create();                
                File.Copy(oldName, newPath);
                var item = new WorkedItem();
                item.PathToImage = newPath;
                item.LinkedUser = LinkedUser;
                item.LinkedProduct = LinkedProduct;
                item.Date = Date;
                item.PageAdder = PageAdder;
                item.PageIndex = PageIndex;
                item.ChangeIndex = ChangeIndex;
                item.Parsed = Parsed;
                item.HasErrors = HasErrors;
                Output.LogFormat("Копирование изображения: {0} -> {1}", oldName, pathToImage);
                return item;
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                return null;
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(userId != toSet)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        long productId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(productId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if (productId != toSet)
                {
                    needUpdate = true;
                }
                productId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        long date;
        /// <summary>
        /// Дата проработки
        /// </summary>
        public DateTime Date
        {
            get
            {
                return new DateTime(date);
            }
            set
            {
                if(date != value.Ticks)
                {
                    needUpdate = true;
                }
                date = value.Ticks;
                OnPropertyChanged("Date");
            }
        }

        long pageIndex;
        /// <summary>
        /// Номер листа
        /// </summary>
        public long PageIndex
        {
            get
            {
                return pageIndex;
            }
            set
            {
                if(pageIndex != value)
                {
                    needUpdate = true;
                }
                pageIndex = value;
                OnPropertyChanged("PageIndex");
            }
        }

        string pageAdder;
        /// <summary>
        /// Дополнительная буква к номеру листа
        /// </summary>
        public string PageAdder
        {
            get
            {
                return pageAdder;
            }
            set
            {
                if(pageAdder != value)
                {
                    needUpdate = true;
                }
                pageAdder = value;
                OnPropertyChanged("PageAdder");
            }
        }

        /// <summary>
        /// Описание чертежа
        /// </summary>
        public string Tooltip
        {
            get
            {
                StringBuilder builder = new StringBuilder(name);

                if (LinkedUser != null)
                {
                    builder.AppendFormat("{0}{1}", Environment.NewLine, LinkedUser.Name);
                }
                builder.AppendFormat("{0}{1}", Environment.NewLine, Date.ToShortDateString());

                return builder.ToString();
            }
        }

        long changeIndex;
        /// <summary>
        /// Номер изменения
        /// </summary>
        public long ChangeIndex
        {
            get
            {
                return changeIndex;
            }
            set
            {
                if(changeIndex != value)
                {
                    needUpdate = true;
                }
                changeIndex = value;
                OnPropertyChanged("ChangeIndex");
            }
        }

        bool parsed;
        /// <summary>
        /// Данные о проработанном изображении полностью разобраны?
        /// </summary>
        public bool Parsed
        {
            get
            {
                return parsed;
            }
            set
            {
                if(parsed != value)
                {
                    needUpdate = true;
                }
                parsed = value;
                OnPropertyChanged("Parsed");
            }
        }

        bool hasErrors;
        /// <summary>
        /// Есть замечания по данному чертежу?
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return hasErrors;
            }
            set
            {
                if(hasErrors != value)
                {
                    needUpdate = true;
                }
                hasErrors = value;
                OnPropertyChanged("HasErrors");
            }
        }

        public WorkedItem() : base()
        {
            needUpdate = true;
        }

        public WorkedItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public WorkedItem(WorkedParseData data) : base()
        {
            name = data.Name;
            pathToImage = data.FilePath;
            LinkedUser = data.LinkedUser;
            LinkedProduct = data.ProductItem;
            Date = data.Date;
            PageIndex = data.PageIndex;
            ChangeIndex = data.ChangeIndex;
            HasErrors = data.HasErrors;
            PageAdder = data.PageAdder;
            Parsed = data.IsParsed;     
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                productId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                PathToImage = data.String("PathToImage");
                Date = new DateTime(data.Long("WorkedDate"));
                PageIndex = data.Long("PageIndex");
                ChangeIndex = data.Long("ChangeIndex");
                HasErrors = data.Bool("HasErrors");
                PageAdder = data.String("PageAdder");
                Parsed = data.Bool("Parsed");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                userId = data.Long("User");
                productId = data.Long("Product");
                pathToImage = data.String("PathToImage");
                date = data.Long("WorkedDate");
                pageIndex = data.Long("PageIndex");
                changeIndex = data.Long("ChangeIndex");
                hasErrors = data.Bool("HasErrors");
                pageAdder = data.String("PageAdder");
                parsed = data.Bool("Parsed");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "WorkedItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO WorkedItems (Name, User, PathToImage, WorkedDate, PageIndex, ChangeIndex, Parsed, HasErrors, PageAdder, Product, UpdateDate) VALUES ('{0}', {1}, '{2}', {3}, {4}, {5}, {6}, {7}, '{8}', {9}, {10})", out id,
                name.Check(), userId, pathToImage, date, pageIndex, changeIndex, parsed.ToLong(), hasErrors.ToLong(), pageAdder, productId, UpdateDate.Ticks);
            base.CreateNew();
            WorkedManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE WorkedItems SET Name = '{0}', User = {1}, PathToImage = '{2}', WorkedDate = {3}, PageIndex = {4}, ChangeIndex = {5}, Parsed = {6}, HasErrors = {7}, PageAdder = '{8}', Product = {9}, UpdateDate = {10} WHERE Id = {11}",
                name.Check(), userId, pathToImage, date, pageIndex, changeIndex, parsed.ToLong(), hasErrors.ToLong(), pageAdder, productId, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM WorkedItems WHERE Id = {0}", id);
            WorkedManager.Instance.Remove(this);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("LinkedUser", userId);
            AddStateItem("LinkedProduct", productId);
            AddStateItem("PathToImage", pathToImage);
            AddStateItem("Date", date);
            AddStateItem("PageIndex", pageIndex);
            AddStateItem("ChangeIndex", changeIndex);
            AddStateItem("Parsed", parsed);
            AddStateItem("HasErrors", hasErrors);
            AddStateItem("PageAdder", pageAdder);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                userId = (long)savedState["LinkedUser"];
                productId = (long)savedState["LinkedProduct"];
                pathToImage = (string)savedState["PathToImage"];
                date = (long)savedState["Date"];
                pageIndex = (long)savedState["PageIndex"];
                changeIndex = (long)savedState["ChangeIndex"];
                parsed = (bool)savedState["Parsed"];
                hasErrors = (bool)savedState["HasErrors"];
                pageAdder = (string)savedState["PageAdder"];
                base.LoadState();
            }
        }

        /// <summary>
        /// Синхронизировать состояние объекта с данными
        /// </summary>
        public void Sync(WorkedParseData data)
        {
            if (data.IsParsed && !Parsed)
            {
                Parsed = data.IsParsed;
            }

            if(data.Name != null)
            {
                name = data.Name;
            }

            if (data.PageIndex != -1)
            {
                PageIndex = data.PageIndex;
            }

            if (data.ChangeIndex != -1)
            {
                ChangeIndex = data.ChangeIndex;
            }

            if (data.LinkedUser != null)
            {
                LinkedUser = data.LinkedUser;
            }

            if(data.ProductItem != null)
            {
                LinkedProduct = data.ProductItem;
            }

            if (data.HasErrors)
            {
                HasErrors = true;
            }

            if (data.PageAdder != null)
            {
                PageAdder = data.PageAdder;
            }
        }
    }
}
