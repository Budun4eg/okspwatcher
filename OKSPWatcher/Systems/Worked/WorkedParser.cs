﻿using OKSPWatcher.Core;
using OKSPWatcher.Parsers;
using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System.IO;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Объект для разбора данных проработанных изображений
    /// </summary>
    public class WorkedParser : ParseManager<WorkedTemplateItem, WorkedParseData>
    {
        static WorkedParser instance;

        protected WorkedParser()
        {
            pathToTemplates = AppSettings.TemplatesFolder + "/WorkedParser.txt";
            UpdateTemplate();
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static WorkedParser Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new WorkedParser();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Разобрать данные
        /// </summary>
        public WorkedParseData Parse(string pathToImage)
        {
            return Parse(new FileInfo(pathToImage), null, null);
        }

        /// <summary>
        /// Разобрать данные
        /// </summary>
        public WorkedParseData Parse(FileInfo image, DirectoryInfo product, DirectoryInfo user)
        {            
            var data = new WorkedParseData();
            data.FilePath = image.FullName;

            if (image.Exists)
            {
                data.Date = image.LastWriteTime;
            }

            string productName = null;

            if (product != null)
            {
                string toCheck = product.Name.ToUpper();
                data.ProductItem = ProductManager.Instance.ByName(toCheck, true).Single;
                if (data.ProductItem != null)
                {
                    productName = toCheck;
                }
            }

            var slash = image.FullName.Split(new char[] { '\\' });
            if (slash.Length > 2 && slash[slash.Length - 2].ToUpper() == WorkedManager.Instance.ErrorFolderName)
            {
                data.HasErrors = true;
            }
            else
            {
                data.HasErrors = false;
            }

            string userName;
            if (user != null)
            {
                userName = user.Name;
            }
            else
            {
                userName = (slash[slash.Length - 2].ToUpper() == WorkedManager.Instance.ErrorFolderName.ToUpper() ? slash[slash.Length - 4] : slash[slash.Length - 3]);
            }
            data.LinkedUser = UserManager.Instance.ByName(userName, true).Single;

            if (data.ProductItem == null)
            {
                string checkName = slash[slash.Length - 2].ToUpper() == WorkedManager.Instance.ErrorFolderName.ToUpper() ? slash[slash.Length - 3].ToUpper() : slash[slash.Length - 2].ToUpper();
                data.ProductItem = ProductManager.Instance.ByName(checkName, true).Single;
                if (data.ProductItem != null)
                {
                    productName = checkName;
                }
            }

            string name = image.Name.Substring(0, image.Name.Length - image.Extension.Length).ToUpper();

            if (data.ProductItem == null)
            {
                data.ProductItem = ProductManager.Instance.FindStart(name, out productName, true);
            }

            if (productName != null)
            {
                data.ProductName = productName;
                if (name.StartsWith(string.Format("{0}.", productName)))
                {
                    name = name.Substring(productName.Length + 1);
                }
            }

            foreach(var template in templates)
            {
                if (template.Parse(name, data))
                {
                    return data;
                }
            }
            return data;
        }
    }
}
