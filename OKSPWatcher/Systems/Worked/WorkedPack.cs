﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Набор проработанных изображений
    /// </summary>
    public class WorkedPack : ItemPack<WorkedItem>
    {
        public WorkedPack() : base()
        {
            name = "WorkedPack";
        }

        /// <summary>
        /// Самый ранний элемент набора
        /// </summary>
        public WorkedItem Top
        {
            get
            {
                if (needRefresh)
                {
                    Refresh();
                }
                if (Count == 0) return null;
                WorkedItem top = items[0];
                for(int i = 1; i < Count - 1; i++)
                {
                    if(items[i].Date < top.Date)
                    {
                        top = items[i];
                    }
                }
                return top;
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = WorkedManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
