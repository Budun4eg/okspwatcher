﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Менеджер проработанных изображений
    /// </summary>
    public class WorkedManager : ManagerObject<WorkedItem>
    {
        static WorkedManager instance;

        protected WorkedManager() : base() {
            pathToWorked = AppSettings.GetString("PathToWorked");
            errorFolderName = AppSettings.GetString("ErrorFolderName");
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static WorkedManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new WorkedManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("WorkedItems").Execute(@"DROP TABLE WorkedItems");
        }

        /// <summary>
        /// Получить изображение по идентификатору
        /// </summary>
        public override WorkedItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {                
                string request = string.Format("SELECT * FROM WorkedItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("WorkedItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new WorkedItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("WorkedItems").Execute(@"CREATE TABLE IF NOT EXISTS WorkedItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    User INTEGER,
                                                    Product INTEGER,
                                                    PathToImage TEXT,
                                                    WorkedDate INTEGER,
                                                    PageIndex INTEGER,
                                                    ChangeIndex INTEGER,
                                                    Parsed INTEGER,
                                                    HasErrors INTEGER,
                                                    PageAdder TEXT DEFAULT '',
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("WorkedItems").Execute(@"CREATE INDEX IF NOT EXISTS WorkedItems_Name ON WorkedItems(Name)");
            SQL.Get("WorkedItems").Execute(@"CREATE INDEX IF NOT EXISTS WorkedItems_PathToImage ON WorkedItems(PathToImage)");
            instance = null;
        }

        /// <summary>
        /// Загрузить все проработанные изображения
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("WorkedItems").ExecuteRead("SELECT * FROM WorkedItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new WorkedItem(row));
            }
        }

        /// <summary>
        /// Список изображений по наименованию
        /// </summary>
        Dictionary<string, WorkedPack> byName = new Dictionary<string, WorkedPack>();

        /// <summary>
        /// Список изображений по наименованию
        /// </summary>
        public List<WorkedPack> SortedByName
        {
            get
            {
                return byName.Values.ToList();
            }
        }

        /// <summary>
        /// Список отсортированных по наименованию и изделию наборов
        /// </summary>
        public Dictionary<Product, List<WorkedPack>> Sorted
        {
            get
            {
                lock (changeLock)
                {
                    Dictionary<Product, List<WorkedPack>> sorted = new Dictionary<Product, List<WorkedPack>>();
                    foreach (var item in byName.Values)
                    {
                        var top = item.Top;
                        if (top == null) continue;
                        var prd = top.LinkedProduct != null ? top.LinkedProduct : ProductManager.Instance.NullItem;
                        if (sorted.ContainsKey(prd))
                        {
                            sorted[prd].Add(item);
                        }
                        else
                        {
                            sorted.Add(prd, new List<WorkedPack>() { item });
                        }
                    }
                    var keys = sorted.Keys.OrderBy(o => o.Name);
                    Dictionary<Product, List<WorkedPack>> toReturn = new Dictionary<Product, List<WorkedPack>>();
                    foreach (var key in keys)
                    {
                        toReturn.Add(key, sorted[key]);
                    }
                    return toReturn;
                }                
            }
        }

        /// <summary>
        /// Получить набор проработанных изображений
        /// </summary>
        public WorkedPack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM WorkedItems WHERE Name = '{0}' ORDER BY Id", key);
                    var data = SQL.Get("WorkedItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new WorkedItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new WorkedPack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Получить проработанное изображение
        /// </summary>
        public WorkedPack ByNameCertain(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM WorkedItems WHERE Name = '{0}' LIMIT 1", key);
                    var data = SQL.Get("WorkedItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new WorkedItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new WorkedPack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        string pathToWorked;
        /// <summary>
        /// Путь до проработанных изображений
        /// </summary>
        public string PathToWorked
        {
            get
            {
                return pathToWorked;
            }
            set
            {
                if(pathToWorked != value)
                {
                    AppSettings.SetString("PathToWorked", value);
                    pathToWorked = value;
                    OnPropertyChanged("PathToWorked");
                }
            }
        }

        string errorFolderName;
        /// <summary>
        /// Наименование папки с замечаниями
        /// </summary>
        public string ErrorFolderName
        {
            get
            {
                return errorFolderName;
            }
            set
            {
                if(errorFolderName != value)
                {
                    AppSettings.SetString("ErrorFolderName", value);
                    errorFolderName = value;
                    OnPropertyChanged("ErrorFolderName");
                }
            }
        }

        List<string> _parsedPaths;

        /// <summary>
        /// Список добавленных изображений
        /// </summary>
        List<string> parsed
        {
            get
            {
                if(_parsedPaths == null)
                {
                    _parsedPaths = new List<string>();
                    var data = SQL.Get("WorkedItems").ExecuteRead("SELECT PathToImage FROM WorkedItems ORDER BY Id");
                    foreach (SQLDataRow row in data)
                    {
                        _parsedPaths.Add(row.String("PathToImage"));
                    }
                }
                return _parsedPaths;
            }
        }

        /// <summary>
        /// Добавить проработанное изображение
        /// </summary>
        public override bool Add(WorkedItem item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new WorkedPack() { item });
                    }
                }
                return true;
            }
           
        }


        /// <summary>
        /// Удалить проработанное изображение
        /// </summary>
        public override void Remove(WorkedItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
            }            
        }

        BasicWindow locked;

        int workedCount;

        int queueLength;

        public override bool CancelUpdate
        {
            set
            {
                base.CancelUpdate = value;
                queueLength = 0;
                CheckComplete();
            }
        }

        DateTime start;

        void CheckComplete()
        {
            lock (changeLock)
            {
                if (completed) return;
                if(queueLength == 0)
                {
                    completed = true;
                    DataIsUpdated(null, null);
                }                
            }
        }

        protected override void DataIsUpdated(object sender, RunWorkerCompletedEventArgs e)
        {
            if (completed)
            {
                base.DataIsUpdated(sender, e);
                DateTime end = DateTime.Now;
                Output.LogFormat("Данные о чертежах обновлены [{0:0.00}с]", (end - start).TotalSeconds);

                mustUpdate = false;
                OnDataUpdated();
                BackgroundHandler.Unregister(worker);
            }
        }

        /// <summary>
        /// Обновление завершено?
        /// </summary>
        bool completed;

        /// <summary>
        /// Обновить данные о проработанных чертежах
        /// </summary>
        protected override void UpdateData(object sender, DoWorkEventArgs e)
        {
            base.UpdateData(sender, e);
            completed = false;
            start = DateTime.Now;
            locked = BasicWindow.Current;
            if (locked != null) {
                BasicWindow.Current.UpdateText = "Проработанные чертежи";
                Output.Log("Обновление проработанных чертежей...");
                locked.Progress = 0f;
            }

            bool mode = AppSettings.GetBool("ThreadMode");

            int cnt = Directory.GetFiles(pathToWorked, "*.*", SearchOption.AllDirectories).Length;
            workedCount = cnt != 0 ? cnt : 1;

            foreach (string depDirectoryPath in Directory.GetDirectories(pathToWorked))
            {
                DirectoryInfo depInfo = new DirectoryInfo(depDirectoryPath);
                foreach (string userDirectoryPath in Directory.GetDirectories(depDirectoryPath))
                {
                    DirectoryInfo userInfo = new DirectoryInfo(userDirectoryPath);

                    foreach (string productDirectoryPath in Directory.GetDirectories(userDirectoryPath))
                    {
                        if (mode)
                        {
                            ThreadPool.QueueUserWorkItem(new WaitCallback(ProductCheck), new KeyValuePair<string, DirectoryInfo>(productDirectoryPath, userInfo));
                            queueLength++;
                        }
                        else
                        {
                            ProductCheck(new KeyValuePair<string, DirectoryInfo>(productDirectoryPath, userInfo));
                        }
                    }
                }
            }

            if (!mode)
            {
                queueLength = 0;
                CheckComplete();
            }            
        }

        void ProductCheck(object obj)
        {
            var productDirectoryPath = ((KeyValuePair<string, DirectoryInfo>)obj).Key;
            var userInfo = ((KeyValuePair<string, DirectoryInfo>)obj).Value;
            DirectoryInfo productInfo = new DirectoryInfo(productDirectoryPath);
            string[] files = Directory.GetFiles(productDirectoryPath, "*.*", SearchOption.AllDirectories);
            foreach (string imagePath in files)
            {
                if (CancelUpdate)
                {
                    return;
                }
                checkedCount++;
                if (parsed.Contains(imagePath))
                {
                    continue;
                }
                FileInfo image = new FileInfo(imagePath);
                ParseImage(image, productInfo, userInfo);

                if (locked != null)
                {
                    locked.Progress = (double)checkedCount / (double)workedCount;
                }
            }
            queueLength--;
            CheckComplete();
        }

        void ParseImage(FileInfo image, DirectoryInfo product, DirectoryInfo user)
        {
            string ext = image.Extension.ToLower();
            if (ext == ".tif" || ext == ".jpg" || ext == ".bmp" || ext == ".png" || ext == ".tiff")
            {
                var parse = WorkedParser.Instance.Parse(image, product, user);

                WorkedItem item = new WorkedItem(parse);
                var check = SQL.Get("WorkedItems").ExecuteScalar("SELECT Id FROM WorkedItems WHERE PathToImage = '{0}' LIMIT 1", item.PathToImage);
                if(check != null)
                {
                    return;
                }
                item.Save();

                Output.LogFormat("Добавлено проработанное изображение: {0}", parse.FilePath);
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
