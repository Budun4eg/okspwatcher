﻿using OKSPWatcher.Core;
using System.Text;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Данные разбора проработанного чертежа
    /// </summary>
    public class WorkedParseData : DseParseData
    {
        int pageIndex = -1;
        /// <summary>
        /// Номер листа
        /// </summary>
        public int PageIndex
        {
            get
            {
                return pageIndex;
            }
            set
            {
                pageIndex = value;
            }
        }

        string pageAdder = "";
        /// <summary>
        /// Дополнительная буква к номеру листа
        /// </summary>
        public string PageAdder
        {
            get
            {
                return pageAdder;
            }
            set
            {
                pageAdder = value;
            }
        }

        int changeIndex = -1;
        /// <summary>
        /// Номер изменения
        /// </summary>
        public int ChangeIndex
        {
            get
            {
                return changeIndex;
            }
            set
            {
                changeIndex = value;
            }
        }

        bool hasErrors;
        /// <summary>
        /// Есть замечания к данному изображению?
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return hasErrors;
            }
            set
            {
                hasErrors = value;
            }
        }

        /// <summary>
        /// Данные изображения разобраны полностью?
        /// </summary>
        public override bool IsParsed
        {
            get
            {
                var ret = base.IsParsed;
                if (!ret) return false;
                if (pageIndex == -1) return false;
                if (changeIndex == -1) return false;
                return true;
            }
        }

        /// <summary>
        /// Получить строку проверки изображения
        /// </summary>
        public override string CheckString
        {
            get
            {
                builder = new StringBuilder(base.CheckString);
                if (pageIndex == -1) builder.Append("лист, ");
                if (changeIndex == -1) builder.Append("изменение, ");

                return builder.ToString();
            }
        }
    }
}
