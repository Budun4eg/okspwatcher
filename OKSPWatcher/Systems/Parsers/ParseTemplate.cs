﻿using OKSPWatcher.Core;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Parsers
{
    /// <summary>
    /// Стандартный шаблон разбора
    /// </summary>
    public class ParseTemplate<T> where T : ParseData
    {
        /// <summary>
        /// Полный шаблон
        /// </summary>
        protected string fullTemplate;

        /// <summary>
        /// Шаблон
        /// </summary>
        protected string temp;

        /// <summary>
        /// Шаблон наименования
        /// </summary>
        protected string nameTemplate;

        /// <summary>
        /// Список проверок
        /// </summary>
        protected Dictionary<char, Dictionary<int, char>> toCheck = new Dictionary<char, Dictionary<int, char>>();

        /// <summary>
        /// Необходимая длина строки
        /// </summary>
        protected int length;

        /// <summary>
        /// Загрузить шаблон
        /// </summary>
        public void Load(string template)
        {
            fullTemplate = template;
            var spl = template.Split(new char[] { '|' }, StringSplitOptions.None);
            if (spl.Length >= 2)
            {
                temp = spl[0].Trim();
                nameTemplate = spl[1].Trim();
                for(int i = 2; i < spl.Length; i++)
                {
                    var newCheck = new ParseCheck();
                    if (newCheck.Load(spl[i].Trim()))
                    {
                        if (toCheck.ContainsKey(newCheck.CheckCode))
                        {
                            toCheck[newCheck.CheckCode].Add(newCheck.Offset, newCheck.ToCheck);
                        }
                        else
                        {
                            toCheck.Add(newCheck.CheckCode, new Dictionary<int, char>() { { newCheck.Offset, newCheck.ToCheck } });
                        }
                    }
                    else
                    {
                        Output.ErrorFormat("Ошибка в шаблоне {0}", template);
                    }
                }
            }
            else
            {
                Output.ErrorFormat("Ошибка в шаблоне {0}", template);
            }
            foreach (var ch in temp)
            {
                if (ch != '{' && ch != '}')
                {
                    length++;
                }
            }
        }

        public ParseTemplate()
        {
            
        }

        public virtual bool Parse(string name, T data)
        {
            return false;
        }

        /// <summary>
        /// Проверить строку на соответствие шаблону
        /// </summary>
        protected bool Check(string name) {
            if (name.Length != length) return false;

            int index = 0;
            bool check = true;

            foreach (var ch in temp)
            {
                if (ch == '{')
                {
                    check = false;
                }
                else if (ch == '}')
                {
                    check = true;
                }
                else
                {
                    if (check)
                    {
                        if (name[index] != ch)
                        {
                            return false;
                        }
                    }
                    index++;
                }
            }
            return true;
        }
    }
}
