﻿using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System;
using System.Text;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Стандартные данные разбора
    /// </summary>
    public class ParseData
    {
        protected Product productItem;
        /// <summary>
        /// Изделие
        /// </summary>
        public Product ProductItem
        {
            get
            {
                return productItem;
            }
            set
            {
                productItem = value;
            }
        }

        protected string productName;
        /// <summary>
        /// Наименование изделия
        /// </summary>
        public string ProductName
        {
            get
            {
                return productName;
            }
            set
            {
                productName = value;
            }
        }

        protected string productSplitter = ".";
        /// <summary>
        /// Разделитель наименования изделия и наименования документа
        /// </summary>
        public string ProductSplitter
        {
            get
            {
                return productSplitter;
            }
            set
            {
                productSplitter = value;
            }
        }

        protected string name;
        /// <summary>
        /// Обозначение ДСЕ
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        protected string filePath;
        /// <summary>
        /// Путь до файла
        /// </summary>
        public string FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;
            }
        }

        protected DateTime date;
        /// <summary>
        /// Дата создания файла
        /// </summary>
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        protected User linkedUser;
        /// <summary>
        /// Пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return linkedUser;
            }
            set
            {
                linkedUser = value;
            }
        }

        /// <summary>
        /// Объект для построения строк
        /// </summary>
        protected StringBuilder builder;

        /// <summary>
        /// Данные были разобраны полностью?
        /// </summary>
        public virtual bool IsParsed
        {
            get
            {
                if (productItem == null) return false;
                if (name == null) return false;
                if (filePath == null) return false;
                if (date == null) return false;
                
                return true;
            }
        }

        /// <summary>
        /// Получить строку проверки
        /// </summary>
        /// 
        public virtual string CheckString
        {
            get
            {
                builder = new StringBuilder();
                if (productItem == null) builder.Append("изделие, ");
                if (name == null) builder.Append("имя, ");
                if (FilePath == null) builder.Append("путь, ");
                if (date == null) builder.Append("дата, ");

                return builder.ToString();
            }
        }
    }
}
