﻿using System.Text;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Общий формат для разбора объекта
    /// </summary>
    public class DseParseData : ParseData
    {
        /// <summary>
        /// Варианты ДСЕ
        /// </summary>
        public enum DseStates { Part, Assembly, Unknown }

        protected DseStates dseState = DseStates.Unknown;
        /// <summary>
        /// Вариант ДСЕ
        /// </summary>
        public DseStates DseState
        {
            get
            {
                return dseState;
            }
            set
            {
                dseState = value;
            }
        }

        protected string group;
        /// <summary>
        /// Группа ДСЕ
        /// </summary>
        public string Group
        {
            get
            {
                return group;
            }
            set
            {
                group = value;
            }
        }

        protected int variantIndex = -1;
        /// <summary>
        /// Вариант изделия
        /// </summary>
        public int VariantIndex
        {
            get
            {
                return variantIndex;
            }
            set
            {
                variantIndex = value;
            }
        }

        /// <summary>
        /// Данные ДСЕ полностью разобраны?
        /// </summary>
        public override bool IsParsed
        {
            get
            {
                var ret = base.IsParsed;
                if (!ret) return false;
                if (variantIndex == -1) return false;
                return true;
            }
        }

        /// <summary>
        /// Получить строку проверки ДСЕ
        /// </summary>
        public override string CheckString
        {
            get
            {
                builder = new StringBuilder(base.CheckString);
                if (variantIndex == -1) builder.Append("вариант, ");

                return builder.ToString();
            }
        }
    }
}
