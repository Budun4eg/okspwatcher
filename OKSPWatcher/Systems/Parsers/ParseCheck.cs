﻿using OKSPWatcher.Core;
using System;

namespace OKSPWatcher.Parsers
{
    /// <summary>
    /// Набор проверки шаблона
    /// </summary>
    public class ParseCheck
    {
        char checkCode;
        /// <summary>
        /// Код проверки
        /// </summary>
        public char CheckCode {
            get
            {
                return checkCode;
            }
        }

        int offset;
        /// <summary>
        /// Смещение элемента
        /// </summary>
        public int Offset {
            get
            {
                return offset;
            }
        }

        char toCheck;
        /// <summary>
        /// Знак для проверки
        /// </summary>
        public char ToCheck {
            get
            {
                return toCheck;
            }
        }

        public bool Load(string data)
        {
            var spl = data.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (spl.Length == 3)
            {
                checkCode = spl[0][0];
                if(!spl[1].ToInt(out offset))
                {
                    return false;
                }
                toCheck = spl[2][0];
            }else if(spl.Length == 2)
            {
                checkCode = spl[0][0];
                if (!spl[1].ToInt(out offset))
                {
                    return false;
                }
                toCheck = ' ';
            }
            else
            {
                return false;
            }
            return true;
        }

        public ParseCheck()
        {
            
        }
    }
}
