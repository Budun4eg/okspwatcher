﻿using OKSPWatcher.Core;
using System.Collections.Generic;
using System.IO;

namespace OKSPWatcher.Parsers
{
    /// <summary>
    /// Менеджер разбора данных
    /// </summary>
    public class ParseManager<T,K> where T : ParseTemplate<K>, new() where K : ParseData
    {
        /// <summary>
        /// Список шаблонов разбора
        /// </summary>
        protected List<T> templates = new List<T>();

        /// <summary>
        /// Путь до шаблонов
        /// </summary>
        protected string pathToTemplates;

        /// <summary>
        /// Обновить шаблонизатор
        /// </summary>
        public virtual void UpdateTemplate()
        {
            Output.Log("Шаблонизатор обновлен");
            templates.Clear();
            List<string> inserted = new List<string>();
            using (StreamReader reader = new StreamReader(pathToTemplates))
            {
                while (!reader.EndOfStream)
                {
                    var str = reader.ReadLine();
                    if (str.StartsWith("#")) continue;
                    if (str.Trim().Length == 0) continue;
                    if (!inserted.Contains(str))
                    {
                        var newItem = new T();
                        newItem.Load(str);
                        templates.Add(newItem);
                        inserted.Add(str);
                    }
                    else
                    {
                        Output.WarningFormat("Дублирование {0}", str);
                    }
                }
            }
        }
    }
}
