﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System.Collections.Generic;
using System.Linq;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Менеджер извещений
    /// </summary>
    public class ChangeManager : ManagerObject<ChangeItem>
    {
        static ChangeManager instance;

        private static object root = new object();

        protected ChangeManager() { }
        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ChangeManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ChangeManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Список извещений по наименованию
        /// </summary>
        Dictionary<string, ChangePack> byName = new Dictionary<string, ChangePack>();

        /// <summary>
        /// Список извещений, отсортированных по наименованию
        /// </summary>
        public List<ChangePack> SortedByName
        {
            get
            {
                return byName.Values.ToList();
            }
        }

        /// <summary>
        /// Получить набор извещений
        /// </summary>
        public ChangePack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM ChangeItems WHERE Name LIKE '%{0}%' ORDER BY ScanDate DESC", key);
                    var data = SQL.Get("ChangeItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new ChangeItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new ChangePack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Список отсортированных по наименованию и изделию наборов
        /// </summary>
        public Dictionary<Product, List<ChangePack>> Sorted
        {
            get
            {
                Dictionary<Product, List<ChangePack>> sorted = new Dictionary<Product, List<ChangePack>>();
                foreach (var item in byName.Values)
                {
                    var top = item.Single;
                    if (top == null) continue;
                    var prd = top.LinkedProduct != null ? top.LinkedProduct : ProductManager.Instance.NullItem;
                    if (sorted.ContainsKey(prd))
                    {
                        sorted[prd].Add(item);
                    }
                    else
                    {
                        sorted.Add(prd, new List<ChangePack>() { item });
                    }
                }
                var keys = sorted.Keys.OrderBy(o => o.Name);
                Dictionary<Product, List<ChangePack>> toReturn = new Dictionary<Product, List<ChangePack>>();
                foreach (var key in keys)
                {
                    toReturn.Add(key, sorted[key]);
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Список извещений по изделию
        /// </summary>
        Dictionary<Product, ChangePack> byProduct = new Dictionary<Product, ChangePack>();

        /// <summary>
        /// Получить набор извещений по изделию
        /// </summary>
        public ChangePack ByProduct(Product key, bool load = false)
        {
            if (byProduct.ContainsKey(key))
            {
                return byProduct[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM ChangeItems WHERE Product = {0} ORDER BY ScanDate DESC", key.Id);
                    var data = SQL.Get("ChangeItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new ChangeItem(row));
                    }
                    return ByProduct(key);
                }
                else
                {
                    var item = new ChangePack();
                    byProduct.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Добавить извещение
        /// </summary>
        public override bool Add(ChangeItem item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new ChangePack() { item });
                    }
                }
                if (item.LinkedProduct != null)
                {
                    if (byProduct.ContainsKey(item.LinkedProduct))
                    {
                        byProduct[item.LinkedProduct].Add(item);
                    }
                    else
                    {
                        byProduct.Add(item.LinkedProduct, new ChangePack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить извещение
        /// </summary>
        public override void Remove(ChangeItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if (item.LinkedProduct != null && byProduct.ContainsKey(item.LinkedProduct) && byProduct[item.LinkedProduct].Contains(item))
                {
                    byProduct[item.LinkedProduct].Remove(item);
                    if (byProduct[item.LinkedProduct].Count == 0)
                    {
                        byProduct.Remove(item.LinkedProduct);
                    }
                }
            }            
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("ChangeItems").Execute(@"DROP TABLE ChangeItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("ChangeItems").Execute(@"CREATE TABLE IF NOT EXISTS ChangeItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    Product INTEGER,
                                                    WorkedIn3D INTEGER,
                                                    Images TEXT,
                                                    ScanDate INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Загрузить все
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("ChangeItems").ExecuteRead("SELECT * FROM ChangeItems ORDER BY ScanDate DESC");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) break;
                Add(new ChangeItem(row));
            }
        }

        /// <summary>
        /// Добавить изображение
        /// </summary>
        public void AddImage(ChangeImage item)
        {
            var changePack = ByName(item.Name, true);
            ChangeItem obj;
            if(changePack.Count == 0)
            {
                obj = new ChangeItem();
                obj.LinkedProduct = item.LinkedProduct;
                obj.Name = item.Name;
                obj.ScanDate = item.ScanDate;
            }else if (changePack.Count == 1)
            {
                obj = changePack.Single;
                if(obj.ScanDate > item.ScanDate)
                {
                    obj.ScanDate = item.ScanDate;
                }
            }
            else
            {
                item.Parsed = false;
                item.Save();
                Output.ErrorFormat("Несколько извещений для изображения {0}", item.Name);
                return;
            }
            obj.Images.Add(item);
            obj.Save();
        }
    }
}
