﻿using OKSPWatcher.Core;
using OKSPWatcher.Parsers;
using OKSPWatcher.Products;
using System.IO;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Объект для разбора данных об извещении
    /// </summary>
    public class ChangeParser : ParseManager<ChangeTemplateItem, ChangeParseData>
    {
        static ChangeParser instance;

        protected ChangeParser()
        {
            pathToTemplates = AppSettings.TemplatesFolder + "/ChangeParser.txt";
            UpdateTemplate();
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ChangeParser Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ChangeParser();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Разобрать данные
        /// </summary>
        public ChangeParseData Parse(string pathToChange)
        {
            return Parse(new FileInfo(pathToChange), null);
        }

        /// <summary>
        /// Разобрать данные
        /// </summary>
        public ChangeParseData Parse(FileInfo change, DirectoryInfo product)
        {
            var data = new ChangeParseData();
            data.FilePath = change.FullName;

            if (change.Exists)
            {
                data.Date = change.LastWriteTime;
            }

            string productName = null;

            if (product != null)
            {
                string toCheck = product.Name.ToUpper();
                data.ProductItem = ProductManager.Instance.ByName(toCheck, true).Single;
                if (data.ProductItem != null)
                {
                    productName = toCheck;
                }
            }

            var slash = change.FullName.Split(new char[] { '\\' });

            string name = change.Name.Substring(0, change.Name.Length - change.Extension.Length).ToUpper();

            if (data.ProductItem == null)
            {
                data.ProductItem = ProductManager.Instance.FindStart(name, out productName, true);
            }

            if (productName != null)
            {
                data.ProductName = productName;
                if (name.StartsWith(string.Format("{0}.", productName)))
                {
                    data.ProductSplitter = ".";
                    name = name.Substring(productName.Length + 1);
                }else if(name.StartsWith(string.Format("{0} ", productName)))
                {
                    data.ProductSplitter = " ";
                    name = name.Substring(productName.Length + 1);
                }
                else if (name.StartsWith(string.Format("{0}-", productName)))
                {
                    data.ProductSplitter = "-";
                    name = name.Substring(productName.Length + 1);
                }
            }

            foreach (var template in templates)
            {
                if (template.Parse(name, data))
                {
                    return data;
                }
            }
            return data;
        }
    }
}
