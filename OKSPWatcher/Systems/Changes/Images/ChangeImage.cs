﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;
using System.IO;

namespace OKSPWatcher.Changes
{
    public class ChangeImage : DBObject
    {
        string pathToImage;
        /// <summary>
        /// Путь до изображения
        /// </summary>
        public string PathToImage
        {
            get
            {
                if (!File.Exists(pathToImage))
                {
                    return AppSettings.NoImagePath;
                }
#if IN_EDITOR && REPLACE_IMAGES
                var info = new FileInfo(pathToImage);
                if(info.Length == 0)
                {
                    return AppSettings.NoImagePath;
                }
#endif
                return pathToImage;
            }
        }

        long scanDate;
        /// <summary>
        /// Дата сканирования изображения
        /// </summary>
        public DateTime ScanDate
        {
            get
            {
                return new DateTime(scanDate);
            }
            set
            {
                if(scanDate != value.Ticks)
                {
                    needUpdate = true;
                }
                scanDate = value.Ticks;
                OnPropertyChanged("ScanDate");
            }
        }
        long pageIndex;
        /// <summary>
        /// Номер листа
        /// </summary>
        public long PageIndex
        {
            get
            {
                return pageIndex;
            }
            set
            {
                if (pageIndex != value)
                {
                    needUpdate = true;
                }
                pageIndex = value;
                OnPropertyChanged("PageIndex");
            }
        }

        string pageAdder;
        /// <summary>
        /// Дополнительная буква к номеру листа
        /// </summary>
        public string PageAdder
        {
            get
            {
                return pageAdder;
            }
            set
            {
                if (pageAdder != value)
                {
                    needUpdate = true;
                }
                pageAdder = value;
                OnPropertyChanged("PageAdder");
            }
        }


        bool parsed;
        /// <summary>
        /// Данные об извещении полностью разобраны?
        /// </summary>
        public bool Parsed
        {
            get
            {
                return parsed;
            }
            set
            {
                if (parsed != value)
                {
                    needUpdate = true;
                }
                parsed = value;
                OnPropertyChanged("Parsed");
            }
        }

        long productId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(productId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if (productId != toSet)
                {
                    needUpdate = true;
                }
                productId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        public ChangeImage(SQLDataRow data) : base()
        {
            Load(data);
        }

        public ChangeImage() : base()
        {
            needUpdate = true;
        }

        public ChangeImage(ChangeParseData data) : base()
        {
            name = data.Name;
            pathToImage = data.FilePath;
            LinkedProduct = data.ProductItem;
            ScanDate = data.Date;
            PageIndex = data.PageIndex;
            PageAdder = data.PageAdder;
            Parsed = data.IsParsed;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                productId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                pathToImage = data.String("PathToImage");
                OnPropertyChanged("PathToImage");
                ScanDate = new DateTime(data.Long("ScanDate"));
                PageIndex = data.Long("PageIndex");
                PageAdder = data.String("PageAdder");
                Parsed = data.Bool("Parsed");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                productId = data.Long("Product");
                pathToImage = data.String("PathToImage");
                scanDate = data.Long("ScanDate");
                pageIndex = data.Long("PageIndex");
                pageAdder = data.String("PageAdder");
                parsed = data.Bool("Parsed");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "ChangeImages";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO ChangeImages (PathToImage, ScanDate, Parsed, Product, PageIndex, PageAdder, Name, UpdateDate) VALUES ('{0}', {1}, {2}, {3}, {4}, '{5}', '{6}', {7})", 
                out id, pathToImage, scanDate, parsed.ToLong(), productId, pageIndex, pageAdder, name.Check(), UpdateDate.Ticks);
            base.CreateNew();
            ChangeImageManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE ChangeImages SET PathToImage = '{0}', ScanDate = {1}, Parsed = {2}, Product = {3}, PageIndex = {4}, PageAdder = '{5}', Name = '{6}', UpdateDate = {7} WHERE Id = {8}", 
                pathToImage, scanDate, parsed.ToLong(), productId, pageIndex, pageAdder, name.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM ChangeImages WHERE Id = {0}", id);
            ChangeImageManager.Instance.Remove(this);
        }

        /// <summary>
        /// Синхронизировать состояние объекта с данными
        /// </summary>
        public void Sync(ChangeParseData data)
        {
            if (data.IsParsed && !Parsed)
            {
                Parsed = data.IsParsed;
            }

            if (data.Name != null)
            {
                name = data.Name;
            }

            if (data.ProductItem != null)
            {
                LinkedProduct = data.ProductItem;
            }

            if(data.PageIndex != -1)
            {
                PageIndex = data.PageIndex;
            }

            if(data.PageAdder != null)
            {
                PageAdder = data.PageAdder;
            }
        }
    }
}
