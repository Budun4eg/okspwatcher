﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Менеджер изображений извещений
    /// </summary>
    public class ChangeImageManager : ManagerObject<ChangeImage>
    {
        static ChangeImageManager instance;

        protected ChangeImageManager() {
            pathToChanges = AppSettings.GetString("PathToChanges");
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ChangeImageManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ChangeImageManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Получить изображение по наименованию
        /// </summary>
        public override ChangeImage ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM ChangeImages WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("ChangeImages").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new ChangeImage(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Список добавленных изображений
        /// </summary>
        List<string> parsedPaths
        {
            get
            {
                List<string> toReturn = new List<string>();
                var data = SQL.Get("ChangeImages").ExecuteRead("SELECT PathToImage FROM ChangeImages ORDER BY Id");
                foreach (SQLDataRow row in data)
                {
                    toReturn.Add(row.String("PathToImage"));
                }
                return toReturn;
            }
        }

        string pathToChanges;
        /// <summary>
        /// Путь до извещений
        /// </summary>
        public string PathToChanges
        {
            get
            {
                return pathToChanges;
            }
            set
            {
                if (pathToChanges != value)
                {
                    AppSettings.SetString("PathToChanges", value);
                    pathToChanges = value;
                    OnPropertyChanged("PathToChanges");
                }
            }
        }

        BasicWindow locked;

        protected override void UpdateData(object sender, DoWorkEventArgs e)
        {
            base.UpdateData(sender, e);
            locked = BasicWindow.Current;
            if(locked != null)
            {
                BasicWindow.Current.UpdateText = "Извещения";
                Output.Log("Обновление извещений...");
                locked.Progress = 0f;
            }
            
            var parsed = parsedPaths;
            int cnt = Directory.GetFiles(pathToChanges, "*.*", SearchOption.AllDirectories).Length;
            var changesCount = cnt != 0 ? cnt : 1;

            foreach (string productDirectoryPath in Directory.GetDirectories(pathToChanges))
            {
                DirectoryInfo productInfo = new DirectoryInfo(productDirectoryPath);
                string[] files = Directory.GetFiles(productDirectoryPath, "*.*", SearchOption.AllDirectories);
                foreach (string changePath in files)
                {
                    if (CancelUpdate)
                    {
                        return;
                    }
                    checkedCount++;
                    if (parsed.Contains(changePath))
                    {
                        continue;
                    }
                    FileInfo change = new FileInfo(changePath);
                    ParseChange(change, productInfo);
                    if(locked != null)
                    {
                        locked.Progress = (double)checkedCount / (double)changesCount;
                    }                    
                }
            }
        }

        void ParseChange(FileInfo change, DirectoryInfo product)
        {
            string ext = change.Extension.ToLower();
            if (ext == ".tif")
            {
                var parse = ChangeParser.Instance.Parse(change, product);

                ChangeImage item = new ChangeImage(parse);
                var check = SQL.Get("ChangeImages").ExecuteScalar("SELECT Id FROM ChangeImages WHERE PathToImage = '{0}'", item.PathToImage);
                if (check != null)
                {
                    return;
                }
                item.Save();

                if (item.Parsed)
                {
                    ChangeManager.Instance.AddImage(item);
                }

                Output.LogFormat("Добавлено изображение извещения: {0}", parse.FilePath);
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("ChangeImages").Execute(@"DROP TABLE ChangeImages");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("ChangeImages").Execute(@"CREATE TABLE IF NOT EXISTS ChangeImages (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    Product INTEGER,
                                                    PathToImage TEXT,
                                                    ScanDate INTEGER,
                                                    PageIndex INTEGER,
                                                    PageAdder TEXT,
                                                    Parsed INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("ChangeImages").Execute(@"CREATE INDEX IF NOT EXISTS ChangeImages_Name ON ChangeImages(Name)");
            SQL.Get("ChangeImages").Execute(@"CREATE INDEX IF NOT EXISTS ChangeImages_PathToImage ON ChangeImages(PathToImage)");
            instance = null;
        }

        /// <summary>
        /// Загрузить все изображения
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("ChangeImages").ExecuteRead("SELECT * FROM ChangeImages ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new ChangeImage(row));
            }
        }
    }
}
