﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Набор изображений извещений
    /// </summary>
    public class ChangeImagePack : ItemPack<ChangeImage>
    {
        /// <summary>
        /// Получить верхнее изображение
        /// </summary>
        public ChangeImage Top
        {
            get
            {
                if (Count == 0) return null;
                ChangeImage top = items[0];
                for (int i = 1; i < Count - 1; i++)
                {
                    if (items[i].ScanDate < top.ScanDate)
                    {
                        top = items[i];
                    }
                }
                return top;
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = ChangeImageManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
