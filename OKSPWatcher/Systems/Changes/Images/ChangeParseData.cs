﻿using OKSPWatcher.Core;
using System.Text;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Данные разбора извещения
    /// </summary>
    public class ChangeParseData : ParseData
    {
        int pageIndex = -1;
        /// <summary>
        /// Номер листа
        /// </summary>
        public int PageIndex
        {
            get
            {
                return pageIndex;
            }
            set
            {
                pageIndex = value;
            }
        }

        string pageAdder = "";
        /// <summary>
        /// Дополнительная буква к номеру листа
        /// </summary>
        public string PageAdder
        {
            get
            {
                return pageAdder;
            }
            set
            {
                pageAdder = value;
            }
        }

        string nameAdder = "";
        /// <summary>
        /// Дополнительное наименование
        /// </summary>
        public string NameAdder
        {
            get
            {
                return nameAdder;
            }
            set
            {
                nameAdder = value;
            }
        }

        /// <summary>
        /// Данные изображения разобраны полностью?
        /// </summary>
        public override bool IsParsed
        {
            get
            {
                var ret = base.IsParsed;
                if (!ret) return false;
                if (pageIndex == -1) return false;
                return true;
            }
        }

        /// <summary>
        /// Получить строку проверки изображения
        /// </summary>
        public override string CheckString
        {
            get
            {
                builder = new StringBuilder(base.CheckString);
                if (pageIndex == -1) builder.Append("лист, ");

                return builder.ToString();
            }
        }
    }
}
