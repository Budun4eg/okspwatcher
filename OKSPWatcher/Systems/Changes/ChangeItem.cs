﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Извещение
    /// </summary>
    public class ChangeItem : DBObject
    {
        /// <summary>
        /// Наименование извещения
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        long productId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(productId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if (productId != toSet)
                {
                    needUpdate = true;
                }
                productId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        bool workedIn3D;
        /// <summary>
        /// Извещение проведено в 3D-модели?
        /// </summary>
        public bool WorkedIn3D
        {
            get
            {
                return workedIn3D;
            }
            set
            {
                if (workedIn3D != value)
                {
                    needUpdate = true;
                }
                workedIn3D = value;
                OnPropertyChanged("WorkedIn3D");
            }
        }

        ChangeImagePack images;
        /// <summary>
        /// Изображения извещения
        /// </summary>
        public ChangeImagePack Images
        {
            get
            {
                if (images == null)
                {
                    images = new ChangeImagePack();
                    images.Updated += PackUpdated;
                }
                return images;
            }
        }

        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        long scanDate;
        /// <summary>
        /// Дата сканирования извещения
        /// </summary>
        public DateTime ScanDate
        {
            get
            {
                return new DateTime(scanDate);
            }
            set
            {
                if(scanDate != value.Ticks)
                {
                    needUpdate = true;
                }
                scanDate = value.Ticks;
                OnPropertyChanged("ScanDate");
            }
        }

        public ChangeItem(SQLDataRow data)
        {
            Load(data);
        }

        public ChangeItem()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                productId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                WorkedIn3D = data.Bool("WorkedIn3D");
                Images.Sync(data.Ids("Images"));
                foreach(var img in Images.Items)
                {
                    if(img != null)
                    {
                        img.CheckUpdate();
                    }
                }
                ScanDate = new DateTime(data.Long("ScanDate"));
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                productId = data.Long("Product");
                workedIn3D = data.Bool("WorkedIn3D");
                Images.Sync(data.Ids("Images"));
                scanDate = data.Long("ScanDate");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "ChangeItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO ChangeItems (Name, Product, WorkedIn3D, Images, ScanDate, UpdateDate) VALUES ('{0}', {1}, {2}, '{3}', {4}, {5})", out id,
                name.Check(), productId, workedIn3D.ToLong(), Images.Serialize(), scanDate, UpdateDate.Ticks);
            base.CreateNew();
            ChangeManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE ChangeItems SET Name = '{0}', Product = {1}, WorkedIn3D = {2}, Images = '{3}', ScanDate = {4}, UpdateDate = {5} WHERE Id = {6}",
                name.Check(), productId, workedIn3D.ToLong(), Images.Serialize(), scanDate, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM ChangeItems WHERE Id = {0}", id);
            Images.Delete();
            ChangeManager.Instance.Remove(this);
        }
    }
}
