﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;

namespace OKSPWatcher.Deps
{
    /// <summary>
    /// Менеджер отделов
    /// </summary>
    public class DepManager : ManagerObject<Dep>
    {
        static DepManager instance;

        protected DepManager() : base()
        {

        }

        /// <summary>
        /// Загрузить все данные
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("Departments").ExecuteRead("SELECT * FROM Departments ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                Add(new Dep(row));
            }
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static DepManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new DepManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Список отдела по имени
        /// </summary>
        Dictionary<string, DepPack> byName = new Dictionary<string, DepPack>();

        /// <summary>
        /// Получить отделы по наименованию
        /// </summary>
        public DepPack ByName(string name, bool load = false)
        {
            if (byName.ContainsKey(name))
            {
                return byName[name];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM Departments WHERE Name = '{0}' ORDER BY Id", name);
                    var data = SQL.Get("Departments").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new Dep(row));
                    }
                    return ByName(name);
                }
                else
                {
                    var item = new DepPack();
                    byName.Add(name, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Получить отдел по наименованию
        /// </summary>
        public DepPack ByNameCertain(string name, bool load = false)
        {
            if (byName.ContainsKey(name))
            {
                return byName[name];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM Departments WHERE Name = '{0}' LIMIT 1", name);
                    var data = SQL.Get("Departments").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new Dep(row));
                    }
                    return ByName(name);
                }
                else
                {
                    var item = new DepPack();
                    byName.Add(name, item);
                    return item;
                }
            }
        }

        public override Dep ById(long id, bool load = false)
        {
            if (id < 0) return null;
            var ret = base.ById(id, load);
            if(ret == null && load)
            {
                var data = SQL.Get("Departments").ExecuteRead("SELECT * FROM Departments WHERE Id = {0} LIMIT 1", id);
                foreach (SQLDataRow row in data)
                {
                    Add(new Dep(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Добавить отдел
        /// </summary>
        public override bool Add(Dep item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new DepPack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить отдел
        /// </summary>
        public override void Remove(Dep item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
            }            
        }

        /// <summary>
        /// Синхронизировать данные
        /// </summary>
        public override void Sync(string dataType = null)
        {
            base.Sync(dataType);
            if(dataType == null || dataType == "Name")
            {
                byName.Clear();
                foreach (var item in items)
                {
                    if (item.Name == null) continue;
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new DepPack() { item });
                    }
                }
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Departments").Execute(@"DROP TABLE Departments");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Departments").Execute(@"CREATE TABLE IF NOT EXISTS Departments (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    BossId INTEGER DEFAULT 0,
                                                    UpdateDate INTEGER                            
                                                    )");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
