﻿using OKSPWatcher.Core;
using System.Collections.Generic;

namespace OKSPWatcher.Deps
{
    /// <summary>
    /// Набор отделов
    /// </summary>
    public class DepPack : ItemPack<Dep>
    {
        public DepPack() : base()
        {
            name = "DepPack";
        }

        public DepPack(List<long> ids) : base()
        {
            foreach(var id in ids)
            {
                var item = DepManager.Instance.ById(id, true);
                if(item != null)
                {
                    Add(item);
                }
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = DepManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }

        public override void LoadAll()
        {
            base.LoadAll();
            DepManager.Instance.LoadAll();
            items.Clear();
            foreach (var item in DepManager.Instance.Items)
            {
                Add(item);
            }
        }
    }
}
