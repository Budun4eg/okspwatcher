﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Deps
{
    /// <summary>
    /// Объект отдела
    /// </summary>
    public class Dep : DBObject
    {
        /// <summary>
        /// Наименование отдела
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                DepManager.Instance.Sync("Name");
                OnPropertyChanged("Name");
            }
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                bossId = data.Long("BossId");
                OnPropertyChanged("Boss");
                if(Boss != null)
                {
                    Boss.CheckUpdate();
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                bossId = data.Long("BossId");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        long bossId = -1;
        /// <summary>
        /// Начальник отдела
        /// </summary>
        public User Boss
        {
            get
            {
                return UserManager.Instance.ById(bossId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(bossId != toSet)
                {
                    needUpdate = true;
                }
                bossId = toSet;
                OnPropertyChanged("Boss");
            }
        }

        public Dep(SQLDataRow data) : base()
        {
            Load(data);
        }

        public Dep() : base()
        {
            needUpdate = true;
        }

        protected override string sqlPath
        {
            get
            {
                return "Departments";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Departments (Name, BossId, UpdateDate) VALUES ('{0}', {1}, {2})", out id, name.Check(), bossId, UpdateDate.Ticks);
            base.CreateNew();
            DepManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Departments SET Name='{0}', BossId = {1}, UpdateDate = {2} WHERE Id = {3}", name.Check(), bossId, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM Departments WHERE Id = {0}", id);
            DepManager.Instance.Remove(this);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("Name", name);
            AddStateItem("Boss", bossId);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                name = (string)savedState["Name"];
                bossId = (long)savedState["Boss"];
                base.LoadState();
            }
        }
    }
}
