﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using System;

namespace OKSPWatcher.Products
{
    /// <summary>
    /// Изделие
    /// </summary>
    public class Product : DBObject
    {
        /// <summary>
        /// Идентификатор отдела, к которому привязано изделие
        /// </summary>
        long depId = -1;

        /// <summary>
        /// Отдел, к которому привязано изделие
        /// </summary>
        public Dep LinkedDep
        {
            get
            {
                return DepManager.Instance.ById(depId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(depId != toSet)
                {
                    needUpdate = true;
                }
                depId = toSet;
                DepManager.Instance.Sync("LinkedDep");
                OnPropertyChanged("LinkedDep");
            }
        }

        /// <summary>
        /// Наименование изделия
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                DepManager.Instance.Sync("Name");
                OnPropertyChanged("Name");
            }
        }

        string analogs = "";
        /// <summary>
        /// Аналоги названия изделия
        /// </summary>
        public string Analogs
        {
            get
            {
                return analogs;
            }
            set
            {
                if(analogs != value)
                {
                    needUpdate = true;
                }
                analogs = value;
                DepManager.Instance.Sync("Name");
                OnPropertyChanged("Analogs");
            }
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                depId = data.Long("Department");
                OnPropertyChanged("LinkedDep");
                if(LinkedDep != null)
                {
                    LinkedDep.CheckUpdate();
                }
                Analogs = data.String("Analogs");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                depId = data.Long("Department");
                analogs = data.String("Analogs");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public Product(SQLDataRow data) : base()
        {
            Load(data);
        }

        public Product() : base()
        {
            needUpdate = true;
        }

        protected override string sqlPath
        {
            get
            {
                return "Products";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Products (Name, Department, Analogs, UpdateDate) VALUES ('{0}',{1},'{2}', {3})", out id, name.Check(), depId, analogs.Check(), UpdateDate.Ticks);
            base.CreateNew();
            ProductManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Products SET Name = '{0}', Department = {1}, Analogs = '{2}', UpdateDate = {3} WHERE Id = {4}", name.Check(), depId, analogs.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            ProductManager.Instance.Remove(this);
            sql.Execute("DELETE FROM Products WHERE Id = {0}", id);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("LinkedDepartment", depId);
            AddStateItem("Name", name);
            AddStateItem("Analogs", analogs);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                depId = (long)savedState["LinkedDepartment"];
                name = (string)savedState["Name"];
                analogs = (string)savedState["Analogs"];
                base.LoadState();
            }
        }
    }
}
