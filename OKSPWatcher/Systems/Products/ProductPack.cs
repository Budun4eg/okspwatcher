﻿using OKSPWatcher.Core;
using OKSPWatcher.Deps;
using System.Collections.Generic;

namespace OKSPWatcher.Products
{
    /// <summary>
    /// Набор изделий
    /// </summary>
    public class ProductPack : ItemPack<Product>
    {
        public ProductPack(List<long> ids) : base()
        {
            name = "ProductPack";
            foreach (var id in ids)
            {
                var item = ProductManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }

        public ProductPack() : base() {
            name = "ProductPack";
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = ProductManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }

        /// <summary>
        /// Загрузить изделия для определенного отдела
        /// </summary>
        public void LoadDep(Dep dep)
        {
            ProductManager.Instance.LoadDep(dep);
            items.Clear();
            foreach (Product item in ProductManager.Instance.ByDep(dep).Sorted())
            {
                Add(item);
            }
        }
    }
}
