﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Products
{
    /// <summary>
    /// Менеджер изделий
    /// </summary>
    public class ProductManager : ManagerObject<Product>
    {
        static ProductManager instance;

        protected ProductManager() : base()
        {

        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static ProductManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ProductManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Список изделий по имени
        /// </summary>
        Dictionary<string, ProductPack> byName = new Dictionary<string, ProductPack>();

        /// <summary>
        /// Получить набор изделий по наименованию
        /// </summary>
        public ProductPack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM Products WHERE Name LIKE '%{0}%' OR Analogs LIKE '%{0}%' ORDER BY Name", key);
                    var data = SQL.Get("Products").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new Product(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new ProductPack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Набор всех изделий
        /// </summary>
        public ProductPack All
        {
            get
            {
                var pack = new ProductPack();
                LoadAll();
                foreach(var item in Items)
                {
                    pack.Add(item);
                }

                return pack;
            }
        }

        /// <summary>
        /// Список изделий по отделу
        /// </summary>
        Dictionary<Dep, ProductPack> byDep = new Dictionary<Dep, ProductPack>();

        /// <summary>
        /// Получить набор изделий по отделу
        /// </summary>
        public ProductPack ByDep(Dep key)
        {
            if (byDep.ContainsKey(key))
            {
                return byDep[key];
            }
            else
            {
                var item = new ProductPack();
                byDep.Add(key, item);
                return item;
            }
        }

        /// <summary>
        /// Список транслитерации изделий
        /// </summary>
        public Dictionary<string, ProductPack> Translited
        {
            get
            {
                LoadAll();
                Dictionary<string, ProductPack> toReturn = new Dictionary<string, ProductPack>();
                foreach(var item in byName)
                {
                    toReturn.Add(item.Key.Translit(), item.Value);
                }
                return toReturn;
            }
        }

        public override Product ById(long id, bool load = false)
        {
            if (id < 0) return null;
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM Products WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("Products").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new Product(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Найти наиболее подходящее изделие по началу строки
        /// </summary>
        public Product FindStart(string key, out string foundName, bool load = false)
        {
            Product found = null;
            foundName = null;

            lock (changeLock)
            {
                foreach (var prd in byName)
                {
                    if (key.StartsWith(prd.Key))
                    {
                        if (found == null || found.Name.Length < prd.Key.Length)
                        {
                            found = prd.Value.Single;
                            foundName = prd.Key;
                        }
                    }
                }
            }
            

            if(found == null && load)
            {
                var spl = key.Split(new char[] { '.', ' ' });
                string request = string.Format("SELECT * FROM Products WHERE Name LIKE '%{0}%' OR Analogs LIKE '%{0}%' ORDER BY Name", spl[0]);
                var data = SQL.Get("Products").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new Product(row));
                }
                return FindStart(key, out foundName);
            }

            return found;
        }

        /// <summary>
        /// Добавить изделие
        /// </summary>
        public override bool Add(Product item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null && item.Name.Length != 0)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new ProductPack() { item });
                    }
                }
                if (item.Analogs != null)
                {
                    var spl = item.Analogs.Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var splItem in spl)
                    {
                        var toAdd = splItem.Trim();
                        if (toAdd != null && toAdd.Length != 0)
                        {
                            if (byName.ContainsKey(toAdd))
                            {
                                byName[toAdd].Add(item);
                            }
                            else
                            {
                                byName.Add(toAdd, new ProductPack() { item });
                            }
                        }
                    }
                }
                if (item.LinkedDep != null)
                {
                    if (byDep.ContainsKey(item.LinkedDep))
                    {
                        byDep[item.LinkedDep].Add(item);
                    }
                    else
                    {
                        byDep.Add(item.LinkedDep, new ProductPack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить изделие
        /// </summary>
        public override void Remove(Product item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if (item.Analogs != null)
                {
                    var spl = item.Analogs.Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var splItem in spl)
                    {
                        var toCheck = splItem.Trim();
                        if (byName.ContainsKey(toCheck) && byName[toCheck].Contains(item))
                        {
                            byName[toCheck].Remove(item);
                            if (byName[toCheck].Count == 0)
                            {
                                byName.Remove(toCheck);
                            }
                        }
                    }
                }
                if (item.LinkedDep != null && byDep.ContainsKey(item.LinkedDep) && byDep[item.LinkedDep].Contains(item))
                {
                    byDep[item.LinkedDep].Remove(item);
                    if (byDep[item.LinkedDep].Count == 0)
                    {
                        byDep.Remove(item.LinkedDep);
                    }
                }
            }            
        }

        /// <summary>
        /// Загрузить все изделия
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("Products").ExecuteRead("SELECT * FROM Products ORDER BY Name");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new Product(row));
            }
        }

        /// <summary>
        /// Загрузить все изделия определенного отдела
        /// </summary>
        public void LoadDep(Dep dep)
        {
            string request = string.Format("SELECT * FROM Products WHERE Department = {0} ORDER BY Name", dep.Id);
            var data = SQL.Get("Products").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new Product(row));
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Products").Execute(@"DROP TABLE Products");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Products").Execute(@"CREATE TABLE IF NOT EXISTS Products (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,                                                                
                                                    Name TEXT,
                                                    Department INTEGER,
                                                    Analogs TEXT DEFAULT '',
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Синхронизировать состояние менеджера
        /// </summary>
        public override void Sync(string dataType = null)
        {
            base.Sync(dataType);
            if(dataType == null || dataType == "LinkedDep")
            {
                byDep.Clear();
                foreach(var item in items)
                {
                    if (item.LinkedDep == null) continue;
                    if (byDep.ContainsKey(item.LinkedDep))
                    {
                        byDep[item.LinkedDep].Add(item);
                    }
                    else
                    {
                        byDep.Add(item.LinkedDep, new ProductPack() { item });
                    }
                }
            }
            if(dataType == null || dataType == "Name")
            {
                byName.Clear();
                foreach (var item in items) {
                    if(item.Name != null)
                    {
                        if (byName.ContainsKey(item.Name))
                        {
                            byName[item.Name].Add(item);
                        }
                        else
                        {
                            byName.Add(item.Name, new ProductPack() { item });
                        }
                    }
                    if (item.Analogs == null) continue;
                    var spl = item.Analogs.Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var splItem in spl)
                    {
                        var toAdd = splItem.Trim();
                        if (byName.ContainsKey(toAdd))
                        {
                            byName[toAdd].Add(item);
                        }
                        else
                        {
                            byName.Add(toAdd, new ProductPack() { item });
                        }
                    }
                }
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
