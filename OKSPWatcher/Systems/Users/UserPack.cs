﻿using OKSPWatcher.Core;
using OKSPWatcher.Deps;
using System.Collections.Generic;

namespace OKSPWatcher.Users
{
    /// <summary>
    /// Набор пользователей
    /// </summary>
    public class UserPack : ItemPack<User>
    {
        public UserPack() : base()
        {
            name = "UserPack";
        }

        /// <summary>
        /// Загрузить пользователей для определенного отдела
        /// </summary>
        public void LoadDep(Dep dep)
        {
            UserManager.Instance.LoadDep(dep);
            items.Clear();
            foreach (User item in UserManager.Instance.ByDep(dep))
            {
                Add(item);
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = UserManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }

        public UserPack(List<long> ids) : base()
        {
            name = "UserPack";
            foreach (var id in ids)
            {
                var item = UserManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
