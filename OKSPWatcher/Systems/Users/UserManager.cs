﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Users
{
    /// <summary>
    /// Менеджер пользователей
    /// </summary>
    public class UserManager : ManagerObject<User>
    {
        static UserManager instance;

        protected UserManager() : base()
        {

        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static UserManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new UserManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override User ById(long id, bool load = false)
        {
            if (id < 0) return null;
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM Users WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("Users").ExecuteRead(request);

                foreach (SQLDataRow row in data)
                {
                    Add(new User(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Список пользователей по имени
        /// </summary>
        Dictionary<string, UserPack> byName = new Dictionary<string, UserPack>();

        /// <summary>
        /// Получить набор пользователей по наименованию
        /// </summary>
        public UserPack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM Users WHERE Name LIKE '%{0}%' OR SearchName LIKE '%{0}%' ORDER BY Name", key);
                    var data = SQL.Get("Users").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new User(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new UserPack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Список пользователей по логину
        /// </summary>
        Dictionary<string, UserPack> byLogin = new Dictionary<string, UserPack>();

        /// <summary>
        /// Получить пользователя по логину
        /// </summary>
        public UserPack ByLogin(string key, bool load = false)
        {            
            key = key.ToLower();
            if (byLogin.ContainsKey(key))
            {
                return byLogin[key];
            }
            else
            {
                lock (changeLock)
                {
                    if (!byLogin.ContainsKey(key))
                    {
                        if (load)
                        {
                            string request = string.Format("SELECT * FROM Users WHERE LoginName = '{0}' LIMIT 1", key);
                            var data = SQL.Get("Users").ExecuteRead(request);
                            foreach (SQLDataRow row in data)
                            {
                                Add(new User(row));
                            }
                            return ByLogin(key);
                        }
                        else
                        {
                            var item = new UserPack();
                            byLogin.Add(key, item);
                            return item;
                        }
                    }
                    else
                    {
                        return byLogin[key];
                    }                    
                }                              
            }
        }

        /// <summary>
        /// Загрузить всех пользователей
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("Users").ExecuteRead("SELECT * FROM Users ORDER BY Name");
            foreach (SQLDataRow row in data)
            {
                Add(new User(row));
            }
        }

        /// <summary>
        /// Список пользователей по отделу
        /// </summary>
        Dictionary<Dep, UserPack> byDep = new Dictionary<Dep, UserPack>();

        /// <summary>
        /// Загрузить всех пользователей определенного отдела
        /// </summary>
        public void LoadDep(Dep dep)
        {
            string request = string.Format("SELECT * FROM Users WHERE Department = {0} ORDER BY Name", dep.Id);
            var data = SQL.Get("Users").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new User(row));
            }
        }

        /// <summary>
        /// Получить набор пользователей по отделу
        /// </summary>
        public UserPack ByDep(Dep dep, bool load = false)
        {
            if (load)
            {
                LoadDep(dep);
            }
            if (byDep.ContainsKey(dep))
            {
                return byDep[dep];
            }
            else
            {
                var item = new UserPack();
                byDep.Add(dep, item);
                return item;
            }
        }

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        public override bool Add(User item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new UserPack() { item });
                    }
                    var spl = item.Name.Split(new char[] { ' ' }, StringSplitOptions.None);
                    if (spl.Length > 0)
                    {
                        if (byName.ContainsKey(spl[0]))
                        {
                            byName[spl[0]].Add(item);
                        }
                        else
                        {
                            byName.Add(spl[0], new UserPack() { item });
                        }
                    }
                }
                if (item.LoginName != null)
                {
                    if (byLogin.ContainsKey(item.LoginName))
                    {
                        byLogin[item.LoginName].Add(item);
                    }
                    else
                    {
                        byLogin.Add(item.LoginName, new UserPack() { item });
                    }
                }
                if (item.SearchName != null)
                {
                    var spl = item.SearchName.Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var splItem in spl)
                    {
                        var toAdd = splItem.Trim();
                        if (byName.ContainsKey(toAdd))
                        {
                            byName[toAdd].Add(item);
                        }
                        else
                        {
                            byName.Add(toAdd, new UserPack() { item });
                        }
                    }
                }
                if (item.LinkedDep != null)
                {
                    if (byDep.ContainsKey(item.LinkedDep))
                    {
                        byDep[item.LinkedDep].Add(item);
                    }
                    else
                    {
                        byDep.Add(item.LinkedDep, new UserPack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        public override void Remove(User item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if (item.LoginName != null && byLogin.ContainsKey(item.LoginName) && byLogin[item.LoginName].Contains(item))
                {
                    byLogin[item.LoginName].Remove(item);
                    if (byLogin[item.LoginName].Count == 0)
                    {
                        byLogin.Remove(item.LoginName);
                    }
                }
                if (item.SearchName != null)
                {
                    var spl = item.SearchName.Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var splItem in spl)
                    {
                        var toCheck = splItem.Trim();
                        if (byName.ContainsKey(toCheck) && byName[toCheck].Contains(item))
                        {
                            byName[toCheck].Remove(item);
                            if (byName[toCheck].Count == 0)
                            {
                                byName.Remove(toCheck);
                            }
                        }
                    }
                }

                if (item.LinkedDep != null && byDep.ContainsKey(item.LinkedDep) && byDep[item.LinkedDep].Contains(item))
                {
                    byDep[item.LinkedDep].Remove(item);
                    if (byDep[item.LinkedDep].Count == 0)
                    {
                        byDep.Remove(item.LinkedDep);
                    }
                }
            }            
        }

        /// <summary>
        /// Синхронизировать состояние менеджера
        /// </summary>
        public override void Sync(string dataType = null)
        {
            base.Sync(dataType);
            if(dataType == null || dataType == "Name")
            {
                byName.Clear();
                foreach (var user in items)
                {
                    if (user.Name != null) {
                        if (byName.ContainsKey(user.Name))
                        {
                            byName[user.Name].Add(user);
                        }
                        else
                        {
                            byName.Add(user.Name, new UserPack() { user });
                        }
                    }
                    
                    if (user.SearchName == null) continue;
                    var spl = user.SearchName.Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var item in spl)
                    {
                        var toAdd = item.Trim();
                        if (byName.ContainsKey(toAdd))
                        {
                            byName[toAdd].Add(user);
                        }
                        else
                        {
                            byName.Add(toAdd, new UserPack() { user });
                        }
                    }
                }
            }
            if(dataType == null || dataType == "LoginName")
            {
                byLogin.Clear();
                foreach (var user in items)
                {
                    if (user.LoginName == null) continue;
                    if (byLogin.ContainsKey(user.LoginName))
                    {
                        byLogin[user.LoginName].Add(user);
                    }
                    else
                    {
                        byLogin.Add(user.LoginName, new UserPack() { user });
                    }
                }
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Users").Execute(@"DROP TABLE Users");
            SQL.Get("UserDatas").Execute(@"DROP TABLE UserDatas");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Users").Execute(@"CREATE TABLE IF NOT EXISTS Users (
                                                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                LoginName TEXT,
                                                Name TEXT,
                                                SearchName TEXT,
                                                Department INTEGER,
                                                Products TEXT,
                                                UpdateDate INTEGER
                                                )");
            SQL.Get("UserDatas").Execute(@"CREATE TABLE IF NOT EXISTS UserDatas (
                                                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                UserId INTEGER,
                                                FirstHelpShown INTEGER,
                                                Rules INTEGER,
                                                GroupRules INTEGER,
                                                UpdateDate INTEGER
                                                )");
            SQL.Get("UserDatas").Execute(@"CREATE INDEX IF NOT EXISTS UserDatas_UserId ON UserDatas(UserId)");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
