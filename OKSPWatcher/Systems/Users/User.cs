﻿using LingvoNET;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using System;

namespace OKSPWatcher.Users
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User : DBObject
    {
        /// <summary>
        /// Имя
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                name = value;
                UserManager.Instance.Sync("Name");
                OnPropertyChanged("Name");
                OnPropertyChanged("ShortName");
                OnPropertyChanged("ShortNameDative");
            }
        }

        /// <summary>
        /// Сокращенный вариант имени
        /// </summary>
        public string ShortName
        {
            get
            {
                if (Name != null && Name != "")
                {
                    var spl = Name.Split(new char[] { ' ' });
                    if (spl.Length >= 3)
                    {
                        try
                        {
                            return string.Format("{0}.{1}. {2}", spl[1][0], spl[2][0], spl[0]);
                        }
                        catch (Exception ex)
                        {
                            Output.Error(ex, "Ошибка при сокращении имени {0}", Name);
                            return Name;
                        }
                    }
                    else
                    {
                        return Name;
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Сокращенный вариант имени в дательном падеже
        /// </summary>
        public string ShortNameDative
        {
            get
            {
                if (Name != null && Name != "")
                {
                    var spl = Name.Split(new char[] { ' ' });
                    if (spl.Length >= 3)
                    {
                        try
                        {
                            return string.Format("{0}.{1}. {2}", spl[1][0], spl[2][0], Nouns.FindSimilar(spl[0])[Case.Dative]);
                        }
                        catch (Exception ex)
                        {
                            Output.Error(ex, "Ошибка при сокращении в дательном падеже {0}", Name);
                            return Name;
                        }
                    }
                    else
                    {
                        return Name;
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        string loginName = "";
        /// <summary>
        /// Имя, используемое при входе в систему
        /// </summary>
        public string LoginName
        {
            get
            {
                return loginName;
            }
            set
            {
                loginName = value;
                UserManager.Instance.Sync("LoginName");
                OnPropertyChanged("LoginName");
            }
        }

        string searchName = "";
        /// <summary>
        /// Строка для поиска пользователя
        /// </summary>
        public string SearchName
        {
            get
            {
                return searchName;
            }
            set
            {
                searchName = value;
                UserManager.Instance.Sync("Name");
                OnPropertyChanged("SearchName");
            }
        }

        long depId = -1;
        /// <summary>
        /// Отдел пользователя
        /// </summary>
        public Dep LinkedDep
        {
            get
            {
                return DepManager.Instance.ById(depId, true);
            }
            set
            {
                depId = value != null ? value.Id : -1;
            }
        }

        ProductPack products;
        /// <summary>
        /// Набор изделий пользователя
        /// </summary>
        public ProductPack Products
        {
            get
            {
                if(products == null)
                {
                    products = new ProductPack();
                    products.Updated += PackUpdated;
                }
                return products;
            }
        }

        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        public User(SQLDataRow pair) : base()
        {
            Load(pair);
        }

        UserData data;
        /// <summary>
        /// Данные пользователя
        /// </summary>
        public UserData Data
        {
            get
            {
                if(data == null)
                {
                    data = new UserData(this);

                    needUpdate = true;
                    data.Updated += PackUpdated;
                }
                return data;
            }
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                SearchName = data.String("SearchName");
                LoginName = data.String("LoginName");
                depId = data.Long("Department");
                OnPropertyChanged("LinkedDep");
                if(LinkedDep != null)
                {
                    LinkedDep.CheckUpdate();
                }
                Products.Sync(data.Ids("Products"));
                foreach(var prd in Products.Items)
                {
                    if(prd != null)
                    {
                        prd.CheckUpdate();
                    }
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                searchName = data.String("SearchName");
                loginName = data.String("LoginName");
                depId = data.Long("Department");
                Products.Sync(data.Ids("Products"));
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public User() : base()
        {
            needUpdate = true;
        }

        public User(string login) : base()
        {
            name = "Пользователь отсутствует";
            LoginName = login;
            saveEnabled = false;
        }

        protected override string sqlPath
        {
            get
            {
                return "Users";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Users (LoginName, Name, SearchName, Department, Products, UpdateDate) VALUES ('{0}','{1}','{2}',{3},'{4}', {5})", out id,
                loginName.Check(), name.Check(), searchName.Check(), depId, Products.Serialize(), UpdateDate.Ticks);

            base.CreateNew();
            UserManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Users SET LoginName = '{0}', Name = '{1}', SearchName = '{2}', Department = {3}, Products = '{4}', UpdateDate = {5} WHERE Id = {6}",
                loginName.Check(), name.Check(), searchName.Check(), depId, Products.Serialize(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            UserManager.Instance.Remove(this);
            sql.Execute("DELETE FROM Users WHERE Id = {0}", id);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("Name", name);
            AddStateItem("LoginName", loginName);
            AddStateItem("SearchName", searchName);
            AddStateItem("LinkedDep", depId);
            Products.SaveState();
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                name = (string)savedState["Name"];
                searchName = (string)savedState["SearchName"];
                loginName = (string)savedState["LoginName"];
                depId = (long)savedState["LinkedDep"];
                Products.LoadState();
                base.LoadState();
            }
        }

        public override bool Save()
        {
            if (!saveEnabled) return false;
            var ret = base.Save();
            if (!ret) return false;
            if (data != null)
            {
                data.Save();
            }
            return true;
        }
    }
}
