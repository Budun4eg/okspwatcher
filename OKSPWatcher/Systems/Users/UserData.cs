﻿using OKSPWatcher.Core;
using System.Collections.Generic;
using OKSPWatcher.Core.SQLProcessor;
using System;
using OKSPWatcher.Rules;

namespace OKSPWatcher.Users
{
    /// <summary>
    /// Объект данных о пользователе
    /// </summary>
    public class UserData : DBObject
    {
        /// <summary>
        /// Список предустановленных пользователей
        /// </summary>
        static Dictionary<string, long> predefined = new Dictionary<string, long>();

        /// <summary>
        /// Событие, вызываемое при изменении данных
        /// </summary>
        public event EventHandler Updated;
        void OnUpdated()
        {
            needUpdate = true;
            var handler = Updated;
            if(handler != null)
            {
                handler(null, null);
            }
        }

        long rulesId = -1;
        /// <summary>
        /// Права пользователя
        /// </summary>
        public RulesPack Rules
        {
            get
            {
                var item = RulesManager.Instance.ById(rulesId, true);
                if(item == null)
                {
                    return null;
                }
                if (rulesId != item.Id)
                {
                    Rules = item;
                    Save();
                }
                return item;
            }
            set
            {
                var ruleId = value != null ? value.Id : -1;
                if(rulesId != ruleId)
                {
                    needUpdate = true;                    
                }
                rulesId = ruleId;
                Save();
                OnPropertyChanged("Rules");
             }
        }

        long groupRulesId = -1;
        /// <summary>
        /// Групповые права
        /// </summary>
        public RuleGroupItem GroupRules
        {
            get
            {
                var item = RuleGroupManager.Instance.ById(groupRulesId, true);
                if(item == null)
                {
                    return null;
                }
                if (groupRulesId != item.Id)
                {
                    GroupRules = item;
                    Save();
                }
                return item;
            }
            set
            {
                var groupId = value != null ? value.Id : -1;
                if(groupId != groupRulesId)
                {
                    needUpdate = true;                    
                }
                groupRulesId = groupId;
                Save();
                OnPropertyChanged("GroupRules");
            }
        }

        bool firstHelpShown;
        /// <summary>
        /// Начальная помощь была показана?
        /// </summary>
        public bool FirstHelpShown
        {
            get
            {
                return firstHelpShown;
            }
            set
            {
                if(firstHelpShown != value)
                {
                    OnUpdated();
                }
                firstHelpShown = value;
                OnPropertyChanged("FirstHelpShown");
            }
        }

        /// <summary>
        /// Пользователь, к которому привязаны данные
        /// </summary>
        User user;

        public UserData(User _user)
        {
            user = _user;
            var data = SQL.Get("UserDatas").ExecuteRead("SELECT * FROM UserDatas WHERE UserId = {0}", user.Id);
            Load(data.Single);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                rulesId = data.Long("Rules");
                OnPropertyChanged("Rules");
                if(Rules != null)
                {
                    Rules.CheckUpdate();
                }
                groupRulesId = data.Long("GroupRules");
                OnPropertyChanged("GroupRules");
                if(GroupRules != null)
                {
                    GroupRules.CheckUpdate();
                }
                FirstHelpShown = data.Bool("FirstHelpShown");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            if(data == null)
            {
                return;
            }
            try
            {
                rulesId = data.Long("Rules");
                groupRulesId = data.Long("GroupRules");
                firstHelpShown = data.Bool("FirstHelpShown");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }            
        }

        protected override string sqlPath
        {
            get
            {
                return "UserDatas";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO UserDatas (UserId, FirstHelpShown, Rules, GroupRules, UpdateDate) VALUES ({0}, {1}, {2}, {3}, {4})", out id,
                user.Id, firstHelpShown.ToLong(), rulesId, groupRulesId, UpdateDate.Ticks);
            return base.CreateNew();
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE UserDatas SET UserId = {0}, FirstHelpShown = {1}, Rules = {2}, GroupRules = {3}, UpdateDate = {4} WHERE Id = {5}",
                user.Id, firstHelpShown.ToLong(), rulesId, groupRulesId, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM UserDatas WHERE Id = {0}", id);
        }
    }
}
