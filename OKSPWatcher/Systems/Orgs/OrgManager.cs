﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;

namespace OKSPWatcher.Orgs
{
    /// <summary>
    /// Менеджер организаций
    /// </summary>
    public class OrgManager : ManagerObject<Org>
    {
        static OrgManager instance;

        protected OrgManager() : base()
        {

        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static OrgManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new OrgManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Список организаций по наименованию
        /// </summary>
        Dictionary<string, OrgPack> byName = new Dictionary<string, OrgPack>();

        /// <summary>
        /// Получить набор организаций по наименованию
        /// </summary>
        public OrgPack ByName(string key)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                var item = new OrgPack();
                byName.Add(key, item);
                return item;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Orgs").Execute(@"DROP TABLE Orgs");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Orgs").Execute(@"CREATE TABLE IF NOT EXISTS Orgs (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Добавить организацию
        /// </summary>
        public override bool Add(Org item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new OrgPack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Загрузить все данные
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("Orgs").ExecuteRead("SELECT * FROM Orgs ORDER BY Name");
            foreach (SQLDataRow row in data)
            {
                Add(new Org(row));
            }
        }

        /// <summary>
        /// Удалить организацию
        /// </summary>
        public override void Remove(Org item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
            }            
        }

        /// <summary>
        /// Синхронизировать состояние менеджера
        /// </summary>
        public override void Sync(string dataType = null)
        {
            base.Sync(dataType);
            if(dataType == null || dataType == "Name")
            {
                byName.Clear();
                foreach (var item in items)
                {
                    if (item.Name == null) continue;
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new OrgPack() { item });
                    }
                }
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override Org ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM Orgs WHERE Id = {0} ORDER BY Name", id);
                var data = SQL.Get("Orgs").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new Org(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }
    }
}
