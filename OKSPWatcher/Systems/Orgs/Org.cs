﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;

namespace OKSPWatcher.Orgs
{
    /// <summary>
    /// Организация
    /// </summary>
    public class Org : DBObject
    {
        /// <summary>
        /// Наименование организации
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OrgManager.Instance.Sync("Name");
                OnPropertyChanged("Name");
            }
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public Org(SQLDataRow data) : base()
        {
            Load(data);
        }

        public Org() : base()
        {

        }

        protected override string sqlPath
        {
            get
            {
                return "Orgs";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Orgs (Name, UpdateDate) VALUES ('{0}', {1})", out id, name.Check(), UpdateDate.Ticks);
            base.CreateNew();
            OrgManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Orgs SET Name = '{0}', UpdateDate = {1} WHERE Id = {2}", name.Check(), Id, UpdateDate.Ticks);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM Orgs WHERE Id = {0}", id);
            OrgManager.Instance.Remove(this);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("Name", name);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                name = (string)savedState["Name"];
                base.LoadState();
            }
        }
    }
}
