﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Orgs
{
    /// <summary>
    /// Набор организаций
    /// </summary>
    public class OrgPack : ItemPack<Org>
    {
        public OrgPack() : base()
        {
            name = "OrgPack";
        }

        public override void LoadAll()
        {
            base.LoadAll();
            OrgManager.Instance.LoadAll();
            items.Clear();
            foreach (var item in OrgManager.Instance.Items)
            {
                Add(item);
            }
        }
    }
}
