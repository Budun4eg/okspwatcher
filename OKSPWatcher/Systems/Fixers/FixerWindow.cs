﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Secondary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace OKSPWatcher.Fixers
{
    /// <summary>
    /// Окно для стандартной проверки данных
    /// </summary>
    public class FixerWindow : BasicWindow
    {
        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));            
        }

        int errorCount = 0;
        /// <summary>
        /// Количество ошибок
        /// </summary>
        public int ErrorCount
        {
            get
            {
                return errorCount;
            }
            set
            {
                errorCount = value;
                OnPropertyChanged("ErrorCount");
            }
        }

        int repairedCount;
        /// <summary>
        /// Количество исправленных
        /// </summary>
        public int RepairedCount
        {
            get
            {
                return repairedCount;
            }
            set
            {
                repairedCount = value;
                OnPropertyChanged("RepairedCount");
            }
        }

        /// <summary>
        /// Общее количество элементов для проверки
        /// </summary>
        public virtual int TotalCount
        {
            get
            {
                return 0;                
            }
        }

        bool autoScroll = true;
        /// <summary>
        /// Автоматическая прокрутка списка
        /// </summary>
        public bool AutoScroll
        {
            get
            {
                return autoScroll;
            }
            set
            {
                autoScroll = value;
                OnPropertyChanged("AutoScroll");
            }
        }

        BackgroundWorker _searchWorker;
        /// <summary>
        /// Объект для фоновой работы
        /// </summary>
        protected BackgroundWorker searchWorker
        {
            get
            {
                if(_searchWorker == null)
                {
                    _searchWorker = new BackgroundWorker() { WorkerReportsProgress = true };
                    _searchWorker.DoWork += SearchErrors;
                    _searchWorker.ProgressChanged += ReportSearchProgress;
                    _searchWorker.RunWorkerCompleted += SearchComplete;
                }
                return _searchWorker;
            }
        }

        /// <summary>
        /// Функция фоновой работы
        /// </summary>
        protected virtual void SearchErrors(object sender, DoWorkEventArgs e)
        {
            BackgroundHandler.Register(_searchWorker);
        }

        /// <summary>
        /// Список сообщений для показа
        /// </summary>
        protected Queue<FixerItem> toShow = new Queue<FixerItem>();

        private void ReportSearchProgress(object sender, ProgressChangedEventArgs e)
        {
            toShow.Enqueue(new FixerItem(e.UserState.ToString(), e.ProgressPercentage));
            Output.Log(e.UserState.ToString());
        }

        private void SearchComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            StartEnabled = true;
            timer.Stop();
            BackgroundHandler.Unregister(_searchWorker);
            NotifyWindow.ShowNote("Проверка выполнена");
        }

        DispatcherTimer _timer;
        /// <summary>
        /// Таймер для проверки вывода
        /// </summary>
        protected DispatcherTimer timer
        {
            get
            {
                if(_timer == null)
                {
                    _timer = new DispatcherTimer();
                    _timer.Interval = new TimeSpan(1000);
                    _timer.Tick += OutputToGUI;
                }
                return _timer;
            }
        }

        protected virtual void OutputToGUI(object sender, EventArgs e)
        {

        }

        protected override void OnClosed(EventArgs e)
        {
            cancel = true;
            timer.Stop();
            base.OnClosed(e);
        }

        /// <summary>
        /// Отмена фоновой операции
        /// </summary>
        protected bool cancel = false;

        bool startEnabled = true;
        /// <summary>
        /// Включен показ начала проверки?
        /// </summary>
        public bool StartEnabled
        {
            get
            {
                return startEnabled;
            }
            set
            {
                startEnabled = value;
                OnPropertyChanged("StartEnabled");
            }
        }

        /// <summary>
        /// Начать проверку
        /// </summary>
        protected virtual void StartCheck(object sender, RoutedEventArgs e)
        {
            StartEnabled = false;
            OnPropertyChanged("TotalCount");
            ErrorCount = 0;
            RepairedCount = 0;
            cancel = false;
            timer.Start();
            searchWorker.RunWorkerCheck();
        }

        /// <summary>
        /// Закончить проверку
        /// </summary>
        protected virtual void StopCheck(object sender, RoutedEventArgs e)
        {
            cancel = true;
            StartEnabled = true;
        }

        /// <summary>
        /// Обновить шаблонизатор
        /// </summary>
        protected virtual void UpdateTemplate(object sender, RoutedEventArgs e)
        {

        }
    }
}
