﻿namespace OKSPWatcher.Fixers
{
    /// <summary>
    /// Элемент вывода
    /// </summary>
    public class FixerItem
    {
        string text;
        /// <summary>
        /// Текст для вывода
        /// </summary>
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        int colorCode;
        /// <summary>
        /// Цветовой ключ
        /// </summary>
        public int ColorCode
        {
            get
            {
                return colorCode;
            }
            set
            {
                colorCode = value;
            }
        }

        public FixerItem(string _text, int _colorCode)
        {
            text = _text;
            colorCode = _colorCode;
        }
    }
}
