﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Rules;
using OKSPWatcher.Users;
using System.Collections.Generic;

namespace OKSPWatcher.Apps
{
    /// <summary>
    /// Менеджер доступных приложений
    /// </summary>
    public class AppManager : ManagerObject<AppItem>
    {
        static AppManager instance;

        protected AppManager()
        {

        }

        /// <summary>
        /// Загрузить все приложения
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("AppItems").ExecuteRead("SELECT * FROM AppItems ORDER BY Id");
            foreach(SQLDataRow row in data)
            {
                Add(new AppItem(row));
            }
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static AppManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new AppManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Предустановленный список администраторов
        /// </summary>
        List<string> admins = new List<string>() {
            "056nvg0128",
            "056zkv0096",
            "020lsi0721",
            "056erf0119"
        };

        /// <summary>
        /// Получить список доступных для пользователя приложений
        /// </summary>
        public List<AppItem> Available(User user)
        {
            bool testMode = false;
#if TEST_MODE
            testMode = true;
            Items.Clear();
            SQL.Get("AppItems").Execute(@"DELETE FROM AppItems");

            var depManagerApp = new AppItem();
            depManagerApp.Name = "Управление отделом";
            depManagerApp.Code = "DepManager";
            depManagerApp.Save();

            var workedSettingsApp = new AppItem();
            workedSettingsApp.Name = "Проработанные чертежи [Настройки]";
            workedSettingsApp.Code = "WorkedSettings";
            workedSettingsApp.Save();

            var workedReportApp = new AppItem();
            workedReportApp.Name = "Проработанные чертежи [Отчет]";
            workedReportApp.Code = "WorkedReport";
            workedReportApp.Save();

            var workedRepairApp = new AppItem();
            workedRepairApp.Name = "Исправление проработанных чертежей";
            workedRepairApp.Code = "WorkedRepair";
            workedRepairApp.Report = workedReportApp;
            workedRepairApp.Settings = workedSettingsApp;
            workedRepairApp.Save();

            var modelSettingsApp = new AppItem();
            modelSettingsApp.Name = "Модели [Настройки]";
            modelSettingsApp.Code = "ModelSettings";
            modelSettingsApp.Save();

            var modelReportApp = new AppItem();
            modelReportApp.Name = "Модели [Отчет]";
            modelReportApp.Code = "ModelReport";
            modelReportApp.Save();

            var modelRepairApp = new AppItem();
            modelRepairApp.Name = "Исправление моделей";
            modelRepairApp.Code = "ModelRepair";
            modelRepairApp.Report = modelReportApp;
            modelRepairApp.Settings = modelSettingsApp;
            modelRepairApp.Save();

            var changesSettingsApp = new AppItem();
            changesSettingsApp.Name = "Извещения [Настройки]";
            changesSettingsApp.Code = "ChangesSettings";
            changesSettingsApp.Save();

            var changesRepairApp = new AppItem();
            changesRepairApp.Name = "Исправление извещений";
            changesRepairApp.Code = "ChangesRepair";
            changesRepairApp.Settings = changesSettingsApp;
            changesRepairApp.Save();

            var changes3DReportApp = new AppItem();
            changes3DReportApp.Name = "Проведение извещений в 3D [Отчет]";
            changes3DReportApp.Code = "Changes3DReport";
            changes3DReportApp.Save();

            var changes3DSettingsApp = new AppItem();
            changes3DSettingsApp.Name = "Проведение извещений в 3D [Настройки]";
            changes3DSettingsApp.Code = "Changes3DSettings";
            changes3DSettingsApp.Save();

            var changes3DApp = new AppItem();
            changes3DApp.Name = "Проведение извещений в 3D";
            changes3DApp.Code = "Changes3D";
            changes3DApp.Report = changes3DReportApp;
            changes3DApp.Settings = changes3DSettingsApp;
            changes3DApp.Save();

            var remarksReportApp = new AppItem();
            remarksReportApp.Name = "Замечания [Отчет]";
            remarksReportApp.Code = "RemarksReport";
            remarksReportApp.Save();

            var remarksSettingsApp = new AppItem();
            remarksSettingsApp.Name = "Замечания [Настройки]";
            remarksSettingsApp.Code = "RemarksSettings";
            remarksSettingsApp.Save();

            var remarksApp = new AppItem();
            remarksApp.Name = "Замечания";
            remarksApp.Code = "Remarks";
            remarksApp.Report = remarksReportApp;
            remarksApp.Settings = remarksSettingsApp;
            remarksApp.Save();

            var chainsApp = new AppItem();
            chainsApp.Name = "Учет писем";
            chainsApp.Code = "Chains";
            chainsApp.Save();

            var utilityApp = new AppItem();
            utilityApp.Name = "Утилиты";
            utilityApp.Code = "Utilities";
            utilityApp.Save();

            var structureSettingsApp = new AppItem();
            structureSettingsApp.Name = "Структуры изделий [Настройки]";
            structureSettingsApp.Code = "StructureSettings";
            structureSettingsApp.Save();

            var createStructureApp = new AppItem();
            createStructureApp.Name = "Структуры изделий";
            createStructureApp.Code = "CreateStructure";
            createStructureApp.Settings = structureSettingsApp;
            createStructureApp.Save();

            var completedIn3DApp = new AppItem();
            completedIn3DApp.Name = "Готовность изделий в 3D";
            completedIn3DApp.Code = "CompletedIn3D";
            completedIn3DApp.Save();

            var infoApp = new AppItem();
            infoApp.Name = "Информационное приложение отдела 3D";
            infoApp.Code = "InfoApp";
            infoApp.Save();
#endif
            LoadAll();
            List<AppItem> toReturn = new List<AppItem>();
            if (testMode || admins.Contains(user.LoginName) || RulesManager.Instance.Can("App_Admin"))
            {
                var admin = new AppItem();
                admin.Name = "Админка";
                admin.Code = "Admin";
                toReturn.Add(admin);
            }
            List<AppItem> inserted = new List<AppItem>();
            foreach(var item in items)
            {
                if (!inserted.Contains(item) && !toReturn.Contains(item))
                {                    
                    inserted.Add(item);
                    if(testMode || item.IsAvailable)
                    {
                        toReturn.Add(item);
                    }
                    if(item.Report != null)
                    {
                        if (!inserted.Contains(item.Report))
                        {
                            inserted.Add(item.Report);
                        }
                        if (toReturn.Contains(item.Report))
                        {
                            toReturn.Remove(item.Report);
                        }
                        if(!testMode && !item.Report.IsAvailable)
                        {
                            item.Report = null;
                        }
                    }
                    if (item.Settings != null)
                    {
                        if (!inserted.Contains(item.Settings))
                        {
                            inserted.Add(item.Settings);
                        }
                        if (toReturn.Contains(item.Settings))
                        {
                            toReturn.Remove(item.Settings);
                        }
                        if(!testMode && !item.Settings.IsAvailable)
                        {
                            item.Settings = null;
                        }
                    }
                }
            }            

            return toReturn;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("AppItems").Execute(@"DROP TABLE AppItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("AppItems").Execute(@"CREATE TABLE IF NOT EXISTS AppItems (
                                                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                Name TEXT,
                                                Code TEXT,
                                                Required INTEGER,
                                                SettingsId INTEGER,
                                                ReportId INTEGER,
                                                Departments TEXT,
                                                UpdateDate INTEGER
                                                )");            
            
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
