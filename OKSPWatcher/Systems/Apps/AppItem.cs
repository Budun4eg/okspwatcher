﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Rules;
using System;

namespace OKSPWatcher.Apps
{
    /// <summary>
    /// Объект представления отдельного приложения
    /// </summary>
    public class AppItem : DBObject
    {
        /// <summary>
        /// Название приложения
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        string code = "";
        /// <summary>
        /// Идентификатор приложения
        /// </summary>
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                if(code != value)
                {
                    needUpdate = true;
                }
                code = value;
                OnPropertyChanged("Code");
            }
        }

        /// <summary>
        /// Приложение доступно?
        /// </summary>
        public bool IsAvailable
        {
            get
            {
                return RulesManager.Instance.Can("App_" + Code);
            }
        }

        long settingsId = -1;
        /// <summary>
        /// Приложение настроек
        /// </summary>
        public AppItem Settings
        {
            get
            {
                return AppManager.Instance.ById(settingsId);
            }
            set
            {
                long toSet = value != null ? value.id : -1;
                if(settingsId != toSet)
                {
                    needUpdate = true;
                }
                settingsId = toSet;
                OnPropertyChanged("Settings");
            }
        }

        /// <summary>
        /// Набор был изменен
        /// </summary>
        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        long reportId = -1;
        /// <summary>
        /// Приложение отчета
        /// </summary>
        public AppItem Report
        {
            get
            {
                return AppManager.Instance.ById(reportId);
            }
            set
            {
                long toSet = value != null ? value.id : -1;
                if(reportId != toSet)
                {
                    needUpdate = true;
                }
                reportId = toSet;
                OnPropertyChanged("Report");
            }
        }

        public AppItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public AppItem() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                Code = data.String("Code");
                settingsId = data.Long("SettingsId");
                OnPropertyChanged("Settings");
                if(Settings != null)
                {
                    Settings.CheckUpdate();
                }
                reportId = data.Long("ReportId");
                OnPropertyChanged("Report");
                if(Report != null)
                {
                    Report.CheckUpdate();
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                code = data.String("Code");
                settingsId = data.Long("SettingsId");
                reportId = data.Long("ReportId");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "AppItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO AppItems (Name, Code, SettingsId, ReportId, UpdateDate) VALUES ('{0}', '{1}', {2}, {3}, {4})", out id,
                name.Check(), code.Check(), settingsId, reportId, UpdateDate.Ticks);
            base.CreateNew();
            AppManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE AppItems SET Name = '{0}', Code = '{1}', SettingsId = {2}, ReportId = {3}, UpdateDate = {4} WHERE Id = {5}",
                name.Check(), code.Check(), settingsId, reportId, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM AppItems WHERE Id = {0}", id);
            AppManager.Instance.Remove(this);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("Name", name);
            AddStateItem("Code", code);
            AddStateItem("Settings", settingsId);
            AddStateItem("Report", reportId);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                name = (string)savedState["Name"];
                code = (string)savedState["Code"];
                settingsId = (long)savedState["Settings"];
                reportId = (long)savedState["Report"];
                base.LoadState();
            }
        }
    }
}
