﻿using Microsoft.Office.Interop.Excel;
using OKSPWatcher.CompletedIn3D;
using OKSPWatcher.Core;
using OKSPWatcher.Products;
using OKSPWatcher.SystemEditors.Windows;
using OKSPWatcher.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Media;

namespace OKSPWatcher.SchedulePlan
{
    /// <summary>
    /// Менеджер построения план-графиков
    /// </summary>
    public class SchedulePlanManager : NotifyObject
    {
        static SchedulePlanManager instance;

        protected SchedulePlanManager()
        {
            AvailableUsers.Sync(AppSettings.GetIds("SchedulePlanUsers"));
            partDiff = AppSettings.GetDouble("SchedulePartDiff");
            assemblyDiff = AppSettings.GetDouble("ScheduleAssemblyDiff");
            assemblyElementDiff = AppSettings.GetDouble("ScheduleAssemblyElementDiff");
            userPower = AppSettings.GetDouble("ScheduleUserPower");
            groupMult = AppSettings.GetDouble("ScheduleGroupMult");
            blockCount = AppSettings.GetInt("ScheduleBlockCount");
            startDate = AppSettings.GetLong("ScheduleStartDate");
            optCoeff = AppSettings.GetDouble("ScheduleOptCoeff");
            autoFormat = AppSettings.GetBool("DSEAutoFormat");
            addName = AppSettings.GetBool("DSEAddName");
            splitTypes = AppSettings.GetBool("DSESplitTypes");
            formatAspect = AppSettings.GetDouble("FormatAspect");
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static SchedulePlanManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new SchedulePlanManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        double partDiff;
        /// <summary>
        /// Стандартная сложность детали
        /// </summary>
        public double PartDiff
        {
            get
            {
                return partDiff;
            }
            set
            {
                if(partDiff != value) {
                    partDiff = value;
                    AppSettings.SetDouble("SchedulePartDiff", value);
                    OnPropertyChanged("PartDiff");
                }                
            }
        }

        bool autoFormat;
        /// <summary>
        /// Автоматически форматировать списки?
        /// </summary>
        public bool AutoFormat
        {
            get
            {
                return autoFormat;
            }
            set
            {
                if(autoFormat != value)
                {
                    autoFormat = value;
                    AppSettings.SetBool("DSEAutoFormat", value);
                    OnPropertyChanged("AutoFormat");
                }
            }
        }

        double formatAspect;
        /// <summary>
        /// Отношение сторон плана
        /// </summary>
        public double FormatAspect
        {
            get
            {
                return formatAspect;
            }
            set
            {
                if(formatAspect != value)
                {
                    formatAspect = value;
                    AppSettings.SetDouble("FormatAspect", value);
                    OnPropertyChanged("FormatAspect");
                }
            }
        }

        bool addName;
        /// <summary>
        /// Добавлять наименование?
        /// </summary>
        public bool AddName
        {
            get
            {
                return addName;
            }
            set
            {
                if (addName != value)
                {
                    addName = value;
                    AppSettings.SetBool("DSEAddName", value);
                    OnPropertyChanged("AddName");
                }
            }
        }

        bool splitTypes;
        /// <summary>
        /// Разделять типы ячеек?
        /// </summary>
        public bool SplitTypes
        {
            get
            {
                return splitTypes;
            }
            set
            {
                if (splitTypes != value)
                {
                    splitTypes = value;
                    AppSettings.SetBool("DSESplitTypes", value);
                    OnPropertyChanged("SplitTypes");
                }
            }
        }

        double assemblyDiff;
        /// <summary>
        /// Стандартная сложность сборки
        /// </summary>
        public double AssemblyDiff
        {
            get
            {
                return assemblyDiff;
            }
            set
            {
                if(assemblyDiff != value)
                {
                    assemblyDiff = value;
                    AppSettings.SetDouble("ScheduleAssemblyDiff", value);
                    OnPropertyChanged("AssemblyDiff");
                }
            }
        }

        double assemblyElementDiff;
        /// <summary>
        /// Увеличение сложности сборки за каждый из элементов
        /// </summary>
        public double AssemblyElementDiff
        {
            get
            {
                return assemblyElementDiff;
            }
            set
            {
                if(assemblyElementDiff != value)
                {
                    assemblyElementDiff = value;
                    AppSettings.SetDouble("ScheduleAssemblyElementDiff", value);
                    OnPropertyChanged("AssemblyElementDiff");
                }
            }
        }

        double userPower;
        /// <summary>
        /// Обработка сложности пользователем в день
        /// </summary>
        public double UserPower
        {
            get
            {
                return userPower;
            }
            set
            {
                if(userPower != value)
                {
                    userPower = value;
                    AppSettings.SetDouble("ScheduleUserPower", value);
                    OnPropertyChanged("UserPower");
                }
            }
        }        

        double groupMult;
        /// <summary>
        /// Уменьшение продуктивности группы при добавлении человека
        /// </summary>
        public double GroupMult
        {
            get
            {
                return groupMult;
            }
            set
            {
                if(groupMult != value)
                {
                    groupMult = value;
                    AppSettings.SetDouble("ScheduleGroupMult", value);
                    OnPropertyChanged("GroupMult");
                }
            }
        }

        double optCoeff;
        /// <summary>
        /// Коэффициент оптимизации распределения людей
        /// </summary>
        public double OptCoeff
        {
            get
            {
                return optCoeff;
            }
            set
            {
                if (optCoeff != value)
                {
                    optCoeff = value;
                    AppSettings.SetDouble("ScheduleOptCoeff", value);
                    OnPropertyChanged("OptCoeff");
                }
            }
        }

        string topNode;
        /// <summary>
        /// Верхняя ячейка
        /// </summary>
        public string TopNode
        {
            get
            {
                return topNode;
            }
            set
            {
                if (topNode != value)
                {
                    topNode = value;
                    AppSettings.SetString("TopNode", value);
                    OnPropertyChanged("TopNode");
                }
            }
        }

        int blockCount;
        /// <summary>
        /// Количество дней в блоке плана
        /// </summary>
        public int BlockCount
        {
            get
            {
                return blockCount;
            }
            set
            {
                if (blockCount != value)
                {
                    blockCount = value;
                    AppSettings.SetInt("ScheduleBlockCount", value);
                    OnPropertyChanged("BlockCount");
                }
            }
        }

        long startDate;
        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                if(startDate == 0)
                {
                    startDate = DateTime.Now.Ticks;
                }
                return new DateTime(startDate);
            }
            set
            {
                if(startDate != value.Ticks)
                {
                    startDate = value.Ticks;
                    AppSettings.SetLong("ScheduleStartDate", value.Ticks);
                    OnPropertyChanged("StartDate");
                }
            }
        }
                
        CompletedItemPack loaded;
        /// <summary>
        /// Загруженный набор
        /// </summary>
        public CompletedItemPack Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
                OnPropertyChanged("Loaded");
            }
        }
                
        Product linkedProduct;
        /// <summary>
        /// Загруженное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return linkedProduct;
            }
        }

        /// <summary>
        /// Загрузить объекты в менеджер
        /// </summary>
        public void Load(Product product, CompletedItemPack pack)
        {
            linkedProduct = product;
            loaded = pack;
        }

        UserPack availableUsers;
        /// <summary>
        /// Список доступных пользователей
        /// </summary>
        public UserPack AvailableUsers
        {
            get
            {
                if(availableUsers == null)
                {
                    availableUsers = new UserPack();
                }
                return availableUsers;
            }
        }

        /// <summary>
        /// Включить редактор пользователей
        /// </summary>
        public void ChooseUsers()
        {
            UserPackEditorWindow.Show(AvailableUsers);
            AppSettings.SetString("SchedulePlanUsers", AvailableUsers.Serialize());
        }

        /// <summary>
        /// Список элементов плана
        /// </summary>
        List<PlanItem> items = new List<PlanItem>();

        /// <summary>
        /// Получить положение элемента
        /// </summary>
        public int Index(PlanItem item)
        {
            return items.IndexOf(item);
        }

        /// <summary>
        /// Список групп людей
        /// </summary>
        List<PlanUserGroup> groups = new List<PlanUserGroup>();

        /// <summary>
        /// Отсортированный список плана
        /// </summary>
        List<PlanItem> sorted;

        /// <summary>
        /// Построить структуру плана
        /// </summary>
        void BuildStructure()
        {
            Output.Log("Построение структуры план-графика");
            items.Clear();
            added.Clear();
            groups.Clear();

            List<Color> avail = new List<Color>();

            var colors = AppSettings.GetString("ScheduleColors");
            var spl = colors.DeserializeString();
            foreach (var cl in spl)
            {
                if (cl == null || cl.Length == 0) continue;
                avail.Add(cl.FromString());
            }

            var rand = new Random();
            foreach (var item in loaded)
            {
                if (BackgroundHandler.AllClose) return;
                Parse(item, null, 0);
            }
            foreach (var item in AvailableUsers)
            {
                Color pickedColor;
                if (avail.Count > 0)
                {
                    pickedColor = avail[0];
                    avail.RemoveAt(0);
                }
                else
                {
                    pickedColor = Color.FromRgb((byte)rand.Next(0, 255), (byte)rand.Next(0, 255), (byte)rand.Next(0, 255));
                }

                groups.Add(new PlanUserGroup(item, pickedColor));
            }
            sorted = items.OrderByDescending(o => o.Difficulty).ToList();
            PlanItem top = null;
            foreach (var item in items)
            {
                if (item.Item.LinkedNode.Name == TopNode)
                {
                    top = item;
                }
            }
            if (top != null)
            {
                sorted.Remove(top);
                sorted.Add(top);
            }            
        }

        /// <summary>
        /// Добавленные ячейки
        /// </summary>
        List<CompletedItem> added = new List<CompletedItem>();

        /// <summary>
        /// Проверить ячейку
        /// </summary>
        void Parse(CompletedItem item, PlanItem parent, int layer)
        {
            if (!item.Enabled) return;
            PlanItem toAdd = parent;
            if(toAdd == null)
            {
                toAdd = new PlanItem();
                toAdd.Item = item;
                items.Add(toAdd);
                item.MainNode = true;
            }

            if (!added.Contains(item))
            {
                if (item.Childs.Count == 0)
                {
                    toAdd.Difficulty += partDiff;
                    toAdd.PartCount++;
                }
                else
                {
                    toAdd.Difficulty += assemblyDiff + (assemblyElementDiff * item.Childs.Count);
                    toAdd.AssemblyCount++;
                }
                added.Add(item);
            }
            
            foreach(var child in item.Childs){
                if (!child.Enabled) continue;
                if(child.Childs.Count == 0)
                {
                    Parse(child, toAdd, layer + 1);
                }
                else
                {
                    if(!child.MainNode)
                    {
                        Parse(child, toAdd, layer + 1);
                    }
                    else
                    {
                        Parse(child, null, layer + 1);
                    }
                }
            }
        }

        /// <summary>
        /// Заполнить данные элемента плана
        /// </summary>
        void InsertStruct(Worksheet sheet, PlanItem item)
        {
            sheet.Range[sheet.Cells[rowOffset, 1], sheet.Cells[rowOffset + 1, 1]].Merge();
            sheet.Range[sheet.Cells[rowOffset, 2], sheet.Cells[rowOffset + 1, 2]].Merge();
            sheet.Range[sheet.Cells[rowOffset, 3], sheet.Cells[rowOffset + 1, 3]].Merge();

            sheet.Cells[rowOffset, 1] = item.Item.LinkedNode.Name;
            sheet.Range[sheet.Cells[rowOffset, 1], sheet.Cells[rowOffset + 1, 1]].Borders.Weight = XlBorderWeight.xlThin;
            sheet.Range[sheet.Cells[rowOffset, 1], sheet.Cells[rowOffset + 1, 1]].Borders.LineStyle = XlLineStyle.xlContinuous;

            sheet.Cells[rowOffset, 2] = item.Item.LinkedNode.Description;
            sheet.Range[sheet.Cells[rowOffset, 2], sheet.Cells[rowOffset + 1, 2]].Borders.Weight = XlBorderWeight.xlThin;
            sheet.Range[sheet.Cells[rowOffset, 2], sheet.Cells[rowOffset + 1, 2]].Borders.LineStyle = XlLineStyle.xlContinuous;

            sheet.Cells[rowOffset, 3] = item.CountStr;
            sheet.Range[sheet.Cells[rowOffset, 3], sheet.Cells[rowOffset + 1, 3]].Borders.Weight = XlBorderWeight.xlThin;
            sheet.Range[sheet.Cells[rowOffset, 3], sheet.Cells[rowOffset + 1, 3]].Borders.LineStyle = XlLineStyle.xlContinuous;

            sheet.Cells[rowOffset, 4] = "План.";
            (sheet.Cells[rowOffset, 4] as Range).Borders.Weight = XlBorderWeight.xlThin;
            (sheet.Cells[rowOffset, 4] as Range).Borders.LineStyle = XlLineStyle.xlContinuous;

            sheet.Cells[rowOffset + 1, 4] = "Факт.";
            (sheet.Cells[rowOffset + 1, 4] as Range).Borders.Weight = XlBorderWeight.xlThin;
            (sheet.Cells[rowOffset + 1, 4] as Range).Borders.LineStyle = XlLineStyle.xlContinuous;
        }

        /// <summary>
        /// Начальное смещение по вертикали
        /// </summary>
        public const int StartRowOffset = 10;

        int rowOffset;
        /// <summary>
        /// Смещение строки
        /// </summary>
        public int RowOffset
        {
            get
            {
                return rowOffset;
            }
            set
            {
                rowOffset = value;
            }
        }

        int columnOffset;
        /// <summary>
        /// Смещение столбца
        /// </summary>
        public int ColumnOffset
        {
            get
            {
                return columnOffset;
            }
            set
            {
                columnOffset = value;
            }
        }

        Worksheet sheet;
        /// <summary>
        /// Объект для вывода
        /// </summary>
        public Worksheet Sheet
        {
            get
            {
                return sheet;
            }
            set
            {
                sheet = value;
            }
        }

        DateTime current;
        /// <summary>
        /// Текущая дата столбца
        /// </summary>
        public DateTime Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
            }
        }

        /// <summary>
        /// Список использованных наименований
        /// </summary>
        List<string> names = new List<string>();

        /// <summary>
        /// Список добавленных основных ячеек
        /// </summary>
        List<CompletedItem> mainNodes = new List<CompletedItem>();

        /// <summary>
        /// Создать список ДСЕ на разработку
        /// </summary>
        public void CreateDseList()
        {
            names.Clear();
            mainNodes.Clear();
            excelApp = new Application();
            excelApp.DisplayAlerts = false;
            excelApp.Visible = true;

            BuildStructure();

            var data = new PlanData(2);

            excelBook = excelApp.Workbooks.Add();
            
            var excelSheet = (Worksheet)excelBook.Worksheets.Add();
            excelSheet.Name = "Общий список";

            excelSheet.Cells[1, 1] = "Обозначение";
            excelSheet.Cells[1, 2] = "Наименование";
            excelSheet.Cells[1, 3] = "Отсутствует";

            foreach (var item in items)
            {
                InsertNode(excelSheet, item.Item, data);
            }

            excelSheet.Columns.AutoFit();

            Marshal.ReleaseComObject(excelBook);
            Marshal.ReleaseComObject(excelApp);

            excelBook = null;
            excelApp = null;
        }

        /// <summary>
        /// Приложение EXCEL
        /// </summary>
        Application excelApp;

        /// <summary>
        /// Книга EXCEL
        /// </summary>
        Workbook excelBook;

        /// <summary>
        /// Включить основную ячейку в отчет
        /// </summary>
        void InsertTopNode(Worksheet sheet, CompletedItem item, PlanData data)
        {
            if(autoFormat && data.Y > data.OptCount)
            {
                data.Y = 1;
                data.X += AddName ? 2 : 1;
            }
            foreach (var child in item.Childs)
            {
                InsertTopNode(sheet, child, data);
            }
            if (!item.Completed && !data.Inserted.Contains(item))
            {
                sheet.Cells[data.Y, data.X + 1] = item.LinkedNode.Name;
                if (AddName)
                {
                    sheet.Cells[data.Y, data.X + 2] = item.LinkedNode.Description;
                }
                
                data.Y++;
                data.Inserted.Add(item);
            }
        }

        /// <summary>
        /// Включить элемент в отчет
        /// </summary>
        void InsertNode(Worksheet sheet, CompletedItem item, PlanData data)
        {
            if (item.MainNode && !mainNodes.Contains(item))
            {
                mainNodes.Add(item);
                var certainData = new PlanData(1);
                float listCount = (item.TotalUniqueCount() - item.CompletedUniqueCount());
                if(listCount <= 0)
                {
                    return;
                }
                if (autoFormat)
                {                    
                    float optCount = 1;
                    while (true)
                    {
                        float verticalCount = listCount / optCount;
                        if(optCount * FormatAspect < listCount)
                        {
                            optCount++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    certainData.OptCount = (int)Math.Round(listCount / optCount);
                }
                else
                {
                    certainData.OptCount = (int)Math.Round(listCount);
                }

                var excelSheet = (Worksheet)excelBook.Worksheets.Add();
                string name = item.LinkedNode.Name;
                if (names.Contains(name))
                {
                    while (names.Contains(name))
                    {
                        name = name + "_";
                    }
                }
                names.Add(name);
                excelSheet.Name = name;

                InsertTopNode(excelSheet, item, certainData);

                excelSheet.Columns.AutoFit();

                var range = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[certainData.OptCount, certainData.X + (AddName ? 2 : 1)]];
                range.Borders.LineStyle = XlLineStyle.xlContinuous;

                int offset = 0;
                while (true)
                {
                    range = excelSheet.Range[excelSheet.Cells[1, offset + (AddName ? 2 : 1)], excelSheet.Cells[certainData.OptCount, offset + (AddName ? 2 : 1)]];
                    range.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
                    offset += (AddName ? 2 : 1);
                    if (offset > certainData.X) break;
                }

            }
            foreach (var child in item.Childs)
            {
                InsertNode(sheet, child, data);
            }
            if (!item.Completed && !data.Inserted.Contains(item))
            {
                sheet.Cells[data.Y, 1] = item.LinkedNode.Name;
                sheet.Cells[data.Y, 2] = item.LinkedNode.Description;
                string check = "";
                if (item.LinkedModel.Count == 0)
                {
                    check += "Модель / ";
                }
                if (item.LinkedWorked.Count == 0)
                {
                    check += "Чертеж / ";
                }
                if (check.Length > 0)
                {
                    check = check.Remove(check.Length - 3);
                }
                sheet.Cells[data.Y, 3] = check;
                data.Y++;
                data.Inserted.Add(item);
            }
        }

        /// <summary>
        /// Создать план по предоставленным данным
        /// </summary>
        public void CreatePlan()
        {
            Output.Log("Создание план-графика");
            current = StartDate;
            excelApp = new Application();
            excelApp.Visible = true;
            excelApp.DisplayAlerts = false;
            var excelBook = excelApp.Workbooks.Add();
            sheet = (Worksheet)excelBook.Worksheets.Add();
            sheet.Name = "План-график";

            BuildStructure();

            rowOffset = StartRowOffset;
            foreach(var item in items)
            {
                if (BackgroundHandler.AllClose) return;
                InsertStruct(sheet, item);
                rowOffset += 2;
            }

            rowOffset = StartRowOffset;
            columnOffset = 5;

            sheet.Columns.AutoFit();
            sheet.UsedRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
            sheet.UsedRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

            bool hasWork = true;

            int monthOffset = 5;

            while(hasWork)
            {
                if (BackgroundHandler.AllClose) return;
                Output.TraceLog("[{0}] Создание план-графика", current.ToShortDateString());

                var date = sheet.Cells[StartRowOffset - 1, columnOffset] as Range;
                date.Value = current.Day;
                date.Borders.LineStyle = XlLineStyle.xlContinuous;

                for (int i = groups.Count - 1; i >= 0; i--)
                {                    
                    if (i > groups.Count - 1) continue;
                    var curr = groups[i];
                    if (curr.Assigned != null)
                    {
                        if(curr.Assigned.Difficulty > 0)
                        {
                            for(int b = 0; b < BlockCount; b++)
                            {
                                curr.ApplyWork();
                            }                            
                            continue;
                        }
                        else
                        {
                            curr.Assigned = null;
                        }
                    }
                    if(sorted.Count > 0)
                    {
                        curr.Assigned = sorted[0];
                        sorted.RemoveAt(0);

                        double summaryDiff = 0d;
                        foreach(var item in sorted)
                        {
                            summaryDiff += item.Difficulty;
                        }
                        while (true)
                        {
                            if (curr.Assigned.Difficulty * OptCoeff / curr.GroupPower > summaryDiff / UserPower)
                            {
                                bool found = false;
                                for (int id = groups.Count - 1; id >= 0; id--)
                                {
                                    if (groups[id] != curr && groups[id].CanBeAttached)
                                    {
                                        curr.Pack.Add(groups[id].Pack.Single);
                                        groups.RemoveAt(id);
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    break;
                                }
                            }else if(sorted.Count == 0)
                            {
                                for(int gi = groups.Count - 1; gi >= 0; gi--)
                                {
                                    if (groups[gi] != curr && groups[gi].CanBeAttached)
                                    {
                                        curr.Pack.Add(groups[gi].Pack.Single);
                                        groups.RemoveAt(gi);
                                    }
                                }
                                break;
                            }
                            else
                            {
                                break;
                            }
                        }

                        for (int b = 0; b < BlockCount; b++)
                        {
                            curr.ApplyWork();
                        }
                    }                      
                }

                

                DateTime next = current;
                for(int i = 0; i < BlockCount; i++)
                {
                    next = next.AddDays(1);
                }
                if (next.DayOfWeek == DayOfWeek.Saturday)
                {
                    next = next.AddDays(2);
                }else if(next.DayOfWeek == DayOfWeek.Sunday)
                {
                    next = next.AddDays(1);
                }

                hasWork = false;
                foreach (var item in items)
                {
                    if (item.Difficulty > 0)
                    {
                        hasWork = true;
                    }
                }

                if (next.Month != current.Month || !hasWork)
                {
                    var range = sheet.Range[sheet.Cells[StartRowOffset - 2, monthOffset], sheet.Cells[StartRowOffset - 2, columnOffset]];
                    range.Merge();
                    range.VerticalAlignment = XlVAlign.xlVAlignCenter;
                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.Value = string.Format("{0}.{1}", current.Month, current.Year);
                    range.Borders.LineStyle = XlLineStyle.xlContinuous;
                    monthOffset = columnOffset + 1;
                }

                for (int ci = 0; ci < items.Count * 2; ci++)
                {
                    var cell = sheet.Cells[ci + StartRowOffset, columnOffset] as Range;
                    if ((XlBorderWeight)cell.Borders[XlBordersIndex.xlEdgeBottom].Weight == XlBorderWeight.xlThin)
                    {
                        cell.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                        if (ci % 2 == 0)
                        {
                            cell.Borders[XlBordersIndex.xlEdgeBottom].ColorIndex = 15;
                        }
                    }
                }

                columnOffset++;
                current = next;
            }

            int lastOffset = ColumnOffset;

            foreach(var item in items)
            {
                item.InsertInfo();
            }

            var checkRange = sheet.Range[sheet.Cells[StartRowOffset, ColumnOffset], sheet.Cells[StartRowOffset + items.Count * 2 + 1, ColumnOffset + 1]];            
            checkRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
            checkRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
            checkRange.Columns.AutoFit();

            Marshal.ReleaseComObject(excelBook);
            Marshal.ReleaseComObject(excelApp);

            excelBook = null;
            excelApp = null;

            Output.Log("План-график создан");
        }
    }

    /// <summary>
    /// Класс для определения смещения плана
    /// </summary>
    class PlanData
    {

        /// <summary>
        /// Смещение плана
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Горизонтальное смещение
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Оптимальное вертикальное количество
        /// </summary>
        public int OptCount { get; set; }

        /// <summary>
        /// Внесенные йчейки
        /// </summary>
        public List<CompletedItem> Inserted = new List<CompletedItem>();

        public PlanData(int y)
        {
            Y = y;
        }
    }
}
