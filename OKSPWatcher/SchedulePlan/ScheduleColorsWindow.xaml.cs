﻿using OKSPWatcher.Core;
using System.Windows.Media;
using System;
using System.Windows.Controls;
using System.Windows;

namespace OKSPWatcher.SchedulePlan
{
    /// <summary>
    /// Логика взаимодействия для ScheduleColorsWindow.xaml
    /// </summary>
    public partial class ScheduleColorsWindow : BasicWindow
    {
        SafeObservableCollection<ScheduleColorsItem> avail = new SafeObservableCollection<ScheduleColorsItem>();
        /// <summary>
        /// Список цветов
        /// </summary>
        public SafeObservableCollection<ScheduleColorsItem> Avail
        {
            get
            {
                return avail;
            }
        }

        public ScheduleColorsWindow()
        {
            InitializeComponent();
            var colors = AppSettings.GetString("ScheduleColors");
            var spl = colors.DeserializeString();
            foreach(var cl in spl)
            {
                if (cl == null || cl.Length == 0) continue;
                Avail.Add(new ScheduleColorsItem(cl));
            }

            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            var save = new SafeObservableCollection<string>();
            foreach(var cl in Avail)
            {
                save.Add(cl.Save());
            }

            AppSettings.SetString("ScheduleColors", save.Serialize());
        }

        private void AddColor(object sender, System.Windows.RoutedEventArgs e)
        {
            Avail.Add(new ScheduleColorsItem());
        }

        private void RemoveColor(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as ScheduleColorsItem;
            Avail.Remove(item);
        }
    }

    public class ScheduleColorsItem : NotifyObject
    {
        Color picked = Color.FromRgb(255,255,255);
        /// <summary>
        /// Выбранный цвет
        /// </summary>
        public Color Picked
        {
            get
            {
                return picked;
            }
            set
            {
                picked = value;
                OnPropertyChanged("Picked");
            }
        }

        public ScheduleColorsItem() { }

        public ScheduleColorsItem(string data)
        {
            var spl = data.Split(new char[] { ';' });
            Picked = Color.FromRgb((byte)spl[0].ToInt(), (byte)spl[1].ToInt(), (byte)spl[2].ToInt());
        }

        /// <summary>
        /// Создать строку из цвета
        /// </summary>
        public string Save()
        {
            var ret = string.Format("{0};{1};{2}", picked.R, picked.G, picked.B);
            return ret;
        }
    }
}
