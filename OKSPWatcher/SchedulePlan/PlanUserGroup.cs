﻿using Microsoft.Office.Interop.Excel;
using OKSPWatcher.Users;
using System;
using System.Drawing;
using System.Text;
using System.Windows.Media;

namespace OKSPWatcher.SchedulePlan
{
    /// <summary>
    /// Группа пользователей в плане
    /// </summary>
    public class PlanUserGroup
    {        
        UserPack pack;
        /// <summary>
        /// Набор пользователей
        /// </summary>
        public UserPack Pack
        {
            get
            {
                if(pack == null)
                {
                    pack = new UserPack();
                }
                return pack;
            }
        }

        /// <summary>
        /// Сила группы
        /// </summary>
        public double GroupPower
        {
            get
            {
                if (Pack.Count == 0) return 0d;
                double ret = 0d;
                for(int i = 0; i <= Pack.Count-1; i++)
                {
                    ret += SchedulePlanManager.Instance.UserPower * (1f - (i == 0 ? 0 : Math.Pow(SchedulePlanManager.Instance.GroupMult, i)));
                }

                return ret;
            }
        }

        /// <summary>
        /// Строка вывода пользователей
        /// </summary>
        public string UsersOutput
        {
            get
            {
                StringBuilder builder = new StringBuilder();

                foreach(var user in Pack)
                {
                    builder.AppendFormat("{0}, ", user.ShortName);
                }

                string ret = builder.ToString();
                if(ret.Length > 0)
                {
                    ret = ret.Remove(ret.Length - 2);
                }

                return ret;
            }
        }

        System.Drawing.Color colorIndex;
        /// <summary>
        /// Цвет группы
        /// </summary>
        public System.Drawing.Color ColorIndex
        {
            get
            {
                return colorIndex;
            }
        }

        public PlanUserGroup(User user, System.Windows.Media.Color _color)
        {
            Pack.Add(user);
            colorIndex = System.Drawing.Color.FromArgb(_color.R, _color.G, _color.B);
        }

        /// <summary>
        /// Предыдущий элемент плана
        /// </summary>
        PlanItem previous;

        /// <summary>
        /// Начальное смещение элемента
        /// </summary>
        int startOffset;

        bool canBeAttached = true;
        /// <summary>
        /// Пользователи могут быть присоединены к другой группе?
        /// </summary>
        public bool CanBeAttached
        {
            get
            {
                return canBeAttached;
            }
        }

        PlanItem assigned;
        /// <summary>
        /// Текущий элемент плана
        /// </summary>
        public PlanItem Assigned
        {
            get
            {
                return assigned;
            }
            set
            {
                if(value != null)
                {
                    canBeAttached = false;
                }
                if(value == null)
                {
                    previous = assigned;
                }
                startOffset = columnOffset;
                assigned = value;

                if(previous != null && assigned != null)
                {
                    int prevRow = rowOffset(previous);
                    int curRow = rowOffset(assigned);
                    if(prevRow > curRow)
                    {
                        prevRow--;
                    }
                    else
                    {
                        prevRow++;
                    }
                    while(prevRow != curRow)
                    {
                        LeftDash(prevRow);
                        if(prevRow < curRow)
                        {
                            prevRow++;
                        }
                        else
                        {
                            prevRow--;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Левая прерывистая линия
        /// </summary>
        void LeftDash(int row)
        {
            var range = (sheet.Cells[row, columnOffset] as Range);
            if((XlBorderWeight)(range.Borders[XlBordersIndex.xlEdgeLeft].Weight) != XlBorderWeight.xlThick)
            {
                range.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlDash;
                range.Borders[XlBordersIndex.xlEdgeLeft].Weight = XlBorderWeight.xlMedium;
            }            
        }

        /// <summary>
        /// Смещение элемента по вертикали
        /// </summary>
        int rowOffset(PlanItem item)
        {
            return (SchedulePlanManager.Instance.Index(item) * 2) + SchedulePlanManager.Instance.RowOffset;
        }

        /// <summary>
        /// Рабочий лист
        /// </summary>
        Worksheet sheet
        {
            get
            {
                return SchedulePlanManager.Instance.Sheet;
            }
        }

        /// <summary>
        /// Смещение столбца
        /// </summary>
        int columnOffset
        {
            get
            {
                return SchedulePlanManager.Instance.ColumnOffset;
            }
        }

        /// <summary>
        /// Применить работу группы
        /// </summary>
        public void ApplyWork()
        {
            int row = rowOffset(Assigned);
            Assigned.Difficulty -= GroupPower;

            if (Assigned.Difficulty <= 0)
            {
                var range = sheet.Range[sheet.Cells[rowOffset(assigned), startOffset], sheet.Cells[rowOffset(assigned), columnOffset]];
                range.Merge();
                range.Borders.LineStyle = XlLineStyle.xlContinuous;
                range.Borders.Weight = XlBorderWeight.xlThick;
                range.ColumnWidth = 2.3;
                range.Interior.Color = ColorTranslator.ToOle(colorIndex);
                range.HorizontalAlignment = XlHAlign.xlHAlignRight;
                Assigned.LinkedGroup = this;
                Assigned.Finish = SchedulePlanManager.Instance.Current;
                range.NumberFormat = "@";
                if (columnOffset - startOffset >= 1)
                {                    
                    range.Value = string.Format("{0}.{1}", Assigned.Finish.Day, Assigned.Finish.Month);
                }
                else
                {
                    Assigned.InsertNote = true;
                    range.Value = "*";
                }
            }
        }
    }
}
