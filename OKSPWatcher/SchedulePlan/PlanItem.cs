﻿using Microsoft.Office.Interop.Excel;
using OKSPWatcher.CompletedIn3D;
using System;
using System.Drawing;
using System.Text;

namespace OKSPWatcher.SchedulePlan
{
    /// <summary>
    /// Элемент плана
    /// </summary>
    public class PlanItem
    {
        int partCount;
        /// <summary>
        /// Количество деталей
        /// </summary>
        public int PartCount
        {
            get
            {
                return partCount;
            }
            set
            {
                partCount = value;
            }
        }

        int assemblyCount;
        /// <summary>
        /// Количество сборок
        /// </summary>
        public int AssemblyCount
        {
            get
            {
                return assemblyCount;
            }
            set
            {
                assemblyCount = value;
            }
        }

        /// <summary>
        /// Вывод количества в строку
        /// </summary>
        public string CountStr
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                if(partCount > 0)
                {
                    builder.AppendFormat("Деталей: {0}", partCount);
                }
                if(assemblyCount > 0)
                {
                    if(builder.Length > 0)
                    {
                        builder.Append(Environment.NewLine);
                    }
                    builder.AppendFormat("Сборок: {0}", assemblyCount);
                }
                return builder.ToString();
            }
        }

        double difficulty;
        /// <summary>
        /// Сложность элемента
        /// </summary>
        public double Difficulty
        {
            get
            {
                return difficulty;
            }
            set
            {
                difficulty = value;
            }
        }

        CompletedItem item;
        /// <summary>
        /// Элемент готовности
        /// </summary>
        public CompletedItem Item
        {
            get
            {
                return item;
            }
            set
            {
                item = value;
            }
        }

        DateTime finish;
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime Finish
        {
            get
            {
                return finish;
            }
            set
            {
                finish = value;
            }
        }

        PlanUserGroup linkedGroup;
        /// <summary>
        /// Привязанная группа
        /// </summary>
        public PlanUserGroup LinkedGroup
        {
            get
            {
                return linkedGroup;
            }
            set
            {
                linkedGroup = value;
            }
        }

        bool insertNote;
        /// <summary>
        /// Вставлять примечание?
        /// </summary>
        public bool InsertNote
        {
            get
            {
                return insertNote;
            }
            set
            {
                insertNote = value;
            }
        }

        /// <summary>
        /// Смещение элемента по вертикали
        /// </summary>
        int rowOffset(PlanItem item)
        {
            return (SchedulePlanManager.Instance.Index(item) * 2) + SchedulePlanManager.Instance.RowOffset;
        }

        /// <summary>
        /// Рабочий лист
        /// </summary>
        Worksheet sheet
        {
            get
            {
                return SchedulePlanManager.Instance.Sheet;
            }
        }

        /// <summary>
        /// Смещение столбца
        /// </summary>
        int columnOffset
        {
            get
            {
                return SchedulePlanManager.Instance.ColumnOffset;
            }
        }

        /// <summary>
        /// Добавить информацию об окончании
        /// </summary>
        public void InsertInfo()
        {
            var userRange = sheet.Range[sheet.Cells[rowOffset(this), columnOffset], sheet.Cells[rowOffset(this) + 1, columnOffset]];
            userRange.Merge();
            userRange.Value = linkedGroup.UsersOutput;
            userRange.Borders.Weight = XlBorderWeight.xlThin;
            userRange.Borders.LineStyle = XlLineStyle.xlContinuous;
            userRange.Interior.Color = ColorTranslator.ToOle(linkedGroup.ColorIndex);
            var noteRange = sheet.Range[sheet.Cells[rowOffset(this), columnOffset + 1], sheet.Cells[rowOffset(this) + 1, columnOffset + 1]];
            noteRange.Merge();
            noteRange.Borders.Weight = XlBorderWeight.xlThin;
            noteRange.Borders.LineStyle = XlLineStyle.xlContinuous;
            if (insertNote)
            {               
                noteRange.Value = string.Format("*-{0}.{1}", finish.Day, finish.Month);
            }
        }
    }
}
