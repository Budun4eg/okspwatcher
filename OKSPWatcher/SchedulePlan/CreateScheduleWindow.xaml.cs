﻿using System.ComponentModel;
using OKSPWatcher.CompletedIn3D;
using System.Windows.Controls;
using System;
using OKSPWatcher.Secondary;
using OKSPWatcher.Products;
using OKSPWatcher.Core;

namespace OKSPWatcher.SchedulePlan
{
    /// <summary>
    /// Логика взаимодействия для CreateScheduleWindow.xaml
    /// </summary>
    public partial class CreateScheduleWindow : BasicWindow
    {
        public CreateScheduleWindow()
        {
            InitializeComponent();
            DataContext = SchedulePlanManager.Instance;
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if(state == States.Plan)
            {
                SchedulePlanManager.Instance.CreatePlan();
            }else if(state == States.List)
            {
                SchedulePlanManager.Instance.CreateDseList();
            }            
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            if(state == States.Plan)
            {
                NotifyWindow.ShowNote("Построение план-графика завершено");
            }
            else if(state == States.List)
            {
                NotifyWindow.ShowNote("Построение списка ДСЕ на разработку завершено");
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            SchedulePlanManager.Instance.Loaded.Save();
        }

        /// <summary>
        /// Варианты поведения
        /// </summary>
        enum States { Plan, List }

        /// <summary>
        /// Текущий вариант поведения
        /// </summary>
        States state;

        /// <summary>
        /// Загрузить набор
        /// </summary>
        public void Load(Product product, CompletedItemPack pack)
        {
            SchedulePlanManager.Instance.Load(product, pack);
        }

        public void CreatePlan(object sender, System.Windows.RoutedEventArgs e)
        {
            if(SchedulePlanManager.Instance.LinkedProduct == null)
            {
                Output.Warning("Изделие не выбрано");
                return;
            }
            state = States.Plan;
            worker.RunWorkerCheck();
        }

        private void ChooseUsers(object sender, System.Windows.RoutedEventArgs e)
        {
            SchedulePlanManager.Instance.ChooseUsers();
        }        

        private void ChooseColors(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new ScheduleColorsWindow();
            window.ShowDialog();
        }

        private void CreateDseList(object sender, System.Windows.RoutedEventArgs e)
        {
            if (SchedulePlanManager.Instance.LinkedProduct == null)
            {
                Output.Warning("Изделие не выбрано");
                return;
            }
            if (SchedulePlanManager.Instance.AutoFormat && SchedulePlanManager.Instance.FormatAspect <= 0f)
            {
                Output.Warning("Отношение сторон при автоматическом форматировании должно быть больше 0");
                return;
            }
            state = States.List;
            worker.RunWorkerCheck();
        }
    }
}
