﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Отчет с замечаниями
    /// </summary>
    public class RemarkReport : DBObject
    {
        public override string BlockCode
        {
            get
            {
                return string.Format("RemarkReport_{0}", Id);
            }
        }

        long depId = -1;
        /// <summary>
        /// Привязанный отдел
        /// </summary>
        public Dep LinkedDep
        {
            get
            {
                return DepManager.Instance.ById(depId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(depId != toSet)
                {
                    needUpdate = true;
                }
                depId = toSet;
                OnPropertyChanged("LinkedDep");
            }
        }

        public override string Name
        {
            get
            {
                return string.Format("№{0} от {1}{2}[{3} {4}]", number, Date.ToShortDateString(), Environment.NewLine, LinkedProduct.Name, LinkedUser.ShortName);
            }
        }

        long prdId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(prdId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(prdId != toSet)
                {
                    needUpdate = true;
                }
                prdId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        public override void RefreshSearch()
        {
            base.RefreshSearch();
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} ", number);
            if (LinkedDep != null)
            {
                builder.AppendFormat("{0} ", LinkedDep.Name.ToUpper());
            }
            if (LinkedProduct != null)
            {
                builder.AppendFormat("{0} ", LinkedProduct.Name.ToUpper());
            }

            foreach (var item in Items)
            {
                builder.AppendFormat("{0} ", item.Dse.ToUpper());
            }

            searchString = builder.ToString();
        }

        long date;
        /// <summary>
        /// Дата отправки замечания
        /// </summary>
        public DateTime Date
        {
            get
            {
                return new DateTime(date);
            }
            set
            {
                if(date != value.Ticks)
                {
                    needUpdate = true;
                }
                date = value.Ticks;
                OnPropertyChanged("Date");
            }
        }

        long number = 0;
        /// <summary>
        /// Номер замечания
        /// </summary>
        public long Number
        {
            get
            {
                return number;
            }
            set
            {
                if(value != number)
                {
                    needUpdate = true;
                }
                number = value;
                OnPropertyChanged("Number");
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(toSet != userId)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        RemarkItemPack items;
        /// <summary>
        /// Набор пунктов замечаний
        /// </summary>
        public RemarkItemPack Items
        {
            get
            {
                if(items == null)
                {
                    items = new RemarkItemPack(this);
                    items.Updated += PackUpdated;
                }
                return items;
            }
        }

        bool enabled;
        /// <summary>
        /// Отчет выбран?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                OnPropertyChanged("Enabled");
            }
        }

        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        string pathToReport;
        /// <summary>
        /// Путь до отчета
        /// </summary>
        public string PathToReport
        {
            get
            {
                return pathToReport;
            }
            set
            {
                if(pathToReport != value)
                {
                    needUpdate = true;
                }
                pathToReport = value;
                OnPropertyChanged("PathToReport");
            }
        }

        /// <summary>
        /// Список групп замечаний
        /// </summary>
        public string NoteGroups
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                List<string> added = new List<string>();
                foreach (RemarkItem note in Items)
                {
                    var foundName = "";
                    var prd = ProductManager.Instance.FindStart(note.Dse, out foundName, true);
                    string productName = prd != null ? prd.Name : "";

                    var splDse = note.Dse.Substring(productName.Length > 0 ? productName.Length + 1 : 0);
                    var spl = splDse.Split(new char[] { '-' });
                    var right = spl[0];
                    if (!right.ToUpper().StartsWith("СБ"))
                    {
                        right = "Сб" + right;
                    }
                    string toAdd = string.Format("{0}.{1}, ", productName, right);
                    if (!added.Contains(toAdd))
                    {
                        builder.AppendFormat(toAdd);
                        added.Add(toAdd);
                    }
                }
                if (builder.Length > 1)
                {
                    builder.Remove(builder.Length - 2, 2);
                }
                return builder.ToString();
            }
        }

        public RemarkReport(SQLDataRow data) : base()
        {
            Load(data);
        }

        public RemarkReport() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                depId = data.Long("Dep");
                OnPropertyChanged("LinkedDep");
                if(LinkedDep != null)
                {
                    LinkedDep.CheckUpdate();
                }
                prdId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                Date = new DateTime(data.Long("Date"));
                Number = data.Long("Number");
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                Items.Sync(data.Ids("Items"));
                foreach(var item in Items.Items)
                {
                    if(item != null)
                    {
                        item.CheckUpdate();
                    }
                }
                PathToReport = data.String("PathToReport");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                depId = data.Long("Dep");
                prdId = data.Long("Product");
                date = data.Long("Date");
                number = data.Long("Number");
                userId = data.Long("User");
                Items.Sync(data.Ids("Items"));
                pathToReport = data.String("PathToReport");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "RemarkReports";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO RemarkReports (Dep, Product, Date, Number, User, Items, PathToReport, UpdateDate) VALUES ({0}, {1}, {2}, {3}, {4}, '{5}', '{6}', {7})", out id,
                depId, prdId, date, number, userId, Items.Serialize(), pathToReport, UpdateDate.Ticks);
            base.CreateNew();
            RemarkManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE RemarkReports SET Dep = {0}, Product = {1}, Date = {2}, Number = {3}, User = {4}, Items = '{5}', PathToReport = '{6}', UpdateDate = {7} WHERE Id = {8}",
                depId, prdId, date, number, userId, Items.Serialize(), pathToReport, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM RemarkReports WHERE Id = {0}", id);
            RemarkManager.Instance.Remove(this);
            foreach(RemarkItem item in Items)
            {
                item.Delete();
            }
            if (File.Exists(PathToReport)){
                File.Delete(PathToReport);
            }
        }

        public override bool Save()
        {
            Items.Save();
            return base.Save();
        }
    }
}
