﻿using System.Windows.Controls;
using System.ComponentModel;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows.Input;
using System.Windows;
using OKSPWatcher.Secondary;
using System.IO;
using OKSPWatcher.Core;
using System;
using OKSPWatcher.Rules;
using OKSPWatcher.Block;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Логика взаимодействия для RemarkReportWindow.xaml
    /// </summary>
    public partial class RemarkReportWindow : BasicWindow
    {
        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            RemarkReportManager.Instance.CheckInstance();
        }

        SafeObservableCollection<string> states = new SafeObservableCollection<string>() {
            "в работе",
            "принято",
            "отклонено"
        };

        /// <summary>
        /// Варианты состояний
        /// </summary>
        public SafeObservableCollection<string> States
        {
            get
            {
                return states;
            }
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            DataContext = RemarkReportManager.Instance;
        }

        public RemarkReportWindow()
        {
            InitializeComponent();

            worker.RunWorkerCheck();
        }

        private void SearchClick(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                RemarkReportManager.Instance.Search();
            }
        }

        private void DeleteReport(object sender, RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("RemarkReportDelete"))
            {
                Output.TraceError("Удаление отчета запрещено");
                return;
            }

            e.Handled = true;
            var item = (sender as Control).DataContext as RemarkReport;

            if (!item.Available())
            {
                return;
            }

            RequestWindow.Show("Удалить отчет?");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                RemarkReportManager.Instance.Items.Remove(item);
                if(RemarkReportManager.Instance.Current == item)
                {
                    RemarkReportManager.Instance.Current = null;
                }
                item.Delete();
            }
        }

        private void OpenReport(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var item = (sender as Control).DataContext as RemarkReport;
            if (!File.Exists(item.PathToReport))
            {
                Output.WarningFormat("Файл отчета отсутствует {0}", item.PathToReport);
                return;
            }
            var wordApp = new Microsoft.Office.Interop.Word.Application();
            var wordDoc = wordApp.Documents.Open(item.PathToReport);
            wordApp.Visible = true;
        }

        private void ShowReport(object sender, RoutedEventArgs e)
        {
            if(RemarkReportManager.Instance.Current != null)
            {
                RemarkReportManager.Instance.Current.UnBlock();
            }
            
            var item = (sender as Control).DataContext as RemarkReport;            
            RemarkReportManager.Instance.Current = item;
            RemarkReportManager.Instance.Current.Block();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (RemarkReportManager.Instance.Current != null) {
                RemarkReportManager.Instance.Current.Save();
                RemarkReportManager.Instance.Current.UnBlock();
            }
            
            base.OnClosed(e);
        }

        private void RefreshReport(object sender, RoutedEventArgs e)
        {
            RemarkReportManager.Instance.RefreshReport();
        }

        private void PerformSearch(object sender, RoutedEventArgs e)
        {
            RemarkReportManager.Instance.Search();
        }
    }
}
