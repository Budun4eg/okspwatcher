﻿using OKSPWatcher.Core;
using System;
using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Логика взаимодействия для SelectReportWindow.xaml
    /// </summary>
    public partial class SelectReportWindow : BasicWindow
    {
        public SelectReportWindow()
        {
            InitializeComponent();
            RemarkManager.Instance.LoadAll();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        RemarkReportPack items;
        /// <summary>
        /// Список элементов
        /// </summary>
        public RemarkReportPack Items
        {
            get
            {
                if(items == null)
                {
                    items = new RemarkReportPack();
                    items.InsertFinished();
                    items.Sort();
                }
                return items;
            }
        }

        /// <summary>
        /// Выбранный отчет
        /// </summary>
        RemarkReport selected;

        private void Select(object sender, RoutedEventArgs e)
        {
            selected = (sender as Button).DataContext as RemarkReport;
            Close();
        }

        /// <summary>
        /// Получить отчет
        /// </summary>
        public static RemarkReport Get()
        {
            var window = new SelectReportWindow();
            window.ShowDialog();
            return window.selected;
        }
    }
}
