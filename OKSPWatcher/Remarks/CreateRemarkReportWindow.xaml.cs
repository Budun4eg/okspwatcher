﻿using System.ComponentModel;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows;
using OKSPWatcher.Core;
using System.Windows.Controls;
using OKSPWatcher.Secondary;
using OKSPWatcher.Rules;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Логика взаимодействия для CreateRemarkReportWindow.xaml
    /// </summary>
    public partial class CreateRemarkReportWindow : BasicWindow
    {
        public CreateRemarkReportWindow()
        {
            InitializeComponent();
            
            worker.RunWorkerCheck();
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if(state == States.Idle)
            {
                CreateRemarkManager.Instance.CheckInstance();
            }else if(state == States.Create)
            {
                CreateRemarkManager.Instance.Create();
            }else if(state == States.Save)
            {
                CreateRemarkManager.Instance.Save();
            }else if(state == States.Open)
            {
                CreateRemarkManager.Instance.OpenUnfinished(toOpen);
            }
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {            
            base.BackgroundCompleted(sender, e);
            if(state == States.Idle)
            {
                DataContext = CreateRemarkManager.Instance;
            }
        }

        /// <summary>
        /// Варианты состояния
        /// </summary>
        enum States { Idle, Create, Save, Open }

        /// <summary>
        /// Текущее состояние
        /// </summary>
        States state = States.Idle;

        private void CreateReport(object sender, RoutedEventArgs e)
        {
            state = States.Create;

            if(CreateRemarkManager.Instance.CurrentDep == null)
            {
                Output.Warning("Выберите отдел");
                return;
            }
            if(CreateRemarkManager.Instance.CurrentProduct == null)
            {
                Output.Warning("Выберите изделие");
                return;
            }

            worker.RunWorkerCheck();          
        }

        private void SaveReport(object sender, RoutedEventArgs e)
        {
            if(CreateRemarkManager.Instance.Current.LinkedUser != MainApp.Instance.CurrentUser)
            {
                if (RulesManager.Instance.Can("ForceRemarkReportSave"))
                {
                    RequestWindow.Show("Этот отчет принадлежит другому пользователю. Вы уверены что хотите сохранить его?");
                    if(RequestWindow.Result != RequestWindow.ResultType.Yes)
                    {
                        return;
                    }
                }
                else
                {
                    Output.TraceError("Сохранение отчета другого пользователя запрещено");
                    return;
                }
            }
            state = States.Save;

            worker.RunWorkerCheck();
        }

        /// <summary>
        /// Отчет для открытия
        /// </summary>
        RemarkReport toOpen;

        private void UnfinishedClicked(object sender, RoutedEventArgs e)
        {
            toOpen = (sender as Control).DataContext as RemarkReport;
            state = States.Open;

            worker.RunWorkerCheck();
        }

        private void RemoveUnfinished(object sender, RoutedEventArgs e)
        {
            var toDelete = (sender as Control).DataContext as RemarkReport;
            if(toDelete.LinkedUser == MainApp.Instance.CurrentUser)
            {
                RequestWindow.Show("Удалить незаконченный отчет?");
                if(RequestWindow.Result == RequestWindow.ResultType.Yes)
                {
                    CreateRemarkManager.Instance.Unfinished.Remove(toDelete);
                    if(CreateRemarkManager.Instance.Current == toDelete)
                    {
                        CreateRemarkManager.Instance.Current = null;
                    }
                    toDelete.Delete();                    
                }
            }
            else
            {
                Output.TraceError("Недостаточно прав для удаления");
            }
        }
    }
}
