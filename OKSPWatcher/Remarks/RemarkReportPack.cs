﻿using OKSPWatcher.Core;
using System.Collections.Generic;
using System.Linq;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Набор отчетов
    /// </summary>
    public class RemarkReportPack : ItemPack<RemarkReport>
    {
        /// <summary>
        /// Список всех отчетов
        /// </summary>
        List<RemarkReport> allItems = new List<RemarkReport>();

        /// <summary>
        /// Загрузить в набор невыполненные отчеты
        /// </summary>
        public void InsertUnfinished()
        {
            RemarkManager.Instance.LoadUnfinished();
            allItems.Clear();
            foreach(var item in RemarkManager.Instance.Items)
            {
                if(item.Date.Ticks == 0)
                {
                    allItems.Add(item);
                }
            }
            Search(null);
        }

        /// <summary>
        /// Загрузить отправленные отчеты
        /// </summary>
        public void InsertFinished()
        {
            RemarkManager.Instance.LoadUnfinished();
            allItems.Clear();
            foreach (var item in RemarkManager.Instance.Items)
            {
                if (item.Date.Ticks != 0)
                {
                    allItems.Add(item);
                }
            }
            Search(null);
        }

        /// <summary>
        /// Сортировать элементы по номеру
        /// </summary>
        public void Sort()
        {
            var sorted = allItems.OrderByDescending(o => o.Number).ToList();
            allItems.Clear();
            foreach(var item in sorted)
            {
                allItems.Add(item);
            }
            Search(null);
        }

        /// <summary>
        /// Осуществить поиск по набору
        /// </summary>
        public void Search(string toSearch)
        {
            Items.Clear();
            var chk = toSearch != null ? toSearch.ToUpper() : "";
            foreach (var item in allItems)
            {
                if (toSearch == null || toSearch == "")
                {
                    Items.Add(item);
                }
                else
                {
                    if (item.SearchString.Contains(chk))
                    {
                        Items.Add(item);
                        continue;
                    }
                }
            }
        }

        public override void Add(RemarkReport item)
        {
            base.Add(item);
            allItems.Add(item);
        }

        public override void Remove(RemarkReport item)
        {
            base.Remove(item);
            allItems.Remove(item);
        }

        public override void Insert(int position, RemarkReport item)
        {
            base.Insert(position, item);
            allItems.Insert(position, item);
        }
    }
}
