﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Rules;
using System;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Пункт отчета с замечаниями
    /// </summary>
    public class RemarkItem : DBObject
    {
        string dse;
        /// <summary>
        /// Наименование ДСЕ
        /// </summary>
        public string Dse
        {
            get
            {
                return dse;
            }
            set
            {
                if(dse != value)
                {
                    needUpdate = true;
                }
                dse = value;
                OnPropertyChanged("Dse");
            }
        }

        /// <summary>
        /// Наименование ДСЕ
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        string info;
        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string Info
        {
            get
            {
                return info;
            }
            set
            {
                if(info != value)
                {
                    needUpdate = true;
                }
                info = value;
                OnPropertyChanged("Info");
            }
        }

        /// <summary>
        /// Описание пункта
        /// </summary>
        public override string Description
        {
            set
            {
                if(description != value)
                {
                    needUpdate = true;
                }
                description = value;
                OnPropertyChanged("Description");
            }
        }

        string note;
        /// <summary>
        /// Примечание
        /// </summary>
        public string Note
        {
            get
            {
                return note;
            }
            set
            {
                if (!RulesManager.Instance.Can("ChangeRemarkItem"))
                {
                    Output.TraceError("Изменение заметки запрещено");
                    return;
                }
                if (!parent.Available())
                {
                    return;
                }
                if(note != value)
                {
                    needUpdate = true;
                }
                note = value;
                OnPropertyChanged("Note");
            }
        }

        string state;
        /// <summary>
        /// Состояние пункта
        /// </summary>
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                if (!RulesManager.Instance.Can("ChangeRemarkItem"))
                {
                    Output.TraceError("Изменение состояния запрещено");
                    return;
                }
                if (!parent.Available())
                {
                    return;
                }
                if (state != value)
                {
                    needUpdate = true;
                }
                state = value;
                OnPropertyChanged("State");
            }
        }

        /// <summary>
        /// Положение пункта
        /// </summary>
        public int Index
        {
            get
            {
                return parent.Items.IndexOf(this) + 1;
            }
        }

        RemarkReport parent;

        public RemarkItem(SQLDataRow data, RemarkReport _parent) : base()
        {
            parent = _parent;
            Load(data);
        }

        public RemarkItem(RemarkReport _parent) : base()
        {
            parent = _parent;
            needUpdate = true;
        }

        public RemarkItem() : base() { }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Dse = data.String("Dse");
                Name = data.String("Name");
                Info = data.String("Info");
                Description = data.String("Description");
                Note = data.String("Note");
                State = data.String("State");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                dse = data.String("Dse");
                name = data.String("Name");
                info = data.String("Info");
                description = data.String("Description");
                note = data.String("Note");
                state = data.String("State");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "RemarkItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO RemarkItems (Dse, Name, Info, Description, Note, State, UpdateDate) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6})", out id,
                dse.Check(), name.Check(), info.Check(), description.Check(), note.Check(), state.Check(), UpdateDate.Ticks);
            base.CreateNew();
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE RemarkItems SET Dse = '{0}', Name = '{1}', Info = '{2}', Description = '{3}', Note = '{4}', State = '{5}', UpdateDate = {6} WHERE Id = {7}",
                dse.Check(), name.Check(), info.Check(), description.Check(), note.Check(), state.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM RemarkItems WHERE Id = {0}", id);
        }
    }
}
