﻿using System;
using OKSPWatcher.Block;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Логика взаимодействия для RemarkReportSettingsWindow.xaml
    /// </summary>
    public partial class RemarkReportSettingsWindow : BasicWindow
    {
        public RemarkReportSettingsWindow()
        {
            InitializeComponent();
            RemarkManager.Instance.CheckInstance();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));        
        }

        /// <summary>
        /// Путь до папки с отчетами
        /// </summary>
        public string PathToReports
        {
            get
            {
                return RemarkManager.Instance.PathToReports;
            }
            set
            {
                RemarkManager.Instance.PathToReports = value;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_RemarkSettings");
        }
    }
}
