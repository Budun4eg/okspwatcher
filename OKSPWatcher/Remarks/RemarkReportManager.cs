﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Office.Interop.Word;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Secondary;
using System;
using System.IO;
using System.Linq;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Менеджер отчета по замечаниям
    /// </summary>
    public class RemarkReportManager : NotifyObject
    {
        static RemarkReportManager instance;

        protected RemarkReportManager()
        {
            RemarkManager.Instance.LoadAll();
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static RemarkReportManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new RemarkReportManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Проверить наличие экземпляра
        /// </summary>
        public void CheckInstance() {
            Search();
        }

        RemarkReportPack items;
        /// <summary>
        /// Отчеты
        /// </summary>
        public RemarkReportPack Items
        {
            get
            {
                if(items == null)
                {
                    items = new RemarkReportPack();
                    items.InsertFinished();
                    items.Sort();
                }
                
                return items;
            }
        }

        RemarkReport current;
        /// <summary>
        /// Текущий отчет
        /// </summary>
        public RemarkReport Current
        {
            get
            {
                return current;
            }
            set
            {
                if(current != value && current != null)
                {
                    current.Save();
                }
                current = value;
                OnPropertyChanged("Current");
            }
        }

        string toSearch = "";
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string ToSearch
        {
            get
            {
                return toSearch;
            }
            set
            {
                toSearch = value;
                OnPropertyChanged("ToSearch");
            }
        }

        /// <summary>
        /// Синхронизировать набор
        /// </summary>
        public void Search()
        {
            Items.Search(ToSearch);
        }

        /// <summary>
        /// Обновить текущий отчет
        /// </summary>
        public void RefreshReport()
        {
            if (Current == null) return;
            if (!File.Exists(Current.PathToReport))
            {
                Output.WarningFormat("Файл отчета отсутствует {0}", Current.PathToReport);
                return;
            }

            if (!Current.Available())
            {
                return;
            }

            RequestWindow.Show("Обновить данные из отчета? Состояние пунктов и заметки сбросятся до начального состояния.");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                bool completed = true;

                try
                {
                    var wordApp = new Application();
                    var wordDoc = wordApp.Documents.Open(Current.PathToReport);

                    while (Current.Items.Count > 0)
                    {
                        Current.Items.Items[0].Delete();
                        Current.Items.Items.RemoveAt(0);
                    }

                    int pageCount = wordDoc.ComputeStatistics(WdStatistic.wdStatisticPages);
                    foreach (Microsoft.Office.Interop.Word.List item in wordDoc.Lists)
                    {
                        foreach (Microsoft.Office.Interop.Word.Paragraph para in item.ListParagraphs)
                        {
                            string text = para.Range.Text.Trim();
                            if (text != "")
                            {
                                text = text.TrimEnd(new char[] { '.' });
                                int leftBracket = text.IndexOf('«');
                                if (leftBracket < 0)
                                {
                                    Output.ErrorFormat("Отсутствует знак '«' в строке {0}", text);
                                    return;
                                }
                                int rightBracket = text.IndexOf('»');
                                if (rightBracket < 0)
                                {
                                    Output.ErrorFormat("Отсутствует знак '»' в строке {0}", text);
                                    return;
                                }

                                try
                                {
                                    string dse = text.Substring(0, leftBracket).Trim();
                                    text = text.Substring(leftBracket + 1);
                                    string name = text.Substring(0, rightBracket - 1 - dse.Length).Trim().TrimStart(new char[] { '«' }).TrimEnd(new char[] { '»' });
                                    if (text.Length >= rightBracket - dse.Length && !char.IsLetterOrDigit(text[rightBracket - dse.Length]))
                                    {
                                        text.Remove(rightBracket - dse.Length);
                                    }
                                    string info = text.Length <= rightBracket - dse.Length ? "-" : text.Substring(rightBracket - dse.Length).Trim();
                                    var note = new RemarkItem(Current);
                                    note.Dse = dse;
                                    note.Info = info;
                                    note.Name = name;
                                    note.State = "в работе";
                                    note.Description = para.Next().Range.Text;
                                    note.Save();
                                    Current.Items.Insert(0, note);
                                }
                                catch (Exception ex)
                                {
                                    Output.Error(ex, "Ошибка при обработке пункта отчета {0}", text);
                                    completed = false;
                                }

                            }
                        }
                    }
                    Current.Save();

                    wordDoc.Close();
                    wordApp.Quit();

                    try
                    {
                        System.Collections.Generic.List<string> tags = new System.Collections.Generic.List<string>() {
                                "PageCount",
                                "DseGroups"
                            };

                        using (WordprocessingDocument doc = WordprocessingDocument.Open(Current.PathToReport, true))
                        {
                            MainDocumentPart mainPart = doc.MainDocumentPart;

                            foreach (string tag in tags)
                            {
                                var found = mainPart.Document.Body.Descendants<SdtRun>().Where(r => r.SdtProperties.GetFirstChild<Tag>().Val == tag);
                                foreach (SdtRun run in found)
                                {
                                    var content = run.GetFirstChild<SdtContentRun>();
                                    var text = content.GetFirstChild<Run>().GetFirstChild<Text>();
                                    string replace = "";
                                    switch (tag)
                                    {
                                        case "PageCount":
                                            replace = (pageCount - 1).ToString();
                                            break;
                                        case "DseGroups":
                                            replace = Current.NoteGroups;
                                            break;
                                    }
                                    text.Text = replace;
                                }
                            }

                            mainPart.Document.Save();
                        }
                    }
                    catch (Exception ex)
                    {
                        Output.WriteError(ex);
                        completed = false;
                    }
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                    completed = false;
                }

                if (completed)
                {
                    Output.Log("Обновление данных из отчета завершено");
                }
            }
        }
    }
}
