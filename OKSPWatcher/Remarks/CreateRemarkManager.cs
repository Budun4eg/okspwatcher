﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Office.Interop.Word;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Менеджер создания отчетов по замечаниям
    /// </summary>
    public class CreateRemarkManager : NotifyObject
    {
        /// <summary>
        /// Проверить существование экземпляра
        /// </summary>
        public void CheckInstance() { }

        static CreateRemarkManager instance;

        protected CreateRemarkManager()
        {
            Deps.LoadAll();
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static CreateRemarkManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if (instance == null) {
                            instance = new CreateRemarkManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        DepPack deps;
        /// <summary>
        /// Набор отделов
        /// </summary>
        public DepPack Deps
        {
            get
            {
                if(deps == null)
                {
                    deps = new DepPack();
                }
                return deps;
            }
        }

        Dep currentDep;
        /// <summary>
        /// Выбранный отдел
        /// </summary>
        public Dep CurrentDep
        {
            get
            {
                return currentDep;
            }
            set
            {
                currentDep = value;
                Products.LoadDep(currentDep);
                OnPropertyChanged("CurrentDep");
            }
        }

        ProductPack products;
        /// <summary>
        /// Список изделий
        /// </summary>
        public ProductPack Products
        {
            get
            {
                if(products == null)
                {
                    products = new ProductPack();
                }
                return products;
            }
        }

        Product currentProduct;
        /// <summary>
        /// Выбранное изделие
        /// </summary>
        public Product CurrentProduct
        {
            get
            {
                return currentProduct;
            }
            set
            {
                currentProduct = value;
                OnPropertyChanged("CurrentProduct");
            }
        }

        /// <summary>
        /// Сохранение отчета включено?
        /// </summary>
        public bool SaveEnabled
        {
            get
            {
                return current != null;
            }
        }

        RemarkReportPack unfinished;
        /// <summary>
        /// Незаконченные отчеты
        /// </summary>
        public RemarkReportPack Unfinished
        {
            get
            {
                if(unfinished == null)
                {
                    unfinished = new RemarkReportPack();
                    unfinished.InsertUnfinished();
                }                
                return unfinished;
            }
        }

        RemarkReport current;
        /// <summary>
        /// Текущий отчет
        /// </summary>
        public RemarkReport Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
                OnPropertyChanged("Current");
            }
        }

        /// <summary>
        /// Приложение Word
        /// </summary>
        Application word;

        /// <summary>
        /// Документ Word
        /// </summary>
        Microsoft.Office.Interop.Word.Document doc;

        /// <summary>
        /// Открыть незавершенный отчет
        /// </summary>
        public void OpenUnfinished(RemarkReport item)
        {
            if (!File.Exists(item.PathToReport))
            {
                Output.WarningFormat("Отсутствует файл отчета {0}", item.PathToReport);
                return;
            }
            Current = item;
            word = new Application();
            doc = word.Documents.Open(Current.PathToReport);

            word.Visible = true;
        } 

        /// <summary>
        /// Создать новый отчет
        /// </summary>
        public void Create()
        {
            object template = string.Format("{0}RemarkReport.dotx", AppSettings.TemplatesFolder);     
            
            word = new Application();
            doc = word.Documents.Add(template);

            Current = new RemarkReport();
            Current.LinkedDep = CurrentDep;
            Current.LinkedProduct = CurrentProduct;
            Current.LinkedUser = MainApp.Instance.CurrentUser;
            Current.PathToReport = string.Format("{0}{1}.docx", AppSettings.TempFolder, Guid.NewGuid());
            Current.Save();
            Unfinished.Add(Current);

            doc.SaveAs2(Current.PathToReport);
            try
            {
                doc.Close();
                word.Quit();
            }
            catch { }

            List<string> tags = new List<string>() {
                "Creator",
                "ProductName",
                "DepartmentId",
                "DepartmentBoss"
            };

            using (WordprocessingDocument doc = WordprocessingDocument.Open(current.PathToReport, true))
            {
                MainDocumentPart mainPart = doc.MainDocumentPart;

                foreach (string tag in tags)
                {
                    var found = mainPart.Document.Body.Descendants<SdtRun>().Where(r => r.SdtProperties.GetFirstChild<Tag>().Val == tag);
                    foreach (SdtRun run in found)
                    {
                        var content = run.GetFirstChild<SdtContentRun>();
                        var text = content.GetFirstChild<Run>().GetFirstChild<Text>();
                        string replace = "";
                        switch (tag)
                        {
                            case "ProductName":
                                replace = CurrentProduct.Name;
                                break;
                            case "Creator":
                                replace = MainApp.Instance.CurrentUser.ShortName;
                                break;
                            case "DepartmentId":
                                replace = CurrentDep.Id.ToString();
                                break;
                            case "DepartmentBoss":
                                replace = CurrentDep.Boss != null ? CurrentDep.Boss.ShortNameDative : "";
                                break;
                        }
                        text.Text = replace;
                    }
                }

                mainPart.Document.Save();
            }

            word = new Application();
            doc = word.Documents.Open(Current.PathToReport);
            word.Visible = true;
        }

        /// <summary>
        /// Сохранить отчет
        /// </summary>
        public void Save()
        {
            if (!File.Exists(Current.PathToReport))
            {
                Output.WarningFormat("Отсутствует файл отчета {0}", Current.PathToReport);
                return;
            }
            var last = SQL.Get("RemarkReports").ExecuteScalar("SELECT MAX(Number) FROM RemarkReports");
            long nextNumber = (long)last + 1;
            Current.Number = nextNumber;
            Current.Date = DateTime.Now;

            try
            {
                doc.Close();
                word.Quit();
            }
            catch
            { }

            int pageCount = 0;

            try
            {
                word = new Application();
                doc = word.Documents.Open(Current.PathToReport);
                pageCount = doc.ComputeStatistics(WdStatistic.wdStatisticPages);
                foreach (List item in doc.Lists)
                {
                    foreach (Microsoft.Office.Interop.Word.Paragraph para in item.ListParagraphs)
                    {
                        string text = para.Range.Text.Trim();
                        if (text != "")
                        {
                            int leftBracket = text.IndexOf('«');
                            if (leftBracket < 0)
                            {
                                Output.ErrorFormat("Отсутствует знак '«' в строке {0}", text);
                                return;
                            }
                            int rightBracket = text.IndexOf('»');
                            if (rightBracket < 0)
                            {
                                Output.ErrorFormat("Отсутствует знак '»' в строке {0}", text);
                                return;
                            }

                            try
                            {
                                string dse = text.Substring(0, leftBracket).Trim();
                                text = text.Substring(leftBracket + 1);
                                string name = text.Substring(0, rightBracket - 1 - dse.Length).Trim().TrimStart(new char[] { '«' }).TrimEnd(new char[] { '»' });
                                if (text.Length >= rightBracket - dse.Length && !char.IsLetterOrDigit(text[rightBracket - dse.Length]))
                                {
                                    text.Remove(rightBracket - dse.Length);
                                }
                                string info = text.Length <= rightBracket - dse.Length ? "-" : text.Substring(rightBracket - dse.Length).Trim();
                                var note = new RemarkItem(Current);
                                note.Dse = dse;
                                note.Info = info;
                                note.Name = name;
                                note.State = "в работе";
                                note.Description = para.Next().Range.Text;
                                note.Save();
                                Current.Items.Insert(0, note);
                            }
                            catch (Exception ex)
                            {
                                Output.Error(ex, "Ошибка при разборе строки {0}", text);
                            }

                        }
                    }
                }
                doc.Close();
                word.Quit();
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }

            try
            {
                List<string> tags = new List<string>() {
                "OutNumber",
                "OutDate",
                "PageCount",
                "DseGroups"
                };

                using (WordprocessingDocument doc = WordprocessingDocument.Open(Current.PathToReport, true))
                {
                    MainDocumentPart mainPart = doc.MainDocumentPart;

                    foreach (string tag in tags)
                    {
                        var found = mainPart.Document.Body.Descendants<SdtRun>().Where(r => r.SdtProperties.GetFirstChild<Tag>().Val == tag);
                        foreach (SdtRun run in found)
                        {
                            var content = run.GetFirstChild<SdtContentRun>();
                            var text = content.GetFirstChild<Run>().GetFirstChild<Text>();
                            string replace = "";
                            switch (tag)
                            {
                                case "OutNumber":
                                    replace = Current.Number.ToString();
                                    break;
                                case "OutDate":
                                    replace = Current.Date.ToShortDateString();
                                    break;
                                case "PageCount":
                                    replace = (pageCount - 1).ToString();
                                    break;
                                case "DseGroups":
                                    replace = Current.NoteGroups;
                                    break;
                            }
                            text.Text = replace;
                        }
                        foreach (HeaderPart part in mainPart.HeaderParts)
                        {
                            found = part.Header.Descendants<SdtRun>().Where(r => r.SdtProperties.GetFirstChild<Tag>().Val == tag);
                            foreach (SdtRun run in found)
                            {
                                var content = run.GetFirstChild<SdtContentRun>();
                                var text = content.GetFirstChild<Run>().GetFirstChild<Text>();
                                string replace = "";
                                switch (tag)
                                {
                                    case "OutNumber":
                                        replace = Current.Number.ToString();
                                        break;
                                    case "OutDate":
                                        replace = Current.Date.ToShortDateString();
                                        break;
                                }
                                text.Text = replace;
                            }
                        }
                    }

                    mainPart.Document.Save();
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }

            string toDelete = Current.PathToReport;

            try
            {
                word = new Application();
                doc = word.Documents.Open(Current.PathToReport);

                string pathToDoc = string.Format("{0}\\Замечания {1} (Исх. 056-3D_{2}).docx", RemarkManager.Instance.PathToReports, Current.LinkedProduct.Name, Current.Number);
                doc.SaveAs2(pathToDoc);
                doc = word.Documents.Open(pathToDoc);
                word.Visible = true;
                File.Delete(toDelete);
                Current.PathToReport = pathToDoc;
                Current.Save();
                Unfinished.Remove(Current);
                RemarkReportManager.Instance.Items.Add(Current);
                RemarkReportManager.Instance.Search();
                Current = null;
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при сохранении отчета");                
            }
        }
    }
}
