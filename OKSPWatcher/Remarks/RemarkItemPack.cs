﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Набор пунктов замечания
    /// </summary>
    public class RemarkItemPack : ItemPack<RemarkItem>
    {
        RemarkReport parent;

        public RemarkItemPack(RemarkReport _parent) : base()
        {
            parent = _parent;
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var data = SQL.Get("RemarkItems").ExecuteRead("SELECT * FROM RemarkItems WHERE Id = {0} LIMIT 1", id);
                Add(new RemarkItem(data.Single, parent));
            }
        }
    }
}
