﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Remarks
{
    /// <summary>
    /// Менеджер замечаний
    /// </summary>
    public class RemarkManager : ManagerObject<RemarkReport>
    {
        static RemarkManager instance;

        protected RemarkManager() {
            pathToReports = AppSettings.GetString("PathToReports");
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static RemarkManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new RemarkManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("RemarkItems").Execute(@"DROP TABLE RemarkItems");
            SQL.Get("RemarkReports").Execute(@"DROP TABLE RemarkReports");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("RemarkItems").Execute(@"CREATE TABLE IF NOT EXISTS RemarkItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Dse TEXT,
                                                    Name TEXT,
                                                    Info TEXT,
                                                    Description TEXT,
                                                    Note TEXT,
                                                    State TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("RemarkReports").Execute(@"CREATE TABLE IF NOT EXISTS RemarkReports (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Dep INTEGER,
                                                    Product INTEGER,
                                                    Date INTEGER,
                                                    Number INTEGER,
                                                    User INTEGER,
                                                    Items TEXT,
                                                    PathToReport TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        public override void LoadAll()
        {
            var data = SQL.Get("RemarkReports").ExecuteRead("SELECT * FROM RemarkReports ORDER BY Number");
            foreach(SQLDataRow row in data)
            {
                Add(new RemarkReport(row));
            }
        }

        /// <summary>
        /// Получить отчет по идентификатору
        /// </summary>
        public override RemarkReport ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM RemarkReports WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("RemarkReports").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new RemarkReport(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Загрузить незаполненные отчеты
        /// </summary>
        public void LoadUnfinished()
        {
            string request = string.Format("SELECT * FROM RemarkReports WHERE Date = 0 ORDER BY Number");
            var data = SQL.Get("RemarkReports").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new RemarkReport(row));
            }
        }

        string pathToReports;
        /// <summary>
        /// Путь до папки с отчетами
        /// </summary>
        public string PathToReports
        {
            get
            {
                return pathToReports;
            }
            set
            {
                if(pathToReports != value)
                {
                    AppSettings.SetString("PathToReports", value);
                    pathToReports = value;
                    OnPropertyChanged("PathToReports");
                }
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
