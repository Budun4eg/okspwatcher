﻿using OKSPWatcher.Core;

namespace OKSPWatcher.SystemEditors
{
    /// <summary>
    /// Объект редактора набора
    /// </summary>
    public class PackEditorItem<T, K> : NotifyObject where T : ItemPack<K> where K : DBObject, new()
    {
        /// <summary>
        /// Набор элементов
        /// </summary>
        T pack;

        /// <summary>
        /// Элемент для проверки
        /// </summary>
        K obj;

        /// <summary>
        /// Элемент включен?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return pack.Contains(obj);
            }
            set
            {
                if (value)
                {
                    pack.Add(obj);
                }
                else
                {
                    pack.Remove(obj);
                }
                OnPropertyChanged("Enabled");
            }
        }

        public override string Name
        {
            get
            {
                return obj.Name;
            }
        }

        public PackEditorItem(T _pack, K _obj)
        {
            pack = _pack;
            obj = _obj;
        }
    }
}
