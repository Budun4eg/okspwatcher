﻿using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using OKSPWatcher.SystemEditors.Editors;

namespace OKSPWatcher.SystemEditors.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductPackEditorWindow.xaml
    /// </summary>
    public partial class ProductPackEditorWindow : ProductPackEditor
    {
        public ProductPackEditorWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Показать окно редактирования набора изделий
        /// </summary>
        public static void Show(ProductPack pack)
        {            
            var window = new ProductPackEditorWindow();
            window.LoadAll(ProductManager.Instance, pack);
            window.ShowDialog();
        }

        public static void ShowDep(ProductPack pack, Dep dep)
        {
            var window = new ProductPackEditorWindow();
            window.LoadDep(pack, dep);
            window.ShowDialog();
        }
    }
}
