﻿using OKSPWatcher.SystemEditors.Editors;
using OKSPWatcher.Users;

namespace OKSPWatcher.SystemEditors.Windows
{
    /// <summary>
    /// Логика взаимодействия для DepPackEditorWindow.xaml
    /// </summary>
    public partial class UserPackEditorWindow : UserPackEditor
    {
        public UserPackEditorWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Показать окно редактирования набора отделов
        /// </summary>
        public static void Show(UserPack pack)
        {
            var window = new UserPackEditorWindow();
            window.LoadAll(UserManager.Instance, pack);
            window.ShowDialog();
        }
    }
}
