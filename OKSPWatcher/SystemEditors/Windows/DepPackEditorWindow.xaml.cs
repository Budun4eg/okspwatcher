﻿using OKSPWatcher.Deps;
using OKSPWatcher.SystemEditors.Editors;

namespace OKSPWatcher.SystemEditors.Windows
{
    /// <summary>
    /// Логика взаимодействия для DepPackEditorWindow.xaml
    /// </summary>
    public partial class DepPackEditorWindow : DepPackEditor
    {
        public DepPackEditorWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Показать окно редактирования набора отделов
        /// </summary>
        public static void Show(DepPack pack)
        {
            var window = new DepPackEditorWindow();
            window.LoadAll(DepManager.Instance, pack);
            window.ShowDialog();
        }
    }
}
