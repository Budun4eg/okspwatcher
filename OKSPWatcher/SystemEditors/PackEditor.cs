﻿using OKSPWatcher.Core;
using System;
using System.Windows;

namespace OKSPWatcher.SystemEditors
{
    /// <summary>
    /// Редактор набора элементов
    /// </summary>
    public class PackEditor<C,T,K> : BasicWindow where T : ItemPack<K> where K : DBObject, new() where C : ManagerObject<K>
    {        
        /// <summary>
        /// Объект для редактирования
        /// </summary>
        protected T edit;

        protected SafeObservableCollection<PackEditorItem<T, K>> items = new SafeObservableCollection<PackEditorItem<T, K>>();
        /// <summary>
        /// Элементы для редактирования
        /// </summary>
        public SafeObservableCollection<PackEditorItem<T,K>> Items
        {
            get
            {
                return items;
            }
        }

        /// <summary>
        /// Загрузить в менеджер все объекты
        /// </summary>
        public virtual void LoadAll(C manager, T _edit)
        {
            manager.LoadAll();
            edit = _edit;            
            foreach(var item in manager.Items)
            {
                items.Add(new PackEditorItem<T, K>(edit, item));
            }
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }
    }
}
