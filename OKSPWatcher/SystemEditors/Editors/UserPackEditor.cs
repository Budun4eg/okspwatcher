﻿using OKSPWatcher.Users;

namespace OKSPWatcher.SystemEditors.Editors
{
    /// <summary>
    /// Редактор набора пользователя
    /// </summary>
    public class UserPackEditor : PackEditor<UserManager, UserPack, User>
    {
    }
}
