﻿using OKSPWatcher.Deps;

namespace OKSPWatcher.SystemEditors.Editors
{
    /// <summary>
    /// Редактор набора отделов
    /// </summary>
    public class DepPackEditor : PackEditor<DepManager, DepPack, Dep>
    {

    }
}
