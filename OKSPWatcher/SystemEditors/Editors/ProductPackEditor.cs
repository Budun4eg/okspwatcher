﻿using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using System;
using System.Windows;

namespace OKSPWatcher.SystemEditors.Editors
{
    /// <summary>
    /// Редактор наборов изделий
    /// </summary>
    public class ProductPackEditor : PackEditor<ProductManager, ProductPack, Product>
    {
        /// <summary>
        /// Загрузить в менеджер объекты определенного отдела
        /// </summary>
        public void LoadDep(ProductPack _edit, Dep dep)
        {
            ProductManager.Instance.LoadDep(dep);
            edit = _edit;
            ProductManager.Instance.Sort();
            foreach (var item in ProductManager.Instance.Items)
            {
                if(item.LinkedDep == dep)
                {
                    items.Add(new PackEditorItem<ProductPack, Product>(edit, item));
                }                
            }
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }
    }
}
