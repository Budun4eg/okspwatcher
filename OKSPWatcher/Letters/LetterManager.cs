﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using System.Collections.Generic;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Менеджер писем
    /// </summary>
    public class LetterManager : ManagerObject<Letter>
    {
        static LetterManager instance;

        protected LetterManager()
        {

        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static LetterManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new LetterManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Letters").Execute(@"DROP TABLE Letters");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Letters").Execute(@"CREATE TABLE IF NOT EXISTS Letters (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Org INTEGER,
                                                    Dep INTEGER,
                                                    OutNumber TEXT,
                                                    OutDate INTEGER,
                                                    OkspNumber TEXT,
                                                    OkspDate INTEGER,
                                                    User INTEGER,
                                                    Description TEXT,
                                                    Attached INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("Letters").Execute(@"CREATE INDEX IF NOT EXISTS Letters_OutNumber ON Letters(OutNumber)");
            SQL.Get("Letters").Execute(@"CREATE INDEX IF NOT EXISTS Letters_Dep ON Letters(Dep)");
            SQL.Get("Letters").Execute(@"CREATE INDEX IF NOT EXISTS Letters_Org ON Letters(Org)");
            instance = null;
        }

        public override Letter ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM Letters WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("Letters").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new Letter(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        /// <summary>
        /// Список писем по отделу
        /// </summary>
        Dictionary<Dep, LetterPack> byDep = new Dictionary<Dep, LetterPack>();

        /// <summary>
        /// Загрузить все письма определенного отдела
        /// </summary>
        public void LoadDep(Dep dep)
        {
            if(dep == null)
            {
                return;
            }
            string request = string.Format("SELECT * FROM Letters WHERE Dep = {0} ORDER BY OutNumber", dep.Id);
            var data = SQL.Get("Letters").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new Letter(row));
            }
        }

        /// <summary>
        /// Загрузить непривязанные письма определенного отдела
        /// </summary>
        public void LoadNotAttachedDep(Dep dep)
        {
            if (dep == null)
            {
                return;
            }
            string request = string.Format("SELECT * FROM Letters WHERE Dep = {0} AND Attached = 0 ORDER BY OutNumber", dep.Id);
            var data = SQL.Get("Letters").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new Letter(row));
            }
        }

        /// <summary>
        /// Добавить письмо
        /// </summary>
        public override bool Add(Letter item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;

                if (item.LinkedDep != null)
                {
                    if (byDep.ContainsKey(item.LinkedDep))
                    {
                        byDep[item.LinkedDep].Add(item);
                    }
                    else
                    {
                        byDep.Add(item.LinkedDep, new LetterPack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить письмо
        /// </summary>
        public override void Remove(Letter item)
        {
            lock (changeLock)
            {
                base.Remove(item);

                if (item.LinkedDep != null && byDep.ContainsKey(item.LinkedDep) && byDep[item.LinkedDep].Contains(item))
                {
                    byDep[item.LinkedDep].Remove(item);
                    if (byDep[item.LinkedDep].Count == 0)
                    {
                        byDep.Remove(item.LinkedDep);
                    }
                }
            }            
        }
    }
}
