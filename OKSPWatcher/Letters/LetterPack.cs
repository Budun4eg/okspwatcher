﻿using OKSPWatcher.Core;
using OKSPWatcher.Deps;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Набор писем
    /// </summary>
    public class LetterPack : ItemPack<Letter>
    {
        /// <summary>
        /// Загрузить набор писем
        /// </summary>
        public void Load(Dep dep, bool notAttached)
        {
            if (notAttached)
            {
                LetterManager.Instance.LoadNotAttachedDep(dep);
            }
            else
            {
                LetterManager.Instance.LoadDep(dep);
            }
            items.Clear();
            foreach(var item in LetterManager.Instance.Items)
            {
                if(item.LinkedDep == dep)
                {
                    if(!notAttached || item.Attached == 0)
                    {
                        items.Add(item);
                    }
                }
            }
        }
    }
}
