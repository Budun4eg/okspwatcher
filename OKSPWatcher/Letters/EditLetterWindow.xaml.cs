﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Windows;
using System.ComponentModel;
using OKSPWatcher.Orgs;
using OKSPWatcher.Users;
using OKSPWatcher.Secondary;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Логика взаимодействия для AddLetterWindow.xaml
    /// </summary>
    public partial class EditLetterWindow : BasicWindow
    {        
        Letter linkedLetter;
        /// <summary>
        /// Письмо для редактирования
        /// </summary>
        public Letter LinkedLetter
        {
            get
            {
                return linkedLetter;
            }
        }

        public EditLetterWindow()
        {
            InitializeComponent();            
        }

        OrgPack availableOrgs;
        /// <summary>
        /// Доступные организации
        /// </summary>
        public OrgPack AvailableOrgs
        {
            get
            {
                return availableOrgs;
            }
        }

        UserPack availableUsers;
        /// <summary>
        /// Доступные пользователи
        /// </summary>
        public UserPack AvailableUsers
        {
            get
            {
                return availableUsers;
            }
        }

        /// <summary>
        /// Загрузить письмо
        /// </summary>
        public void Load(Letter letter)
        {
            if(letter == null)
            {
                letter = new Letter();
                letter.LinkedUser = MainApp.Instance.CurrentUser;
                letter.OutDate = DateTime.Now;
                letter.OkspDate = DateTime.Now;
                letter.LinkedDep = MainApp.Instance.CurrentUser.LinkedDep;
            }
            else
            {
                letter.CheckUpdate();
            }

            linkedLetter = letter;

            availableOrgs = new OrgPack();
            availableOrgs.LoadAll();

            availableUsers = UserManager.Instance.ByDep(MainApp.Instance.CurrentUser.LinkedDep, true);

            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        /// <summary>
        /// Открыть письмо на редактирование
        /// </summary>
        public static Letter Edit(Letter letter = null)
        {
            var window = new EditLetterWindow();
            window.Load(letter);
            
            window.ShowDialog();
            return window.linkedLetter;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            var chk = Save();
            if (chk)
            {
                Close();
            }            
        }

        bool Save()
        {
            if (linkedLetter == null) return false;
            if (linkedLetter.OutNumber == null || linkedLetter.OutNumber.Length == 0)
            {
                Output.Warning("Необходимо ввести номер письма");
                return false;
            }
            var check = SQL.Get("Letters").ExecuteScalar("SELECT Id FROM Letters WHERE OutNumber = '{0}' AND Dep = {1} AND Org = {2}", linkedLetter.OutNumber,
                linkedLetter.LinkedDep != null ? linkedLetter.LinkedDep.Id : -1, linkedLetter.LinkedOrg != null ? linkedLetter.LinkedOrg.Id : -1);
            if (check != null)
            {
                var id = (long)check;
                if (linkedLetter.Id != id)
                {
                    Output.WarningFormat("Письмо с указанным номером уже существует ({0})", linkedLetter.OutNumber);
                    return false;
                }
            }
            linkedLetter.Save();
            return true;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            var chk = Save();
            if (!chk)
            {
                RequestWindow.Show("Письмо не сохранено из-за ошибок. Выйти?");
                if(RequestWindow.Result != RequestWindow.ResultType.Yes)
                {
                    e.Cancel = true;
                    return;
                }
            }
            base.OnClosing(e);
        }
    }
}
