﻿using OKSPWatcher.Core;
using OKSPWatcher.Rules;
using OKSPWatcher.Secondary;
using System;
using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Логика взаимодействия для ChooseLetterWindow.xaml
    /// </summary>
    public partial class ChooseLetterWindow : BasicWindow
    {
        public ChooseLetterWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Выбранное письмо
        /// </summary>
        Letter chosen;

        LetterPack pack;
        /// <summary>
        /// Набор писем
        /// </summary>
        public LetterPack Pack
        {
            get
            {
                if(pack == null)
                {
                    pack = new LetterPack();
                }
                return pack;
            }
        }

        /// <summary>
        /// Получить письмо
        /// </summary>
        public static Letter Get()
        {
            var window = new ChooseLetterWindow();
            window.LoadLetters();
            window.ShowDialog();
            return window.chosen;
        }

        /// <summary>
        /// Загрузить письма
        /// </summary>
        void LoadLetters()
        {
            LetterManager.Instance.CheckUpdate();
            Pack.Load(MainApp.Instance.CurrentUser.LinkedDep, onlyFree);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        bool onlyFree = true;
        /// <summary>
        /// Показать все
        /// </summary>
        public bool OnlyFree
        {
            get
            {
                return onlyFree;
            }
            set
            {
                onlyFree = value;
                LoadLetters();
                OnPropertyChanged("OnlyFree");
            }
        }

        private void NewLetter(object sender, RoutedEventArgs e)
        {
            chosen = EditLetterWindow.Edit();
            if(chosen.Id >= 0)
            {
                Close();
            }            
        }

        private void ChooseLetter(object sender, RoutedEventArgs e)
        {
            chosen = (sender as Button).DataContext as Letter;
            Close();
        }

        private void RemoveAttaches(object sender, RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("LetterRemoveAttaches"))
            {
                Output.Warning("Обнуление привязок запрещено");
                return;
            }
            var letter = (sender as Control).DataContext as Letter;
            letter.Attached = 0;
            letter.Save();
        }

        private void RemoveLetter(object sender, RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("LettersRemove"))
            {
                Output.Warning("Удаление свободных писем запрещено");
                return;
            }
            var item = (sender as Control).DataContext as Letter;
            if(item.Attached > 0)
            {
                Output.Warning("Удаление привязанных писем запрещено");
                return;
            }
            RequestWindow.Show("Удалить письмо?");
            if (RequestWindow.Result == RequestWindow.ResultType.Yes)
            {                
                item.Delete();
                Pack.Load(MainApp.Instance.CurrentUser.LinkedDep, onlyFree);
            }
        }
    }
}
