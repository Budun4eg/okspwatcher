﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Orgs;
using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Письмо
    /// </summary>
    public class Letter : DBObject
    {
        public override string Name
        {
            get
            {
                return string.Format("{0} от {1}{2}[{3} от {4}]", OutNumber, OutDate.ToShortDateString(), Environment.NewLine, OkspNumber, OkspDate.ToShortDateString());
            }
        }

        long orgId = -1;
        /// <summary>
        /// Привязанная организация
        /// </summary>
        public Org LinkedOrg
        {
            get
            {
                return OrgManager.Instance.ById(orgId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(orgId != toSet)
                {
                    needUpdate = true;
                }
                orgId = toSet;
                OnPropertyChanged("LinkedOrg");
            }
        }

        long depId = -1;
        /// <summary>
        /// Привязанный отдел
        /// </summary>
        public Dep LinkedDep
        {
            get
            {
                return DepManager.Instance.ById(depId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(depId != toSet)
                {
                    needUpdate = true;
                }
                depId = toSet;
                OnPropertyChanged("LinkedDep");
            }
        }

        string outNumber = "";
        /// <summary>
        /// Номер письма
        /// </summary>
        public string OutNumber
        {
            get
            {
                return outNumber;
            }
            set
            {
                if(outNumber != value)
                {
                    needUpdate = true;
                }
                outNumber = value;
                OnPropertyChanged("OutNumber");
                OnPropertyChanged("Name");
            }
        }

        long outDate;
        /// <summary>
        /// Дата письма
        /// </summary>
        public DateTime OutDate
        {
            get
            {
                return new DateTime(outDate);
            }
            set
            {
                if(outDate != value.Ticks)
                {
                    needUpdate = true;
                }
                outDate = value.Ticks;
                OnPropertyChanged("OutDate");
                OnPropertyChanged("Name");
            }
        }

        string okspNumber = "";
        /// <summary>
        /// Номер в ОКСП
        /// </summary>
        public string OkspNumber
        {
            get
            {
                return okspNumber;
            }
            set
            {
                if(okspNumber != value)
                {
                    needUpdate = true;
                }
                okspNumber = value;
                OnPropertyChanged("OkspNumber");
                OnPropertyChanged("Name");
            }
        }

        long okspDate;
        /// <summary>
        /// Конвертированная дата письма
        /// </summary>
        public DateTime OkspDate
        {
            get
            {
                return new DateTime(okspDate);
            }
            set
            {
                if(okspDate != value.Ticks)
                {
                    needUpdate = true;
                }
                okspDate = value.Ticks;
                OnPropertyChanged("OkspDate");
                OnPropertyChanged("Name");
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный исполнитель письма
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(userId != toSet)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }
        
        /// <summary>
        /// Описание письма
        /// </summary>
        public override string Description
        {
            get
            {
                return base.Description;
            }
            set
            {
                if(description != value)
                {
                    needUpdate = true;
                }
                description = value;
                OnPropertyChanged("Description");
            }
        }

        long attached;
        /// <summary>
        /// Количество закреплений к цепочке
        /// </summary>
        public long Attached
        {
            get
            {
                return attached;
            }
            set
            {
                if(attached != value)
                {
                    needUpdate = true;
                }
                attached = value;
                if(attached < 0)
                {
                    attached = 0;
                }
                OnPropertyChanged("Attached");
            }
        }

        public Letter(SQLDataRow data) : base()
        {
            Load(data);
        }

        public Letter() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                orgId = data.Long("Org");
                OnPropertyChanged("LinkedOrg");
                if(LinkedOrg != null)
                {
                    LinkedOrg.CheckUpdate();
                }
                depId = data.Long("Dep");
                OnPropertyChanged("LinkedDep");
                if(LinkedDep != null)
                {
                    LinkedDep.CheckUpdate();
                }
                OutNumber = data.String("OutNumber");
                OutDate = new DateTime(data.Long("OutDate"));
                OkspNumber = data.String("OkspNumber");
                OkspDate = new DateTime(data.Long("OkspDate"));
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                Description = data.String("Description");
                Attached = data.Long("Attached");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                orgId = data.Long("Org");
                depId = data.Long("Dep");
                outNumber = data.String("OutNumber");
                outDate = data.Long("OutDate");
                okspNumber = data.String("OkspNumber");
                okspDate = data.Long("OkspDate");
                userId = data.Long("User");
                description = data.String("Description");
                attached = data.Long("Attached");
                if(attached < 0)
                {
                    attached = 0;
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "Letters";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Letters (Org, Dep, OutNumber, OutDate, OkspNumber, OkspDate, User, Description, Attached, UpdateDate) VALUES ({0}, {1}, '{2}', {3}, '{4}', {5}, {6}, '{7}', {8}, {9})", out id,
                orgId, depId, outNumber.Check(), outDate, okspNumber.Check(), okspDate, userId, description.Check(), attached, UpdateDate.Ticks);
            base.CreateNew();
            LetterManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Letters SET Org = {0}, Dep = {1}, OutNumber = '{2}', OutDate = {3}, OkspNumber = '{4}', OkspDate = {5}, User = {6}, Description = '{7}', Attached = {8}, UpdateDate = {9} WHERE Id = {10}",
                orgId, depId, outNumber.Check(), outDate, okspNumber.Check(), okspDate, userId, description.Check(), attached, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM Letters WHERE Id = {0}", id);
            LetterManager.Instance.Remove(this);
        }
    }
}
