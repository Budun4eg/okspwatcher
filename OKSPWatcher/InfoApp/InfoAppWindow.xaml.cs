﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Models;
using OKSPWatcher.Secondary;
using OKSPWatcher.Worked;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Forms;

namespace OKSPWatcher.InfoApp
{
    /// <summary>
    /// Логика взаимодействия для InfoAppWindow.xaml
    /// </summary>
    public partial class InfoAppWindow : BasicWindow
    {

        public InfoAppWindow()
        {
            InitializeComponent();
            WorkedManager.Instance.DataUpdated += WorkedUpdated;
            ModelManager.Instance.DataUpdated += ModelUpdated;
            
            DataContext = InfoAppManager.Instance;

            Update(null,null);
        }

        /// <summary>
        /// Варианты состояния окна
        /// </summary>
        enum States { Init, Open, Update }

        /// <summary>
        /// Текущее состояние окна
        /// </summary>
        States state = States.Init;

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if(state == States.Open)
            {
                Thread.Sleep(1);
            }else if(state == States.Update)
            {
                WorkedManager.Instance.LoadAll();
                ModelManager.Instance.LoadAll();
                ChangeManager.Instance.LoadAll();
            }else if(state == States.Init)
            {
                InfoAppManager.Instance.UpdateData();
            }
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            if(state == States.Open)
            {
                WindowState = WindowState.Normal;
            }else if(state == States.Update)
            {
                LoadShown = true;
                WorkedManager.Instance.Update();
            }else if(state == States.Init)
            {
                LoadShown = false;
            }
        }

        private void ToggleDay(object sender, RoutedEventArgs e)
        {
            current.IsChecked = false;
            current = sender as System.Windows.Controls.CheckBox;
            InfoAppManager.Instance.State = InfoAppManager.States.Day;
        }

        private void ToggleReport(object sender, RoutedEventArgs e)
        {
            current.IsChecked = false;
            current = sender as System.Windows.Controls.CheckBox;
            InfoAppManager.Instance.State = InfoAppManager.States.Period;
        }

        private void TogglePrevious(object sender, RoutedEventArgs e)
        {
            current.IsChecked = false;
            current = sender as System.Windows.Controls.CheckBox;
            InfoAppManager.Instance.State = InfoAppManager.States.Previous;
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            mustCancel = false;
            LoadShown = true;
            UpdateShown = true;
            state = States.Update;
            worker.RunWorkerCheck();       
        }

        private void WorkedUpdated(object sender, EventArgs e)
        {
            ModelManager.Instance.Update();
            if (mustCancel)
            {
                ModelManager.Instance.CancelUpdate = true;
            }
        }

        private void ModelUpdated(object sender, EventArgs e)
        {
            UpdateShown = false;
            state = States.Init;
            worker.RunWorkerCheck();            
        }

        System.Windows.Controls.CheckBox current;

        private void CheckItem(object sender, RoutedEventArgs e)
        {
            current = sender as System.Windows.Controls.CheckBox;
            current.IsChecked = true;
        }

        bool mustCancel = false;

        public override void StopUpdate()
        {
            base.StopUpdate();
            mustCancel = true;
            WorkedManager.Instance.CancelUpdate = true;
            ModelManager.Instance.CancelUpdate = true;
        }

        private void ToggleWeek(object sender, RoutedEventArgs e)
        {
            current.IsChecked = false;
            current = sender as System.Windows.Controls.CheckBox;
            InfoAppManager.Instance.State = InfoAppManager.States.Week;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var workingArea = SystemParameters.WorkArea;
            Left = workingArea.Right - Width;
            Top = workingArea.Bottom - Height;
        }
    }
}
