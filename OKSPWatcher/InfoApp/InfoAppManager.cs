﻿using OKSPWatcher.Changes;
using OKSPWatcher.Changes3D;
using OKSPWatcher.Core;
using OKSPWatcher.Models;
using OKSPWatcher.Worked;
using System;

namespace OKSPWatcher.InfoApp
{
    /// <summary>
    /// Менеджер информационного приложения
    /// </summary>
    public class InfoAppManager : NotifyObject
    {
        private static object root = new object();

        static InfoAppManager instance;

        protected InfoAppManager() { }

        public static InfoAppManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new InfoAppManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        int workedCount;
        /// <summary>
        /// Количество проработанных
        /// </summary>
        public int WorkedCount
        {
            get
            {
                return workedCount;
            }
            set
            {
                workedCount = value;
                OnPropertyChanged("WorkedCount");
            }
        }

        int modelCount;
        /// <summary>
        /// Количество моделей
        /// </summary>
        public int ModelCount
        {
            get
            {
                return modelCount;
            }
            set
            {
                modelCount = value;
                OnPropertyChanged("ModelCount");
            }
        }

        int changesCount;
        /// <summary>
        /// Количество извещений
        /// </summary>
        public int ChangesCount
        {
            get
            {
                return changesCount;
            }
            set
            {
                changesCount = value;
                OnPropertyChanged("ChangesCount");
            }
        }

        /// <summary>
        /// Варианты состояний
        /// </summary>
        public enum States { Day, Week, Period, Previous }

        string variant = "Личная";
        /// <summary>
        /// Варианты поведения
        /// </summary>
        public string Variant
        {
            get
            {
                return variant;
            }
            set
            {
                variant = value;
                UpdateData();
                OnPropertyChanged("Variant");
            }
        }

        SafeObservableCollection<string> variants = new SafeObservableCollection<string>() { "Личная", "Всего" };
        /// <summary>
        /// Варианты поведения
        /// </summary>
        public SafeObservableCollection<string> Variants
        {
            get
            {
                return variants;
            }
        }

        States state = States.Day;
        /// <summary>
        /// Текущее состояние
        /// </summary>
        public States State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
                UpdateData();
                OnPropertyChanged("State");
            }
        }

        /// <summary>
        /// Обновить менеджер
        /// </summary>
        public void UpdateData() {
            WorkedCount = 0;
            ModelCount = 0;
            ChangesCount = 0;
            var dayStart = DateTime.Now.Date;
            var weekStart = DateTime.Now;
            while(weekStart.DayOfWeek != DayOfWeek.Monday)
            {
                weekStart = weekStart.AddDays(-1);
            }
            weekStart = weekStart.Date;
            var periodStart = DateTime.Now;
            while(periodStart.Day != 24)
            {
                periodStart = periodStart.AddDays(-1);
            }
            periodStart = periodStart.Date;
            var previousPeriod = periodStart.AddDays(-1);
            while(previousPeriod.Day != 24)
            {
                previousPeriod = previousPeriod.AddDays(-1);
            }
            previousPeriod = previousPeriod.Date;
            foreach (var item in WorkedManager.Instance.SortedByName)
            {
                if (BackgroundHandler.AllClose) return;
                var img = item.Top;
                if (img != null)
                {
                    switch (state)
                    {                        
                        case States.Day:
                            if (img.Date >= dayStart)
                            {
                                if(variant == "Личная")
                                {
                                    if(img.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        WorkedCount++;
                                    }
                                }
                                else
                                {
                                    WorkedCount++;
                                }                                
                            }
                            break;
                        case States.Week:
                            if (img.Date >= weekStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (img.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        WorkedCount++;
                                    }
                                }
                                else
                                {
                                    WorkedCount++;
                                }
                            }
                            break;
                        case States.Period:
                            if (img.Date >= periodStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (img.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        WorkedCount++;
                                    }
                                }
                                else
                                {
                                    WorkedCount++;
                                }
                            }
                            break;
                        case States.Previous:
                            if (img.Date >= previousPeriod && img.Date < periodStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (img.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        WorkedCount++;
                                    }
                                }
                                else
                                {
                                    WorkedCount++;
                                }
                            }
                            break;
                    }
                }
            }
            foreach (var item in ModelManager.Instance.SortedByName)
            {
                if (BackgroundHandler.AllClose) return;
                var mod = item.Top;
                if (mod != null)
                {
                    switch (state)
                    {                        
                        case States.Day:
                            if (mod.Date >= dayStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (mod.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ModelCount++;
                                    }
                                }
                                else
                                {
                                    ModelCount++;
                                }
                            }
                            break;
                        case States.Week:
                            if (mod.Date >= weekStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (mod.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ModelCount++;
                                    }
                                }
                                else
                                {
                                    ModelCount++;
                                }
                            }
                            break;
                        case States.Period:
                            if (mod.Date >= periodStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (mod.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ModelCount++;
                                    }
                                }
                                else
                                {
                                    ModelCount++;
                                }
                            }
                            break;
                        case States.Previous:
                            if (mod.Date >= previousPeriod && mod.Date < periodStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (mod.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ModelCount++;
                                    }
                                }
                                else
                                {
                                    ModelCount++;
                                }
                            }
                            break;
                    }
                }
            }
            foreach (var item in ChangeManager.Instance.SortedByName)
            {
                if (BackgroundHandler.AllClose) return;
                var ch = item.Single;                
                if (ch != null && ch.WorkedIn3D)
                {
                    var data = Changes3DManager.Instance.ByChange(ch);
                    switch (state)
                    {                        
                        case States.Day:
                            if (data.Date >= dayStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (data.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ChangesCount++;
                                    }
                                }
                                else
                                {
                                    ChangesCount++;
                                }
                            }
                            break;
                        case States.Week:
                            if (data.Date >= weekStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (data.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ChangesCount++;
                                    }
                                }
                                else
                                {
                                    ChangesCount++;
                                }
                            }
                            break;
                        case States.Period:
                            if (data.Date >= periodStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (data.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ChangesCount++;
                                    }
                                }
                                else
                                {
                                    ChangesCount++;
                                }
                            }
                            break;
                        case States.Previous:
                            if (data.Date >= previousPeriod && data.Date < periodStart)
                            {
                                if (variant == "Личная")
                                {
                                    if (data.LinkedUser == MainApp.Instance.CurrentUser)
                                    {
                                        ChangesCount++;
                                    }
                                }
                                else
                                {
                                    ChangesCount++;
                                }
                            }
                            break;
                    }
                }
            }
        }
    }
}
