﻿using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Fixers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Documents;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Логика взаимодействия для ModelRepairWindow.xaml
    /// </summary>
    public partial class ModelRepairWindow : FixerWindow
    {
        public override void StopUpdate()
        {
            base.StopUpdate();
            ModelManager.Instance.CancelUpdate = true;
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            ignoredString = AppSettings.GetString("PathCheckIgnore");
            ModelManager.Instance.LoadAll();
        }

        public ModelRepairWindow()
        {
            InitializeComponent();
            LoadShown = true;
            UpdateShown = true;
            ModelManager.Instance.DataUpdated += ProceedStructure;
            ModelManager.Instance.Update();
        }

        private void ProceedStructure(object sender, EventArgs e)
        {
            UpdateShown = false;
            worker.RunWorkerCheck();
        }

        protected override void OnClosed(EventArgs e)
        {
            ModelManager.Instance.CancelUpdate = true;
            ModelManager.Instance.DataUpdated -= ProceedStructure;
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_ModelRepair");
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        public override int TotalCount
        {
            get
            {
                int count = 0;
                for(int i = 0; i < ModelManager.Instance.Items.Count; i++)
                {
                    var item = ModelManager.Instance.Items[i];
                    if (item == null) continue;
                    if (!item.Parsed)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        protected override void StartCheck(object sender, RoutedEventArgs e)
        {
            OutputBox.Document.Blocks.Clear();
            base.StartCheck(sender, e);
            IgnoredCount = 0;
        }

        protected override void OutputToGUI(object sender, EventArgs e)
        {
            base.OutputToGUI(sender, e);
            lock (toShow)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    while (toShow.Count > 0)
                    {
                        var item = toShow.Dequeue();
                        OutputBox.AddLine(item.Text);
                    }
                    if (AutoScroll)
                    {
                        OutputBox.ScrollToEnd();
                    }
                }));
            }
        }

        bool deleteOnError;
        /// <summary>
        /// Удалять из БД модели с неправильным наименованием
        /// </summary>
        public bool DeleteOnError
        {
            get
            {
                return deleteOnError;
            }
            set
            {
                deleteOnError = value;
                OnPropertyChanged("DeleteOnError");
            }
        }

        string ignoredString;
        /// <summary>
        /// Игнорируемые модели
        /// </summary>
        public string IgnoredString
        {
            get
            {
                return ignoredString;
            }
            set
            {
                ignoredString = value;
                AppSettings.SetString("PathCheckIgnore", value);
                OnPropertyChanged("IgnoredString");
            }
        }

        int ignoredCount;
        /// <summary>
        /// Количество проигнорированных моделей
        /// </summary>
        public int IgnoredCount
        {
            get
            {
                return ignoredCount;
            }
            set
            {
                ignoredCount = value;
                OnPropertyChanged("IgnoredCount");
            }
        }

        protected override void SearchErrors(object sender, DoWorkEventArgs e)
        {
            base.SearchErrors(sender, e);
            List<ModelItem> toDelete = new List<ModelItem>();
            List<string> ignored = new List<string>();

            foreach(var str in ignoredString.Split(new char[] { ',' }))
            {
                var toCheck = str.Trim();
                if(toCheck.Length > 0)
                {
                    ignored.Add(toCheck.ToUpper());
                }
            }

            foreach (ModelItem item in ModelManager.Instance.Items)
            {
                if (cancel)
                {
                    return;
                }
                if (!item.Parsed)
                {
                    bool mustIgnore = false;
                    foreach(var check in ignored)
                    {
                        if (item.PathToModel.ToUpper().Contains(check))
                        {
                            mustIgnore = true;
                            break;
                        }
                    }
                    if (mustIgnore)
                    {
                        IgnoredCount++;
                        continue;
                    }
                    var parse = ModelParser.Instance.Parse(item.PathToModel);
                    item.Sync(parse);
                    item.Save();

                    if (!parse.IsParsed)
                    {
                        if (DeleteOnError)
                        {
                            Output.LogFormat("На удаление из БД: {0}", item.PathToModel);
                            toDelete.Add(item);
                        }
                        else
                        {
                            searchWorker.ReportProgress(2, string.Format("[Ошибка наименования] {0} {1}", item.PathToModel, parse.CheckString));
                            ErrorCount++;
                            Thread.Sleep(10);
                        }
                    }
                    else
                    {
                        Output.LogFormat("Исправлена модель: {0}", item.PathToModel);
                        RepairedCount++;
                    }
                }
            }

            while (toDelete.Count > 0)
            {
                toDelete[0].Delete();
                toDelete.RemoveAt(0);
            }
        }

        protected override void UpdateTemplate(object sender, RoutedEventArgs e)
        {
            base.UpdateTemplate(sender, e);
            ModelParser.Instance.UpdateTemplate();
        }
    }
}
