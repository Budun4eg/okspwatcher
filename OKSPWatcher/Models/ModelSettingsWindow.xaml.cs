﻿using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.ComponentModel;
using System;
using System.Windows;

namespace OKSPWatcher.Models
{
    /// <summary>
    /// Логика взаимодействия для ModelSettingsWindow.xaml
    /// </summary>
    public partial class ModelSettingsWindow : BasicWindow
    {
        public string PathToModel
        {
            get
            {
                return ModelManager.Instance.PathToModel;
            }
            set
            {
                ModelManager.Instance.PathToModel = value;
            }
        }

        public ModelSettingsWindow()
        {
            InitializeComponent();
            
            ModelManager.Instance.CheckInstance();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_ModelSettings");
        }
    }
}
