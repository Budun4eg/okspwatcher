﻿using OKSPWatcher.Core;
using OKSPWatcher.Secondary;
using OKSPWatcher.Test;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace OKSPWatcher
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application, IDisposable
    {
#if SINGLE_INSTANCE
        private uint m_Message;
        private Mutex m_Mutex;

        private IntPtr HandleMessages(IntPtr handle, int message, IntPtr wParameter, IntPtr lParameter, ref bool handled)
        {
            if(message == m_Message)
            {
                StartWindow.Instance.Toggle();
            }
            return IntPtr.Zero;
        }

        private void Dispose(bool disposing)
        {
            if(disposing && (m_Mutex != null))
            {
                m_Mutex.ReleaseMutex();
                m_Mutex.Close();
                m_Mutex = null;
            }
        }

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern uint RegisterWindowMessage(string lpString);

        const int HWND_BROADCAST = 0xffff;
#endif

        public void Dispose()
        {
#if SINGLE_INSTANCE
            Dispose(true);
            GC.SuppressFinalize(this);
#endif
        }

        protected override void OnStartup(StartupEventArgs e)
        {
#if SINGLE_INSTANCE
            Assembly assembly = Assembly.GetExecutingAssembly();
            bool mutexCreated;
            string mutexName = string.Format(CultureInfo.InvariantCulture, "Local\\{{{0}}}{{{1}}}", assembly.GetType().GUID, assembly.GetName().Name);

            m_Mutex = new Mutex(true, mutexName, out mutexCreated);
            m_Message = RegisterWindowMessage(mutexName);

            if (!mutexCreated)
            {
                m_Mutex = null;

                SendMessage(HWND_BROADCAST, m_Message, IntPtr.Zero, IntPtr.Zero);

                Current.Shutdown();
            }
#endif

            base.OnStartup(e);

            //TestWindow window = new TestWindow();
            StartWindow window = new StartWindow();
            window.Show();

#if SINGLE_INSTANCE
            HwndSource.FromHwnd((new WindowInteropHelper(window)).Handle).AddHook(new HwndSourceHook(HandleMessages));
#endif
        }

#if SINGLE_INSTANCE
        protected override void OnExit(ExitEventArgs e)
        {
            Dispose();
            base.OnExit(e);
        }
#endif

        private void MinimizeWindow(object sender, RoutedEventArgs e)
        {
            try
            {
                BasicWindow.Current.Minimize();
            }
            catch { }
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            try
            {
                BasicWindow.Current.Close();
            }
            catch { }
        }

        private void MaximizeWindow(object sender, RoutedEventArgs e)
        {
            try
            {
                BasicWindow.Current.WindowState = WindowState.Maximized;
            }
            catch { }
        }

        private void RestoreWindow(object sender, RoutedEventArgs e)
        {
            try
            {
                BasicWindow.Current.WindowState = WindowState.Normal;
            }
            catch { }
        }

        private void ShowHelp(object sender, RoutedEventArgs e)
        {
            try
            {
                BasicWindow.Current.ShowHelp();
            }
            catch { }
        }

        public bool HelpShown
        {
            get
            {
                return BasicWindow.Current.HelpShown;
            }
        }

        public bool IsLocked
        {
            get
            {
                return BasicWindow.Current.IsLocked;
            }
        }

        public bool DBTrans
        {
            get
            {
                return BasicWindow.Current.DBTrans;
            }
        }

        public bool Warn
        {
            get
            {
                return BasicWindow.Current.Warn;
            }
        }

        public bool Error
        {
            get
            {
                return BasicWindow.Current.Error;
            }
        }

        public bool LoadShown
        {
            get
            {
                return BasicWindow.Current.LoadShown;
            }
        }

        public bool UpdateShown
        {
            get
            {
                return BasicWindow.Current.UpdateShown;
            }
        }

        public double Progress
        {
            get
            {
                return BasicWindow.Current.Progress;
            }
        }

        public string OutputText
        {
            get
            {
                return BasicWindow.Current.OutputText;
            }
        }

        public string UpdateText
        {
            get
            {
                return BasicWindow.Current.UpdateText;
            }
        }

        public bool TopMenuEnabled
        {
            get
            {
                return BasicWindow.Current.TopMenuEnabled;
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWind, uint Msg, IntPtr wParam, IntPtr IParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(int hWind, uint Msg, IntPtr wParam, IntPtr IParam);

        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void WindowDrag(object sender, MouseButtonEventArgs e)
        {
            if (!BasicWindow.Current.ShowInTaskbar) {
                return;
            }
            ReleaseCapture();
            SendMessage(new WindowInteropHelper(BasicWindow.Current).Handle, 0xA1, (IntPtr)0x2, (IntPtr)0);
        }

        private void WindowResize(object sender, MouseButtonEventArgs e)
        {
            if (!BasicWindow.Current.ShowInTaskbar)
            {
                return;
            }
            HwndSource hwndSource = PresentationSource.FromVisual((BasicWindow.Current)) as HwndSource;
            SendMessage(hwndSource.Handle, 0x112, (IntPtr)61448, IntPtr.Zero);
        }

        private void StopUpdate(object sender, RoutedEventArgs e)
        {
            BasicWindow.Current.StopUpdate();
        }

        private void OpenLog(object sender, RoutedEventArgs e)
        {
            Output.OpenLog();
        }

        private void OpenErrors(object sender, RoutedEventArgs e)
        {
            Output.OpenErrors();
        }

        private void CopyToClipboard(object sender, RoutedEventArgs e)
        {
            if (BasicWindow.Current.OutputText != null) {
                Clipboard.SetText(BasicWindow.Current.OutputText);
                NotifyWindow.ShowNote("Текст скопирован");
            }            
        }
    }
}
