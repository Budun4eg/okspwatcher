﻿using System;

namespace OKSPWatcher.NotifyRequest
{
    /// <summary>
    /// Данные ответа на запрос
    /// </summary>
    public class NotifyRequestEventArgs : EventArgs
    {
        string code;
        /// <summary>
        /// Код запроса
        /// </summary>
        public string Code
        {
            get
            {
                return code;
            }
        }

        public NotifyRequestEventArgs(string _code)
        {
            code = _code;
        }
    }
}
