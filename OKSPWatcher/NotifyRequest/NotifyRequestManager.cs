﻿using OKSPWatcher.Block;
using OKSPWatcher.Utilities.Apps;

namespace OKSPWatcher.NotifyRequest
{
    /// <summary>
    /// Менеджер реагирования на запросы
    /// </summary>
    public static class NotifyRequestManager
    {
        /// <summary>
        /// Менеджер инициализирован?
        /// </summary>
        static bool inited = false;

        /// <summary>
        /// Провести инициализацию запросов
        /// </summary>
        public static void Init()
        {
            if (!inited)
            {
                inited = true;
                NotifyRequestWindow.Requested += NotifyRequestWindow_Requested;
            }            
        }

        private static void NotifyRequestWindow_Requested(object sender, NotifyRequestEventArgs e)
        {
            switch (e.Code)
            {
                case "Backup":
                    if (BlockManager.Instance.Block("Utility_CreateBackup"))
                    {
                        var window = new DBBackupWindow();
                        window.Show();
                    }
                    
                    break;
            }
        }
    }
}
