﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace OKSPWatcher.NotifyRequest
{
    /// <summary>
    /// Логика взаимодействия для NotifyRequestWindow.xaml
    /// </summary>
    public partial class NotifyRequestWindow : BasicWindow
    {
        /// <summary>
        /// Таймер автоматического закрытия
        /// </summary>
        DispatcherTimer timer;

        /// <summary>
        /// Количество секунд до автоматического закрытия
        /// </summary>
        const int secondsToWait = 10;

        public NotifyRequestWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, secondsToWait);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Показать окно информации
        /// </summary>
        public static void ShowNote(string code, string text, string buttonText)
        {
            var window = new NotifyRequestWindow();
            window.Load(code, text, buttonText);
            window.Show();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (timer != null)
            {
                timer.Tick += Timer_Tick;
                if (timer.IsEnabled)
                {
                    timer.Stop();
                }
            }
        }

        string noteText;
        /// <summary>
        /// Текст информации
        /// </summary>
        public string NoteText
        {
            get
            {
                return noteText;
            }
            set
            {
                noteText = value;
                OnPropertyChanged("NoteText");
            }
        }

        string buttonText;
        /// <summary>
        /// Текст кнопки
        /// </summary>
        public string ButtonText
        {
            get
            {
                return buttonText;
            }
            set
            {
                buttonText = value;
                OnPropertyChanged("ButtonText");
            }
        }

        /// <summary>
        /// Код запроса
        /// </summary>
        string code;

        public void Load(string _code, string _text, string _buttonText)
        {
            NoteText = _text;
            code = _code;
            ButtonText = _buttonText;
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        /// <summary>
        /// Запрос получен
        /// </summary>
        public static event EventHandler<NotifyRequestEventArgs> Requested;
        void OnRequested()
        {
            var handler = Requested;
            if(handler != null)
            {
                handler(null, new NotifyRequestEventArgs(code));
            }
        }

        private void OkClick(object sender, System.Windows.RoutedEventArgs e)
        {
            OnRequested();
            Close();
        }

        private void WindowLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var workingArea = SystemParameters.WorkArea;
            Left = workingArea.Right - Width;
            Top = workingArea.Bottom - Height;
            Topmost = true;
        }
    }
}
