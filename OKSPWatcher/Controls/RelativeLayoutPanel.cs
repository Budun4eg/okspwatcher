﻿using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher
{
    public class RelativeLayoutPanel : Panel
    {
        public static readonly DependencyProperty RelativeRectProperty = DependencyProperty.RegisterAttached("RelativeRect", typeof(Rect), typeof(RelativeLayoutPanel), new FrameworkPropertyMetadata(new Rect(), FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsArrange));

        public static Rect GetRelativeRect(UIElement element)
        {
            return (Rect)element.GetValue(RelativeRectProperty);
        }

        public static void SetRelativeRect(UIElement element, Rect value)
        {
            element.SetValue(RelativeRectProperty, value);
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            availableSize = new Size(double.PositiveInfinity, double.PositiveInfinity);

            foreach(UIElement element in InternalChildren)
            {
                element.Measure(availableSize);
            }

            return new Size();
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            foreach(UIElement element in InternalChildren)
            {
                var rect = GetRelativeRect(element);

                element.Arrange(new Rect(rect.X * finalSize.Width, rect.Y * finalSize.Height, rect.Width * finalSize.Width, rect.Height * finalSize.Height));
            }

            return finalSize;
        }
    }
}
