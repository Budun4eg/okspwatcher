﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Элемент для управления правами пользователя
    /// </summary>
    public class RuleEditorItem : NotifyObject
    {
        RulesItem rule;
        /// <summary>
        /// Правила пользователя
        /// </summary>
        public RulesItem Rule
        {
            get
            {
                return rule;
            }
        }

        RulesItem groupRule;
        /// <summary>
        /// Групповое правило
        /// </summary>
        public RulesItem GroupRule
        {
            get
            {
                return groupRule;
            }
        }

        /// <summary>
        /// Суммарное правило
        /// </summary>
        public bool Summary
        {
            get
            {
                if (groupRule.Enabled)
                {
                    return true;
                }
                else
                {
                    if (rule.Enabled)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public RuleEditorItem(RulesItem _rule, RulesItem _groupRule)
        {
            rule = _rule;
            groupRule = _groupRule;
            rule.StateChanged += Rule_StateChanged;
        }

        private void Rule_StateChanged(object sender, System.EventArgs e)
        {
            OnPropertyChanged("Summary");
        }
    }
}
