﻿using OKSPWatcher.Core;
using OKSPWatcher.Secondary;
using OKSPWatcher.Users;
using System.Windows.Controls;
using System;
using System.Windows;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Логика взаимодействия для RulesManagerWindow.xaml
    /// </summary>
    public partial class RulesManagerWindow : BasicWindow
    {
        public RulesManagerWindow()
        {
            InitializeComponent();
            Load();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        /// <summary>
        /// Список пользователей
        /// </summary>
        public SafeObservableCollection<User> Users
        {
            get
            {
                return UserManager.Instance.Items;
            }
        }

        /// <summary>
        /// Список групп
        /// </summary>
        public SafeObservableCollection<RuleGroupItem> Groups
        {
            get
            {
                return RuleGroupManager.Instance.Items;
            }
        }

        /// <summary>
        /// Загрузить редактор
        /// </summary>
        void Load()
        {
            UserManager.Instance.LoadAll();
            RuleGroupManager.Instance.LoadAll();
        }

        private void AddGroup(object sender, System.Windows.RoutedEventArgs e)
        {
            string name = GetTextWindow.Get("Наименование группы", "Напишите наименование новой группы", "");
            var group = new RuleGroupItem();
            group.Name = name;
            group.Save();
        }

        private void ChangeGroup(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new RuleGroupWindow();
            var item = (sender as Button).DataContext as RuleGroupItem;
            window.Load(item);
            window.ShowDialog();
        }

        private void ToggleUser(object sender, System.Windows.RoutedEventArgs e)
        {
            var user = (sender as Button).DataContext as User;
            if(user.Data.GroupRules == null)
            {
                Output.Warning("Группа пользователя не установлена");
                return;
            }
            var window = new RuleUserWindow();
            window.Load(user);
            window.ShowDialog();
        }

        private void RenameGroup(object sender, System.Windows.RoutedEventArgs e)
        {
            var group = (sender as MenuItem).DataContext as RuleGroupItem;
            group.Name = GetTextWindow.Get("Наименование группы", "Введите новое наименование группы", group.Name);
            group.Save();
        }

        private void DeleteGroup(object sender, System.Windows.RoutedEventArgs e)
        {
            var group = (sender as MenuItem).DataContext as RuleGroupItem;
            RequestWindow.Show("Удалить группу {0}", group.Name);
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                group.Delete();
            }
        }
    }
}
