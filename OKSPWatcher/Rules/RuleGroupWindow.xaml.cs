﻿using OKSPWatcher.Core;
using System;
using System.Windows;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Логика взаимодействия для RuleGroupWindow.xaml
    /// </summary>
    public partial class RuleGroupWindow : BasicWindow
    {
        public RuleGroupWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        RuleGroupItem item;
        /// <summary>
        /// Объект для редактирования
        /// </summary>
        public RuleGroupItem Item
        {
            get
            {
                return item;
            }
        }

        /// <summary>
        /// Загрузить объект
        /// </summary>
        public void Load(RuleGroupItem _item)
        {
            item = _item;
        }

        bool isEnabled = false;

        private void ToggleAll(object sender, System.Windows.RoutedEventArgs e)
        {
            isEnabled = !isEnabled;
            foreach(var rule in item.Rule.Rules)
            {
                rule.Enabled = isEnabled;
            }
        }
    }
}
