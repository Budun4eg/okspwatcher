﻿using OKSPWatcher.Core;
using OKSPWatcher.Users;
using System;
using System.Windows;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Логика взаимодействия для RuleUserWindow.xaml
    /// </summary>
    public partial class RuleUserWindow : BasicWindow
    {
        public RuleUserWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        SafeObservableCollection<RuleEditorItem> items = new SafeObservableCollection<RuleEditorItem>();
        /// <summary>
        /// Элементы
        /// </summary>
        public SafeObservableCollection<RuleEditorItem> Items
        {
            get
            {
                return items;
            }
        }

        User linkedUser;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return linkedUser;
            }
        }

        /// <summary>
        /// Загрузить объект
        /// </summary>
        public void Load(User _user)
        {
            items.Clear();
            linkedUser = _user;
            foreach(var rule in linkedUser.Data.Rules.Rules)
            {
                RulesItem groupRule = linkedUser.Data.GroupRules.Rule.ByKey(rule.Name);
                items.Add(new RuleEditorItem(rule, groupRule));
            }
        }

        bool isEnabled = false;

        private void ToggleAll(object sender, System.Windows.RoutedEventArgs e)
        {
            isEnabled = !isEnabled;
            foreach (var rule in items)
            {
                rule.Rule.Enabled = isEnabled;
            }
        }
    }
}
