﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Набор правил
    /// </summary>
    public class RulesPack : DBObject
    {
        /// <summary>
        /// Сгенерировать строку сохранения
        /// </summary>
        string GenerateSave()
        {
            SafeObservableCollection<string> saveStrings = new SafeObservableCollection<string>();
            foreach(var item in rules)
            {
                if (item.Enabled)
                {
                    saveStrings.Add(item.Name);
                }
            }
            return saveStrings.Serialize();
        }

        SafeObservableCollection<RulesItem> rules = new SafeObservableCollection<RulesItem>();
        /// <summary>
        /// Правила
        /// </summary>
        public SafeObservableCollection<RulesItem> Rules
        {
            get
            {
                return rules;
            }
        }

        /// <summary>
        /// Список правил по ключу
        /// </summary>
        Dictionary<string, RulesItem> byKey = new Dictionary<string, RulesItem>();

        /// <summary>
        /// Получить правило по ключу
        /// </summary>
        public RulesItem ByKey(string key)
        {
            if (byKey.ContainsKey(key))
            {
                return byKey[key];
            }
            else
            {
                return null;
            }
        }

        public RulesPack() : base() {
            needUpdate = true;
            foreach (var key in RulesManager.Instance.RuleKeys)
            {
                var newRule = new RulesItem(key, false);
                newRule.StateChanged += NewRule_StateChanged;
                rules.Add(newRule);
            }
            Sync();
        }

        public RulesPack(SQLDataRow data) : base()
        {
            Load(data);
        }

        public override void Load(SQLDataRow data)
        {
            rules.Clear();
            base.Load(data);
            try
            {
                var enabledRules = data.String("Rules").DeserializeString();
                foreach(var key in RulesManager.Instance.RuleKeys)
                {
                    var newRule = new RulesItem(key, enabledRules.Contains(key.Key));
                    newRule.StateChanged += NewRule_StateChanged;
                    rules.Add(newRule);
                }
                Sync();
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                var enabledRules = data.String("Rules").DeserializeString();
                foreach (var key in rules)
                {
                    key.Enabled = enabledRules.Contains(key.Name);
                }
                Sync();
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        private void NewRule_StateChanged(object sender, EventArgs e)
        {
            needUpdate = true;
            Sync();
            Save();
        }

        /// <summary>
        /// Синхронизировать состояние
        /// </summary>
        void Sync()
        {
            byKey.Clear();
            foreach(var item in rules)
            {
                byKey.Add(item.Name, item);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "RuleItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO RuleItems (Rules, UpdateDate) VALUES ('{0}', {1})", out id, GenerateSave(), UpdateDate.Ticks);
            base.CreateNew();
            RulesManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE RuleItems SET Rules = '{0}', UpdateDate = {1} WHERE Id = {2}", GenerateSave(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM RuleItems WHERE Id = {0}", id);
            RulesManager.Instance.Remove(this);
        }
    }
}
