﻿namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Данные правила
    /// </summary>
    public class RuleData
    {
        string key;
        /// <summary>
        /// Ключ правила
        /// </summary>
        public string Key
        {
            get
            {
                return key;
            }
        }

        string description;
        /// <summary>
        /// Описание правила
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
        }

        public RuleData(string _key, string _description)
        {
            key = _key;
            description = _description;
        }
    }
}
