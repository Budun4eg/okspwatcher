﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Менеджер прав пользователей
    /// </summary>
    public class RulesManager : ManagerObject<RulesPack>
    {
        private static object root = new object();

        static RulesManager instance;
        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static RulesManager Instance
        {
            get {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new RulesManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Возможность доступна?
        /// </summary>
        public bool Can(string key)
        {
            var groupRules = MainApp.Instance.CurrentUser.Data.GroupRules;
            var userRules = MainApp.Instance.CurrentUser.Data.Rules;

            if(groupRules != null && groupRules.Rule != null)
            {
                var groupKey = groupRules.Rule.ByKey(key);
                if(groupKey != null && groupKey.Enabled)
                {
                    return true;
                }
            }

            if (userRules != null)
            {
                var userKey = userRules.ByKey(key);
                if (userKey != null && userKey.Enabled)
                {
                    return true;
                }
            }

            return false;
        }

        List<RuleData> ruleKeys = new List<RuleData>() {
            new RuleData("App_Admin", "[Приложение] Админка"),
            new RuleData("App_DepManager", "[Приложение] Управление отделом"),
            new RuleData("App_WorkedSettings", "[Приложение] Настройки проработанных чертежей"),
            new RuleData("App_WorkedReport", "[Приложение] Отчет по проработанным чертежам"),
            new RuleData("App_WorkedRepair", "[Приложение] Исправление проработанных чертежей"),
            new RuleData("App_ModelSettings", "[Приложение] Настройки моделей"),
            new RuleData("App_ModelReport", "[Приложение] Отчет по разработанным моделям"),
            new RuleData("App_ModelRepair", "[Приложение] Исправление моделей"),
            new RuleData("App_ChangesSettings", "[Приложение] Настройки извещений"),
            new RuleData("App_ChangesRepair", "[Приложение] Исправление извещений"),
            new RuleData("App_Changes3DReport", "[Приложение] Отчет по проведенным извещениям в 3D-моделях"),
            new RuleData("App_Changes3DSettings", "[Приложение] Настройки проведения извещений в 3D-моделях"),
            new RuleData("App_Changes3D", "[Приложение] Проведение извещений в 3D"),
            new RuleData("App_RemarksReport", "[Приложение] Отчет по замечаниям"),
            new RuleData("App_RemarksSettings", "[Приложение] Настройки замечаний"),
            new RuleData("App_Remarks", "[Приложение] Составление письма по замечаниям"),
            new RuleData("App_Chains", "[Приложение] Учет писем"),
            new RuleData("App_ChainSettings", "[Приложение] Настройки учета писем"),
            new RuleData("App_Utilities", "[Приложение] Утилиты"),
            new RuleData("App_StructureSettings", "[Приложение] Настройки составления структур изделий"),
            new RuleData("App_CreateStructure", "[Приложение] Составление структур изделий"),
            new RuleData("App_CompletedIn3D", "[Приложение] Готовность изделий в 3D"),
            new RuleData("App_InfoApp", "[Приложение] Информационное приложение бюро 3D"),
            new RuleData("Utility_ChangeDatePack", "[Утилита] Замена даты изменения файлов"),
            new RuleData("Utility_SplitNamePack", "[Утилита] Разделение наименования файлов"),
            new RuleData("Utility_RandomDatePack", "[Утилита] Случайная дата изменения файлов"),
            new RuleData("Utility_DBBackup", "[Утилита] Бэкап БД"),
            new RuleData("Utility_ModelPathCheck","[Утилита] Проверка расположения моделей"),
            new RuleData("Utility_CreateSearch", "[Утилита] Создание файла поиска"),
            new RuleData("StructureTopNode", "[Структура изделий] Изменение верхний ячеек структуры"),
            new RuleData("ForceRemarkReportSave", "[Создание отчета по замечаниям] Сохранение отчета другого пользователя"),
            new RuleData("RemarkReportDelete", "[Отчет по замечаниям] Удаление отчета"),
            new RuleData("ChangeRemarkItem", "[Отчет по замечаниям] Изменение состояния пункта замечания"),
            new RuleData("DeleteChangeItem", "[Извещения] Удаление извещения"),
            new RuleData("ClearChangeState", "[Проведение извещений в 3D] Снять выполнение извещения"),
            new RuleData("ChangeStructureState", "[Структура изделия] Изменение структуры изделия"),
            new RuleData("AddNewStructureState", "[Структура изделия] Добавление, изменение состояний структуры"),
            new RuleData("DeleteStructureState", "[Структура изделия] Удаление состояний структуры"),
            new RuleData("CreateSchedule", "[План-график] Создание план-графиков и списков ДСЕ на разработку"),
            new RuleData("UnblockObjects", "[Блокировка] Разблокировка объектов и приложений"),
            new RuleData("DeleteChains", "[Цепочки] Удаление цепочек"),
            new RuleData("ForcedDeleteChains", "[Цепочки] Удаление цепочек других пользователей"),
            new RuleData("DeleteChainItems", "[Элементы цепочек] Удаление элементов цепочки"),
            new RuleData("ForcedDeleteChainItems", "[Элементы цепочек] Удаление элементов цепочки другого пользователя"),
            new RuleData("ChainAttachLetters", "[Элементы цепочек] Добавление писем в цепочку"),
            new RuleData("ChainAttachText", "[Элементы цепочек] Добавление текстовых заметок в цепочку"),
            new RuleData("ChainAttachRequest", "[Элементы цепочек] Добавление выполненных заявок по 3D-моделям"),
            new RuleData("ChainAttachRemarks", "[Элементы цепочек] Добавление отчетов по замечаниям"),
            new RuleData("LettersRemove", "[Письма] Удаление свободных писем"),
            new RuleData("LetterRemoveAttaches", "[Письма] Обнуление количества привязок")
        };
        /// <summary>
        /// Ключи прав
        /// </summary>
        public List<RuleData> RuleKeys
        {
            get
            {
                return ruleKeys;
            }
        }

        /// <summary>
        /// Получить набор правил по идентификатору
        /// </summary>
        public override RulesPack ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM RuleItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("RuleItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new RulesPack(row));
                }
                return ById(id);
            }
            else
            {
                if (ret != null) {
                    return ret;
                }
                else
                {
                    var item = new RulesPack();
                    item.Save();
                    return item;
                }                
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("RuleItems").Execute(@"DROP TABLE RuleItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("RuleItems").Execute(@"CREATE TABLE IF NOT EXISTS RuleItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Rules TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }
    }
}
