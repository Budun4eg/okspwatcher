﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Менеджер групп пользователей
    /// </summary>
    public class RuleGroupManager : ManagerObject<RuleGroupItem>
    {
        private static object root = new object();

        static RuleGroupManager instance;
        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static RuleGroupManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new RuleGroupManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void LoadAll()
        {
            string request = string.Format("SELECT * FROM RuleGroups");

            var data = SQL.Get("RuleGroups").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new RuleGroupItem(row));
            }
        }

        /// <summary>
        /// Получить набор правил по идентификатору
        /// </summary>
        public override RuleGroupItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM RuleGroups WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("RuleGroups").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new RuleGroupItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("RuleGroups").Execute(@"DROP TABLE RuleGroups");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("RuleGroups").Execute(@"CREATE TABLE IF NOT EXISTS RuleGroups (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    Rule INTEGER,
                                                    Users TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }
    }
}
