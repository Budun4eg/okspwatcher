﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Группа пользователей с определенными правами
    /// </summary>
    public class RuleGroupItem : DBObject
    {
        /// <summary>
        /// Наименование группы
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Идентификатор привязанного правила
        /// </summary>
        long ruleId = -1;

        public RulesPack Rule
        {
            get
            {
                var item = RulesManager.Instance.ById(ruleId, true);
                if(ruleId != item.Id)
                {
                    ruleId = item.Id;
                    needUpdate = true;
                    Save();
                }
                return item;
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(toSet != ruleId)
                {
                    needUpdate = true;
                }
                ruleId = toSet;
                OnPropertyChanged("Rule");
            }
        }

        UserPack users;
        /// <summary>
        /// Набор пользователей
        /// </summary>
        public UserPack Users
        {
            get
            {
                if(users == null)
                {
                    users = new UserPack();
                }
                return users;
            }
        }

        public RuleGroupItem() : base() {
            needUpdate = true;
        }

        public RuleGroupItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                ruleId = data.Long("Rule");
                OnPropertyChanged("Rule");
                if(Rule != null)
                {
                    Rule.CheckUpdate();
                }
                Users.Sync(data.Ids("Users"));
                foreach(var user in Users.Items)
                {
                    if(user != null)
                    {
                        user.CheckUpdate();
                    }
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                ruleId = data.Long("Rule");
                Users.Sync(data.Ids("Users"));
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "RuleGroups";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO RuleGroups (Name, Rule, Users, UpdateDate) VALUES ('{0}', {1}, '{2}', {3})", out id, name.Check(), ruleId, Users.Serialize(), UpdateDate.Ticks);
            base.CreateNew();
            RuleGroupManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE RuleGroups SET Name = '{0}', Rule = {1}, Users = '{2}', UpdateDate = {3} WHERE Id = {4}", name.Check(), ruleId, Users.Serialize(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM RuleGroups WHERE Id = {0}", id);
            RuleGroupManager.Instance.Remove(this);
        }
    }
}
