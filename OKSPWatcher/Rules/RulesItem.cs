﻿using OKSPWatcher.Core;
using System;

namespace OKSPWatcher.Rules
{
    /// <summary>
    /// Отдельное правило
    /// </summary>
    public class RulesItem : NotifyObject
    {
        /// <summary>
        /// Событие, вызываемое при изменении состояния правила
        /// </summary>
        public event EventHandler StateChanged;
        void OnStateChanged()
        {
            var handler = StateChanged;
            if(handler != null)
            {
                handler(null, null);
            }
        }

        bool enabled;
        /// <summary>
        /// Правило включено?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                OnPropertyChanged("Enabled");
                OnStateChanged();
            }
        }

        public RulesItem(RuleData data, bool _enabled)
        {
            name = data.Key;
            description = data.Description;
            enabled = _enabled;
        }
    }
}
