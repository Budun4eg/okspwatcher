﻿using OKSPWatcher.Block;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Стандартный объект для хранения в базе
    /// </summary>
    public class DBObject : NotifyObject
    {
        long updateDate;
        /// <summary>
        /// Дата обновления объекта
        /// </summary>
        public DateTime UpdateDate
        {
            get
            {
                return new DateTime(updateDate);
            }
            set
            {
                updateDate = value.Ticks;
            }
        }

        /// <summary>
        /// Состояние объекта синхронизировано?
        /// </summary>
        bool isSynced
        {
            get
            {
                return updateDate == GetUpdateDate();
            }            
        }

        long GetUpdateDate()
        {
            var chk = sql.ExecuteScalar("SELECT UpdateDate FROM {0} WHERE Id = {1}", sqlPath, id);
            if (chk != null)
            {
                return (long)chk;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Данные об элементе в БД
        /// </summary>
        protected SQLDataRow updateData
        {
            get
            {
                var data = sql.ExecuteRead("SELECT * FROM {0} WHERE Id = {1}", sqlPath, id);
                return data.Single;
            }
        }

        /// <summary>
        /// Проверить состояние объекта
        /// </summary>
        public void CheckUpdate()
        {
            if (!isSynced)
            {
                var data = updateData;
                if(data != null)
                {
                    UpdateState(updateData);
                }                
            }
        }

        /// <summary>
        /// Обновить состояние объекта
        /// </summary>
        protected virtual void UpdateState(SQLDataRow data)
        {
            try
            {
                UpdateDate = new DateTime(data.Long("UpdateDate"));
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }            
        }

        /// <summary>
        /// Код блокировки объекта
        /// </summary>
        public virtual string BlockCode
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Заблокировать объект
        /// </summary>
        public void Block()
        {
            if(BlockCode == null)
            {
                return;
            }
            BlockManager.Instance.Block(BlockCode);
        }

        /// <summary>
        /// Разблокировать объект
        /// </summary>
        public void UnBlock()
        {
            if (BlockCode == null)
            {
                return;
            }
            BlockManager.Instance.UnBlock(BlockCode);
        }

        /// <summary>
        /// Редактирование объекта доступно?
        /// </summary>
        public bool Available()
        {
            if (BlockCode == null)
            {
                return true;
            }
            return BlockManager.Instance.Available(BlockCode);
        }

        /// <summary>
        /// Сохранение объекта доступно?
        /// </summary>
        protected bool saveEnabled = true;

        protected long id = -1;
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public virtual long Id
        {
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Загрузить объект
        /// </summary>
        public virtual void Load(SQLDataRow data)
        {
            if (data == null) return;
            try
            {
                id = data.Long("Id");
                updateDate = data.Long("UpdateDate");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }            
        }
                
        protected bool needUpdate;
        /// <summary>
        /// Необходимо обновить данные в БД?
        /// </summary>
        public bool NeedUpdate
        {
            get
            {
                return needUpdate;
            }
        }
        
        /// <summary>
        /// Сохранить объект
        /// </summary>
        public virtual bool Save()
        {
            if (!saveEnabled) return false;
            if (!needUpdate) return false;
            needUpdate = false;
            if(id >= 0)
            {
                return Update();
            }
            else
            {
                return CreateNew();
            }            
        }

        /// <summary>
        /// Создать новый объект
        /// </summary>
        protected virtual bool CreateNew()
        {            
            return true;
        }

        /// <summary>
        /// Обновить объект
        /// </summary>
        protected virtual bool Update()
        {
            return true;
        }

        /// <summary>
        /// Удалить объект
        /// </summary>
        public virtual void Delete() {

        }

        /// <summary>
        /// Сохраненное состояние объекта
        /// </summary>
        protected Dictionary<string, object> savedState = new Dictionary<string, object>();

        /// <summary>
        /// Список состояний для обновления
        /// </summary>
        protected List<string> traces = new List<string>();

        /// <summary>
        /// Объект имеет сохраненное состояние?
        /// </summary>
        protected bool hasSavedState;

        /// <summary>
        /// Сохраняет состояние объекта
        /// </summary>
        public virtual void SaveState()
        {
            hasSavedState = true;
        }

        /// <summary>
        /// Добавить объект состояния
        /// </summary>
        protected void AddStateItem(string id, object obj)
        {
            savedState.Add(id, obj);
            traces.Add(id);
        }

        /// <summary>
        /// Загрузить сохраненное состояние
        /// </summary>
        public virtual void LoadState()
        {
            TraceState();
        }

        /// <summary>
        /// Очистить сохраненное состояние
        /// </summary>
        public virtual void ClearState()
        {
            hasSavedState = false;
            savedState.Clear();
            traces.Clear();
        }

        /// <summary>
        /// Разослать обновленное состояние
        /// </summary>
        void TraceState()
        {
            foreach(string traceId in traces)
            {
                OnPropertyChanged(traceId);
            }
        }

        /// <summary>
        /// Путь до БД
        /// </summary>
        protected virtual string sqlPath
        {
            get
            {
                return "";
            }
        }

        SQLItem _sql;
        /// <summary>
        /// Соединение с БД
        /// </summary>
        protected SQLItem sql
        {
            get
            {
                if(_sql == null)
                {
                    _sql = SQL.Get(sqlPath);
                }
                return _sql;
            }
        }
    }
}
