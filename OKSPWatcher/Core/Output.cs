﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Класс для вывода информации
    /// </summary>
    public static class Output
    {
        static StreamWriter errors = null;
        static StreamWriter output = null;

        static string _outputPath;
        static string _errorsPath;

        static string outputPath
        {
            get
            {
                if(_outputPath == null)
                {
                    _outputPath = string.Format("{0}Trace {1} {2}.txt", AppSettings.OutputFolder, Environment.UserName, DateTime.Now.ToString().Replace(':', '.'));
                }
                return _outputPath;
            }
        }

        static string errorsPath
        {
            get
            {
                if(_errorsPath == null)
                {
                    _errorsPath = string.Format("{0}Error {1} {2}.txt", AppSettings.OutputFolder, Environment.UserName, DateTime.Now.ToString().Replace(':', '.')); ;
                }
                return _errorsPath;
            }
        }

        /// <summary>
        /// Открыть лог
        /// </summary>
        public static void OpenLog()
        {
            try
            {
                Process.Start(outputPath);
            }
            catch (FileNotFoundException)
            {
                Log("Текущий лог отсутствует");
            }
            catch {
                Error("Ошибка при открытии лога");
            }
        }

        /// <summary>
        /// Открыть список ошибок
        /// </summary>
        public static void OpenErrors()
        {
            try
            {
                Process.Start(errorsPath);
            }
            catch (FileNotFoundException)
            {
                Log("Текущий список ошибок отсутствует");
            }
            catch
            {
                Error("Ошибка при открытии списка ошибок");
            }
        }

        /// <summary>
        /// Состояния вывода
        /// </summary>
        enum States { Default, Warning, Error }

        /// <summary>
        /// Состояние вывода
        /// </summary>
        static States State
        {
            set
            {
                BasicWindow.Current.Warn = value == States.Warning;
                BasicWindow.Current.Error = value == States.Error;
            }
        }

        /// <summary>
        /// Очистить вывод
        /// </summary>
        public static void Clear()
        {
            BasicWindow.TraceInfo("");
        }

        #region Trace
        /// <summary>
        /// Вывод текста в окне
        /// </summary>
        public static void TraceLog(string text)
        {
            State = States.Default;
            BasicWindow.TraceInfo(text);
#if IN_EDITOR && FULL_OUTPUT
            Debug.Print("[TraceLog] {0}", text);
#endif
        }

        /// <summary>
        /// Вывод текста в окне
        /// </summary>
        public static void TraceLog(string text, params object[] args)
        {
            TraceLog(string.Format(text, args));
        }

        /// <summary>
        /// Вывод ошибки в окне
        /// </summary>
        public static void TraceError(string text)
        {
            State = States.Error;
            BasicWindow.TraceInfo(text);
#if IN_EDITOR && FULL_OUTPUT
            Debug.Print("[TraceError] {0}", text);
#endif
        }

        /// <summary>
        /// Вывод ошибки в окне
        /// </summary>
        public static void TraceError(string text, params object[] args)
        {
            TraceError(string.Format(text, args));
        }

        /// <summary>
        /// Вывод предупреждения в окне
        /// </summary>
        public static void TraceWarning(string text)
        {
            State = States.Warning;
            BasicWindow.TraceInfo(text);
#if IN_EDITOR && FULL_OUTPUT
            Debug.Print("[TraceWarning] {0}", text);
#endif
        }

        /// <summary>
        /// Вывод предупреждения в окне
        /// </summary>
        public static void TraceWarning(string text, params object[] args)
        {
            TraceWarning(string.Format(text, args));
        }

        #endregion
        #region Write

        private static object writeLock = new object();

        /// <summary>
        /// Вывод текста в файл
        /// </summary>
        public static void WriteLog(string text)
        {
#if IN_EDITOR && FULL_OUTPUT
            Debug.Print("[WriteLog] {0}", text);
#endif
            lock (writeLock)
            {
                if (output == null)
                {
                    try
                    {
                        output = new StreamWriter(outputPath);
                    }
                    catch (DirectoryNotFoundException)
                    {
                        TraceLog(string.Format("Директория для файлов вывода отсутствует {0}", outputPath));
                        return;
                    }
                    catch (Exception ex)
                    {
#if IN_EDITOR
                        Debug.Print(ex.ToString());
#endif
                        return;
                    }

                }
                try
                {
                    output.WriteLine("[{0}] {1}", DateTime.Now.ToShortTimeString(), text);
                    output.Flush();
                }
                catch
                {
                    try
                    {
                        _outputPath = null;
                        output = new StreamWriter(outputPath);
                        output.WriteLine("[{0}] {1}", DateTime.Now.ToShortTimeString(), text);
                        output.Flush();
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Вывод ошибки в файл
        /// </summary>
        public static void WriteError(string text)
        {
#if IN_EDITOR && FULL_OUTPUT
            Debug.Print("[WriteError] {0}", text);
#endif
            lock (writeLock)
            {
                if (errors == null)
                {
                    try
                    {
                        errors = new StreamWriter(errorsPath);
                    }
                    catch (DirectoryNotFoundException)
                    {
                        BasicWindow.TraceInfo("Директория для файлов вывода отсутствует {0}", errorsPath);
                        return;
                    }
                    catch (Exception ex)
                    {
#if IN_EDITOR
                        Debug.Print(ex.ToString());
#endif
                        return;
                    }
                }
                try
                {
                    errors.WriteLine("[{0}] {1}", DateTime.Now.ToShortTimeString(), text);
                    errors.Flush();
                }
                catch
                {
                    try
                    {
                        _errorsPath = null;
                        errors = new StreamWriter(errorsPath);
                        errors.WriteLine("[{0}] {1}", DateTime.Now.ToShortTimeString(), text);
                        errors.Flush();
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Вывод ошибки в файл
        /// </summary>
        public static void WriteError(string text, params object[] args)
        {
            WriteError(string.Format(text, args));
        }

        /// <summary>
        /// Вывод ошибки в файл
        /// </summary>
        public static void WriteError(Exception ex)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(ex.ToString());
            builder.AppendLine(ex.StackTrace);
            builder.AppendLine(ex.Source);
            if(ex.TargetSite != null)
            {
                builder.AppendLine(ex.TargetSite.ToString());
            }
            if(ex.InnerException != null)
            {
                builder.AppendLine(ex.InnerException.ToString());
            }            
            WriteError(builder.ToString());
        }

        #endregion

        /// <summary>
        /// Вывод текста
        /// </summary>
        public static void Log(string text)
        {
            TraceLog(text);
            WriteLog(text);            
        }

        /// <summary>
        /// Вывод текста
        /// </summary>
        public static void LogFormat(string text, params object[] args)
        {
            var str = string.Format(text, args);
            TraceLog(str);
            WriteLog(str);
        }
                
        /// <summary>
        /// Вывод ошибки
        /// </summary>
        public static void Error(string text)
        {
            TraceError(text);
            WriteError(text);          
        }

        /// <summary>
        /// Вывод ошибки
        /// </summary>
        public static void ErrorFormat(string text, params object[] args)
        {
            var str = string.Format(text, args);
            TraceError(str);
            WriteError(str);
        }

        /// <summary>
        /// Вывод предупреждения
        /// </summary>
        public static void Warning(string text)
        {
            Log(text);
            State = States.Warning;
        }

        /// <summary>
        /// Вывод предупреждения
        /// </summary>
        public static void WarningFormat(string text, params object[] args)
        {
            LogFormat(text, args);
            State = States.Warning;
        }

        /// <summary>
        /// Вывод ошибки
        /// </summary>
        public static void Error(Exception ex, string text, params object[] args)
        {
            WriteError(ex);
            TraceError(text, args);
        }
    }
}
