﻿using OKSPWatcher.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace OKSPWatcher
{
    /// <summary>
    /// Стандартное окно приложения
    /// </summary>
    public class BasicWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Свойство окна изменилось
        /// </summary>
        protected void OnPropertyChanged(string id)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(id));
            }
        }

        /// <summary>
        /// Минимизировать окно
        /// </summary>
        public virtual void Minimize()
        {
            WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// Вывести информацию снизу окна
        /// </summary>
        public static void TraceInfo(string text)
        {
            if(Current != null)
            {
                Current.OutputText = text;
            }            
        }

        /// <summary>
        /// Вывести информацию снизу окна
        /// </summary>
        public static void TraceInfo(string text, params object[] args)
        {
            TraceInfo(string.Format(text, args));
        }

        /// <summary>
        /// Объект для отработки фоновых задач
        /// </summary>
        protected BackgroundWorker worker = new BackgroundWorker() { WorkerReportsProgress = true };

        bool helpShown = false;
        /// <summary>
        /// Кнопка помощи показана?
        /// </summary>
        public bool HelpShown
        {
            get
            {
                return helpShown;
            }
            set
            {
                helpShown = value;
                OnPropertyChanged("HelpShown");
            }
        }

        bool isLocked;
        /// <summary>
        /// Объект заблокирован пользователем
        /// </summary>
        public bool IsLocked
        {
            get
            {
                return isLocked;
            }
            set
            {
                isLocked = value;
                OnPropertyChanged("IsLocked");
            }
        }

        bool dbTrans = false;
        /// <summary>
        /// Производится транзакция с БД?
        /// </summary>
        public bool DBTrans
        {
            get
            {
                return dbTrans;
            }
            set
            {
                dbTrans = value;
                OnPropertyChanged("DBTrans");
            }
        }

        bool warn = false;
        /// <summary>
        /// Предупреждение
        /// </summary>
        public bool Warn
        {
            get
            {
                return warn;
            }
            set
            {
                warn = value;
                OnPropertyChanged("Warn");
            }
        }

        bool error = false;
        /// <summary>
        /// Ошибка
        /// </summary>
        public bool Error
        {
            get
            {
                return error;
            }
            set
            {
                error = value;
                OnPropertyChanged("Error");
            }
        }

        bool topMenuEnabled = true;
        /// <summary>
        /// Верхнее меню доступно?
        /// </summary>
        public bool TopMenuEnabled
        {
            get
            {
                return topMenuEnabled;
            }
            set
            {
                topMenuEnabled = value;
                OnPropertyChanged("TopMenuEnabled");
            }
        }

        bool loadShown = false;
        /// <summary>
        /// Показывается панель загрузки?
        /// </summary>
        public bool LoadShown
        {
            get
            {
                return loadShown;
            }
            set
            {
                loadShown = value;
                OnPropertyChanged("LoadShown");
            }
        }

        bool updateShown = false;
        /// <summary>
        /// Показывается обновление данных?
        /// </summary>
        public bool UpdateShown
        {
            get
            {
                return updateShown;
            }
            set
            {
                updateShown = value;
                OnPropertyChanged("UpdateShown");
            }
        }

        double progress;
        /// <summary>
        /// Прогресс обновления
        /// </summary>
        public double Progress
        {
            get
            {
                return progress;
            }
            set
            {
                progress = value * 100;
                OnPropertyChanged("Progress");
            }
        }

        bool mustClose = false;

        protected override void OnInitialized(EventArgs e)
        {
            if (windows.ContainsKey(Id))
            {
                windows[Id].Activate();
                mustClose = true;
                Close();
                return;
            }
            windows.Add(Id, this);
            MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
            MaxWidth = SystemParameters.MaximizedPrimaryScreenWidth;
            current = this;
            base.OnInitialized(e);
            worker.DoWork += BackgroundWork;
            worker.RunWorkerCompleted += BackgroundCompleted;
        }

        string outputText;
        /// <summary>
        /// Текст для вывода
        /// </summary>
        public string OutputText
        {
            get
            {
                return outputText;
            }
            set
            {
                outputText = value;
                OnPropertyChanged("OutputText");
            }
        }

        string updateText;
        /// <summary>
        /// Текст обновления
        /// </summary>
        public string UpdateText
        {
            get
            {
                return updateText;
            }
            set
            {
                updateText = value;
                OnPropertyChanged("UpdateText");
            }
        }

        /// <summary>
        /// Показать окно
        /// </summary>
        public new void Show()
        {
            if (!mustClose)
            {
                base.Show();
            }
        }

        protected virtual void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadShown = false;
            BackgroundHandler.Unregister(worker);
        }

        protected virtual void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            LoadShown = true;
            BackgroundHandler.Register(worker);
        }

        /// <summary>
        /// Показать помощь по окну
        /// </summary>
        public virtual void ShowHelp()
        {

        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            current = this;
        }

        static BasicWindow current;
        /// <summary>
        /// Текущее окно
        /// </summary>
        public static BasicWindow Current
        {
            get
            {
                return current;
            }
        }

        /// <summary>
        /// Обновление данных было отменено
        /// </summary>
        public virtual void StopUpdate()
        {
            if (IsActive)
            {
                UpdateShown = false;
            }            
        }

        /// <summary>
        /// Список всех окон
        /// </summary>
        protected static Dictionary<string, BasicWindow> windows = new Dictionary<string, BasicWindow>();

        /// <summary>
        /// Идентификатор окна
        /// </summary>
        public string Id
        {
            get
            {
                return Tag != null ? Tag.ToString() : "";
            }
        }

        /// <summary>
        /// Получить идентификатор текущего окна
        /// </summary>
        public static string GetWindowId()
        {
            return current != null ? current.Id : "";
        }

        protected override void OnClosed(EventArgs e)
        {            
            if (!mustClose && windows.ContainsKey(Id))
            {
                windows.Remove(Id);
            }
            base.OnClosed(e);
            StopUpdate();
        }
    }
}
