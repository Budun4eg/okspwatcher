﻿using OKSPWatcher.Apps;
using OKSPWatcher.Secondary;
using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Основное приложение
    /// </summary>
    public class MainApp : NotifyObject
    {
        private static object root = new object();

        static MainApp instance;

        protected MainApp() { }

        /// <summary>
        /// Текущий экземпляр приложения
        /// </summary>
        public static MainApp Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new MainApp();
                        }
                    }                    
                }
                return instance;
            }
        }

        User currentUser;
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        public User CurrentUser
        {
            get
            {
                return currentUser;
            }
            set
            {
                currentUser = value;
                OnPropertyChanged("CurrentUser");
                SyncApps();
            }
        }

        SafeObservableCollection<AppItem> apps = new SafeObservableCollection<AppItem>();
        /// <summary>
        /// Список доступных для пользователя приложений
        /// </summary>
        public SafeObservableCollection<AppItem> Apps
        {
            get
            {
                return apps;
            }
        }

        /// <summary>
        /// Синхронизировать доступные приложения
        /// </summary>
        public void SyncApps()
        {
            apps.Clear();
            foreach(var app in AppManager.Instance.Available(CurrentUser))
            {
                apps.Add(app);
            }
        }

        /// <summary>
        /// Загрузить основное приложение
        /// </summary>
        public void Load(string login = null)
        {
#if IN_EDITOR
            debugMode = true;
#endif
            string toLog = login != null ? login : debugMode ? debugLogin : Environment.UserName;
            var user = UserManager.Instance.ByLogin(toLog, true).Single;            
#if TEST_MODE
            CurrentUser = new User();
            CurrentUser.Name = "Тестовый режим";
#else
            CurrentUser = user != null ? user : new User(toLog);
#endif
            if (!CurrentUser.Data.FirstHelpShown)
            {
                CurrentUser.Data.FirstHelpShown = true;
                NotifyWindow.ShowNote("Для получения помощи используйте кнопку \"?\" сверху текущего открытого окна (при её наличии)");
                CurrentUser.Save();
            }
        }

        bool debugMode = false;
        string debugLogin = "056nvg0128";
        //056aks0117
        //056gyo0120
        //056nvg0128
        //056mab0005
    }
}
