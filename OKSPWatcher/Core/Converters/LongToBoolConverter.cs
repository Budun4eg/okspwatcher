﻿using System;
using System.Globalization;
using System.Windows.Data;
using OKSPWatcher.Core;

namespace OKSPWatcher
{
    public class LongToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((long)value).ToBool();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
