﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace OKSPWatcher
{
    public class BoolToColorConverter : IValueConverter
    {
        static SolidColorBrush pos;
        static SolidColorBrush neg;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (pos == null)
            {
                pos = Application.Current.FindResource("BackgroundBrush") as SolidColorBrush;
            }
            if (neg == null)
            {
                neg = Application.Current.FindResource("SelectedBrush") as SolidColorBrush;
            }
            return (bool)value ? neg : pos;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
