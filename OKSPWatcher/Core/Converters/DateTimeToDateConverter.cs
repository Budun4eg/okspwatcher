﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace OKSPWatcher
{
    public class DateTimeToDateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime date = (DateTime)value;
            if(date.Ticks <= 0)
            {
                return "-";
            }
            else
            {
                return date.ToShortDateString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
