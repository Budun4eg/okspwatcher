﻿using OKSPWatcher.Core;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace OKSPWatcher
{
    public class ListBackgroundConverter : IValueConverter
    {
        static SolidColorBrush pos;
        static SolidColorBrush neg;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (pos == null)
                {
                    pos = Application.Current.FindResource("BackgroundBrush") as SolidColorBrush;
                }
                if (neg == null)
                {
                    neg = Application.Current.FindResource("ListItemBrush") as SolidColorBrush;
                }
                var item = value as DependencyObject;
                var listItem = item.GetVisualParent<ListViewItem>();
                ListView listView = ItemsControl.ItemsControlFromItemContainer(listItem) as ListView;

                int index = listView.ItemContainerGenerator.IndexFromContainer(listItem);

                if (index % 2 == 0)
                {
                    return pos;
                }
                else
                {
                    return neg;
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                return pos;
            }            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
