﻿using OKSPWatcher.Core;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OKSPWatcher
{
    public class SplitStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var spl = parameter.ToString().Split(new char[] { ':' });
                if (spl.Length != 2) {
                    Output.WriteError("Неправильная строка ввода конвертера {0}", parameter);
                    return null;
                }
                if ((bool)value)
                {
                    return spl[0];
                }
                else
                {
                    return spl[1];
                }
            }catch(Exception ex)
            {
                Output.WriteError(ex);
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
