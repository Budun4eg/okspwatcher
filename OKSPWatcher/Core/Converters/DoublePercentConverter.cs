﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace OKSPWatcher
{
    public class DoublePercentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (double)value;
            val /= 100;
            if (val <= 0)
            {
                return "-";
            }
            else
            {
                return string.Format("{0:P1}", val);
            }            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
