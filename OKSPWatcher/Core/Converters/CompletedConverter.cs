﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace OKSPWatcher
{
    public class CompletedConverter : IValueConverter
    {
        static SolidColorBrush pos;
        static SolidColorBrush neg;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (pos == null)
            {
                pos = Application.Current.FindResource("ForegroundBrush") as SolidColorBrush;
            }
            if (neg == null)
            {
                neg = Application.Current.FindResource("CompletedBrush") as SolidColorBrush;
            }
            return (bool)value ? neg : pos;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
