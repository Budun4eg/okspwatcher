﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OKSPWatcher
{
    public class ThicknessReverseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var rect = (Thickness)value;
            return new Thickness(-rect.Left, -rect.Top, -rect.Right, -rect.Bottom);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
