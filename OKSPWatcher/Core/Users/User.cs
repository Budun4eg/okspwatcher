﻿//using LingvoNET;
//using OKSPWatcher.Core.Apps;
//using OKSPWatcher.Core.Departments;
//using OKSPWatcher.Core.Products;
//using System;
//using System.Collections.Generic;

//namespace OKSPWatcher.Core.Users
//{
//    /// <summary>
//    /// Пользователь
//    /// </summary>
//    public class User : DBObject
//    {
//        string name = "";
//        /// <summary>
//        /// Имя
//        /// </summary>
//        public string Name
//        {
//            get
//            {
//                return name;
//            }
//            set
//            {
//                name = value;
//                OnPropertyChanged("Name");
//                OnPropertyChanged("ShortName");
//            }
//        }

//        string loginName = "";
//        /// <summary>
//        /// Имя, используемое при входе в систему
//        /// </summary>
//        public string LoginName
//        {
//            get
//            {
//                return loginName;
//            }
//            set
//            {
//                loginName = value;
//                OnPropertyChanged("LoginName");
//            }
//        }

//        long lastNote = 0;
//        /// <summary>
//        /// Последняя просмотренная информация
//        /// </summary>
//        public long LastNote {
//            get {
//                return lastNote;
//            }
//        }

//        long userType = 0;
//        /// <summary>
//        /// Тип пользователя
//        /// </summary>
//        public long UserType
//        {
//            get
//            {
//                if (UserManager.PredefinedRules.ContainsKey(loginName) && UserManager.PredefinedRules[loginName] > userType)
//                {
//                    return UserManager.PredefinedRules[loginName];
//                }
//                else
//                {
//                    return userType;
//                }
//            }
//            set
//            {
//                userType = value;
//                OnPropertyChanged("UserType");
//            }
//        }

//        string searchName;
//        /// <summary>
//        /// Строка для поиска пользователя
//        /// </summary>
//        public string SearchName
//        {
//            get
//            {
//                return searchName;
//            }
//            set
//            {
//                searchName = value;
//                OnPropertyChanged("SearchName");
//            }
//        }

//        bool firstHelpShown;
//        /// <summary>
//        /// Начальная помощь была показана?
//        /// </summary>
//        public bool FirstHelpShown
//        {
//            get
//            {
//                return firstHelpShown;
//            }
//            set
//            {
//                firstHelpShown = value;
//                OnPropertyChanged("FirstHelpShown");
//            }
//        }

//        public User(Dictionary<string, object> pair)
//        {
//            Load(pair);
//        }

//        /// <summary>
//        /// Сокращенный вариант имени
//        /// </summary>
//        public string ShortName
//        {
//            get
//            {
//                if(Name != null && Name != "")
//                {
//                    var spl = Name.Split(new char[] { ' ' });
//                    if(spl.Length >= 3)
//                    {
//                        try
//                        {
//                            return string.Format("{0}.{1}. {2}", spl[1][0], spl[2][0], spl[0]);
//                        }
//                        catch(Exception ex)
//                        {
//                            Output.Error(ex);
//                            return Name;
//                        }                        
//                    }
//                    else
//                    {
//                        return Name;
//                    }                    
//                }
//                else
//                {
//                    return "";
//                }                
//            }
//        }

//        /// <summary>
//        /// Сокращенный вариант имени в дательном падеже
//        /// </summary>
//        public string ShortNameDative
//        {
//            get
//            {
//                if(Name != null && Name != "")
//                {
//                    var spl = Name.Split(new char[] { ' ' });
//                    if(spl.Length >= 3)
//                    {
//                        try {
//                            return string.Format("{0}.{1}. {2}", spl[1][0], spl[2][0], Nouns.FindSimilar(spl[0])[Case.Dative]);
//                        }catch(Exception ex)
//                        {
//                            Output.Error(ex);
//                            return Name;
//                        }                        
//                    }
//                    else
//                    {
//                        return Name;
//                    }                    
//                }
//                else
//                {
//                    return "";
//                }                
//            }
//        }

        
//        Dictionary<string, object> savedData;
//        /// <summary>
//        /// Сохраненные данные пользователя
//        /// </summary>
//        public Dictionary<string, object> SavedData
//        {
//            get
//            {
//                return savedData;
//            }
//        }

//        public override void Load(Dictionary<string, object> data)
//        {
//            base.Load(data);
//            try
//            {
//                if (data.ContainsKey("Name") && data["Name"] != DBNull.Value)
//                {
//                    Name = (string)data["Name"];
//                    SearchName = name.Split(new char[] { ' ' })[0];
//                }
//                else
//                {
//                    Output.Error("У пользователя #{0} отсутствует параметр Name", id);
//                }
//                if (data.ContainsKey("SearchName") && data["SearchName"] != DBNull.Value)
//                {
//                    SearchName = (string)data["SearchName"];
//                }
//                if (data.ContainsKey("LoginName") && data["LoginName"] != DBNull.Value)
//                {
//                    LoginName = (string)data["LoginName"];
//                }
//                else
//                {
//                    Output.Error("У пользователя #{0} отсутствует параметр LoginName", id);
//                }
//                if (data.ContainsKey("LastNote") && data["LastNote"] != DBNull.Value)
//                {
//                    lastNote = (long)data["LastNote"];
//                }
//                else
//                {
//                    Output.Error("У пользователя #{0} отсутствует параметр LastNote", id);
//                }
//                if (data.ContainsKey("UserType") && data["UserType"] != DBNull.Value)
//                {
//                    UserType = (long)data["UserType"];
//                }
//                else
//                {
//                    Output.Error("У пользователя #{0} отсутствует параметр UserType", id);
//                }
//                if(data.ContainsKey("FirstHelpShown") && data["FirstHelpShown"] != DBNull.Value)
//                {
//                    FirstHelpShown = ((long)data["FirstHelpShown"]).ToBool();
//                }
//                else
//                {
//                    Output.Error("У пользователя #{0} отсутствует параметр FirstHelpShown", id);
//                }
//            }
//            catch (Exception ex)
//            {
//                Output.Error(ex);
//            }
//            savedData = data;
//        }

//        public User(string login)
//        {
//            name = "Пользователь отсутствует";
//            loginName = login;
//            userType = -1;
//        }

//        /// <summary>
//        /// Синхронизировать основные системы
//        /// </summary>
//        public void Sync()
//        {            
//            if (savedData.ContainsKey("Departments") && savedData["Departments"] != DBNull.Value)
//            {
//                DepartmentManager.Load(savedData["Departments"], true);
//            }
//            else
//            {
//                Output.Error("У пользователя #{0} отсутствует параметр Departments", id);
//            }
//            AppManager.Load(true);                       
//        }

//        /// <summary>
//        /// Дополнительная синхронизация
//        /// </summary>
//        public void LateSync()
//        {
//            UserManager.Load(true, true);
//            if (savedData.ContainsKey("Products") && savedData["Products"] != DBNull.Value)
//            {
//                ProductManager.Load(savedData["Products"], true);
//            }
//            else
//            {
//                Output.Error("У пользователя #{0} отсутствует параметр Products", id);
//            }
//        }

//        public User()
//        {
            
//        }

//        protected override bool CreateNew()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Users WHERE LoginName = '{0}'", loginName);
//            if (check != null)
//            {
//                errorId = (long)check;
//                errorString = "Dublicate";
//                return false;
//            }
//            SQL.ExecuteId("INSERT INTO Users (LoginName, Name, UserType, LastNote, Departments, Products, SearchName, FirstHelpShown) VALUES ('{0}','{1}',{2},{3},'{4}','{5}', '{6}', {7})",
//                loginName.Check(), name.Check(), userType, lastNote, DepartmentManager.Departments.Serialize(), ProductManager.Products.Serialize(), searchName.Check(), FirstHelpShown.ToLong());
            
//            base.CreateNew();
//            UserManager.Add(this);
//            return true;
//        }

//        protected override bool Update()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Users WHERE LoginName = '{0}'", loginName);
//            if (check != null)
//            {
//                var usrId = (long)check;
//                if (usrId != id)
//                {
//                    errorString = "Dublicate";
//                    return false;
//                }
//            }
//            base.Update();
//            SQL.Execute("UPDATE Users SET LoginName = '{0}', Name = '{1}', UserType = {2}, LastNote = {3}, Departments = '{4}', Products = '{5}', SearchName = '{6}', FirstHelpShown = {7} WHERE Id = {8}",
//                loginName.Check(), name.Check(), userType, lastNote, DepartmentManager.Departments.Serialize(), ProductManager.Products.Serialize(), searchName.Check(), FirstHelpShown.ToLong(), id);
//            savedData = SQL.ExecuteRead("SELECT * FROM Users WHERE Id = {0}", id)[0];
//            return true;
//        }

//        public override void Delete()
//        {
//            base.Delete();
//            UserManager.Delete(this);
//            SQL.Execute("DELETE FROM Users WHERE Id={0}",id);
//        }

//        public override void SaveState()
//        {
//            base.SaveState();
//            AddStateItem("Name", name);
//            AddStateItem("LoginName", loginName);
//            AddStateItem("UserType", userType);
//            AddStateItem("SearchName", searchName);
//        }

//        public override void LoadState()
//        {
//            if (hasSavedState)
//            {
//                name = (string)savedState["Name"];
//                searchName = (string)savedState["SearchName"];
//                loginName = (string)savedState["LoginName"];
//                userType = (long)savedState["UserType"];
//                base.LoadState();
//            }
//        }
//    }
//}
