﻿//using OKSPWatcher.Core.Departments;
//using OKSPWatcher.Core.Products;
//using System;
//using System.Collections.Generic;

//namespace OKSPWatcher.Core.Users
//{
//    /// <summary>
//    /// Объект для редактирования пользователя
//    /// </summary>
//    public class UserEditConverter : NotifyObject
//    {        
//        User toEdit;
//        /// <summary>
//        /// Пользователь для редактирования
//        /// </summary>
//        public User ToEdit
//        {
//            get
//            {
//                return toEdit;
//            }
//        }

//        SafeObservableCollection<DepartmentEnablerConverter> deps = new SafeObservableCollection<DepartmentEnablerConverter>();
//        /// <summary>
//        /// Список отделов
//        /// </summary>
//        public SafeObservableCollection<DepartmentEnablerConverter> Deps
//        {
//            get
//            {
//                return deps;
//            }
//        }

//        SafeObservableCollection<ProductEnablerConverter> prds = new SafeObservableCollection<ProductEnablerConverter>();
//        /// <summary>
//        /// Список изделий
//        /// </summary>
//        public SafeObservableCollection<ProductEnablerConverter> Prds
//        {
//            get
//            {
//                return prds;
//            }
//        }

//        public UserEditConverter(User _toEdit)
//        {
//            toEdit = _toEdit;
//            List<long> depList = new List<long>();

//            if (toEdit.SavedData.ContainsKey("Departments") && toEdit.SavedData["Departments"] != DBNull.Value)
//            {
//                depList = toEdit.SavedData["Departments"].DeserializeLong();
//            }
//            else
//            {
//                Output.Error("У пользователя #{0} отсутствует параметр Departments", toEdit.Id);
//            }

//            foreach (Department dep in DepartmentManager.Departments)
//            {
//                deps.Add(new DepartmentEnablerConverter(dep, depList.Contains(dep.Id)));
//            }

//            List<long> prdList = new List<long>();

//            if (toEdit.SavedData.ContainsKey("Products") && toEdit.SavedData["Products"] != DBNull.Value)
//            {
//                prdList = toEdit.SavedData["Products"].DeserializeLong();
//            }
//            else
//            {
//                Output.Error("У пользователя #{0} отсутствует параметр Products", toEdit.Id);
//            }

//            foreach (Product prd in ProductManager.Products)
//            {
//                prds.Add(new ProductEnablerConverter(prd, prdList.Contains(prd.Id)));
//            }
//        }

//        /// <summary>
//        /// Сохранить измененное состояние пользователя
//        /// </summary>
//        public void Save()
//        {
//            toEdit.Save();
//            var dep = deps.SerializeToggle();
//            var prd = prds.SerializeToggle();

//            if (toEdit.SavedData.ContainsKey("Departments"))
//            {
//                toEdit.SavedData["Departments"] = dep;
//            }
//            else
//            {
//                toEdit.SavedData.Add("Departments", dep);
//            }

//            if (toEdit.SavedData.ContainsKey("Products"))
//            {
//                toEdit.SavedData["Products"] = prd;
//            }
//            else
//            {
//                toEdit.SavedData.Add("Products", prd);
//            }

//            SQL.Execute("UPDATE Users SET Departments = '{0}', Products = '{1}' WHERE Id = {2}", dep, prd, toEdit.Id);
//        }
//    }
//}
