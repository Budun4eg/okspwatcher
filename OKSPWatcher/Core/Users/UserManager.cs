﻿//using OKSPWatcher.Core.Departments;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;

//namespace OKSPWatcher.Core.Users
//{
//    /// <summary>
//    /// Менеджер управления пользователями
//    /// </summary>
//    public static class UserManager
//    {
//        /// <summary>
//        /// Список доступных пользователей
//        /// </summary>
//        public static SafeObservableCollection<User> Users = new SafeObservableCollection<User>();

//        /// <summary>
//        /// Доступ к пользователю по ключу
//        /// </summary>
//        public static Dictionary<long, User> IdUsers = new Dictionary<long, User>();

//        /// <summary>
//        /// Доступ к пользователю по имени
//        /// </summary>
//        public static Dictionary<string, User> UsersName = new Dictionary<string, User>();

//        /// <summary>
//        /// Список предустановленных правил
//        /// </summary>
//        public static Dictionary<string, int> PredefinedRules = new Dictionary<string, int>() {
//            {"056nvg0128", 100},
//            {"001", 100},
//            {"020lsi0721",100},
//            {"056erf0119",100}
//        };

//        /// <summary>
//        /// Список переноса пользователей
//        /// </summary>
//        static Dictionary<string, string> userDefines = new Dictionary<string, string>();

//        /// <summary>
//        /// Переименовать пользователя
//        /// </summary>
//        public static string DefineUser(this string oldName)
//        {
//            return userDefines.ContainsKey(oldName) ? userDefines[oldName] : oldName;
//        }

//        /// <summary>
//        /// Менеджер загружен?
//        /// </summary>
//        static bool loaded;

//        /// <summary>
//        /// Загрузить менеджер в соответствии с ограничениями
//        /// </summary>
//        public static void Load(bool restriction = true, bool forced = false) {

//            if(!loaded || forced)
//            {
//                List<Dictionary<string, object>> usersData;
//                if (restriction)
//                {
//                    usersData = SQL.ExecuteRead("SELECT * FROM Users WHERE {0} ORDER BY Name", DepartmentManager.Departments.CreateSearch("Departments"));
//                }
//                else
//                {
//                    usersData = SQL.ExecuteRead("SELECT * FROM Users ORDER BY Name");
//                }

//                foreach (Dictionary<string, object> row in usersData)
//                {
//                    var user = new User(row);
//                    Add(user);
//                }
//                loaded = true;
//            }            
//        }

//        /// <summary>
//        /// Доступ к пользователям по идентификатору
//        /// </summary>
//        public static Dictionary<string, User> LoginNames = new Dictionary<string, User>();

//        /// <summary>
//        /// Получить пользователя по идентификатору
//        /// </summary>
//        public static User Get(string loginName)
//        {
//            if (LoginNames.ContainsKey(loginName))
//            {
//                return LoginNames[loginName];
//            }
//            else
//            {
//                var usersData = SQL.ExecuteRead("SELECT * FROM Users WHERE LoginName = '{0}'", loginName);
//                if(usersData.Count == 1)
//                {
//                    var user = new User(usersData[0]);
//                    Add(user);
//                    return user;
//                }
//                else if(usersData.Count > 1)
//                {
//                    Output.Error("Дублирование пользователя с логином {0}", loginName);
//                }
//                else
//                {
//                    Output.Error("Отсутствует пользователь с логином {0}", loginName);
//                }
//                return new User(loginName);
//            }
//        }

//        /// <summary>
//        /// Добавить пользователя
//        /// </summary>
//        public static void Add(User user)
//        {
//            if (LoginNames.ContainsKey(user.LoginName)) {
//                return;
//            }
//            else
//            {
//                LoginNames.Add(user.LoginName, user);
//            }
//            if (userDefines.ContainsKey(user.SearchName))
//            {
//                user.SearchName = user.ShortName;
//                if (userDefines.ContainsKey(user.SearchName))
//                {
//                    Output.Error("[UserManager] Дублирование поискового имени {0}", user.SearchName);

//                    while (userDefines.ContainsKey(user.SearchName))
//                    {
//                        user.SearchName += "_";
//                    }
//                }
//            }
//            userDefines.Add(user.SearchName, user.Name);
            
//            if (IdUsers.ContainsKey(user.Id))
//            {
//                Output.Error("Дублирование пользователя с идентификатором {0}", user.Id);
//                return;
//            }
//            else
//            {
//                IdUsers.Add(user.Id, user);
//            }
//            if (UsersName.ContainsKey(user.Name))
//            {
//                Output.Error("Дублирование пользователя с наименованием {0}", user.Name);
//            }
//            else
//            {
//                UsersName.Add(user.Name, user);
//            }
//            Users.Add(user);
//        }

//        /// <summary>
//        /// Удалить пользователя
//        /// </summary>
//        public static void Delete(User user)
//        {
//            if (userDefines.ContainsKey(user.SearchName))
//            {
//                userDefines.Remove(user.SearchName);
//            }
//            if (Users.Contains(user))
//            {
//                Users.Remove(user);
//            }
//            if (LoginNames.ContainsKey(user.LoginName))
//            {
//                LoginNames.Remove(user.LoginName);
//            }
//            if (IdUsers.ContainsKey(user.Id))
//            {
//                IdUsers.Remove(user.Id);
//            }
//            if (UsersName.ContainsKey(user.Name))
//            {
//                UsersName.Remove(user.Name);
//            }          
//        }

//        /// <summary>
//        /// Очистить менеджер
//        /// </summary>
//        public static void Clear(bool saveAppUser = true)
//        {
//            userDefines.Clear();
//            Users.Clear();
//            LoginNames.Clear();
//            IdUsers.Clear();
//            UsersName.Clear();
//            if (saveAppUser)
//            {
//                Add(MainApp.CurrentUser);
//            }
//        }
//    }
//}
