﻿using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System;

/// <summary>
/// v.2 opt
/// @ Vsevolod Nikitin
/// </summary>

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Настройки приложения
    /// </summary>
    public static class AppSettings
    {
        /// <summary>
        /// Создать структуру БД
        /// </summary>
        public static void GenerateDBStructure()
        {
            SQL.Get("Settings").Execute(@"CREATE TABLE IF NOT EXISTS Settings (
                                                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                Key TEXT,
                                                LongValue INTEGER DEFAULT 0,
                                                StringValue TEXT DEFAULT '',
                                                UpdateDate INTEGER
                                                )");
            SQL.Get("Settings").Execute(@"CREATE INDEX IF NOT EXISTS Settings_Key ON Settings(Key)");
        }

        /// <summary>
        /// Путь до БД
        /// </summary>
        public static string BaseFolder {
            get {
                return Properties.Settings.Default["BaseFolder"].ToString();
            }
            set {
                Properties.Settings.Default["BaseFolder"] = value;
                Properties.Settings.Default.Save();
            }
        }

        static string outputFolder = string.Format("{0}\\log\\", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
        /// <summary>
        /// Путь до папки с выводом
        /// </summary>
        public static string OutputFolder
        {
            get
            {
                return outputFolder;
            }
        }

        static string templatesFolder = string.Format("{0}\\templates\\", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
        /// <summary>
        /// Путь до папки с шаблонами
        /// </summary>
        public static string TemplatesFolder
        {
            get
            {
                return templatesFolder;
            }
        }

        static string noImagePath;
        /// <summary>
        /// Ошибка загрузки изображения
        /// </summary>
        public static string NoImagePath
        {
            get
            {
                if(noImagePath == null)
                {
                    noImagePath = templatesFolder + "NoImage.png";
                }
                return noImagePath;
            }
        }

        static string tempFolder = string.Format("{0}\\temp\\", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
        /// <summary>
        /// Путь до временной папки
        /// </summary>
        public static string TempFolder
        {
            get
            {
                return tempFolder;
            }
        }

        /// <summary>
        /// Стандартная кодировка
        /// </summary>
        public static Encoding Encode {
            get {
                return Encoding.UTF8;
            }
        }

        static string splitter = "|s|";
        /// <summary>
        /// Разделитель сериализатора
        /// </summary>
        public static string Splitter
        {
            get
            {
                return splitter;
            }
        }

        static string splitPreceed;
        /// <summary>
        /// Начальный разделитель сериализатора
        /// </summary>
        public static string SplitPreceed
        {
            get
            {
                if(splitPreceed == null && splitter.Length > 0)
                {
                    splitPreceed = splitter[splitter.Length - 1].ToString();
                }
                return splitPreceed;
            }
        }

        static char splitPreceedC;
        /// <summary>
        /// Начальный разделитель сериализатора
        /// </summary>
        public static char SplitPreceedC
        {
            get
            {
                if (splitPreceedC == '\0' && splitter.Length > 0)
                {
                    splitPreceedC = splitter[splitter.Length - 1];
                }
                return splitPreceedC;
            }
        }

        /// <summary>
        /// Получить строковый параметр из базы
        /// </summary>
        public static string GetString(string key)
        {
            var toReturn = SQL.Get("Settings").ExecuteScalar("SELECT StringValue FROM Settings WHERE Key = '{0}'", key);
            if (toReturn != null) {
                return toReturn.ToString();
            }
            else
            {
#if IN_EDITOR
                Output.ErrorFormat("Отсутствует параметр в настройках {0}", key);
#endif
                return "";
            }
        }

        /// <summary>
        /// Получить параметр из базы
        /// </summary>
        public static double GetDouble(string key)
        {
            return GetString(key).ToDouble();
        }

        /// <summary>
        /// Получить параметр из базы
        /// </summary>
        public static bool GetBool(string key)
        {
            return GetDouble(key) == 1;
        }

        /// <summary>
        /// Установить строковый параметр в базе
        /// </summary>
        public static void SetString(string key, string value)
        {
            var toReturn = SQL.Get("Settings").ExecuteScalar("SELECT * FROM Settings WHERE Key = '{0}'", key);
            if (toReturn != null)
            {
                SQL.Get("Settings").Execute("UPDATE Settings SET StringValue = '{0}' WHERE Key = '{1}'", value.Check(), key);
            }
            else
            {
                SQL.Get("Settings").Execute("INSERT INTO Settings (Key, StringValue) VALUES ('{0}','{1}')", key, value.Check());
            }
        }

        /// <summary>
        /// Установить параметр в базе
        /// </summary>
        public static void SetDouble(string key, double value)
        {
            SetString(key, value.ToString());
        }

        /// <summary>
        /// Установить параметр в базе
        /// </summary>
        public static void SetBool(string key, bool value)
        {
            SetDouble(key, value ? 1 : 0);
        }

        /// <summary>
        /// Получить числовой параметр из базы
        /// </summary>
        public static long GetLong(string key)
        {
            var toReturn = SQL.Get("Settings").ExecuteScalar("SELECT LongValue FROM Settings WHERE Key = '{0}'", key);
            if (toReturn != null)
            {
                return (long)toReturn;
            }
            else
            {
#if IN_EDITOR
                Output.ErrorFormat("Отсутствует параметр в настройках {0}", key);
#endif
                return default(long);
            }
        }

        /// <summary>
        /// Получить числовой параметр из базы
        /// </summary>
        public static int GetInt(string key)
        {
            return (int)GetLong(key);
        }

        /// <summary>
        /// Установить числовой параметр в базе
        /// </summary>
        public static void SetLong(string key, long value)
        {
            var toReturn = SQL.Get("Settings").ExecuteScalar("SELECT * FROM Settings WHERE Key = '{0}'", key);
            if (toReturn != null)
            {
                SQL.Get("Settings").Execute("UPDATE Settings SET LongValue = {0} WHERE Key = '{1}'", value, key);
            }
            else
            {
                SQL.Get("Settings").Execute("INSERT INTO Settings (Key, LongValue) VALUES ('{0}',{1})", key, value);
            }
        }

        /// <summary>
        /// Установить числовой параметр в базе
        /// </summary>
        public static void SetInt(string key, int value)
        {
            SetLong(key, value);
        }

        /// <summary>
        /// Получить набор идентификаторов
        /// </summary>
        public static List<long> GetIds(string key)
        {
            return GetString(key).DeserializeLong();
        }
    }
}
