﻿using System.ComponentModel;

/// <summary>
/// v.2 opt
/// @ Vsevolod Nikitin
/// </summary>

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Объект с возможностью рассылки информации
    /// </summary>
    public class NotifyObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Параметр объекта изменился
        /// </summary>
        public virtual void OnPropertyChanged(string id)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(id));
            }
        }

        protected string name = "";
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        protected string description = "";
        /// <summary>
        /// Описание объекта
        /// </summary>
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }

        bool isSelected;
        /// <summary>
        /// Объект выбран?
        /// </summary>
        public virtual bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        protected string searchString;
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString {
            get
            {
                if(searchString == null)
                {
                    RefreshSearch();
                }
                return searchString;
            }
        }

        /// <summary>
        /// Обновить строку поиска
        /// </summary>
        public virtual void RefreshSearch()
        {

        }
    }
}
