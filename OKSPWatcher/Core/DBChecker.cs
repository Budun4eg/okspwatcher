﻿using OKSPWatcher.Apps;
using OKSPWatcher.Block;
using OKSPWatcher.Chains;
using OKSPWatcher.Changes;
using OKSPWatcher.Changes3D;
using OKSPWatcher.CompletedIn3D;
using OKSPWatcher.CompletedIn3D.StateManager;
using OKSPWatcher.Deps;
using OKSPWatcher.ImageNotation;
using OKSPWatcher.Letters;
using OKSPWatcher.Models;
using OKSPWatcher.Orgs;
using OKSPWatcher.Products;
using OKSPWatcher.Remarks;
using OKSPWatcher.RequestModel;
using OKSPWatcher.Rules;
using OKSPWatcher.Structure;
using OKSPWatcher.Structure.LoodsmanData;
using OKSPWatcher.Texts;
using OKSPWatcher.Users;
using OKSPWatcher.Utilities.Apps.BDBackup;
using OKSPWatcher.Utilities.Apps.CreateSearch;
using OKSPWatcher.Utilities.Apps.PathCheck;
using OKSPWatcher.Worked;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Объект для проверки БД
    /// </summary>
    public static class DBChecker
    {
        /// <summary>
        /// Проверить БД
        /// </summary>
        public static void Check()
        {
            AppSettings.GenerateDBStructure();
            AppManager.Instance.GenerateDBStructure();
            UserManager.Instance.GenerateDBStructure();
            DepManager.Instance.GenerateDBStructure();
            ProductManager.Instance.GenerateDBStructure();
            OrgManager.Instance.GenerateDBStructure();
            WorkedManager.Instance.GenerateDBStructure();
            ModelManager.Instance.GenerateDBStructure();
            ChangeImageManager.Instance.GenerateDBStructure();
            ChangeManager.Instance.GenerateDBStructure();
            Changes3DManager.Instance.GenerateDBStructure();
            ImageAnchorManager.Instance.GenerateDBStructure();
            RemarkManager.Instance.GenerateDBStructure();
            LetterManager.Instance.GenerateDBStructure();
            TextItemManager.Instance.GenerateDBStructure();
            ChainItemManager.Instance.GenerateDBStructure();
            ChainManager.Instance.GenerateDBStructure();
            ProductStructureManager.Instance.GenerateDBStructure();
            StructureNodeManager.Instance.GenerateDBStructure();
            CompletedItemManager.Instance.GenerateDBStructure();
            //PathCheckManager.Instance.GenerateDBStructure();
            StructureStateManager.Instance.GenerateDBStructure();
            RulesManager.Instance.GenerateDBStructure();
            RuleGroupManager.Instance.GenerateDBStructure();
            AdditionalPathManager.Instance.GenerateDBStructure();
            ProductSearchManager.Instance.GenerateDBStructure();
            BlockManager.Instance.GenerateDBStructure();
            AdditionalBackupManager.Instance.GenerateDBStructure();
            LoodsmanObjectManager.Instance.GenerateDBStructure();
            LoodsmanTreeManager.Instance.GenerateDBStructure();
            RequestModelManager.Instance.GenerateDBStructure();
            Output.Log("Структура БД проверена");
        }
    }
}
