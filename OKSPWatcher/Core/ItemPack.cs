﻿using System.Collections.Generic;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Набор элементов из БД
    /// </summary>
    public class ItemPack<T> : NotifyPack<T> where T : DBObject
    {               
        /// <summary>
        /// Идентификаторы данного набора
        /// </summary>
        public List<long> Ids
        {
            get
            {
                if (needRefresh)
                {
                    Refresh();
                }
                List<long> toReturn = new List<long>();
                foreach(var item in items)
                {
                    toReturn.Add(item.Id);
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Набор для сохранения в БД
        /// </summary>
        public string Serialize()
        {
            return Ids.Serialize();
        }

        /// <summary>
        /// Сохраненное состояние
        /// </summary>
        List<long> serialized = new List<long>();

        /// <summary>
        /// Сохранить состояние объекта
        /// </summary>
        public void SaveState()
        {
            serialized.Clear();
            foreach(var id in Ids)
            {
                serialized.Add(id);
            }
        }

        /// <summary>
        /// Загрузить состояние объекта
        /// </summary>
        public void LoadState()
        {
            Sync(serialized);
        }

        /// <summary>
        /// Идентификаторы для синхронизирования
        /// </summary>
        protected List<long> idsToSync = new List<long>();

        public override int Count
        {
            get
            {
                if (needRefresh)
                {
                    return idsToSync.Count;
                }
                else
                {
                    return base.Count;
                }                
            }
        }

        /// <summary>
        /// Синхронизировать состояние объекта
        /// </summary>
        public virtual void Sync(List<long> ids)
        {
            idsToSync = ids;
            needRefresh = true;
        }

        /// <summary>
        /// Удалить все элементы набора из БД
        /// </summary>
        public void Delete()
        {
            foreach(var item in items)
            {
                item.Delete();
            }
        }

        /// <summary>
        /// Загрузить все элементы в набор
        /// </summary>
        public virtual void LoadAll()
        {

        }

        /// <summary>
        /// Сохранить все объекты
        /// </summary>
        public void Save()
        {
            foreach(var item in items)
            {
                item.Save();
            }
        }
    }
}
