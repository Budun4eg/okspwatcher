﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Набор элементов
    /// </summary>
    public class NotifyPack<T> : NotifyObject, IEnumerable<T> where T : NotifyObject
    {
        /// <summary>
        /// Добавить элементы из другого набора
        /// </summary>
        public void InsertFrom(NotifyPack<T> pack)
        {
            foreach(var item in pack)
            {
                Add(item);
            }
        }

        protected SafeObservableCollection<T> items = new SafeObservableCollection<T>();
        /// <summary>
        /// Список элементов
        /// </summary>
        public SafeObservableCollection<T> Items
        {
            get
            {
                if (needRefresh)
                {
                    Refresh();
                }
                return items;
            }
        }

        /// <summary>
        /// Событие, вызываемое при изменении набора элементов
        /// </summary>
        public event EventHandler Updated;
        protected void OnUpdated()
        {
            var handler = Updated;
            if (handler != null)
            {
                handler(null, null);
            }
        }

        /// <summary>
        /// Добавить элемент
        /// </summary>
        public virtual void Add(T item)
        {            
            if (needRefresh)
            {
                Refresh();
            }
            if (items.Contains(item))
            {
#if IN_EDITOR
                Output.ErrorFormat("Повторное добавление элемента {0} в {1}", item.Name, Name);
#endif
                return;
            }
            items.Add(item);
            OnUpdated();
        }

        /// <summary>
        /// Добавить элемент
        /// </summary>
        public virtual void Insert(int position, T item)
        {            
            if (needRefresh)
            {
                Refresh();
            }
            if (items.Contains(item))
            {
#if IN_EDITOR
                Output.ErrorFormat("Повторное добавление элемента {0} в {1}", item.Name, Name);
#endif
                return;
            }
            items.Insert(position, item);
            OnUpdated();
        }

        /// <summary>
        /// Очистить набор
        /// </summary>
        public void Clear()
        {
            items.Clear();
        }

        /// <summary>
        /// Удалить элемент
        /// </summary>
        public virtual void Remove(T item)
        {            
            if (needRefresh)
            {
                Refresh();
            }
            if (!items.Contains(item))
            {
                return;
            }
            items.Remove(item);
            OnUpdated();
        }

        /// <summary>
        /// Положение элемента
        /// </summary>
        public int IndexOf(T item)
        {
            return Items.IndexOf(item);
        }

        /// <summary>
        /// Количество элементов в наборе
        /// </summary>
        public virtual int Count
        {
            get
            {
                if (needRefresh)
                {
                    Refresh();
                }
                return items.Count;
            }
        }

        /// <summary>
        /// Элемент содержится?
        /// </summary>
        public bool Contains(T item)
        {
            if (needRefresh)
            {
                Refresh();
            }
            return items.Contains(item);
        }

        /// <summary>
        /// Получить элемент, при условии, что он единственный в наборе
        /// </summary>
        public T Single
        {
            get
            {
                if (needRefresh)
                {
                    Refresh();
                }
                if (items.Count == 1)
                {
                    return items[0];
                }
                else if (items.Count == 0)
                {
#if IN_EDITOR && FULL_OUTPUT
                    Output.ErrorFormat("Элементы набора {0} отсутствуют", Name);
#endif
                    return null;
                }
                else
                {
#if IN_EDITOR && FULL_OUTPUT
                    Output.ErrorFormat("Присутствует больше одного элемента {0}", Name);
#endif
                    return null;
                }
            }
        }

        /// <summary>
        /// Необходимо обновить состояние объекта после синхронизации?
        /// </summary>
        protected bool needRefresh;

        /// <summary>
        /// Обновить состояние объекта
        /// </summary>
        protected virtual void Refresh()
        {
            needRefresh = false;
        }

        /// <summary>
        /// Сортированный по представлению список
        /// </summary>
        public SafeObservableCollection<T> Sorted()
        {
            var sorted = Items.OrderBy(o => o.Name);
            var toReturn = new SafeObservableCollection<T>();
            foreach(var item in sorted)
            {
                toReturn.Add(item);
            }
            return toReturn;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (needRefresh)
            {
                Refresh();
            }
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Обновить состояние объекта вручную
        /// </summary>
        public void ForceRefresh()
        {
            Refresh();
        }
    }
}
