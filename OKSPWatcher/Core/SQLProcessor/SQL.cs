﻿using OKSPWatcher.Secondary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace OKSPWatcher.Core.SQLProcessor
{
    /// <summary>
    /// Менеджер соединений
    /// </summary>
    public class SQL
    {
        static bool repeatMode = true;
        /// <summary>
        /// Повторять запросы к БД после ошибки?
        /// </summary>
        public static bool RepeatMode {
            get
            {
                return repeatMode;
            }
            set
            {
                repeatMode = value;
            }
        }

        /// <summary>
        /// Список объектов по идентификатору
        /// </summary>
        Dictionary<string, SQLItem> items = new Dictionary<string, SQLItem>();

        private static object root = new object();

        static SQL instance;
        /// <summary>
        /// Текущий основной экземпляр
        /// </summary>
        public static SQL Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if (instance == null)
                        {
                            instance = new SQL();
                            instance.Load();
                        }
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// Создать необходимые БД
        /// </summary>
        public void Create(List<string> tables)
        {
            try
            {
                foreach (var table in tables)
                {
                    var sql = SQL.Get(table);
                    sql.Create();
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        /// <summary>
        /// Закрыть все соединения
        /// </summary>
        public void Dispose()
        {
            try
            {
                foreach(var item in items.Values) {
                    item.Dispose();
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        /// <summary>
        /// Событие, вызываемое при выполнении команды
        /// </summary>
        public event EventHandler<SQLEventArgs> Proceed;
        void OnProceed(SQLEventArgs e)
        {
            var handler = Proceed;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        /// <summary>
        /// Настроить работу с БД
        /// </summary>
        public void Load(string path = null)
        {
#if TEST_MODE
            AppSettings.BaseFolder = string.Format("{0}", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            DBChecker.Check();
            return;
#endif
            if (path == null)
            {
                if (AppSettings.BaseFolder == null || AppSettings.BaseFolder == "")
                {
                    var window = new ChoosePathWindow();
                    window.ShowDialog();
                }
                else
                {
                    if (!Directory.Exists(AppSettings.BaseFolder))
                    {
                        var window = new ChoosePathWindow();
                        window.ShowDialog();
                    }
                }
            }
        }

        /// <summary>
        /// Получить объект по идентификатору
        /// </summary>
        public static SQLItem Get(string id)
        {
            if (Instance.items.ContainsKey(id))
            {
                return Instance.items[id];
            }
            else
            {
                lock (root)
                {
                    if (Instance.items.ContainsKey(id))
                    {
                        return Instance.items[id];
                    }
                    else
                    {
                        var sql = new SQLItem(id);
                        sql.Create();
                        sql.Proceed += Instance.Sql_Proceed;
                        Instance.items.Add(id, sql);
                        return sql;
                    }
                }                
            }
        }

        private void Sql_Proceed(object sender, SQLEventArgs e)
        {
            OnProceed(e);
        }

        bool debugMode;
        /// <summary>
        /// Режим отладки
        /// </summary>
        public bool DebugMode
        {
            get
            {
                return debugMode;
            }
            set
            {
                debugMode = value;
            }
        }
    }
}
