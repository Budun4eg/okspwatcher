﻿using System;

namespace OKSPWatcher.Core.SQLProcessor
{
    /// <summary>
    /// Данные ответа
    /// </summary>
    public class SQLEventArgs : EventArgs
    {
        string text;
        /// <summary>
        /// Форматированный ответ
        /// </summary>
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
        
        public SQLEventArgs(string _text)
        {
            text = _text;
        }
    }
}
