﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// v.2 opt
/// @ Vsevolod Nikitin
/// </summary>

namespace OKSPWatcher.Core.SQLProcessor
{
    /// <summary>
    /// Объект представления данных из БД
    /// </summary>
    public class SQLData : IEnumerable<SQLDataRow>
    {
        /// <summary>
        /// Список данных из БД
        /// </summary>
        List<SQLDataRow> rows = new List<SQLDataRow>();

        public SQLData()
        {
            
        }

        /// <summary>
        /// Добавить строку данных
        /// </summary>
        public void Add(SQLDataRow row)
        {
            rows.Add(row);
        }

        public IEnumerator<SQLDataRow> GetEnumerator()
        {
            return rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Получить единичную строку данных
        /// </summary>
        public SQLDataRow Single
        {
            get
            {
                if(rows.Count == 1)
                {
                    return rows[0];
                }else if(rows.Count == 0)
                {
#if IN_EDITOR && FULL_OUTPUT
                    Output.Error("Данные в SQL наборе отсутствуют");
#endif
                    return null;
                }
                else
                {
#if IN_EDITOR && FULL_OUTPUT
                    Output.Error("Присутствуют несколько данных в SQL наборе");
#endif
                    return null;
                }
            }
        }
    }
}
