﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// v.2 opt
/// @ Vsevolod Nikitin
/// </summary>

namespace OKSPWatcher.Core.SQLProcessor
{
    /// <summary>
    /// Строка данных из БД
    /// </summary>
    public class SQLDataRow : IEnumerable
    {
        /// <summary>
        /// Данные строки
        /// </summary>
        Dictionary<string, object> data = new Dictionary<string, object>();

        /// <summary>
        /// Добавить данные
        /// </summary>
        public void Add(string key, object value)
        {
            if (!data.ContainsKey(key))
            {
                data.Add(key, value);
            }
        }

        /// <summary>
        /// Получить целочисленные данные
        /// </summary>
        public long Long(string key)
        {
            if(data.ContainsKey(key) && data[key] != DBNull.Value)
            {
                try
                {
                    return data[key].ToString().ToLong();
                }catch(Exception ex)
                {
                    Output.Error(ex, "Невозможно преобразовать объект {0} в long", data[key]);
                    return -1;
                }                
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Получить целочисленные данные
        /// </summary>
        public int Int(string key)
        {
            if (data.ContainsKey(key) && data[key] != DBNull.Value)
            {
                try
                {
                    return data[key].ToString().ToInt();
                }
                catch (Exception ex)
                {
                    Output.Error(ex, "Невозможно преобразовать объект {0} в int", data[key]);
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Получить строковые данные
        /// </summary>
        public string String(string key)
        {
            if(data.ContainsKey(key) && data[key] != DBNull.Value)
            {
                return data[key].ToString();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Получить булевые данные
        /// </summary>
        public bool Bool(string key)
        {
            if(data.ContainsKey(key) && data[key] != DBNull.Value)
            {
                try
                {
                    return data[key].ToString().ToLong().ToBool();
                }catch(Exception ex)
                {
                    Output.WriteError(ex);
                    return false;
                }                
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Получить значение с плавающей запятой 64bit
        /// </summary>
        public double Double(string key)
        {
            if (data.ContainsKey(key) && data[key] != DBNull.Value)
            {
                var chk = data[key].ToString();
                if (chk.Length == 0)
                {
                    return -1;
                }
                return chk.ToDouble();
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Получить набор идентификаторов
        /// </summary>
        public List<long> Ids(string key)
        {
            if(data.ContainsKey(key) && data[key] != DBNull.Value)
            {
                var chk = data[key].ToString();
                if(chk.Length == 0)
                {
                    return new List<long>();
                }
                try
                {
                    return data[key].DeserializeLong();
                }catch(Exception ex)
                {
                    Output.WriteError(ex);
                    return new List<long>();
                }
            }
            else
            {
                return new List<long>();
            }
        }

        public IEnumerator GetEnumerator()
        {
            return data.GetEnumerator();
        }
    }
}
