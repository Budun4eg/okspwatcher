﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Timers;

namespace OKSPWatcher.Core.SQLProcessor
{
    /// <summary>
    /// Объект для работы с БД
    /// </summary>
    public class SQLItem
    {
        /// <summary>
        /// Закрыть все соединения
        /// </summary>
        public void Dispose()
        {
            try
            {                
                Close(true);
                connection.Close();
                connection.Dispose();
                _connection = null;
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
            finally
            {
                Unlock();
                if (connection != null)
                {
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// Событие, вызываемое при выполнении команды
        /// </summary>
        public event EventHandler<SQLEventArgs> Proceed;
        void OnProceed(string text)
        {
            var handler = Proceed;
            if (handler != null)
            {
                handler(null, new SQLEventArgs(text));
            }
        }

        void OnProceed(string request, string answer)
        {
            OnProceed(string.Format("{0} -> {1}", request, answer));
        }

        void OnProceed(string request, Exception ex)
        {
            OnProceed(request, string.Format("{0} -> {1}", request, ex.ToString()));
        }

        /// <summary>
        /// Время ожидания данных
        /// </summary>
        const int timeout = 3;

        string id;
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public string Id
        {
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Строка соединения
        /// </summary>
        string connectionString;

        public SQLItem(string _id)
        {
            lock (connLock)
            {
                id = _id;
                connectionString = string.Format("Data Source={0}\\data\\{1}.db;Version=3;", AppSettings.BaseFolder, id);
                closeTimer = new System.Timers.Timer(closeCheckInterval);
                closeTimer.Elapsed += CloseCheck;
                closeTimer.Start();
            }            
        }

        private static object connLock = new object();

        SQLiteConnection _connection;
        /// <summary>
        /// Соединение с базой данных
        /// </summary>
        public SQLiteConnection connection
        {
            get
            {
                if(_connection == null)
                {
                    lock (connLock)
                    {
                        if(_connection == null)
                        {
                            try
                            {
                                RequestLock();
                                canBeClosed = false;
                                _connection = new SQLiteConnection(connectionString, true);
                                _connection.DefaultTimeout = timeout;
                                _connection.BusyTimeout = timeout;
                                _connection.Open();
                                SQLiteCommand sql = new SQLiteCommand("PRAGMA synchronous=OFF", _connection);
                                sql.ExecuteNonQuery();
                                sql = new SQLiteCommand("PRAGMA temp_store=2", _connection);
                                sql.ExecuteNonQuery();
                                sql = new SQLiteCommand("PRAGMA journal_mode=OFF", _connection);
                                sql.ExecuteNonQuery();
                                sql = new SQLiteCommand("PRAGMA count_changes=OFF", _connection);
                                sql.ExecuteNonQuery();
                                sql = new SQLiteCommand("PRAGMA cache_size=10000", _connection);
                                sql.ExecuteNonQuery();
                                _connection.Close();
                                canBeClosed = true;
                                Unlock();
                            }
                            catch (Exception ex)
                            {
                                Output.WriteError(ex);
                                if (SQL.RepeatMode)
                                {
                                    Output.WriteError("Open connection for {0}", id);
                                    Output.TraceError("Ошибка при соединении с БД, повтор запроса...");
                                    Thread.Sleep(errorWait);
                                    return connection;
                                }
                            }
                        }
                    }                    
                }
                return _connection;
            }
        }

        /// <summary>
        /// Создать новую базу данных
        /// </summary>
        public void Create()
        {
            try
            {                
                string path = string.Format("{0}\\data\\{1}.db", AppSettings.BaseFolder, id);
                if (!File.Exists(path))
                {
                    SQLiteConnection.CreateFile(path);
                    Output.LogFormat("БД создана по пути {0}", path);
                }                
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при создании БД {0}", id);
            }
        }        

        /// <summary>
        /// Открыть транзакцию
        /// </summary>
        void Open()
        {
            transactionOpened = true;
        }

        /// <summary>
        /// Закрытие транзакции мгновенно?
        /// </summary>
        bool instantMode = false;

        /// <summary>
        /// Закрыть транзакцию
        /// </summary>
        void Close(bool forced = false)
        {
            if (instantMode || forced)
            {
                transactionOpened = false;
            }
        }

        /// <summary>
        /// Таймер для закрытия транзакции
        /// </summary>
        System.Timers.Timer closeTimer;

        /// <summary>
        /// Частота выполнения проверки на закрытие
        /// </summary>
        int closeCheckInterval = 50;

        /// <summary>
        /// Соединение может быть закрыто?
        /// </summary>
        bool canBeClosed = false;

        private void CloseCheck(object sender, EventArgs e)
        {
            if (transactionOpened && canBeClosed)
            {
                if ((DateTime.Now - lastExecuted).Milliseconds > transactionCloseTime)
                {
                    transactionOpened = false;
                }
            }
        }

        /// <summary>
        /// Окно с открытой транзакцией
        /// </summary>
        BasicWindow locked;

        bool _transactionOpened;
        /// <summary>
        /// Состояние транзакции
        /// </summary>
        bool transactionOpened
        {
            get
            {
                return _transactionOpened;
            }
            set
            {
                try
                {                    
                    if (_transactionOpened == value) return;
                    lock (connLock)
                    {
                        if (_transactionOpened == value) return;
                        
                        SQLiteCommand sql;

                        if (connection != null && value)
                        {
                            RequestLock();
                            connection.Open();
                            sql = new SQLiteCommand("BEGIN TRANSACTION", connection);
                            locked = BasicWindow.Current;
                            if (locked != null)
                            {
                                locked.DBTrans = true;
                            }

                            sql.ExecuteNonQuery();
#if IN_EDITOR && FULL_OUTPUT
                            Output.LogFormat("Open transaction for {0}", id);
#endif
                        }
                        else if (connection != null && !value)
                        {
                            sql = new SQLiteCommand("END TRANSACTION", connection);
                            if (locked != null)
                            {
                                locked.DBTrans = false;
                            }

                            sql.ExecuteNonQuery();

                            connection.Close();
#if IN_EDITOR && FULL_OUTPUT
                            Output.LogFormat("Close transaction for {0}", id);
#endif
                            Unlock();
                        }
                        _transactionOpened = value;                        
                    }                    
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                    if (SQL.RepeatMode)
                    {
                        Output.WriteError("Transaction for {0}", id);
                        Output.TraceError("Ошибка при соединении с БД, повтор запроса...");
                        Thread.Sleep(errorWait);
                        transactionOpened = value;
                    }                    
                }
            }
        }

        /// <summary>
        /// Время в милисекундах для повторной попытки связи с БД
        /// </summary>
        int errorWait = 50;

        /// <summary>
        /// Длительность до закрытия транзакции при отсутствии запросов
        /// </summary>
        int transactionCloseTime = 100;

        /// <summary>
        /// Время последней выполненной команды
        /// </summary>
        DateTime lastExecuted;

        /// <summary>
        /// Выполнить команду
        /// </summary>
        public bool Execute(string command)
        {
            canBeClosed = false;
            bool output = true;
            try
            {                
                lock (connLock)
                {
                    Open();
                    SQLiteCommand sql = new SQLiteCommand(command, connection);

                    sql.ExecuteNonQuery();
#if IN_EDITOR && FULL_OUTPUT
                    Output.LogFormat("[SQL Execute] {0}", command);
#endif
                    OnProceed(command, "OK");
                }
                
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                Output.WriteError("Request {0} for {1}", command, id);
                if (SQL.RepeatMode)
                {                    
                    Output.Error("Ошибка при соединении с БД, повтор запроса...");
                    Thread.Sleep(errorWait);
                    return Execute(command);
                }                
            }

            lastExecuted = DateTime.Now;
            canBeClosed = true;
            return output;
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        public bool Execute(string command, params object[] args)
        {
            return Execute(string.Format(command, args));
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        public bool Execute(string command, out long lastId, params object[] args)
        {
            return Execute(string.Format(command, args), out lastId);
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        public bool Execute(string command, out long lastId)
        {
            canBeClosed = false;
            bool output = true;
            try
            {
                lock (connLock)
                {
                    Open();
                    SQLiteCommand sql = new SQLiteCommand(command, connection);
                    sql.ExecuteNonQuery();
                    lastId = connection.LastInsertRowId;

#if IN_EDITOR && FULL_OUTPUT
                    Output.LogFormat("[SQL Execute] {0}", command);
#endif

                    OnProceed(command, "OK");
                }                
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                Output.WriteError("Request {0} for {1}", command, id);
                if (SQL.RepeatMode)
                {                    
                    Output.TraceError("Ошибка при соединении с БД, повтор запроса...");
                    Thread.Sleep(errorWait);
                    return Execute(command, out lastId);
                }
                else
                {
                    lastId = -1;
                }         
            }
            
            lastExecuted = DateTime.Now;
            canBeClosed = true;
            return output;
        }

        /// <summary>
        /// Выполнить команду с ответом от базы
        /// </summary>
        public SQLData ExecuteRead(string command, params object[] args)
        {
            return ExecuteRead(string.Format(command, args));
        }

        /// <summary>
        /// Запаковать БД
        /// </summary>
        public void Vacuum() {
            SQLiteCommand sql = new SQLiteCommand("VACUUM", connection);
            connection.Open();
            sql.ExecuteNonQuery();
            connection.Close();
        }

        /// <summary>
        /// Выполнить команду с ответом от базы
        /// </summary>
        public SQLData ExecuteRead(string command)
        {
            canBeClosed = false;
            SQLData output = new SQLData();
            try
            {
                lock (connLock)
                {
                    Open();
                    SQLiteCommand sql = new SQLiteCommand(command, connection);
                    OnProceed(command);

                    var outData = sql.ExecuteReader();
#if IN_EDITOR && FULL_OUTPUT
                    Output.LogFormat("[SQL Read] {0}", command);
#endif

                    List<int> columnWidth = new List<int>();
                    if (SQL.Instance.DebugMode)
                    {
                        for (int i = 0; i < outData.FieldCount; i++)
                        {
                            columnWidth.Add(outData.GetName(i).Length + 2);
                        }
                    }

                    while (outData.Read())
                    {
                        SQLDataRow row = new SQLDataRow();
                        for (int i = 0; i < outData.FieldCount; i++)
                        {
                            row.Add(outData.GetName(i), outData[i]);

                            if (SQL.Instance.DebugMode)
                            {
                                if (outData[i].ToString().Length + 2 > columnWidth[i])
                                {
                                    columnWidth[i] = outData[i].ToString().Length + 2;
                                }
                            }
                        }
                        output.Add(row);
                    }

                    if (SQL.Instance.DebugMode)
                    {
                        StringBuilder builder = new StringBuilder();

                        int totalWidth = 0;

                        builder.Append("###### ");
                        for (int i = 0; i < columnWidth.Count; i++)
                        {
                            int leftSpace = (columnWidth[i] - outData.GetName(i).Length) / 2;
                            int rightSpace = columnWidth[i] - outData.GetName(i).Length - leftSpace;
                            builder.AppendFormat("{0}{1}{2}", new string(' ', leftSpace), outData.GetName(i), new string(' ', rightSpace));
                            while (builder.Length < columnWidth[i])
                            {
                                builder.Append(" ");
                            }
                            builder.Append("|");
                            totalWidth += columnWidth[i] + 1;
                        }

                        OnProceed(builder.ToString());
                        int index = 1;

                        foreach (SQLDataRow row in output)
                        {
                            builder = new StringBuilder();
                            builder.Append('-', totalWidth);
                            OnProceed(builder.ToString());
                            builder = new StringBuilder();
                            string ind = string.Format("#{0}", index);
                            while (ind.Length < 7)
                            {
                                ind = ind + " ";
                            }
                            builder.Append("###### ");
                            int i = 0;
                            foreach (KeyValuePair<string, object> obj in row)
                            {
                                int leftSpace = (columnWidth[i] - obj.Value.ToString().Length) / 2;
                                int rightSpace = columnWidth[i] - obj.Value.ToString().Length - leftSpace;
                                builder.AppendFormat("{0}{1}{2}", new string(' ', leftSpace), obj.Value.ToString(), new string(' ', rightSpace));
                                while (builder.Length < columnWidth[i])
                                {
                                    builder.Append(" ");
                                }
                                builder.Append("|");
                                i++;
                            }
                            OnProceed(builder.ToString());
                            index++;
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                Output.WriteError("Request {0} for {1}", command, id);
                if (SQL.RepeatMode)
                {
                    Output.TraceError("Ошибка при соединении с БД, повтор запроса...");
                    Thread.Sleep(errorWait);
                    return ExecuteRead(command);
                }
                
            }
            lastExecuted = DateTime.Now;
            canBeClosed = true;
            return output;
        }

        /// <summary>
        /// Выполнить команду с единичным ответом
        /// </summary>
        public object ExecuteScalar(string command)
        {
            canBeClosed = false;
            object output = null;

            try
            {
                lock (connLock)
                {
                    Open();
                    SQLiteCommand sql = new SQLiteCommand(command, connection);

                    output = sql.ExecuteScalar();
#if IN_EDITOR && FULL_OUTPUT
                    Output.LogFormat("[Scalar] {0}", command);
#endif

                    OnProceed(command, output != null ? output.ToString() : "");
                }                
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                Output.WriteError("Request {0} for {1}", command, id);
                if (SQL.RepeatMode)
                {
                    Output.TraceError("Ошибка при соединении с БД, повтор запроса...");
                    Thread.Sleep(errorWait);
                    return ExecuteScalar(command);
                }                
            }
            lastExecuted = DateTime.Now;
            canBeClosed = true;
            return output;
        }

        /// <summary>
        /// Выполнить команду с единичным ответом
        /// </summary>
        public object ExecuteScalar(string command, params object[] args)
        {
            return ExecuteScalar(string.Format(command, args));
        }

        string _lockPath;
        /// <summary>
        /// Путь до папки
        /// </summary>
        string lockPath
        {
            get
            {
                if (_lockPath == null)
                {
                    lock (connLock)
                    {
                        if (_lockPath == null)
                        {
                            _lockPath = string.Format("{0}\\dbLocks\\{1}", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), id);
                        }
                    }
                }
                return _lockPath;
            }
        }

        /// <summary>
        /// Объект запроса
        /// </summary>
        FileStream lockStream;

        /// <summary>
        /// Доступ к транзакции заблокирован
        /// </summary>
        bool transactionLocked = false;

        /// <summary>
        /// Запросить доступ
        /// </summary>
        void RequestLock()
        {
            if (transactionLocked) return;

            while (File.Exists(lockPath))
            {
                try
                {
                    File.Delete(lockPath);
                }
                catch
                {
                    Thread.Sleep(errorWait);
                }
            }

            lock (connLock)
            {
                try
                {
                    lockStream = File.Create(lockPath);
                    transactionLocked = true;
                }
                catch (Exception ex)
                {
                    Output.Error(ex, "Ошибка при запросе доступа к БД {0}", id);
                }
            }
        }

        /// <summary>
        /// Снять запрос доступа
        /// </summary>
        void Unlock()
        {
            if (!transactionLocked) return;
            lock (connLock)
            {
                try
                {
                    lockStream.Close();
                    lockStream.Dispose();
                    File.Delete(lockPath);
                }
                catch (Exception ex)
                {
                    Output.Error(ex, "Ошибка при снятии доступа с БД {0}", id);
                }
            }            
        }
    }
}
