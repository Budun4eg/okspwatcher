﻿using System.ComponentModel;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Класс для управления фоновой работой
    /// </summary>
    public static class BackgroundHandler
    {
        static bool allClose = false;
        /// <summary>
        /// Вся фоновая работа должна быть отключена?
        /// </summary>
        public static bool AllClose
        {
            get
            {
                return allClose;
            }
        }

        /// <summary>
        /// Остановить все задачи
        /// </summary>
        public static void StopAll()
        {
            allClose = true;
        }

        /// <summary>
        /// Количество работающих фоновых задач
        /// </summary>
        static int workerCount;

        /// <summary>
        /// Приложение может быть закрыто?
        /// </summary>
        public static bool CanBeClosed
        {
            get
            {
                return workerCount == 0;
            }
        }

        /// <summary>
        /// Зарегистрировать фоновую задачу
        /// </summary>
        public static void Register(BackgroundWorker worker)
        {
#if IN_EDITOR && FULL_OUTPUT
            Output.LogFormat("[Background] Register {0}", worker.GetHashCode());
#endif
            workerCount++;
        }

        /// <summary>
        /// Закрыть задачу
        /// </summary>
        public static void Unregister(BackgroundWorker worker)
        {
#if IN_EDITOR && FULL_OUTPUT
            Output.LogFormat("[Background] Unregister {0}", worker.GetHashCode());
#endif
            workerCount--;
        }

        static int timesToWait = 5;
        /// <summary>
        /// Количество запросов до выхода
        /// </summary>
        public static int TimesToWait
        {
            get
            {
                return timesToWait;
            }
        }
    }
}
