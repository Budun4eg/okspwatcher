﻿//using OKSPWatcher.Core.Users;
//using System;
//using System.Collections.Generic;

//namespace OKSPWatcher.Core.Departments
//{
//    /// <summary>
//    /// Объект представления отдела
//    /// </summary>
//    public class Department : DBObject
//    {
//        string name = "";
//        /// <summary>
//        /// Наименование отдела
//        /// </summary>
//        public string Name {
//            get
//            {
//                return name;
//            }
//            set
//            {
//                name = value;
//                OnPropertyChanged("Name");
//            }
//        }

//        public override void Load(Dictionary<string, object> data)
//        {
//            base.Load(data);
//            try
//            {
//                if (data.ContainsKey("Name"))
//                {
//                    name = (string)data["Name"];
//                }
//                else
//                {
//                    Output.Error("У отдела #{0} отсутствует параметр Name", id);
//                }
//                if (data.ContainsKey("BossId"))
//                {
//                    bossId = (long)data["BossId"];
//                }
//                else
//                {
//                    Output.Error("У отдела #{0} отсутствует параметр BossId", id);
//                }
//            }
//            catch (Exception ex) {
//                Output.Error(ex);
//            }
//        }

//        long bossId = 0;
//        /// <summary>
//        /// Начальник отдела
//        /// </summary>
//        public User Boss
//        {
//            get
//            {
//                return UserManager.IdUsers.ContainsKey(bossId) ? UserManager.IdUsers[bossId] : null;
//            }
//            set
//            {
//                bossId = value != null ? value.Id : 0;
//                OnPropertyChanged("Boss");
//            }
//        }

//        public Department(Dictionary<string, object> data)
//        {
//            Load(data);
//        }

//        public Department()
//        {
            
//        }

//        protected override bool CreateNew()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Departments WHERE Name = '{0}'", name);
//            if (check != null)
//            {
//                errorId = (long)check;
//                errorString = "Dublicate";
//                return false;
//            }
//            SQL.ExecuteId("INSERT INTO Departments (Name, BossId) VALUES ('{0}', {1})", name.Check(), bossId);
//            base.CreateNew();
//            DepartmentManager.Add(this);
//            return true;
//        }

//        protected override bool Update()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Departments WHERE Name = '{0}'", name);
//            if (check != null)
//            {
//                var depId = (long)check;
//                if (depId != id)
//                {
//                    errorString = "Dublicate";
//                    return false;
//                }
//            }
//            base.Update();
//            SQL.Execute("UPDATE Departments SET Name='{0}', BossId = {1} WHERE Id={2}", name.Check(), bossId, id);
//            return true;
//        }

//        public override void Delete()
//        {
//            base.Delete();
//            SQL.Execute("DELETE FROM Departments WHERE Id = {0}", id);
//            DepartmentManager.Delete(this);
//        }

//        public override void SaveState()
//        {
//            base.SaveState();
//            AddStateItem("Name", name);
//            AddStateItem("Boss", bossId);
//        }

//        public override void LoadState()
//        {
//            if (hasSavedState)
//            {
//                name = (string)savedState["Name"];
//                bossId = (long)savedState["Boss"];
//                base.LoadState();
//            }
//        }
//    }
//}
