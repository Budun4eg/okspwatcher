﻿//using System.Collections.Generic;
//using System.Collections.ObjectModel;

//namespace OKSPWatcher.Core.Departments
//{
//    /// <summary>
//    /// Менеджер объектов отдела
//    /// </summary>
//    public static class DepartmentManager
//    {
//        /// <summary>
//        /// Список доступных отделов
//        /// </summary>
//        public static SafeObservableCollection<Department> Departments = new SafeObservableCollection<Department>();

//        /// <summary>
//        /// Доступ к отделу по ключу
//        /// </summary>
//        public static Dictionary<long, Department> IdDepartments = new Dictionary<long, Department>();

//        /// <summary>
//        /// Список всех отделов
//        /// </summary>
//        static SafeObservableCollection<Department> allDepartments;

//        public static SafeObservableCollection<Department> AllDepartments
//        {
//            get
//            {
//                if(allDepartments == null)
//                {
//                    allDepartments = new SafeObservableCollection<Department>();
//                    var depsData = SQL.ExecuteRead("SELECT * FROM Departments ORDER BY Id");

//                    foreach (Dictionary<string, object> row in depsData)
//                    {
//                        long id = row["Id"].ToString().ToLong();
//                        var prd = IdDepartments.ContainsKey(id) ? IdDepartments[id] : new Department(row);
//                        allDepartments.Add(prd);
//                    }
//                }
//                return allDepartments;
//            }
//        }

//        /// <summary>
//        /// Менеджер загружен?
//        /// </summary>
//        static bool loaded;

//        /// <summary>
//        /// Загрузить менеджер
//        /// </summary>
//        public static void Load(bool forced = false)
//        {
//            if(!loaded || forced)
//            {
//                var depsData = SQL.ExecuteRead("SELECT * FROM Departments ORDER BY Id");

//                foreach (Dictionary<string, object> row in depsData)
//                {
//                    var dep = new Department(row);
//                    Add(dep);
//                }

//                loaded = true;
//            }            
//        }

//        /// <summary>
//        /// Загрузить менеджер с ограничениями
//        /// </summary>
//        public static void Load(object restriction, bool forced = false)
//        {
//            if(!loaded || forced)
//            {
//                var toLoad = restriction.DeserializeLong();
//                var depsData = SQL.ExecuteRead("SELECT * FROM Departments WHERE {0} ORDER BY Id", toLoad.CreateSearchIn("Id"));

//                foreach (Dictionary<string, object> row in depsData)
//                {
//                    var dep = new Department(row);
//                    Add(dep);
//                }

//                loaded = true;
//            }            
//        }



//        /// <summary>
//        /// Добавить отдел
//        /// </summary>
//        public static void Add(Department dep)
//        {
//            if (IdDepartments.ContainsKey(dep.Id))
//            {
//                return;
//            }
//            IdDepartments.Add(dep.Id, dep);
//            Departments.Add(dep);              
//        }

//        /// <summary>
//        /// Очистить менеджер
//        /// </summary>
//        public static void Clear()
//        {
//            IdDepartments.Clear();
//            Departments.Clear();
//        }

//        /// <summary>
//        /// Удалить отдел
//        /// </summary>
//        public static void Delete(Department dep)
//        {
//            if (Departments.Contains(dep))
//            {
//                Departments.Remove(dep);
//            }
//            if (IdDepartments.ContainsKey(dep.Id))
//            {
//                IdDepartments.Remove(dep.Id);
//            }            
//        }
//    }
//}
