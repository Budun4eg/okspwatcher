﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using OKSPWatcher.Secondary;

namespace OKSPWatcher
{
    /// <summary>
    /// Окно для редактирования менеджеров объектов
    /// </summary>
    public class ManagerEditorWindow<T,K> : BasicWindow where T : ManagerObject<K> where K : DBObject, new()
    {
        /// <summary>
        /// Объект для редактирования
        /// </summary>
        protected T edit;

        /// <summary>
        /// Загрузить менеджер редактирования
        /// </summary>
        public virtual void Load(T _edit)
        {
            edit = _edit;
            DataContext = edit;
            edit.SaveState();
        }

        /// <summary>
        /// Добавить новый элемент
        /// </summary>
        protected virtual void Add(object sender, RoutedEventArgs e)
        {
            var item = new K();
            item.Save();
        }

        /// <summary>
        /// Загрузить предустановленные данные
        /// </summary>
        protected virtual void InsertData(object sender, RoutedEventArgs e)
        {
            
        }

        /// <summary>
        /// Удалить элемент
        /// </summary>
        protected virtual void Remove(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as K;
            item.Delete();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (edit.NeedUpdate)
            {
                RequestWindow.Show("Сохранить изменения?");
                if(RequestWindow.Result == RequestWindow.ResultType.Yes)
                {
                    edit.Save();
                }
                else
                {
                    edit.LoadState();
                }
            }
            edit.ClearState();
            base.OnClosing(e);
        }
    }
}
