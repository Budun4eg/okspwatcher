﻿using OKSPWatcher.Products;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Стандартный менеджер данных
    /// </summary>
    public class ManagerObject<T> : NotifyObject where T : DBObject, new()
    {
        /// <summary>
        /// Обновить состояние объектов менеджера
        /// </summary>
        public void CheckUpdate()
        {
            foreach(var item in Items)
            {
                if (item == null) continue;
                item.CheckUpdate();
            }
        }

        /// <summary>
        /// Выгрузить менеджер
        /// </summary>
        public virtual void Unload()
        {

        }

        /// <summary>
        /// Проверить наличие экземпляра менеджера
        /// </summary>
        public void CheckInstance()
        {

        }

        /// <summary>
        /// Отсортировать элементы по представлению
        /// </summary>
        public void Sort()
        {
            var sorted = Items.OrderBy(o => o.Name).ToList();
            Items.Clear();
            foreach(var item in sorted)
            {
                Items.Add(item);
            }
        }

        /// <summary>
        /// Объект для фонового обновления данных
        /// </summary>
        protected BackgroundWorker worker = new BackgroundWorker();

        protected SafeObservableCollection<T> items = new SafeObservableCollection<T>() {  };
        /// <summary>
        /// Объекты менеджера
        /// </summary>
        public SafeObservableCollection<T> Items
        {
            get
            {
                return items;
            }
        }

        protected T nullItem;
        /// <summary>
        /// Пустой объект
        /// </summary>
        public T NullItem
        {
            get
            {
                if(nullItem == null)
                {
                    nullItem = new T();
                    nullItem.Name = "Не определено";                    
                }
                return nullItem;
            }
        }

        /// <summary>
        /// Набор элементов для выбора
        /// </summary>
        public SafeObservableCollection<T> ToChoose
        {
            get
            {
                var toReturn = new SafeObservableCollection<T>();
                toReturn.Add(NullItem);
                
                foreach(var item in items)
                {
                    toReturn.Add(item);
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Синхронизировать состояние менеджера
        /// </summary>
        public virtual void Sync(string dataType = null)
        {

        }

        /// <summary>
        /// Список объектов по идентификатору
        /// </summary>
        protected Dictionary<long, T> byId = new Dictionary<long, T>();

        /// <summary>
        /// Получить объект по идентификатору
        /// </summary>
        public virtual T ById(long id, bool load = false)
        {
            if (byId.ContainsKey(id))
            {
                return byId[id];
            }
            else
            {
                return null;
            }
        }

        protected object changeLock = new object();

        /// <summary>
        /// Добавить объект в менеджер
        /// </summary>
        public virtual bool Add(T item)
        {
            try
            {
                if (byId.ContainsKey(item.Id))
                {
                    return false;
                }
                items.Add(item);
                byId.Add(item.Id, item);
                return true;
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                return false;
            }
        }

        /// <summary>
        /// Удалить объект из менеджера
        /// </summary>
        public virtual void Remove(T item)
        {
            if (byId.ContainsKey(item.Id))
            {
                byId.Remove(item.Id);
            }
            if (items.Contains(item))
            {
                items.Remove(item);
            }
        }

        /// <summary>
        /// Сохранить менеджер
        /// </summary>
        public virtual void Save()
        {
            foreach(var item in items)
            {
                item.Save();
            }
        }

        /// <summary>
        /// Необходимо обновление менеджера?
        /// </summary>
        public bool NeedUpdate
        {
            get
            {
                foreach(var item in items)
                {
                    if (item.NeedUpdate) return true;
                }
                return false;
            }
        }

        protected ManagerObject()
        {
            worker.DoWork += UpdateData;
            worker.RunWorkerCompleted += DataIsUpdated;
            items.CollectionChanged += CollectionChanged;
        }

        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("ToChoose");
        }

        /// <summary>
        /// Создать структуру в БД для хранения данного объекта
        /// </summary>
        public virtual void GenerateDBStructure()
        {
            
        }

        /// <summary>
        /// Удалить структуру из БД
        /// </summary>
        public virtual void DeleteStructure()
        {

        }

        /// <summary>
        /// Загрузить все доступные данные
        /// </summary>
        public virtual void LoadAll()
        {

        }

        /// <summary>
        /// Сохранить состояние менеджера
        /// </summary>
        public void SaveState()
        {
            foreach(var item in items)
            {
                item.SaveState();
            }
        }

        /// <summary>
        /// Загрузить состояние менеджера
        /// </summary>
        public void LoadState()
        {
            foreach(var item in items)
            {
                item.LoadState();
            }
        }

        /// <summary>
        /// Очистить состояние менеджера
        /// </summary>
        public void ClearState()
        {
            foreach(var item in items)
            {
                item.ClearState();
            }
        }

        /// <summary>
        /// Событие, вызываемое при обновлении данных
        /// </summary>
        public event EventHandler DataUpdated;
        protected void OnDataUpdated()
        {
            var handler = DataUpdated;
            if(handler != null)
            {
                handler(null, null);
            }
        }

        /// <summary>
        /// Данные были обновлены
        /// </summary>
        protected virtual void DataIsUpdated(object sender, RunWorkerCompletedEventArgs e)
        {
            mustUpdate = false;
            OnDataUpdated();
            BackgroundHandler.Unregister(worker);
        }

        /// <summary>
        /// Функция обновления данных
        /// </summary>
        protected virtual void UpdateData(object sender, DoWorkEventArgs e)
        {
            BackgroundHandler.Register(worker);
        }

        protected bool mustUpdate;
        /// <summary>
        /// Требуется обновление объекта?
        /// </summary>
        public bool MustUpdate
        {
            get
            {
                return mustUpdate;
            }
            set
            {
                mustUpdate = value;
                OnPropertyChanged("MustUpdate");
            }
        }

        bool cancelUpdate;
        /// <summary>
        /// Остановить обновление данных?
        /// </summary>
        public virtual bool CancelUpdate
        {
            get
            {
                return cancelUpdate;
            }
            set
            {
                cancelUpdate = value;
                OnPropertyChanged("CancelUpdate");
            }
        }

        /// <summary>
        /// Количество проверенных данных
        /// </summary>
        protected int checkedCount;

        /// <summary>
        /// Текущее изделие менеджера
        /// </summary>
        Product product;

        /// <summary>
        /// Обновить данные касательно определенного изделия
        /// </summary>
        public void Update(Product _product = null)
        {
            checkedCount = 0;
            cancelUpdate = false;
            product = _product;
            try
            {
                worker.RunWorkerCheck();
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }            
        }
    }
}
