﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Threading;

/// <summary>
/// v.2 opt
/// @ Vsevolod Nikitin
/// </summary>

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Потокобезопасная коллекция
    /// </summary>
    public class SafeObservableCollection<T> : ObservableCollection<T>
    {
        private readonly object locker = new object();

        private bool suspendCollectionChangeNotification;

        public SafeObservableCollection() : base()
        {
            this.suspendCollectionChangeNotification = false;
        }

        public override event NotifyCollectionChangedEventHandler CollectionChanged;

        public void AddItems(IList<T> items)
        {
            lock (locker)
            {
                this.SuspendCollectionChangeNotification();
                foreach(var i in items)
                {
                    InsertItem(Count, i);
                }
                this.NotifyChanges();
            }
        }

        public void NotifyChanges()
        {
            this.ResumeCollectionChangeNotification();
            var arg = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            this.OnCollectionChanged(arg);
        }

        public void RemoveItems(IList<T> items)
        {
            lock (locker)
            {
                this.SuspendCollectionChangeNotification();
                foreach(var i in items)
                {
                    Remove(i);
                }
                this.NotifyChanges();
            }
        }

        public void ResumeCollectionChangeNotification()
        {
            this.suspendCollectionChangeNotification = false;
        }

        public void SuspendCollectionChangeNotification()
        {
            this.suspendCollectionChangeNotification = true;
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            using (BlockReentrancy())
            {
                if (!this.suspendCollectionChangeNotification)
                {
                    NotifyCollectionChangedEventHandler eventHandler = this.CollectionChanged;
                    if (eventHandler == null) {
                        return;
                    }

                    Delegate[] delegates = eventHandler.GetInvocationList();
                    foreach(NotifyCollectionChangedEventHandler handler in delegates)
                    {
                        DispatcherObject dispatcherObject = handler.Target as DispatcherObject;

                        if(dispatcherObject != null && !dispatcherObject.CheckAccess())
                        {
                            dispatcherObject.Dispatcher.BeginInvoke(DispatcherPriority.DataBind, handler, this, e);
                        }
                        else
                        {
                            handler(this, e);
                        }
                    }
                }
            }
        }
    }
}
