﻿//using OKSPWatcher.Core.Departments;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;

//namespace OKSPWatcher.Core.Organizations
//{
//    /// <summary>
//    /// Организация
//    /// </summary>
//    public class Organization : DBObject
//    {
//        /// <summary>
//        /// Отделы, к которым привязана организация
//        /// </summary>
//        public SafeObservableCollection<Department> Departments = new SafeObservableCollection<Department>();

//        string name = "";
//        /// <summary>
//        /// Наименование организации
//        /// </summary>
//        public string Name
//        {
//            get
//            {
//                return name;
//            }
//            set
//            {
//                name = value;
//                OnPropertyChanged("Name");
//            }
//        }

//        public override void Load(Dictionary<string, object> data)
//        {
//            base.Load(data);
//            try {
//                if (data.ContainsKey("Name"))
//                {
//                    name = (string)data["Name"];
//                }
//                else
//                {
//                    Output.Error("[Organization Load] Отсутствует параметр Name");
//                }                
//                if (data.ContainsKey("Departments"))
//                {
//                    Departments.Clear();
//                    foreach (long depId in data["Departments"].DeserializeLong())
//                    {
//                        Departments.Add(DepartmentManager.IdDepartments[depId]);
//                    }
//                }
//                else
//                {
//                    Output.Error("[Organization Load] Отсутствует параметр Departments");
//                }
//            }
//            catch(Exception ex)
//            {
//                Output.Error(ex);
//            }
//        }

//        public Organization(Dictionary<string, object> data)
//        {
//            Load(data);
//        }

//        public Organization()
//        {
            
//        }

//        protected override bool CreateNew()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Organizations WHERE Name = '{0}'", name);
//            if (check != null)
//            {
//                errorId = (long)check;
//                errorString = "Dublicate";
//                return false;
//            }

//            SQL.ExecuteId("INSERT INTO Organizations (Name,Departments) VALUES ('{0}','{1}')", name.Check(), Departments.Serialize(), true);
//            base.CreateNew();
//            OrganizationManager.Add(this);
//            return true;
//        }

//        protected override bool Update()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Organizations WHERE Name = '{0}'", name);
//            if (check != null)
//            {
//                var orgId = (long)check;
//                if (orgId != id)
//                {
//                    errorString = "Dublicate";
//                    return false;
//                }
//            }
//            base.Update();
//            SQL.Execute("UPDATE Organizations SET Name = '{0}', Departments = '{1}' WHERE Id = {2}", name.Check(), Departments.Serialize(), Id);
//            return true;
//        }

//        public override void Delete()
//        {
//            base.Delete();
//            SQL.Execute("DELETE FROM Organizations WHERE Id = {0}",id);
//            OrganizationManager.Delete(this);
//        }

//        public override void SaveState()
//        {
//            base.SaveState();
//            AddStateItem("Name", name);
//            AddStateItem("Departments", Departments.Serialize());
//        }

//        public override void LoadState()
//        {
//            if (hasSavedState)
//            {
//                name = (string)savedState["Name"];
//                Departments.Clear();
//                foreach(int depId in savedState["Departments"].DeserializeLong())
//                {
//                    Departments.Add(DepartmentManager.IdDepartments[depId]);
//                }
//                base.LoadState();
//            }
//        }
//    }
//}
