﻿//using OKSPWatcher.Core.Departments;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;

//namespace OKSPWatcher.Core.Organizations
//{
//    /// <summary>
//    /// Менеджер организаций
//    /// </summary>
//    public static class OrganizationManager
//    {
//        /// <summary>
//        /// Список доступных организаций
//        /// </summary>
//        public static SafeObservableCollection<Organization> Organizations = new SafeObservableCollection<Organization>();

//        /// <summary>
//        /// Доступ к организации по ключу
//        /// </summary>
//        public static Dictionary<long, Organization> IdOrganizations = new Dictionary<long, Organization>();

//        /// <summary>
//        /// Доступ к организации по наименованию
//        /// </summary>
//        public static Dictionary<string, Organization> OrganizationsName = new Dictionary<string, Organization>();

//        /// <summary>
//        /// Менеджер загружен?
//        /// </summary>
//        static bool loaded;

//        /// <summary>
//        /// Загрузить менеджер
//        /// </summary>
//        public static void Load(bool restriction = true, bool forced = false)
//        {
//            if(!loaded || forced)
//            {
//                List<Dictionary<string, object>> orgsData;
//                if (restriction)
//                {
//                    orgsData = SQL.ExecuteRead("SELECT * FROM Organizations WHERE {0} ORDER BY Name", DepartmentManager.Departments.CreateSearch("Departments"));
//                }
//                else
//                {
//                    orgsData = SQL.ExecuteRead("SELECT * FROM Organizations ORDER BY Name");
//                }                

//                foreach (Dictionary<string, object> row in orgsData)
//                {
//                    var org = new Organization(row);
//                    Add(org);
//                }
//                loaded = true;
//            }            
//        }

//        /// <summary>
//        /// Добавить организацию
//        /// </summary>
//        public static void Add(Organization org)
//        {
//            if (IdOrganizations.ContainsKey(org.Id))
//            {
//                return;
//            }
//            else
//            {
//                IdOrganizations.Add(org.Id, org);
//            }
//            if (OrganizationsName.ContainsKey(org.Name))
//            {
//                Output.Error("Дублирование организации с наименованием {0}", org.Name);
//            }
//            else
//            {
//                OrganizationsName.Add(org.Name, org);
//            }
            
//            Organizations.Add(org);
//        }

//        /// <summary>
//        /// Удалить организацию
//        /// </summary>
//        public static void Delete(Organization org)
//        {
//            if (Organizations.Contains(org))
//            {
//                Organizations.Remove(org);
//            }
//            if (IdOrganizations.ContainsKey(org.Id))
//            {
//                IdOrganizations.Remove(org.Id);
//            }
//            if (OrganizationsName.ContainsKey(org.Name))
//            {
//                OrganizationsName.Remove(org.Name);
//            }            
//        }

//        /// <summary>
//        /// Очистить менеджер
//        /// </summary>
//        public static void Clear()
//        {
//            Organizations.Clear();
//            IdOrganizations.Clear();
//            OrganizationsName.Clear();
//        }
//    }
//}
