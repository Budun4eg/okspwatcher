﻿//using OKSPWatcher.Core.Departments;
//using System;
//using System.Collections.Generic;

//namespace OKSPWatcher.Core.Products
//{
//    /// <summary>
//    /// Изделие
//    /// </summary>
//    public class Product : DBObject
//    {
//        /// <summary>
//        /// Идентификатор отдела, к которому привязано изделие
//        /// </summary>
//        long depId = 0;

//        /// <summary>
//        /// Отдел, к которому привязано изделие
//        /// </summary>
//        public Department LinkedDepartment
//        {
//            get
//            {
//                if (DepartmentManager.IdDepartments.ContainsKey(depId))
//                {
//                    return DepartmentManager.IdDepartments[depId];
//                }
//                else
//                {
//                    Output.Log("[Product] Отдел с идентификатором {0} отсутствует", depId);
//                    return null;
//                }
//            }
//            set
//            {
//                depId = value != null ? value.Id : 0;
//                OnPropertyChanged("LinkedDepartment");
//            }
//        }

//        string name = "";
//        /// <summary>
//        /// Наименование изделия
//        /// </summary>
//        public string Name
//        {
//            get
//            {
//                return name;
//            }
//            set
//            {
//                name = value;
//                OnPropertyChanged("Name");
//            }
//        }

//        string analogs = "";
//        /// <summary>
//        /// Аналоги названия изделия
//        /// </summary>
//        public string Analogs
//        {
//            get
//            {
//                return analogs;
//            }
//            set
//            {
//                analogs = value;
//                OnPropertyChanged("Analogs");
//            }
//        }

//        /// <summary>
//        /// Список аналогов
//        /// </summary>
//        public List<string> AnalogList
//        {
//            get
//            {
//                List<string> analogList = new List<string>();
//                foreach(var spl in analogs.Split(new char[] { ',' }, StringSplitOptions.None))
//                {
//                    string toAdd = spl.Trim();
//                    if(toAdd != null && toAdd != "")
//                    {
//                        analogList.Add(toAdd);
//                    }                    
//                }
//                return analogList;
//            }
//        }

//        string topNodeName;
//        /// <summary>
//        /// Наименование верхней ячейки
//        /// </summary>
//        public string TopNodeName
//        {
//            get
//            {
//                return topNodeName;
//            }
//            set
//            {
//                topNodeName = value;
//                OnPropertyChanged("TopNodeName");
//            }
//        }

//        bool isCached;
//        /// <summary>
//        /// Есть кэшированная версия структуры
//        /// </summary>
//        public bool IsCached
//        {
//            get
//            {
//                return isCached;
//            }
//            set
//            {
//                isCached = value;
//                OnPropertyChanged("IsCached");
//            }
//        }

//        bool isActual;
//        /// <summary>
//        /// Изделие актуальное?
//        /// </summary>
//        public bool IsActual
//        {
//            get
//            {
//                return isActual;
//            }
//            set
//            {
//                isActual = value;
//                OnPropertyChanged("IsActual");
//            }
//        }

//        public override void Load(Dictionary<string, object> data)
//        {
//            base.Load(data);
//            try
//            {
//                if (data.ContainsKey("Name"))
//                {
//                    name = (string)data["Name"];
//                }
//                else
//                {
//                    Output.Error("У изделия #{0} отсутствует параметр Name", id);
//                }
//                if (data.ContainsKey("Department"))
//                {
//                    depId = (long)data["Department"];
//                }
//                else
//                {
//                    Output.Error("У изделия #{0} отсутствует параметр Department", id);
//                }
//                if (data.ContainsKey("IsActual"))
//                {
//                    isActual = ((long)data["IsActual"]).ToBool();
//                }
//                else
//                {
//                    Output.Error("У изделия #{0} отсутствует параметр IsActual", id);
//                }
//                if (data.ContainsKey("Analogs"))
//                {
//                    analogs = (string)data["Analogs"];
//                }
//                else
//                {
//                    Output.Error("У изделия #{0} отсутствует параметр Analogs", id);
//                }

//                if (data.ContainsKey("TopNodeName"))
//                {
//                    topNodeName = (string)data["TopNodeName"];
//                }
//                else
//                {
//                    Output.Error("У изделия #{0} отсутствует параметр TopNodeName", id);
//                }
//                if (data.ContainsKey("IsCached"))
//                {
//                    isCached = ((long)data["IsCached"]).ToBool();
//                }
//                else
//                {
//                    Output.Error("У изделия #{0} отсутствует параметр IsCached", id);
//                }
//            }
//            catch (Exception ex)
//            {
//                Output.Error(ex);
//            }
//        }

//        public Product(Dictionary<string, object> data)
//        {
//            Load(data);            
//        }

//        public Product()
//        {
            
//        }

//        protected override bool CreateNew()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Products WHERE Name = '{0}'", name);
//            if (check != null)
//            {
//                errorId = (long)check;
//                errorString = "Dublicate";
//                return false;
//            }
//            SQL.ExecuteId("INSERT INTO Products (Name, Department, IsActual, Analogs, TopNodeName, IsCached) VALUES ('{0}',{1},{2},'{3}','{4}',{5})", name.Check(), depId, isActual.ToLong(), analogs, topNodeName, isCached.ToLong());
//            base.CreateNew();            
//            ProductManager.Add(this);
//            return true;
//        }

//        protected override bool Update()
//        {
//            var check = SQL.ExecuteScalar("SELECT Id FROM Products WHERE Name = '{0}'", name);
//            if (check != null)
//            {
//                var prdId = (long)check;
//                if (prdId != id)
//                {
//                    errorString = "Dublicate";
//                    return false;
//                }
//            }
//            base.Update();
//            SQL.Execute("UPDATE Products SET Name = '{0}', Department = {1}, IsActual = {2}, Analogs = '{3}', TopNodeName = '{4}', IsCached = {5} WHERE Id = {6}", name.Check(), depId,isActual.ToLong(), analogs, topNodeName, isCached.ToLong(), id);
//            return true;
//        }

//        public override void Delete()
//        {
//            base.Delete();
//            ProductManager.Delete(this);
//            SQL.Execute("DELETE FROM Products WHERE Id = {0}", id);
//        }

//        public override void SaveState()
//        {
//            base.SaveState();
//            AddStateItem("LinkedDepartment", depId);
//            AddStateItem("Name", name);
//            AddStateItem("IsActual", isActual);
//            AddStateItem("Analogs", analogs);
//            AddStateItem("TopNodeName", topNodeName);
//            AddStateItem("IsCached", isCached);
//        }

//        public override void LoadState()
//        {
//            if (hasSavedState)
//            {
//                depId = (long)savedState["LinkedDepartment"];
//                name = (string)savedState["Name"];
//                isActual = (bool)savedState["IsActual"];
//                analogs = (string)savedState["Analogs"];
//                topNodeName = (string)savedState["TopNodeName"];
//                isCached = (bool)savedState["IsCached"];
//                base.LoadState();
//            }
//        }
//    }
//}
