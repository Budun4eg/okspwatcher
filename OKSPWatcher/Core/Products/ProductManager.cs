﻿//using System.Collections.Generic;

//namespace OKSPWatcher.Core.Products
//{
//    /// <summary>
//    /// Менеджер изделий
//    /// </summary>
//    public static class ProductManager
//    {
//        /// <summary>
//        /// Список доступных изделий
//        /// </summary>
//        public static SafeObservableCollection<Product> Products = new SafeObservableCollection<Product>();

//        /// <summary>
//        /// Доступ к изделию по ключу
//        /// </summary>
//        public static Dictionary<long, Product> IdProducts = new Dictionary<long, Product>();

//        /// <summary>
//        /// Доступ к изделию по наименованию
//        /// </summary>
//        public static Dictionary<string, Product> ProductNames = new Dictionary<string, Product>();

//        static SafeObservableCollection<Product> allProducts;

//        /// <summary>
//        /// Список переноса организаций
//        /// </summary>
//        static Dictionary<string, string> orgDefines = new Dictionary<string, string>()
//        {
//            {" КБСМ  (2 подписи)","КБСМ" },
//            {"Упр-ие качества","ГОЗ Управление качества" },
//            {"ОАО НПП Пружинный Центр","ОАО НПП Пружинный центр" }
//        };

//        /// <summary>
//        /// Переименовать организацию
//        /// </summary>
//        public static string DefineOrg(this string oldName)
//        {
//            return orgDefines.ContainsKey(oldName) ? orgDefines[oldName] : oldName;
//        }

//        public static SafeObservableCollection<Product> AllProducts
//        {
//            get
//            {
//                if (allProducts == null)
//                {
//                    allProducts = new SafeObservableCollection<Product>();

//                    var objData = SQL.ExecuteRead("SELECT * FROM Products ORDER BY Name");

//                    foreach (Dictionary<string, object> row in objData)
//                    {
//                        long id = row["Id"].ToString().ToLong();
//                        var prd = IdProducts.ContainsKey(id) ? IdProducts[id] : new Product(row);
//                        allProducts.Add(prd);
//                    }
//                }

//                return allProducts;
//            }
//        }

//        /// <summary>
//        /// Неопределенное изделие
//        /// </summary>
//        public static Product UnknownProduct { get; set; }

//        static ProductManager()
//        {
//            UnknownProduct = new Product();
//            UnknownProduct.Name = "Неопределено";
//        }

//        /// <summary>
//        /// Менеджер загружен?
//        /// </summary>
//        static bool loaded;

//        /// <summary>
//        /// Загрузить менеджер
//        /// </summary>
//        public static void Load(bool forced = false)
//        {
//            if(!loaded || forced)
//            {
//                var objData = SQL.ExecuteRead("SELECT * FROM Products ORDER BY Name");

//                foreach (Dictionary<string, object> row in objData)
//                {
//                    var prd = new Product(row);
//                    Add(prd);
//                }

//                loaded = true;
//            }            
//        }

//        /// <summary>
//        /// Загрузить менеджер с ограничениями
//        /// </summary>
//        public static void Load(object restriction, bool forced = false)
//        {
//            if(!loaded || forced)
//            {
//                var deserialize = restriction.DeserializeLong();
//                var objData = SQL.ExecuteRead("SELECT * FROM Products WHERE {0} ORDER BY Name", deserialize.CreateSearchIn("Id"));

//                foreach (Dictionary<string, object> row in objData)
//                {
//                    var prd = new Product(row);
//                    Add(prd);
//                }

//                loaded = true;
//            }            
//        }

//        /// <summary>
//        /// Добавить изделие
//        /// </summary>
//        public static void Add(Product prd)
//        {
//            if (IdProducts.ContainsKey(prd.Id))
//            {
//                return;
//            }
//            else
//            {
//                IdProducts.Add(prd.Id, prd);
//            }
//            if (ProductNames.ContainsKey(prd.Name))
//            {
//                Output.Error("Дублирование изделия с наименованием {0}", prd.Name);
//            }
//            else
//            {
//                ProductNames.Add(prd.Name, prd);
//            }
//            foreach(var analog in prd.AnalogList)
//            {
//                if (ProductNames.ContainsKey(analog))
//                {
//                    Output.Error("Дублирование аналога {0} для изделия {1}", analog, prd.Name);
//                }
//                else
//                {
//                    ProductNames.Add(analog, prd);
//                }
//            }
//            Products.Add(prd);
            
//        }

//        /// <summary>
//        /// Удалить изделие
//        /// </summary>
//        public static void Delete(Product prd)
//        {
//            if (Products.Contains(prd))
//            {
//                Products.Remove(prd);
//            }
//            if (IdProducts.ContainsKey(prd.Id))
//            {
//                IdProducts.Remove(prd.Id);
//            }
//            if (ProductNames.ContainsKey(prd.Name))
//            {
//                ProductNames.Remove(prd.Name);
//            }
//            foreach(var analog in prd.AnalogList)
//            {
//                if (ProductNames.ContainsKey(analog))
//                {
//                    ProductNames.Remove(analog);
//                }
//            }
//        }

//        /// <summary>
//        /// Очистить менеджер
//        /// </summary>
//        public static void Clear()
//        {
//            Products.Clear();
//            IdProducts.Clear();
//            ProductNames.Clear();
//        }
//    }
//}
