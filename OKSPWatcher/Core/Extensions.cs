﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

/// <summary>
/// v.2 opt
/// @ Vsevolod Nikitin
/// </summary>

namespace OKSPWatcher.Core
{
    /// <summary>
    /// Расширения классов
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Конвертировать long в bool
        /// </summary>
        public static bool ToBool(this long value)
        {
            return value != 0;
        }

        /// <summary>
        /// Конвертировать bool в long
        /// </summary>
        public static long ToLong(this bool value)
        {
            return value ? 1 : 0;
        }

        /// <summary>
        /// Конвертировать string в long
        /// </summary>
        public static long ToLong(this string value)
        {
            try
            {
                return long.Parse(value);
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                return -1;
            }
        }

        /// <summary>
        /// Конвертировать string в int
        /// </summary>
        public static int ToInt(this string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                return -1;
            }
        }

        /// <summary>
        /// Конвертировать string в int
        /// </summary>
        public static bool ToInt(this string value, out int ret)
        {
            try
            {
                int retVal = -1;
                if (int.TryParse(value, out retVal))
                {
                    ret = retVal;
                    return true;
                }
                else
                {
                    Output.WriteError("Не удалось конвертировать значение {0} в int", value);
                    ret = -1;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                ret = -1;
                return false;
            }
        }

        /// <summary>
        /// Конвертировать string в long
        /// </summary>
        public static bool ToLong(this string value, out long ret)
        {
            try
            {
                long retVal = -1;
                if (long.TryParse(value, out retVal))
                {
                    ret = retVal;
                    return true;
                }
                else
                {
                    Output.WriteError("Не удалось конвертировать значение {0} в long", value);
                    ret = -1;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                ret = -1;
                return false;
            }
        }

        /// <summary>
        /// Конвертировать string в int
        /// </summary>
        public static int ToInt(this string value, out bool ret)
        {
            try
            {
                int retVal = -1;
                if(int.TryParse(value, out retVal))
                {
                    ret = true;
                    return retVal;
                }
                else
                {
                    Output.WriteError("Не удалось конвертировать значение {0} в int" , value);
                    ret = false;
                    return -1;
                }                
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                ret = false;
                return -1;
            }
        }

        /// <summary>
        /// Конвертировать bool в видимость
        /// </summary>
        public static Visibility ToVisibility(this bool value)
        {
            return value ? Visibility.Visible : Visibility.Hidden;
        }

        /// <summary>
        /// Конвертировать string в double
        /// </summary>
        public static double ToDouble(this string value)
        {
            try
            {
                return double.Parse(value);
            }catch(Exception ex)
            {
                Output.WriteError(ex);
                return default(double);
            }            
        }

        #region SerializeLong
        /// <summary>
        /// Запаковать список long в строку
        /// </summary>
        public static string Serialize(this List<long> dict)
        {
            StringBuilder builder = new StringBuilder(AppSettings.SplitPreceed);
            foreach(long val in dict)
            {
                builder.AppendFormat("{0}{1}", val, AppSettings.Splitter);
            }
            if(builder.Length > 1)
            {
                builder.Remove(builder.Length - AppSettings.Splitter.Length, AppSettings.Splitter.Length - 1);
                return builder.ToString();
            }
            else
            {
                return "";
            }            
        }

        /// <summary>
        /// Распаковать объект в список long
        /// </summary>
        public static List<long> DeserializeLong(this object obj)
        {
            return obj.ToString().DeserializeLong();
        }

        /// <summary>
        /// Распаковать строку в список long
        /// </summary>
        public static List<long> DeserializeLong(this string str)
        {
            str = str.TrimStart(new char[] { AppSettings.SplitPreceedC }).TrimEnd(new char[] { AppSettings.SplitPreceedC });
            var splittedStr = str.Split(new string[] { AppSettings.Splitter }, StringSplitOptions.None);
            List<long> returnList = new List<long>();
            foreach (string spl in splittedStr)
            {
                if (spl == null || spl == "") continue;
                try
                {
                    returnList.Add(long.Parse(spl));
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                }
            }
            return returnList;
        }

        #endregion

        #region SerializeDBObj
        /// <summary>
        /// Запаковать в строку список DBObject
        /// </summary>
        public static string Serialize<T>(this SafeObservableCollection<T> coll, bool removeZero = false) where T : DBObject {
            StringBuilder builder = new StringBuilder(AppSettings.SplitPreceed);
            foreach (T val in coll)
            {
                if (removeZero)
                {
                    if(val != null && val.Id != 0)
                    {
                        builder.AppendFormat("{0}{1}", val.Id, AppSettings.Splitter);
                    }
                }
                else
                {
                    builder.AppendFormat("{0}{1}", val != null ? val.Id : 0, AppSettings.Splitter);
                }                
            }
            if(builder.Length > 1)
            {
                builder.Remove(builder.Length - AppSettings.Splitter.Length, AppSettings.Splitter.Length - 1);
                return builder.ToString();
            }
            else
            {
                return "";
            }            
        }

        #endregion
        
        #region SerializeString
        /// <summary>
        /// Запаковать в строку список строк
        /// </summary>
        public static string Serialize(this SafeObservableCollection<string> dict)
        {
            StringBuilder builder = new StringBuilder(AppSettings.SplitPreceed);
            foreach (string val in dict)
            {
                builder.AppendFormat("{0}{1}", val, AppSettings.Splitter);
            }
            if (builder.Length > 1)
            {
                builder.Remove(builder.Length - AppSettings.Splitter.Length, AppSettings.Splitter.Length - 1);
                return builder.ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Распаковать строку в список строк
        /// </summary>
        public static SafeObservableCollection<string> DeserializeString(this string str)
        {
            str = str.TrimStart(new char[] { AppSettings.SplitPreceedC }).TrimEnd(new char[] { AppSettings.SplitPreceedC });
            var splittedStr = str.Split(new string[] { AppSettings.Splitter }, StringSplitOptions.None);
            SafeObservableCollection<string> returnList = new SafeObservableCollection<string>();
            foreach (string spl in splittedStr)
            {
                try
                {
                    returnList.Add(spl);
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                }
            }
            return returnList;
        }
        #endregion
        
        /// <summary>
        /// Функция для получения дочернего объекта
        /// </summary>
        public static T GetVisualChild<T>(this DependencyObject obj) where T : DependencyObject
        {
            if(obj is T)
            {
                return (T)obj;
            }
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                var child = VisualTreeHelper.GetChild(obj, i);
                var result = child.GetVisualChild<T>();
                if(result == null)
                {
                    continue;
                }
                else
                {
                    return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Функция для получения родительского объекта
        /// </summary>
        public static T GetVisualParent<T>(this DependencyObject obj, string _tag = null) where T : DependencyObject
        {
            if (obj == null) return null;
            if (obj is T)
            {
                if(_tag != null)
                {
                    var objControl = (obj as FrameworkElement);
                    if(objControl != null)
                    {
                        var tagObj = objControl.Tag;
                        if (tagObj != null)
                        {
                            var tag = tagObj.ToString();
                            if (tag == _tag)
                            {
                                return (T)obj;
                            }
                        }
                    }                                       
                }
                else
                {
                    return (T)obj;
                }               
                
            }
            var parent = VisualTreeHelper.GetParent(obj);
            if(parent != null)
            {
                return parent.GetVisualParent<T>(_tag);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Создать строку поиска
        /// </summary>
        public static string CreateSearch(this List<long> items, string name,  bool withEmpty = true)
        {
            StringBuilder builder = new StringBuilder();
            if (withEmpty)
            {
                builder.AppendFormat("{0} = '' OR ", name);
            }
            
            foreach (long ind in items)
            {
                builder.AppendFormat("{0} LIKE '%|{1}|%' OR ", name, ind);
            }
            if(builder.Length > 0)
            {
                builder.Remove(builder.Length - 4, 4);
            }
            
            return builder.ToString();
        }

        /// <summary>
        /// Создать строку поиска
        /// </summary>
        public static string CreateSearch<T>(this SafeObservableCollection<T> items, string name, bool withEmpty = true) where T : DBObject
        {
            List<long> toCheck = new List<long>();
            foreach(T obj in items)
            {
                toCheck.Add(obj.Id);
            }
            return CreateSearch(toCheck, name, withEmpty);
        }

        /// <summary>
        /// Создать строку поиска
        /// </summary>
        public static string CreateSearchIn(this List<long> items, string name, bool withZero = true)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} IN (", name);
            if (withZero)
            {
                builder.Append("0,");
            }

            foreach (long ind in items)
            {
                builder.AppendFormat("{0},", ind);
            }
            if(builder.Length > 1)
            {
                builder.Remove(builder.Length - 1, 1);
            }
            builder.Append(")");
            
            return builder.ToString();
        }

        /// <summary>
        /// Создать строку поиска
        /// </summary>
        public static string CreateSearchIn<T>(this SafeObservableCollection<T> items, string name, bool withZero = true) where T :DBObject
        {
            List<long> toCheck = new List<long>();
            foreach (T obj in items)
            {
                toCheck.Add(obj.Id);
            }
            return CreateSearchIn(toCheck, name, withZero);
        }

        /// <summary>
        /// Словарь транслитерации
        /// </summary>
        static Dictionary<char, string> transDict = new Dictionary<char, string>() {
            {'а',"a" }, {'б',"b" }, {'в',"v" }, {'г',"g" }, {'д',"d" }, {'е',"e" }, {'ё',"e" }, {'ж',"j" }, {'з',"z" }, {'и',"i" }, {'й',"i" }, {'к',"k" }, {'л',"l" }, {'м',"m" }, {'н',"n" }, {'о',"o" }, {'п',"p" }, {'р',"r" }, {'с',"s" },
            {'т',"t" }, {'у',"u" }, {'ф',"f" }, {'х',"h" }, {'ц',"c" }, {'ч',"ch" }, {'ш',"sh" }, {'щ',"sch" }, {'ы',"yi" }, {'э',"ye" }, {'ю',"yu" }, {'я',"ya" },
            {'А',"A" }, {'Б',"B" }, {'В',"V" }, {'Г',"G" }, {'Д',"D" }, {'Е',"E" }, {'Ё',"E" }, {'Ж',"J" }, {'З',"Z" }, {'И',"I" }, {'Й',"I" }, {'К',"K" }, {'Л',"L" }, {'М',"M" }, {'Н',"N" }, {'О',"O" }, {'П',"P" }, {'Р',"R" }, {'С',"S" },
            {'Т',"T" }, {'У',"U" }, {'Ф',"F" }, {'Х',"H" }, {'Ц',"C" }, {'Ч',"CH" }, {'Ш',"SH" }, {'Щ',"SCH" }, {'Ы',"YI" }, {'Э',"YE" }, {'Ю',"YU" }, {'Я',"YA" }, {'.',"_" }
        };

        /// <summary>
        /// Провести транслитерацию строки
        /// </summary>
        public static string Translit(this string text)
        {
            StringBuilder builder = new StringBuilder();
            foreach(var ch in text)
            {
                if (transDict.ContainsKey(ch))
                {
                    builder.Append(transDict[ch]);
                }
                else
                {
                    builder.Append(ch);
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Варианты замены латинских символов на русские
        /// </summary>
        static Dictionary<string, string> replaceDict = new Dictionary<string, string>()
        {
            { "c", "с" },
            { "C", "С" },
            { "m", "м" },
            { "M", "М" }
        };

        /// <summary>
        /// Проверить наличие латинских символов под замену и заменить их
        /// </summary>
        public static bool CheckReplace(this string text, out string newVal)
        {
            var ret = text;
            bool found = false;
            foreach(var check in replaceDict)
            {
                if (ret.Contains(check.Key))
                {
                    found = true;
                    ret = ret.Replace(check.Key, check.Value);
                }
            }
            newVal = ret;
            return found;
        }

        /// <summary>
        /// Экранировать данные перед отправкой в БД
        /// </summary>
        public static string Check(this string toCheck)
        {
            return toCheck != null ? toCheck.Replace("'", '"'.ToString()).Trim() : null;
        }

        /// <summary>
        /// Создать цвет из строки
        /// </summary>
        public static Color FromString(this string data)
        {
            var spl = data.Split(new char[] { ';' });
            var c = Color.FromRgb((byte)spl[0].ToInt(), (byte)spl[1].ToInt(), (byte)spl[2].ToInt());
            return c;
        }

        /// <summary>
        /// Создать строку из цвета
        /// </summary>
        public static string FromColor(this Color color)
        {
            var ret = string.Format("{0};{1};{2}", color.R, color.G, color.B);
            return ret;
        }

        public static void AddLine(this RichTextBox box, string text)
        {
            var p = new Paragraph();
            p.Inlines.Add(text != null ? text : "");
            box.Document.Blocks.Add(p);
        }

        /// <summary>
        /// Задержка до проверки выполнения
        /// </summary>
        const int workerDelay = 250;

        /// <summary>
        /// Количество проверок на выполнение
        /// </summary>
        const int workerStep = 5;

        /// <summary>
        /// Запустить обработчик задач с проверкой
        /// </summary>
        public static void RunWorkerCheck(this BackgroundWorker worker)
        {
            if (!worker.IsBusy)
            {
                worker.RunWorkerAsync();
                return;
            }
            Output.WriteError("Фоновая задача {0} не может быть выполнена", worker.GetHashCode());
        }
    }
}
