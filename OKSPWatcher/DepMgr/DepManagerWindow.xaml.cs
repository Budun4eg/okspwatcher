﻿using System.Windows;
using System.ComponentModel;
using OKSPWatcher.Users;
using OKSPWatcher.Core;
using OKSPWatcher.Products;
using OKSPWatcher.Orgs;
using System.Windows.Controls;
using OKSPWatcher.SystemEditors.Windows;
using OKSPWatcher.Core.SQLProcessor;
using System;
using OKSPWatcher.Block;

namespace OKSPWatcher.DepMgr
{
    /// <summary>
    /// Логика взаимодействия для DepManagerWindow.xaml
    /// </summary>
    public partial class DepManagerWindow : BasicWindow
    {
        public DepManagerWindow()
        {
            InitializeComponent();

            worker.RunWorkerCheck();
        }

        UserPack users;
        /// <summary>
        /// Список пользователей отдела
        /// </summary>
        public UserPack Users
        {
            get
            {
                if(users == null)
                {
                    users = new UserPack();
                }
                return users;
            }
        }

        ProductPack products;
        /// <summary>
        /// Список изделий отдела
        /// </summary>
        public ProductPack Products
        {
            get
            {
                if(products == null)
                {
                    products = new ProductPack();
                }
                return products;
            }
        }

        OrgPack orgs;
        /// <summary>
        /// Список организаций
        /// </summary>
        public OrgPack Orgs
        {
            get
            {
                if(orgs == null)
                {
                    orgs = new OrgPack();
                }
                return orgs;
            }
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            Users.LoadDep(MainApp.Instance.CurrentUser.LinkedDep);
            Products.LoadDep(MainApp.Instance.CurrentUser.LinkedDep);
            Orgs.LoadAll();
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        private void AddUserClick(object sender, RoutedEventArgs e)
        {
            var user = new User();
            user.LinkedDep = MainApp.Instance.CurrentUser.LinkedDep;
            Users.Add(user);

            UserList.SelectedIndex = UserList.Items.Count - 1;
            UserList.ScrollIntoView(UserList.SelectedItem);
        }

        private void AddProdClick(object sender, RoutedEventArgs e)
        {
            var product = new Product();
            product.LinkedDep = MainApp.Instance.CurrentUser.LinkedDep;
            Products.Add(product);

            ProductList.SelectedIndex = ProductList.Items.Count - 1;
            ProductList.ScrollIntoView(ProductList.SelectedItem);
        }

        private void AddOrgClick(object sender, RoutedEventArgs e)
        {
            var org = new Org();
            Orgs.Add(org);

            OrgList.SelectedIndex = OrgList.Items.Count - 1;
            OrgList.ScrollIntoView(OrgList.SelectedItem);
        }

        private void ChangeProducts(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as User;
            ProductPackEditorWindow.ShowDep(item.Products, MainApp.Instance.CurrentUser.LinkedDep);
        }

        protected override void OnClosed(EventArgs e)
        {
            Users.Save();
            Products.Save();
            Orgs.Save();
            base.OnClosed(e);
            string blockCode = string.Format("App_DepManager_{0}", MainApp.Instance.CurrentUser.LinkedDep.Id);
            BlockManager.Instance.UnBlock(blockCode);
        }
    }
}
