﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Rules;
using OKSPWatcher.Secondary;
using OKSPWatcher.Users;

namespace OKSPWatcher.Block
{
    /// <summary>
    /// Менеджер блокировок
    /// </summary>
    public class BlockManager
    {
        private static object root = new object();

        static BlockManager instance;
        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static BlockManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new BlockManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public void Unload()
        {
            instance = null;
        }

        public void DeleteStructure()
        {
            SQL.Get("BlockItems").Execute(@"DROP TABLE BlockItems");
        }

        public void GenerateDBStructure()
        {
            SQL.Get("BlockItems").Execute(@"CREATE TABLE IF NOT EXISTS BlockItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Code TEXT,
                                                    User INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("BlockItems").Execute(@"CREATE INDEX IF NOT EXISTS BlockItems_Code ON BlockItems(Code)");
            instance = null;
        }

        /// <summary>
        /// Объект доступен?
        /// </summary>
        public bool Available(string code)
        {
            var item = SQL.Get("BlockItems").ExecuteRead("SELECT * FROM BlockItems WHERE Code = '{0}'", code).Single;
            if(item == null)
            {
                return true;
            }
            else
            {
                var user = UserManager.Instance.ById(item.Long("User"), true);
                if(user != null && user.Id == MainApp.Instance.CurrentUser.Id)
                {
                    return true;
                }
                Output.ErrorFormat("Заблокировано пользователем {0}", user != null ? user.Name : "");
                if (RulesManager.Instance.Can("UnblockObjects"))
                {
                    RequestWindow.Show("Разблокировать объект принудительно? Убедитесь, что объект не заблокирован намеренно пользователем {0}", user != null ? user.Name : "");
                    if(RequestWindow.Result == RequestWindow.ResultType.Yes)
                    {
                        UnBlock(code, true);
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Заблокировать объект
        /// </summary>
        public bool Block(string code, bool autoLock = false)
        {
            if (Available(code))
            {
                SQL.Get("BlockItems").Execute(@"INSERT INTO BlockItems (Code, User) VALUES ('{0}', {1})", code, MainApp.Instance.CurrentUser.Id);
                if (autoLock)
                {
                    BasicWindow.Current.IsLocked = true;
                }
                
                return true;
            }
            return false;
        }

        /// <summary>
        /// Разблокировать объект
        /// </summary>
        public void UnBlock(string code, bool forced = false)
        {
            if (forced || Available(code))
            {
                SQL.Get("BlockItems").Execute(@"DELETE FROM BlockItems WHERE Code = '{0}'", code);
                BasicWindow.Current.IsLocked = false;
            }            
        }
    }
}
