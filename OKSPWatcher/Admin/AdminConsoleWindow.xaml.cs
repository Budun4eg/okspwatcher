﻿using System;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows.Documents;
using System.Windows;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Логика взаимодействия для AdminConsoleWindow.xaml
    /// </summary>
    public partial class AdminConsoleWindow : BasicWindow
    {
        public AdminConsoleWindow()
        {
            SQL.RepeatMode = false;
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
            SQL.Instance.DebugMode = true;
            SQL.Instance.Proceed += GotAnswer;
        }

        protected override void OnClosed(EventArgs e)
        {
            SQL.Instance.Proceed -= GotAnswer;
            SQL.Instance.DebugMode = false;
            base.OnClosed(e);
            SQL.RepeatMode = true;
        }

        /// <summary>
        /// Получен ответ от БД
        /// </summary>
        private void GotAnswer(object sender, SQLEventArgs e)
        {
            TextRange range = new TextRange(OutputBox.Document.ContentEnd, OutputBox.Document.ContentEnd);
            range.Text = string.Format("{0}{1}", e.Text, Environment.NewLine);
        }

        string commandText;
        /// <summary>
        /// Команда для выполнения
        /// </summary>
        public string CommandText
        {
            get
            {
                return commandText;
            }
            set
            {
                commandText = value;
                OnPropertyChanged("CommandText");
            }
        }

        string dbSelector(string command)
        {
            try
            {
                var spl = CommandText.Split(new char[] { ' ' });
                var cm = command.ToUpper();
                if(cm.StartsWith("DROP TABLE"))
                {
                    for (int i = 0; i < spl.Length; i++)
                    {
                        var chk = spl[i].ToUpper();
                        if (chk == "TABLE")
                        {
                            return spl[i + 1];
                        }
                    }
                }
                if (cm.StartsWith("SELECT"))
                {
                    for (int i = 0; i < spl.Length; i++)
                    {
                        var chk = spl[i].ToUpper();
                        if (chk == "FROM")
                        {
                            return spl[i + 1];
                        }
                    }
                }
                if (cm.StartsWith("CREATE TABLE IF NOT EXISTS"))
                {
                    for (int i = 0; i < spl.Length; i++)
                    {
                        var chk = spl[i].ToUpper();
                        if (chk == "EXISTS")
                        {
                            return spl[i + 1];
                        }
                    }
                }
                if (cm.StartsWith("CREATE INDEX IF NOT EXISTS"))
                {
                    for (int i = 0; i < spl.Length; i++)
                    {
                        var chk = spl[i].ToUpper();
                        if (chk == "ON")
                        {
                            return spl[i + 1];
                        }
                    }
                }
                if (cm.StartsWith("ALTER TABLE"))
                {
                    for (int i = 0; i < spl.Length; i++)
                    {
                        var chk = spl[i].ToUpper();
                        if (chk == "TABLE")
                        {
                            return spl[i + 1];
                        }
                    }
                }
                return "Main";
            }
            catch
            {
                return "Main";
            }            
        }

        private void Proceed(object sender, System.Windows.RoutedEventArgs e)
        {

            SQL.Get(dbSelector(CommandText)).Execute(CommandText);
        }

        private void ProceedScalar(object sender, System.Windows.RoutedEventArgs e)
        {
            SQL.Get(dbSelector(CommandText)).ExecuteScalar(CommandText);
        }

        private void ProceedSet(object sender, System.Windows.RoutedEventArgs e)
        {
            SQL.Get(dbSelector(CommandText)).ExecuteRead(CommandText);
        }
    }
}
