﻿//using Microsoft.Office.Interop.Excel;
//using OKSPWatcher.Changes;
//using OKSPWatcher.Core;
//using OKSPWatcher.Core.Departments;
//using OKSPWatcher.Core.Organizations;
//using OKSPWatcher.Core.Products;
//using OKSPWatcher.Core.Users;
//using OKSPWatcher.Letters;
//using OKSPWatcher.RemarkReport;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Runtime.InteropServices;

//namespace OKSPWatcher.Admin
//{
//    /// <summary>
//    /// Класс для работы с Excel
//    /// </summary>
//    public static class ExcelHandler
//    {       
//        public static void ProceedDataFile(string path)
//        {
//            SQL.Open();
//            SQL.Execute("DELETE FROM Letters");
//            SQL.Execute("DELETE FROM LetterChains");
//            ChangeManager.Load();

//            var excelApp = new Application();
//            var excelBook = excelApp.Workbooks.Open(path);
//            var excelSheet = (Worksheet)excelBook.Worksheets[1];

//            var range = excelSheet.UsedRange;

//            StreamWriter errors = null;
//            StreamWriter output = null;
//            try {
//                errors = new StreamWriter(string.Format("{0}Ошибки импорта писем.txt", Core.AppSettings.OutputFolder));
//                output = new StreamWriter(string.Format("{0}Импорт писем.txt", Core.AppSettings.OutputFolder));
//            }
//            catch
//            {
//            }            

//            for(int row = 2; row <= range.Rows.Count; row++)
//            {
//                var letterChain = new LettersChain();
//                letterChain.LinkedDepartment = DepartmentManager.IdDepartments[2];
//                letterChain.Save();
//                if(output != null ) output.WriteLine("[Output] ===== Добавлена цепочка #{0} =====", letterChain.Id);
//                for(int column = 1; column <= range.Columns.Count; column++)
//                {
//                    Range cell = range.Cells[row, column] as Range;
//                    if(cell != null && cell.Value2 != null)
//                    {
//                        string value = cell.Value2.ToString();
//                        if(column == 2)
//                        {
//                            if (ProductManager.ProductNames.ContainsKey(value))
//                            {
//                                letterChain.ProductsList[0] = ProductManager.ProductNames[value];
//                            }
//                            else
//                            {
//                                if(errors != null) errors.WriteLine("[Error] Изделие {0} отсутствует ({1},{2})", value, row,column);
//                            }
//                        }else if(column == 3)
//                        {
//                            letterChain.DSEList = value;
//                        }else if(column == 4)
//                        {
//                            if(value == "Корректировка")
//                            {
//                                letterChain.ChainType = LettersManager.ChainTypes[0];
//                            }else if(value == "Информационное")
//                            {
//                                letterChain.ChainType = LettersManager.ChainTypes[1];
//                            }
//                        }
//                        else if(column == 5)
//                        {
//                            var letter1 = new Letter();
//                            letter1.LinkedDepartment = DepartmentManager.IdDepartments[2];
//                            value = value.DefineOrg();
//                            if (OrganizationManager.OrganizationsName.ContainsKey(value))
//                            {
//                                letter1.LinkedOrganization = OrganizationManager.OrganizationsName[value];
//                            }
//                            else
//                            {
//                                if (errors != null) errors.WriteLine("[Error] Организация {0} отсутствует ({1},{2})", value, row,column);
//                            }
//                            for (int i = 1; i <= 6; i++)
//                            {
//                                Range innerCell = range.Cells[row, column+i] as Range;
//                                if(innerCell != null && innerCell.Value2 != null)
//                                {
//                                    string innerValue = innerCell.Value2.ToString();
//                                    if(i == 1)
//                                    {
//                                        letter1.OutNumber = innerValue;
//                                    }else if(i == 2)
//                                    {
//                                        if (innerValue == "") continue;
//                                        try {
//                                            letter1.OutDate = DateTime.Parse(innerValue);
//                                        }
//                                        catch
//                                        {
//                                            try
//                                            {
//                                                letter1.OutDate = DateTime.FromOADate(innerValue.ToDouble());
//                                            }
//                                            catch
//                                            {
//                                                if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", innerValue, row, column + i);
//                                            }
//                                        }
//                                    }
//                                    else if(i == 3)
//                                    {
//                                        letter1.OkspNumber = innerValue;
//                                    }else if(i == 4)
//                                    {
//                                        if (innerValue == "") continue;
//                                        try {
//                                            letter1.OkspDate = DateTime.Parse(innerValue);
//                                        }
//                                        catch
//                                        {
//                                            try
//                                            {
//                                                letter1.OkspDate = DateTime.FromOADate(innerValue.ToDouble());
//                                            }
//                                            catch
//                                            {
//                                                if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", innerValue, row, column + i);
//                                            }
//                                        }
//                                    }else if(i == 5)
//                                    {
//                                        letter1.Description = innerValue;
//                                    }else if(i == 6)
//                                    {
//                                        string userName = innerValue.DefineUser();
//                                        if (UserManager.UsersName.ContainsKey(userName))
//                                        {
//                                            letter1.LinkedUser = UserManager.UsersName[userName];
//                                        }
//                                        else
//                                        {
//                                            if (errors != null) errors.WriteLine("[Error] Пользователь {0} отсутствует ({1},{2})", userName, row, column + i);
//                                        }
//                                    }
//                                }
//                            }
//                            if (letter1.Save())
//                            {
//                                letter1.Attach(letterChain, 0);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} присоединено",letter1.OutNumber);
//                            }
//                            else
//                            {
//                                LettersManager.IdLetters[letter1.ErrorId].Attach(letterChain, 0);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} добавлено ссылкой", LettersManager.IdLetters[letter1.ErrorId].OutNumber);
//                            }
//                        }
//                        else if (column == 12)
//                        {
//                            var letter2 = new Letter();
//                            letter2.LinkedDepartment = DepartmentManager.IdDepartments[2];
//                            value = value.DefineOrg();
//                            if (OrganizationManager.OrganizationsName.ContainsKey(value))
//                            {
//                                letter2.LinkedOrganization = OrganizationManager.OrganizationsName[value];
//                            }
//                            else
//                            {
//                                if (errors != null) errors.WriteLine("[Error] Организация {0} отсутствует ({1},{2})", value, row, column);
//                            }
//                            for (int i = 1; i <= 4; i++)
//                            {
//                                Range innerCell = range.Cells[row, column + i] as Range;
//                                if (innerCell != null && innerCell.Value2 != null)
//                                {
//                                    string innerValue = innerCell.Value2.ToString();
//                                    if (i == 1)
//                                    {
//                                        letter2.OutNumber = innerValue;
//                                    }
//                                    else if (i == 2)
//                                    {
//                                        if (innerValue == "") continue;
//                                        try
//                                        {
//                                            letter2.OutDate = DateTime.Parse(innerValue);
//                                        }
//                                        catch
//                                        {
//                                            try
//                                            {
//                                                letter2.OutDate = DateTime.FromOADate(innerValue.ToDouble());
//                                            }
//                                            catch
//                                            {
//                                                if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", innerValue, row, column + i);
//                                            }
//                                        }
//                                    }                                    
//                                    else if (i == 3)
//                                    {
//                                        letter2.Description = innerValue;
//                                    }
//                                    else if (i == 4)
//                                    {
//                                        string userName = innerValue.DefineUser();
//                                        if (UserManager.UsersName.ContainsKey(userName))
//                                        {
//                                            letter2.LinkedUser = UserManager.UsersName[userName];
//                                        }
//                                        else
//                                        {
//                                            if (errors != null) errors.WriteLine("[Error] Пользователь {0} отсутствует ({1},{2})", userName, row, column + i);
//                                        }
//                                    }
//                                }
//                            }
//                            if (letter2.Save())
//                            {
//                                letter2.Attach(letterChain, 1);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} присоединено", letter2.OutNumber);
//                            }
//                            else
//                            {
//                                LettersManager.IdLetters[letter2.ErrorId].Attach(letterChain, 1);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} добавлено ссылкой", LettersManager.IdLetters[letter2.ErrorId].OutNumber);
//                            }
//                        }
//                        else if (column == 17)
//                        {
//                            var letter3 = new Letter();
//                            letter3.LinkedDepartment = DepartmentManager.IdDepartments[2];
//                            if (letterChain.LettersList[1] != null && letterChain.LettersList[1].LinkedOrganization != null)
//                            {
//                                letter3.LinkedOrganization = letterChain.LettersList[1].LinkedOrganization;
//                            }
                            
//                            for (int i = 0; i <= 5; i++)
//                            {
//                                Range innerCell = range.Cells[row, column + i] as Range;
//                                if (innerCell != null && innerCell.Value2 != null)
//                                {
//                                    string innerValue = innerCell.Value2.ToString();
//                                    if (i == 0)
//                                    {
//                                        letter3.OutNumber = innerValue;
//                                    }
//                                    else if (i == 1)
//                                    {
//                                        if (innerValue == "") continue;
//                                        try
//                                        {
//                                            letter3.OutDate = DateTime.Parse(innerValue);
//                                        }
//                                        catch
//                                        {
//                                            try {
//                                                letter3.OutDate = DateTime.FromOADate(innerValue.ToDouble());
//                                            }
//                                            catch
//                                            {
//                                                if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", innerValue, row, column + i);
//                                            }
//                                        }
//                                    }
//                                    else if (i == 2)
//                                    {
//                                        letter3.OkspNumber = innerValue;
//                                    }
//                                    else if (i == 3)
//                                    {
//                                        if (innerValue == "") continue;
//                                        try
//                                        {
//                                            letter3.OkspDate = DateTime.Parse(innerValue);
//                                        }
//                                        catch
//                                        {
//                                            try {
//                                                letter3.OkspDate = DateTime.FromOADate(innerValue.ToDouble());
//                                            }
//                                            catch
//                                            {
//                                                if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", innerValue, row, column + i);
//                                            }
//                                        }
//                                    }
//                                    else if (i == 4)
//                                    {
//                                        letter3.Description = innerValue;
//                                    }
//                                    else if (i == 5)
//                                    {
//                                        string userName = innerValue.DefineUser();
//                                        if (UserManager.UsersName.ContainsKey(userName))
//                                        {
//                                            letter3.LinkedUser = UserManager.UsersName[userName];
//                                        }
//                                        else
//                                        {
//                                            if (errors != null) errors.WriteLine("[Error] Пользователь {0} отсутствует ({1},{2})", userName, row, column + i);
//                                        }
//                                    }
//                                }
//                            }
//                            if (letter3.Save())
//                            {
//                                letter3.Attach(letterChain, 2);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} присоединено", letter3.OutNumber);
//                            }
//                            else
//                            {
//                                LettersManager.IdLetters[letter3.ErrorId].Attach(letterChain, 2);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} добавлено ссылкой", LettersManager.IdLetters[letter3.ErrorId].OutNumber);
//                            }
//                        }
//                        else if (column == 23)
//                        {
//                            var letter4 = new Letter();
//                            letter4.LinkedDepartment = DepartmentManager.IdDepartments[2];
//                            if (letterChain.LettersList[0] != null && letterChain.LettersList[0].LinkedOrganization != null)
//                            {
//                                letter4.LinkedOrganization = letterChain.LettersList[0].LinkedOrganization;
//                            }
                            
//                            for (int i = 0; i <= 3; i++)
//                            {
//                                Range innerCell = range.Cells[row, column + i] as Range;
//                                if (innerCell != null && innerCell.Value2 != null)
//                                {
//                                    string innerValue = innerCell.Value2.ToString();
//                                    if (i == 0)
//                                    {
//                                        letter4.OutNumber = innerValue;
//                                    }
//                                    else if (i == 1)
//                                    {
//                                        if (innerValue == "") continue;
//                                        try
//                                        {
//                                            letter4.OutDate = DateTime.Parse(innerValue);
//                                        }
//                                        catch
//                                        {
//                                            try {
//                                                letter4.OutDate = DateTime.FromOADate(innerValue.ToDouble());
//                                            }
//                                            catch
//                                            {
//                                                if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", innerValue, row, column + i);
//                                            }
//                                        }
//                                    }
//                                    else if (i == 2)
//                                    {
//                                        letter4.Description = innerValue;
//                                    }
//                                    else if (i == 3)
//                                    {
//                                        string userName = innerValue.DefineUser();
//                                        if (UserManager.UsersName.ContainsKey(userName))
//                                        {
//                                            letter4.LinkedUser = UserManager.UsersName[userName];
//                                        }
//                                        else
//                                        {
//                                            if (errors != null) errors.WriteLine("[Error] Пользователь {0} отсутствует ({1},{2})", userName, row, column + i);
//                                        }
//                                    }
//                                }
//                            }
//                            if (letter4.Save())
//                            {
//                                letter4.Attach(letterChain, 3);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} присоединено", letter4.OutNumber);
//                            }
//                            else
//                            {
//                                LettersManager.IdLetters[letter4.ErrorId].Attach(letterChain, 3);
//                                if (output != null) output.WriteLine("[Output] Письмо {0} добавлено ссылкой", LettersManager.IdLetters[letter4.ErrorId].OutNumber);
//                            }
//                        }
//                        else if(column == 27)
//                        {
//                            letterChain.Correction = value.Length > 0 ? value : null;
//                        }
//                        else if(column == 28)
//                        {
//                            int index = 0;
//                            foreach(string spl in value.Split(new string[] { "\r\n","\n", "," }, StringSplitOptions.RemoveEmptyEntries))
//                            {
//                                if(spl.Length > 1)
//                                {
//                                    var splValue = spl.Trim();
//                                    var newNote = new ChangeNotice();
//                                    int splIndex = splValue.LastIndexOf('.');
//                                    if (splIndex > 0) {
//                                        var productName = splValue.Substring(0, splIndex);
//                                        if (ProductManager.ProductNames.ContainsKey(productName))
//                                        {
//                                            newNote.LinkedProduct = ProductManager.ProductNames[productName];
//                                            newNote.Name = splValue.Substring(splIndex + 1);

//                                            if (newNote.Save())
//                                            {
//                                                newNote.Attach(letterChain, index);
//                                                if (output != null) output.WriteLine("[Output] Извещение {0} присоединено", newNote.Name);
//                                                index++;
//                                            }
//                                            else
//                                            {
//                                                ChangeManager.IdChangeNotices[newNote.ErrorId].Attach(letterChain, index);
//                                                if (output != null) output.WriteLine("[Output] Извещение {0} добавлено ссылкой", ChangeManager.IdChangeNotices[newNote.ErrorId].Name);
//                                                index++;
//                                            }
//                                        }
//                                        else
//                                        {
//                                            if (errors != null) errors.WriteLine("[Error] Невозможно определить изделие извещения {0} ({1},{2})", splValue, row, column);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if (errors != null) errors.WriteLine("[Error] Невозможно определить изделие извещения {0} ({1},{2})", splValue, row, column);
//                                    }                                  
//                                }
//                            }
//                        }
//                        else if(column == 29)
//                        {
//                            int index = 0;
//                            foreach (string spl in value.Split(new string[] { "\r\n", "\n", "," }, StringSplitOptions.RemoveEmptyEntries))
//                            {
//                                if (spl.Length > 1)
//                                {
//                                    var splValue = spl.Trim();
//                                    var newNote = new ChangeProject();
//                                    int splIndex = splValue.LastIndexOf('.');
//                                    if (splIndex > 0)
//                                    {
//                                        var productName = splValue.Substring(0, splIndex);
//                                        if (ProductManager.ProductNames.ContainsKey(productName))
//                                        {
//                                            newNote.LinkedProduct = ProductManager.ProductNames[productName];
//                                            newNote.Name = splValue.Substring(splIndex + 1);

//                                            if (newNote.Save())
//                                            {
//                                                newNote.Attach(letterChain, index);
//                                                if (output != null) output.WriteLine("[Output] Проект {0} присоединен", newNote.Name);
//                                                index++;
//                                            }
//                                            else
//                                            {
//                                                ChangeManager.IdChangeProjects[newNote.ErrorId].Attach(letterChain, index);
//                                                if (output != null) output.WriteLine("[Output] Проект {0} добавлен ссылкой", ChangeManager.IdChangeProjects[newNote.ErrorId].Name);
//                                                index++;
//                                            }
//                                        }
//                                        else
//                                        {
//                                            if (errors != null) errors.WriteLine("[Error] Невозможно определить изделие проекта извещения {0} ({1},{2})", splValue, row, column);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if (errors != null) errors.WriteLine("[Error] Невозможно определить изделие проекта извещения {0} ({1},{2})", splValue, row, column);
//                                    }
//                                }
//                            }
//                        }
//                        else if(column == 30)
//                        {
//                            if(value == "Выполнено")
//                            {
//                                letterChain.IsCompleted = true;
//                            }
//                        }
//                    }                    
//                }
//                letterChain.Save();
//                if (errors != null) errors.Flush();
//                if (output != null) output.Flush();
//            }

//            if (errors != null) errors.Close();
//            if (output != null) output.Close();
//            excelBook.Close();
//            excelApp.Quit();

//            Marshal.ReleaseComObject(excelBook);
//            Marshal.ReleaseComObject(excelApp);
//            SQL.Close();
//        }

//        public static void ProceedReportFile(string path)
//        {
//            SQL.Open();
//            SQL.Execute("DELETE FROM RemarkReports");
//            SQL.Execute("DELETE FROM RemarkReportNotes");
//            RemarkReportManager.Load();

//            var excelApp = new Application();
//            var excelBook = excelApp.Workbooks.Open(path);
//            var excelSheet = (Worksheet)excelBook.Worksheets[1];

//            var range = excelSheet.UsedRange;

//            StreamWriter errors = null;
//            StreamWriter output = null;
//            try
//            {
//                errors = new StreamWriter(string.Format("{0}Ошибки импорта замечаний.txt", Core.AppSettings.OutputFolder));
//                output = new StreamWriter(string.Format("{0}Импорт замечаний.txt", Core.AppSettings.OutputFolder));
//            }
//            catch
//            {
//            }

//            var report = new RemarkReportItem();
//            long lastOutNumber = 1;
//            report.OutNumber = lastOutNumber;

//            for (int row = 3; row <= range.Rows.Count; row++)
//            {
//                long rowId = (range.Cells[row, 4] as Range).Value2.ToString().ToLong();
//                if (rowId != lastOutNumber)
//                {
//                    lastOutNumber = rowId;
//                    report = new RemarkReportItem();
//                    report.OutNumber = lastOutNumber;
//                }

//                var reportNote = new RemarkReportNote();

//                for (int column = 1; column <= range.Columns.Count; column++)
//                {
//                    Range cell = range.Cells[row, column] as Range;
//                    if (cell != null && cell.Value2 != null)
//                    {
//                        string value = cell.Value2.ToString();
//                        if (column == 1)
//                        {
//                            if (ProductManager.ProductNames.ContainsKey(value))
//                            {
//                                report.LinkedProduct = ProductManager.ProductNames[value];
//                            }
//                            else
//                            {
//                                if (errors != null) errors.WriteLine("[Error] Изделие {0} отсутствует ({1},{2})", value, row, column);
//                            }
//                        }
//                        else if (column == 2)
//                        {
//                            reportNote.Dse = value;
//                            string productName = "";
//                            if(report.LinkedProduct == null)
//                            {
//                                foreach(string name in ProductManager.ProductNames.Keys)
//                                {
//                                    if (value.StartsWith(name) && productName.Length < name.Length)
//                                    {
//                                        productName = name;
//                                    }
//                                }
//                            }

//                            if(productName != "")
//                            {
//                                report.LinkedProduct = ProductManager.ProductNames[productName];
//                            }

//                            if(report.LinkedDepartment == null && report.LinkedProduct != null)
//                            {
//                                report.LinkedDepartment = report.LinkedProduct.LinkedDepartment;
//                            }
//                        }
//                        else if (column == 3)
//                        {
//                            try
//                            {
//                                report.OutDate = DateTime.Parse(value);
//                            }
//                            catch
//                            {
//                                try
//                                {
//                                    report.OutDate = DateTime.FromOADate(value.ToDouble());
//                                }
//                                catch
//                                {
//                                    if (errors != null) errors.WriteLine("[Error] Дата {0} неправильная ({1},{2})", value, row, column);
//                                }
//                            }
//                        }
//                        else if (column == 6)
//                        {
//                            reportNote.State = value;
//                            if (value == "принято" || value == "отклонено")
//                            {
//                                reportNote.IsCompleted = true;
//                            }
//                        }
//                        else if (column == 8)
//                        {
//                            string userName = value.DefineUser();
//                            if (UserManager.UsersName.ContainsKey(userName))
//                            {
//                                report.LinkedUser = UserManager.UsersName[userName];
//                            }
//                            else
//                            {
//                                if (errors != null) errors.WriteLine("[Error] Пользователь {0} отсутствует ({1},{2})", userName, row, column);
//                            }
//                        }
//                        else if (column == 9)
//                        {
//                            reportNote.Description = value;
//                        }
//                        else if (column == 10)
//                        {
//                            reportNote.Note = value;
//                        }
//                    }
//                }

//                if(report.LinkedProduct == null)
//                {
//                    if (errors != null) errors.WriteLine("[Error] Невозможно определить изделие ({0})", row);
//                }

//                reportNote.Save();
//                report.Notes.Add(reportNote);
//                report.Save();
//            }

//            if (errors != null) errors.Close();
//            if (output != null) output.Close();
//            excelBook.Close();
//            excelApp.Quit();

//            Marshal.ReleaseComObject(excelBook);
//            Marshal.ReleaseComObject(excelApp);
//            SQL.Close();
//        }
//    }
//}
