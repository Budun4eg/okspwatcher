﻿using OKSPWatcher.Apps;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Orgs;
using OKSPWatcher.Products;
using OKSPWatcher.Secondary;
using OKSPWatcher.Users;
using OKSPWatcher.Worked;
using System.Windows;
using System.ComponentModel;
using OKSPWatcher.Models;
using OKSPWatcher.Changes;
using OKSPWatcher.ImageNotation;
using OKSPWatcher.Changes3D;
using OKSPWatcher.Remarks;
using OKSPWatcher.Letters;
using OKSPWatcher.Chains;
using OKSPWatcher.Texts;
using OKSPWatcher.Structure;
using OKSPWatcher.CompletedIn3D;
using OKSPWatcher.CompletedIn3D.StateManager;
using OKSPWatcher.Rules;
using OKSPWatcher.Utilities.Apps.PathCheck;
using OKSPWatcher.Utilities.Apps.CreateSearch;
using OKSPWatcher.Block;
using System.Diagnostics;
using OKSPWatcher.Utilities.Apps.BDBackup;
using OKSPWatcher.Structure.LoodsmanData;
using System.IO;
using System.Collections.Generic;
using System;
using OKSPWatcher.RequestModel;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Логика взаимодействия для AdminMainWindow.xaml
    /// </summary>
    public partial class AdminMainWindow : BasicWindow
    {
        public AdminMainWindow()
        {
            InitializeComponent();
        }

        private void EditApps(object sender, RoutedEventArgs e)
        {
            var window = new AdminAppEditWindow();
            window.Load(AppManager.Instance);
            window.Show();
        }

        private void BuildStructure(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Перестроить структуру БД? Все данные будут удалены", false);
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {                
                SQL.Instance.Create(tables);
                CheckDB(null, null);
            }
        }

        private void EditDeps(object sender, RoutedEventArgs e)
        {
            var window = new AdminDepEditWindow();
            window.Load(DepManager.Instance);
            window.Show();
        }

        private void EditProducts(object sender, RoutedEventArgs e)
        {
            var window = new AdminProductEditWindow();
            window.Load(ProductManager.Instance);
            window.Show();
        }

        private void EditUsers(object sender, RoutedEventArgs e)
        {
            var window = new AdminUserEditWindow();
            window.Load(UserManager.Instance);
            window.Show();
        }

        private void EditOrgs(object sender, RoutedEventArgs e)
        {
            var window = new AdminOrgEditWindow();
            window.Load(OrgManager.Instance);
            window.Show();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            AppManager.Instance.Unload();
            UserManager.Instance.Unload();
            DepManager.Instance.Unload();
            ProductManager.Instance.Unload();
            OrgManager.Instance.Unload();
            WorkedManager.Instance.Unload();
            ModelManager.Instance.Unload();
            ChangeImageManager.Instance.Unload();
            ChangeManager.Instance.Unload();
            Changes3DManager.Instance.Unload();
            ImageAnchorManager.Instance.Unload();
            RemarkManager.Instance.Unload();
            LetterManager.Instance.Unload();
            TextItemManager.Instance.Unload();
            ChainItemManager.Instance.Unload();
            ChainManager.Instance.Unload();
            ProductStructureManager.Instance.Unload();
            StructureNodeManager.Instance.Unload();
            CompletedItemManager.Instance.Unload();
            //PathCheckManager.Instance.Unload();
            StructureStateManager.Instance.Unload();
            RulesManager.Instance.Unload();
            RuleGroupManager.Instance.Unload();
            AdditionalPathManager.Instance.Unload();
            ProductSearchManager.Instance.Unload();
            BlockManager.Instance.Unload();
            AdditionalBackupManager.Instance.Unload();
            LoodsmanObjectManager.Instance.Unload();
            RequestModelManager.Instance.Unload();
            MainApp.Instance.Load();
            
        }

        private void OpenConsole(object sender, RoutedEventArgs e)
        {
            var window = new AdminConsoleWindow();
            window.Show();
        }

        private void RefreshTable(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Обнулить все данные менеджера?");
            if (RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                var tag = (sender as System.Windows.Controls.Control).Tag.ToString();
                switch (tag)
                {
                    case "AppManager":
                        AppManager.Instance.DeleteStructure();
                        AppManager.Instance.GenerateDBStructure();
                        break;
                    case "UserManager":
                        UserManager.Instance.DeleteStructure();
                        UserManager.Instance.GenerateDBStructure();
                        break;
                    case "DepManager":
                        DepManager.Instance.DeleteStructure();
                        DepManager.Instance.GenerateDBStructure();
                        break;
                    case "ProductManager":
                        ProductManager.Instance.DeleteStructure();
                        ProductManager.Instance.GenerateDBStructure();
                        break;
                    case "OrgManager":
                        OrgManager.Instance.DeleteStructure();
                        OrgManager.Instance.GenerateDBStructure();
                        break;
                    case "WorkedManager":
                        WorkedManager.Instance.DeleteStructure();
                        WorkedManager.Instance.GenerateDBStructure();
                        break;
                    case "ModelManager":
                        ModelManager.Instance.DeleteStructure();
                        ModelManager.Instance.GenerateDBStructure();
                        break;
                    case "ChangeImageManager":
                        ChangeImageManager.Instance.DeleteStructure();
                        ChangeImageManager.Instance.GenerateDBStructure();
                        break;
                    case "ChangeManager":
                        ChangeManager.Instance.DeleteStructure();
                        ChangeManager.Instance.GenerateDBStructure();
                        break;
                    case "Changes3DManager":
                        Changes3DManager.Instance.DeleteStructure();
                        Changes3DManager.Instance.GenerateDBStructure();
                        break;
                    case "ImageAnchorManager":
                        ImageAnchorManager.Instance.DeleteStructure();
                        ImageAnchorManager.Instance.GenerateDBStructure();
                        break;
                    case "RemarkManager":
                        RemarkManager.Instance.DeleteStructure();
                        RemarkManager.Instance.GenerateDBStructure();
                        break;
                    case "LetterManager":
                        LetterManager.Instance.DeleteStructure();
                        LetterManager.Instance.GenerateDBStructure();
                        break;
                    case "TextItemManager":
                        TextItemManager.Instance.DeleteStructure();
                        TextItemManager.Instance.GenerateDBStructure();
                        break;
                    case "ChainItemManager":
                        ChainItemManager.Instance.DeleteStructure();
                        ChainItemManager.Instance.GenerateDBStructure();
                        break;
                    case "ChainManager":
                        ChainManager.Instance.DeleteStructure();
                        ChainManager.Instance.GenerateDBStructure();
                        break;
                    case "ProductStructureManager":
                        ProductStructureManager.Instance.DeleteStructure();
                        ProductStructureManager.Instance.GenerateDBStructure();
                        break;
                    case "StructureNodeManager":
                        StructureNodeManager.Instance.DeleteStructure();
                        StructureNodeManager.Instance.GenerateDBStructure();
                        break;
                    case "CompletedItemManager":
                        CompletedItemManager.Instance.DeleteStructure();
                        CompletedItemManager.Instance.GenerateDBStructure();
                        break;
                    case "StructureStateManager":
                        StructureStateManager.Instance.DeleteStructure();
                        StructureStateManager.Instance.GenerateDBStructure();
                        break;
                    case "RulesManager":
                        RulesManager.Instance.DeleteStructure();
                        RulesManager.Instance.GenerateDBStructure();
                        RuleGroupManager.Instance.DeleteStructure();
                        RuleGroupManager.Instance.GenerateDBStructure();
                        break;
                    case "PathCheckManager":
                        //PathCheckManager.Instance.DeleteStructure();
                        //PathCheckManager.Instance.GenerateDBStructure();
                        break;
                    case "AdditionalPathManager":
                        AdditionalPathManager.Instance.DeleteStructure();
                        AdditionalPathManager.Instance.GenerateDBStructure();
                        break;
                    case "ProductSearchManager":
                        ProductSearchManager.Instance.DeleteStructure();
                        ProductSearchManager.Instance.GenerateDBStructure();
                        break;
                    case "BlockManager":
                        BlockManager.Instance.DeleteStructure();
                        BlockManager.Instance.GenerateDBStructure();
                        break;
                    case "AdditionalBackupManager":
                        AdditionalBackupManager.Instance.DeleteStructure();
                        AdditionalBackupManager.Instance.GenerateDBStructure();
                        break;
                    case "LoodsmanObjectManager":
                        LoodsmanObjectManager.Instance.DeleteStructure();
                        LoodsmanObjectManager.Instance.GenerateDBStructure();
                        break;
                    case "LoodsmanTreeManager":
                        LoodsmanTreeManager.Instance.DeleteStructure();
                        LoodsmanTreeManager.Instance.GenerateDBStructure();
                        break;
                    case "RequestModelManager":
                        RequestModelManager.Instance.DeleteStructure();
                        RequestModelManager.Instance.GenerateDBStructure();
                        break;
                }
                Output.Log("Структура перестроена");
            }
        }

        private void EditRules(object sender, RoutedEventArgs e)
        {
            var window = new RulesManagerWindow();
            window.Show();
        }

        private void CheckDB(object sender, RoutedEventArgs e)
        {
            AppSettings.GenerateDBStructure();
            AppManager.Instance.GenerateDBStructure();
            UserManager.Instance.GenerateDBStructure();
            DepManager.Instance.GenerateDBStructure();
            ProductManager.Instance.GenerateDBStructure();
            OrgManager.Instance.GenerateDBStructure();
            WorkedManager.Instance.GenerateDBStructure();
            ModelManager.Instance.GenerateDBStructure();
            ChangeImageManager.Instance.GenerateDBStructure();
            ChangeManager.Instance.GenerateDBStructure();
            Changes3DManager.Instance.GenerateDBStructure();
            ImageAnchorManager.Instance.GenerateDBStructure();
            RemarkManager.Instance.GenerateDBStructure();
            LetterManager.Instance.GenerateDBStructure();
            TextItemManager.Instance.GenerateDBStructure();
            ChainItemManager.Instance.GenerateDBStructure();
            ChainManager.Instance.GenerateDBStructure();
            ProductStructureManager.Instance.GenerateDBStructure();
            StructureNodeManager.Instance.GenerateDBStructure();
            CompletedItemManager.Instance.GenerateDBStructure();
            //PathCheckManager.Instance.GenerateDBStructure();
            StructureStateManager.Instance.GenerateDBStructure();
            RulesManager.Instance.GenerateDBStructure();
            RuleGroupManager.Instance.GenerateDBStructure();
            AdditionalPathManager.Instance.GenerateDBStructure();
            ProductSearchManager.Instance.GenerateDBStructure();
            BlockManager.Instance.GenerateDBStructure();
            AdditionalBackupManager.Instance.GenerateDBStructure();
            LoodsmanObjectManager.Instance.GenerateDBStructure();
            LoodsmanTreeManager.Instance.GenerateDBStructure();
            RequestModelManager.Instance.GenerateDBStructure();
            Output.Log("Структура БД проверена");
        }

        private void ToggleThreadMode(object sender, RoutedEventArgs e)
        {
            var mode = AppSettings.GetBool("ThreadMode");
            AppSettings.SetBool("ThreadMode", !mode);
            Output.LogFormat("Многопотоковый режим {0}", !mode);
            Debug.Print((!mode).ToString());
        }

        /// <summary>
        /// Список доступных таблиц
        /// </summary>
        List<string> tables = new List<string>()
        {
            "Settings",
            "AppItems",
            "Users",
            "UserDatas",
            "Departments",
            "Products",
            "Orgs",
            "WorkedItems",
            "ModelItems",
            "ChangeImages",
            "ChangeItems",
            "Changes3DItems",
            "Changes3DImages",
            "ImageAnchorItems",
            "RemarkItems",
            "RemarkReports",
            "Letters",
            "TextNotes",
            "ChainItems",
            "Chains",
            "StructureItems",
            "StructureNodes",
            "CompletedItems",
            "PathCheckedItems",
            "PathIgnoredItems",
            "StructureStateItems",
            "RuleItems",
            "RuleGroups",
            "AdditionalPathItems",
            "ProductSearchItems",
            "BlockItems",
            "AdditionalBackupItems",
            "LoodsmanObjectItems",
            "LoodsmanTreeItems",
            "RequestModelItems"
        };

        /// <summary>
        /// Список индексов к таблицам
        /// </summary>
        Dictionary<string, List<string>> indexes = new Dictionary<string, List<string>>() {
            {"Settings", new List<string>()
            {
                "Settings_Key"
            } },
            {"UserDatas", new List<string>()
            {
                "UserDatas_UserId"
            } },
            {"WorkedItems", new List<string>() {
                "WorkedItems_Name",
                "WorkedItems_PathToImage"
            } },
            {"ModelItems", new List<string>()
            {
                "ModelItems_Name",
                "ModelItems_PathToModel"
            } },
            {"ChangeImages", new List<string>()
            {
                "ChangeImages_Name",
                "ChangeImages_PathToImage"
            } },
            {"Changes3DItems", new List<string>()
            {
                "Changes3DItems_Change"
            } },
            {"Letters", new List<string>()
            {
                "Letters_OutNumber"
            } },
            {"StructureNodes", new List<string>()
            {
                "StructureNodes_Name",
                "StructureNodes_LoodsmanId"
            } },
            {"CompletedItems", new List<string>()
            {
                "CompletedItems_LinkedNode"
            } },
            {"PathCheckedItems", new List<string>()
            {
                "PathCheckedItems_Path"
            } },
            {"PathIgnoredItems", new List<string>()
            {
                "PathIgnoredItems_Path"
            } },
            {"BlockItems", new List<string>() {
                "BlockItems_Code"
            } },
            {"LoodsmanObjectItems", new List<string>() {
                "LoodsmanObjectItems_Name",
                "LoodsmanObjectItems_ObjectId"
            } },
            {"LoodsmanTreeItems", new List<string>()
            {
                "LoodsmanTreeItems_Name",
                "LoodsmanTreeItems_ObjectId",
                "LoodsmanTreeItems_ParentId"
            } }
        };

        void CleanDB(string table)
        {
            var sql = SQL.Get(table);

            foreach(var chk in tables)
            {
                if(table != chk)
                {
                    sql.Execute("DROP TABLE IF EXISTS {0}", chk);
                    if (indexes.ContainsKey(chk))
                    {
                        foreach(var ind in indexes[chk])
                        {
                            sql.Execute("DROP INDEX IF EXISTS {0}", ind);
                        }
                    }
                }
            }
        }

        string path, main;

        private void PackData(object sender, RoutedEventArgs e)
        {
            SQL.Get("Main").Vacuum();
            foreach (var table in tables)
            {
                SQL.Get(table).Vacuum();
            }
        }

        private void CopyDB(object sender, RoutedEventArgs e)
        {
            path = AppSettings.BaseFolder + "\\data\\";
            main = path + "Main.db";
            foreach (var table in tables)
            {
                try
                {
                    var to = path + string.Format("{0}.db", table);
                    if (!File.Exists(to))
                    {
                        File.Copy(main, to);
                    }
                }
                catch (Exception ex)
                {
                    Output.Error(ex, "Ошибка при копировании таблицы {0}", table);
                }
            }
        }

        private void AddUpdateDate(object sender, RoutedEventArgs e)
        {
            SQL.RepeatMode = false;
            foreach (var table in tables)
            {
                SQL.Get(table).Execute("ALTER TABLE {0} ADD COLUMN UpdateDate INTEGER DEFAULT 0", table);
            }
            SQL.RepeatMode = true;
        }

        private void SplitData(object sender, RoutedEventArgs e)
        {
            SQL.RepeatMode = false;
            path = AppSettings.BaseFolder + "\\data\\";
            main = path + "Main.db";
            foreach (var table in tables) {
                try
                {
                    CleanDB(table);
                }
                catch(Exception ex)
                {
                    Output.Error(ex, "Ошибка при разделении таблицы {0}", table);
                }                
            }
            SQL.RepeatMode = true;
        }
    }
}
