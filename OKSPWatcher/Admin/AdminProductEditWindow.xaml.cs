﻿using System.Windows;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Логика взаимодействия для AdminProductEditWindow.xaml
    /// </summary>
    public partial class AdminProductEditWindow : AdminProductEditor
    {
        public AdminProductEditWindow()
        {
            InitializeComponent();
        }

        protected override void Add(object sender, RoutedEventArgs e)
        {
            base.Add(sender, e);
            Scroll.SelectedIndex = Scroll.Items.Count - 1;
            Scroll.ScrollIntoView(Scroll.SelectedItem);
        }
    }
}
