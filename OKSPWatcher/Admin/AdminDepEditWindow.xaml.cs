﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Users;
using System.Windows;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Логика взаимодействия для AdminDepEditWindow.xaml
    /// </summary>
    public partial class AdminDepEditWindow : AdminDepEditor
    {
        public AdminDepEditWindow()
        {
            InitializeComponent();
        }

        public override void Load(DepManager _edit)
        {
            base.Load(_edit);
            UserManager.Instance.LoadAll();
        }

        /// <summary>
        /// Список пользователей
        /// </summary>
        public SafeObservableCollection<User> Users
        {
            get
            {
                return UserManager.Instance.ToChoose;
            }
        }

        protected override void Add(object sender, RoutedEventArgs e)
        {
            base.Add(sender, e);
            Scroll.SelectedIndex = Scroll.Items.Count - 1;
            Scroll.ScrollIntoView(Scroll.SelectedItem);
        }
    }
}
