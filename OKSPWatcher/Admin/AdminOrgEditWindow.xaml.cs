﻿using System.Windows;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Логика взаимодействия для AdminOrgEditWindow.xaml
    /// </summary>
    public partial class AdminOrgEditWindow : AdminOrgEditor
    {
        public AdminOrgEditWindow()
        {
            InitializeComponent();
        }

        protected override void Add(object sender, RoutedEventArgs e)
        {
            base.Add(sender, e);
            Scroll.SelectedIndex = Scroll.Items.Count - 1;
            Scroll.ScrollIntoView(Scroll.SelectedItem);
        }
    }
}
