﻿using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.SystemEditors.Windows;
using OKSPWatcher.Users;
using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Логика взаимодействия для AdminUserEditWindow.xaml
    /// </summary>
    public partial class AdminUserEditWindow : AdminUserEditor
    {
        public AdminUserEditWindow()
        {
            InitializeComponent();
        }

        public override void Load(UserManager _edit)
        {
            base.Load(_edit);
        }

        private void ChangeProducts(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as User;
            ProductPackEditorWindow.Show(item.Products);
        }

        protected override void Add(object sender, RoutedEventArgs e)
        {
            base.Add(sender, e);
            Scroll.SelectedIndex = Scroll.Items.Count - 1;
            Scroll.ScrollIntoView(Scroll.SelectedItem);
        }
    }
}
