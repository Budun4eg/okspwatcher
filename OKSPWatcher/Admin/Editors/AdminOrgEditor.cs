﻿using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Orgs;
using OKSPWatcher.Secondary;
using System.Collections.Generic;
using System.Windows;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Редактор организаций
    /// </summary>
    public class AdminOrgEditor : ManagerEditorWindow<OrgManager, Org>
    {
        public override void Load(OrgManager _edit)
        {
            base.Load(_edit);
            
            edit.LoadAll();
            edit.Sort();
        }

        protected override void InsertData(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Загрузить стандартные данные?");
            if (RequestWindow.Result != RequestWindow.ResultType.Yes)
            {
                return;
            }

            base.InsertData(sender, e);
            List<string> toInsert = new List<string>() {
                        "394 ВП МО РФ",
                        "5341 ВП МО РФ",
                        "444 ВП МО РФ",
                        "АО АДС",
                        "АО ВПК НПО машиностроения",
                        "АО ГРЦ Макеева",
                        "АО ПСЗ Янтарь",
                        "Арсенал",
                        "Воткинский завод",
                        "АО Центр технологии судостроения и судоремонта",
                        "ГОЗ ОГТ",
                        "ГОЗ ОКБ",
                        "ГОЗ ОКСП",
                        "ГОЗ отд.061",
                        "ГОЗ Отдел №021",
                        "ГОЗ Отдел №071",
                        "ГОЗ Отдел внешних связей",
                        "ГОЗ ОТК",
                        "ГОЗ ПК-2",
                        "ГОЗ ППО",
                        "ГОЗ ПЭО",
                        "ГОЗ Служба металлургического производства №183",
                        "ГОЗ СМП-005",
                        "ГОЗ Управление №036",
                        "ГОЗ Управление №107",
                        "ГОЗ Управление внешней кооперации и субконтрактинга",
                        "ГОЗ Управление закупок",
                        "ГОЗ Управление поставок и маркетинга",
                        "ГОЗ Центр испытаний №134",
                        "ГОЗ Цех №003",
                        "ГОЗ цех №046",
                        "ГОЗ Цех №210",
                        "ГОЗ Цех №220",
                        "ДВЗ Звезда",
                        "КБСМ",
                        "Красный гидропресс",
                        "ОАО НПП Пружинный центр",
                        "ОАО Пелла",
                        "ОКБ Новатор",
                        "ООО Металлографика",
                        "ООО Промлинк",
                        "ПАО АСЗ",
                        "ПАО НМЗ",
                        "ПАО НПО Алмаз",
                        "ПЗ Маш",
                        "Рубин",
                        "Северная верфь",
                        "Севмаш",
                        "Электроприбор",
                        "Юрга",
                        "ЦНИИ РТК",
                        "Прометей",
                        "ГОЗ Управление качества",
                        "Северное ПКБ",
                        "МЗиК",
                        "ЗАО Барс",
                        "ГОЗ ФЭУ",
                        "АО ЦТСС",
                        "ПАО Агрегат",
                        "АО ЦМКБ Алмаз",
                        "ГОЗ ОТД №121"
                    };
            foreach (var str in toInsert)
            {
                var newItem = new Org();
                newItem.Name = str;
                newItem.Save();
            }
        }
    }
}
