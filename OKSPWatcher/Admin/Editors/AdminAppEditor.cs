﻿using OKSPWatcher.Apps;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Core;
using System;
using System.Windows;
using OKSPWatcher.Deps;
using OKSPWatcher.Secondary;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Редактор приложений
    /// </summary>
    public class AdminAppEditor : ManagerEditorWindow<AppManager, AppItem>
    {
        public override void Load(AppManager _edit)
        {
            base.Load(_edit);
            
            edit.LoadAll();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            MainApp.Instance.SyncApps();
        }

        protected override void InsertData(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Загрузить стандартные данные?");
            if(RequestWindow.Result != RequestWindow.ResultType.Yes)
            {
                return;
            }

            base.InsertData(sender, e);

            var depManagerApp = new AppItem();
            depManagerApp.Name = "Управление отделом";
            depManagerApp.Code = "DepManager";
            depManagerApp.Save();

            var workedSettingsApp = new AppItem();
            workedSettingsApp.Name = "Проработанные чертежи [Настройки]";
            workedSettingsApp.Code = "WorkedSettings";
            workedSettingsApp.Save();

            var workedReportApp = new AppItem();
            workedReportApp.Name = "Проработанные чертежи [Отчет]";
            workedReportApp.Code = "WorkedReport";
            workedReportApp.Save();

            var workedRepairApp = new AppItem();
            workedRepairApp.Name = "Исправление проработанных чертежей";
            workedRepairApp.Code = "WorkedRepair";
            workedRepairApp.Report = workedReportApp;
            workedRepairApp.Settings = workedSettingsApp;
            workedRepairApp.Save();

            var modelSettingsApp = new AppItem();
            modelSettingsApp.Name = "Модели [Настройки]";
            modelSettingsApp.Code = "ModelSettings";
            modelSettingsApp.Save();

            var modelReportApp = new AppItem();
            modelReportApp.Name = "Модели [Отчет]";
            modelReportApp.Code = "ModelReport";
            modelReportApp.Save();

            var modelRepairApp = new AppItem();
            modelRepairApp.Name = "Исправление моделей";
            modelRepairApp.Code = "ModelRepair";
            modelRepairApp.Report = modelReportApp;
            modelRepairApp.Settings = modelSettingsApp;
            modelRepairApp.Save();

            var changesSettingsApp = new AppItem();
            changesSettingsApp.Name = "Извещения [Настройки]";
            changesSettingsApp.Code = "ChangesSettings";
            changesSettingsApp.Save();

            var changesRepairApp = new AppItem();
            changesRepairApp.Name = "Исправление извещений";
            changesRepairApp.Code = "ChangesRepair";
            changesRepairApp.Settings = changesSettingsApp;
            changesRepairApp.Save();

            var changes3DReportApp = new AppItem();
            changes3DReportApp.Name = "Проведение извещений в 3D [Отчет]";
            changes3DReportApp.Code = "Changes3DReport";
            changes3DReportApp.Save();

            var changes3DSettingsApp = new AppItem();
            changes3DSettingsApp.Name = "Проведение извещений в 3D [Настройки]";
            changes3DSettingsApp.Code = "Changes3DSettings";
            changes3DSettingsApp.Save();

            var changes3DApp = new AppItem();
            changes3DApp.Name = "Проведение извещений в 3D";
            changes3DApp.Code = "Changes3D";
            changes3DApp.Report = changes3DReportApp;
            changes3DApp.Settings = changes3DSettingsApp;
            changes3DApp.Save();

            var remarksReportApp = new AppItem();
            remarksReportApp.Name = "Замечания [Отчет]";
            remarksReportApp.Code = "RemarksReport";
            remarksReportApp.Save();

            var remarksSettingsApp = new AppItem();
            remarksSettingsApp.Name = "Замечания [Настройки]";
            remarksSettingsApp.Code = "RemarksSettings";
            remarksSettingsApp.Save();

            var remarksApp = new AppItem();
            remarksApp.Name = "Замечания";
            remarksApp.Code = "Remarks";
            remarksApp.Report = remarksReportApp;
            remarksApp.Settings = remarksSettingsApp;
            remarksApp.Save();

            var chainsApp = new AppItem();
            chainsApp.Name = "Учет писем";
            chainsApp.Code = "Chains";
            chainsApp.Save();

            var utilityApp = new AppItem();
            utilityApp.Name = "Утилиты";
            utilityApp.Code = "Utilities";
            utilityApp.Save();

            var structureSettingsApp = new AppItem();
            structureSettingsApp.Name = "Структуры изделий [Настройки]";
            structureSettingsApp.Code = "StructureSettings";
            structureSettingsApp.Save();

            var createStructureApp = new AppItem();
            createStructureApp.Name = "Структуры изделий";
            createStructureApp.Code = "CreateStructure";
            createStructureApp.Settings = structureSettingsApp;
            createStructureApp.Save();

            var completedIn3DApp = new AppItem();
            completedIn3DApp.Name = "Готовность изделий в 3D";
            completedIn3DApp.Code = "CompletedIn3D";
            completedIn3DApp.Save();

            var infoApp = new AppItem();
            infoApp.Name = "Информационное приложение отдела 3D";
            infoApp.Code = "InfoApp";
            infoApp.Save();
            
        }
    }
}
