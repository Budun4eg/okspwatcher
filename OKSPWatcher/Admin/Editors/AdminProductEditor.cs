﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using OKSPWatcher.Secondary;
using System.Collections.Generic;
using System.Windows;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Редактор изделий
    /// </summary>
    public class AdminProductEditor : ManagerEditorWindow<ProductManager, Product>
    {
        public override void Load(ProductManager _edit)
        {
            base.Load(_edit);
            
            DepManager.Instance.LoadAll();
            edit.LoadAll();
            edit.Sort();
        }

        /// <summary>
        /// Набор отделов
        /// </summary>
        public SafeObservableCollection<Dep> DepList
        {
            get
            {
                return DepManager.Instance.Items;
            }
        }

        protected override void InsertData(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Загрузить стандартные данные?");
            if (RequestWindow.Result != RequestWindow.ResultType.Yes)
            {
                return;
            }

            base.InsertData(sender, e);
            Dictionary<string, List<string>> toInsert = new Dictionary<string, List<string>>() {
                { "ПВО", new List<string>()
                {
                    "14П222",
                    "14Т535",
                    "2ТА100",
                    "2ТА120",
                    "50П6",
                    "51П6А",
                    "5П85СМ2",
                    "В20А",
                    "В30",
                    "В50А",
                    "СМ-853",
                    "ТМ966",
                    "51П6",
                    "5П85СЕ",
                    "СМ-915",
                    "СМ-592",
                    "СМ-599",
                    "14Б910",
                    "СМ-А275",
                    "СМ-598",
                    "СМ-808",
                    "Н1",
                    "Ф1",
                    "ШИ2",
                    "ШИ3",
                    "ШИ5",
                    "ШИ6",
                    "ШИ7",
                    "ШИ8",
                    "ШИ9",
                    "ЫК6",
                    "ЫК8",
                    "ЫК9",
                    "5П73-2М",
                    "5П85СМ",
                    "5П85С",
                    "ЫК4",
                    "ИТНЯ",
                    "ИЯШУ",
                    "ЮТ24"
                } },
                { "ВМФ", new List<string>() {
                    "3С-14-11356М",
                    "3С-14-11442М",
                    "3С-14-1155",
                    "3С-14-11661К",
                    "3С-14-20385",
                    "3С-14-21631",
                    "3С-14-22350",
                    "3С-14Э",
                    "3С-44",
                    "3С-44-02",
                    "3К96-3",
                    "3С97.2К.00",
                    "3С97.2К.00-20385",
                    "3С97.2К.10",
                    "3С97.2К.10-20385",
                    "3С97.2Ф.10",
                    "3С97.2Ф.20",
                    "3Ф-04",
                    "Б-204",
                    "СМ-456-11356М",
                    "СМ-456-11442М",
                    "СМ-456-11661К",
                    "СМ-456-22350",
                    "СМ-704",
                    "СМ-878",
                    "3С97.2Ф.00",
                    "СМ-225"
                } },
                { "РВСН", new List<string>()
                {
                    "15Т414",
                    "15У189",
                    "15У190",
                    "15У191",
                    "15Э615",
                    "15У196"
                } },
                { "", new List<string>()
                {
                    "ОБЕЗЛИЧЕННЫЕ",
                    "15У73",
                    "9К8",
                    "СМ-А246"
                } }
            };
            foreach (var dep in toInsert)
            {
                var depObj = DepManager.Instance.ByName(dep.Key).Single;
                if (depObj == null && dep.Key != "") return;
                foreach(var name in dep.Value)
                {
                    var newItem = new Product();
                    newItem.LinkedDep = depObj;
                    newItem.Name = name;
                    newItem.Save();
                }
            }
        }
    }
}
