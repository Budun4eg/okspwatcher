﻿using System.Windows;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using System.Collections.Generic;
using OKSPWatcher.Secondary;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Редактор отделов
    /// </summary>
    public class AdminDepEditor : ManagerEditorWindow<DepManager, Dep>
    {
        public override void Load(DepManager _edit)
        {
            base.Load(_edit);
            
            edit.LoadAll();
        }

        protected override void InsertData(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Загрузить стандартные данные?");
            if (RequestWindow.Result != RequestWindow.ResultType.Yes)
            {
                return;
            }

            base.InsertData(sender, e);
            List<string> toInsert = new List<string>() {
                "ПВО",
                "ВМФ",
                "РВСН",
                "Электрики",
                "3D",
                "ОКСП"
            };
            foreach(var str in toInsert)
            {
                var newItem = new Dep();
                newItem.Name = str;
                newItem.Save();
            }
        }
    }
}
