﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Users;
using System.Collections.Generic;
using System.Windows;
using System;
using OKSPWatcher.Secondary;

namespace OKSPWatcher.Admin
{
    /// <summary>
    /// Редактор пользователей
    /// </summary>
    public class AdminUserEditor : ManagerEditorWindow<UserManager, User>
    {
        public override void Load(UserManager _edit)
        {
            base.Load(_edit);
            
            DepManager.Instance.LoadAll();
            edit.LoadAll();
            edit.Sort();
        }

        /// <summary>
        /// Набор отделов
        /// </summary>
        public SafeObservableCollection<Dep> DepList
        {
            get
            {
                return DepManager.Instance.Items;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            MainApp.Instance.SyncApps();
        }

        protected override void InsertData(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Загрузить стандартные данные?");
            if (RequestWindow.Result != RequestWindow.ResultType.Yes)
            {
                return;
            }

            base.InsertData(sender, e);
            Dictionary<string, Dictionary<string, string>> toInsert = new Dictionary<string, Dictionary<string, string>>()
                    {
                        { "ПВО", new Dictionary<string, string>() {
                            {"056sag0010","Соколов Александр Георгиевич" },
                            {"056mai0011","Малюга Александр Иванович" },
                            {"056blv0013","Бахтина Лариса Валентиновна" },
                            {"056bmv0020","Бобылев Максим Витальевич" },
                            {"056isa0015","Иванов Сергей Александрович" },
                            {"056aay0132","Алексеева Анна Юрьевна" },
                            {"056dpv0019","Доценко  Павел Викторович" },
                            {"056lnv0016","Лебедева Наталья Владимировна" },
                            {"056fgg0017","Фирсанова Галина Геннадьевна" },
                            {"056sma0112","Серова Мария Александровна" },
                            {"056fdy0113","Федоров Дмитрий Юрьевич" },
                            {"056bsa0122","Бурлаков Степан Андреевич" },
                            {"056kpv0012","Коробов Павел Викторович" },
                            {"056kka0131","Хонин Кирилл Андреевич" },
                            {"056sef0107","Скуиньш Евгений Феликсович" }
                        } },
                        { "ВМФ", new Dictionary<string, string>() {
                            {"056tav0043","Цыганов Алексей Владимирович" },
                            {"056kdy0038","Киров Дмитрий Юрьевич" },
                            {"056bav0032","Березин Александр Владимирович" },
                            {"056evv0044","Еремеев Владимир Вячеславович" },
                            {"056anv0032","Александрова Наталья Владимировна" },
                            {"056bll0091","Беликов Леонид Леонидович" },
                            {"056smg0040","Слободинская Маргарита Германовна" },
                            {"056vvv0041","Воронина Виктория Валерьевна" },
                            {"056kmv0138","Коренев Максим Владимирович" },
                            {"056aks0117","Авдеева Ксения Сергеевна" },
                            {"056pas0133","Поляков Александр Сергеевич" },
                            {"056gyo0120","Гурский Ярослав Олегович" },
                            {"056kys0046","Коуру Яков Сергеевич" },
                            {"056pnd0130","Прокопенко Никита Денисович" },
                            {"056yis0135","Яшина Ирина Сергеевна" },
                            {"056fav0123","Федотов Алексей Витальевич" },
                            {"Некрасов Валентин Анатольевич","Некрасов Валентин Анатольевич" },
                            {"Бардина Ольга Павловна","Бардина Ольга Павловна" }
                        } },
                        { "РВСН", new Dictionary<string, string>()
                        {
                            {"056sas0095","Сердюков Александр Сергеевич" },
                            {"056sfv0127","Саенко Федор Валентинович" },
                            {"056mvb0056","Мядзюта Виктория Борисовна" },
                            {"056mzn0057","Морозова Зинаида Николаевна" },
                            {"056oal0054","Орлов Артур Леонидович" },
                            {"056baa0118","Бобкова Анастасия Андреевна" }
                        } },
                        { "Электрики", new Dictionary<string, string>() {
                            {"056gks0076","Гнездилов Константин Сергеевич" },
                            {"056mip0073","Мартьянова Ирина Петровна" },
                            {"056nks0071","Науменко Константин Сергеевич" },
                            {"056saa0077","Семенов Андрей Алексеевич" },
                            {"056mzr0137","Минахъметова Зульфира Рифовна" },
                            {"056big0078","Бычков Игорь Геннадьевич" },
                            {"056zea0109","Жукова Екатерина Андреевна" },
                            {"Молочкова Ольга Владимировна","Молочкова Ольга Владимировна" }
                        } },
                        {"3D", new Dictionary<string, string>() {
                            {"020lsi0721","Лончинский Сергей Игоревич" },
                            {"056ssv0100","Спирина Светлана Викторовна" },
                            {"020gia0484","Голубева Ирина Александровна" },
                            {"056vti0102","Вершинина Татьяна Ильинична" },
                            {"056zkv0096","Зуев Константин Викторович" },
                            {"056ngi0103","Нагорная Галина Ивановна" },
                            {"056sdr0111","Шагалеев Дмитрий Ринатович" },
                            {"056nvg0128","Никитин Всеволод Георгиевич" },
                            {"056erf0119","Еникеев Радмир Филаридович" },
                            {"020bna0138","Балашева Наталья Абибовна" },
                            {"Спиридонова Елена Александровна","Спиридонова Елена Александровна" },
                            {"Бусарева Мария Валерьевна","Бусарева Мария Валерьевна" }
                        } },
                        {"ОКСП", new Dictionary<string, string>() {
                            {"056ema0001","Евстафьев Михаил Альбертович" },
                            {"Голубева Вера Александровна","Голубева Вера Александровна" },
                            {"056sio0101","Сизова Ирина Олеговна" },
                            {"056mab0005","Мокринский Андрей Богданович" },
                        }}
                    };
            foreach(var dep in toInsert)
            {
                var depObj = DepManager.Instance.ByName(dep.Key);
                if (depObj == null || depObj.Single == null) return;
                foreach (var pack in dep.Value)
                {
                    var newItem = new User();
                    newItem.LinkedDep = depObj.Single;
                    newItem.LoginName = pack.Key;                    
                    newItem.Name = pack.Value;
                    newItem.Save();
                }
            }
        }
    }
}
