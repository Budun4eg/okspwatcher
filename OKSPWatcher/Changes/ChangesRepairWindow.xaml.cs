﻿using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Fixers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Documents;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Логика взаимодействия для ChangesRepairWindow.xaml
    /// </summary>
    public partial class ChangesRepairWindow : FixerWindow
    {
        public ChangesRepairWindow()
        {
            InitializeComponent();
            LoadShown = true;
            UpdateShown = true;
            ChangeImageManager.Instance.DataUpdated += ProceedStructure;
            ChangeImageManager.Instance.Update();
        }

        public override void StopUpdate()
        {
            base.StopUpdate();
            ChangeImageManager.Instance.CancelUpdate = true;
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            ignoredString = AppSettings.GetString("IgnoredChanges");
            ChangeImageManager.Instance.LoadAll();
        }

        private void ProceedStructure(object sender, EventArgs e)
        {
            UpdateShown = false;
            worker.RunWorkerCheck();
        }

        protected override void OnClosed(EventArgs e)
        {
            ChangeImageManager.Instance.CancelUpdate = true;
            ChangeImageManager.Instance.DataUpdated -= ProceedStructure;
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_ChangesRepair");
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        public override int TotalCount
        {
            get
            {
                int count = 0;
                for(int i = 0; i < ChangeImageManager.Instance.Items.Count; i++)
                {
                    var item = ChangeImageManager.Instance.Items[i];
                    if (!item.Parsed)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        protected override void StartCheck(object sender, RoutedEventArgs e)
        {
            OutputBox.Document.Blocks.Clear();
            base.StartCheck(sender, e);
            IgnoredCount = 0;
        }

        protected override void OutputToGUI(object sender, EventArgs e)
        {
            base.OutputToGUI(sender, e);
            lock (toShow)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    while (toShow.Count > 0)
                    {
                        var item = toShow.Dequeue();
                        OutputBox.AddLine(item.Text);
                    }
                    if (AutoScroll)
                    {
                        OutputBox.ScrollToEnd();
                    }
                }));
            }
        }

        bool deleteOnError;
        /// <summary>
        /// Удалять из БД извещения с неправильным наименованием
        /// </summary>
        public bool DeleteOnError
        {
            get
            {
                return deleteOnError;
            }
            set
            {
                deleteOnError = value;
                OnPropertyChanged("DeleteOnError");
            }
        }

        string ignoredString;
        /// <summary>
        /// Игнорируемые модели
        /// </summary>
        public string IgnoredString
        {
            get
            {
                return ignoredString;
            }
            set
            {
                ignoredString = value;
                AppSettings.SetString("IgnoredChanges", value);
                OnPropertyChanged("IgnoredString");
            }
        }

        int ignoredCount;
        /// <summary>
        /// Количество проигнорированных извещений
        /// </summary>
        public int IgnoredCount
        {
            get
            {
                return ignoredCount;
            }
            set
            {
                ignoredCount = value;
                OnPropertyChanged("IgnoredCount");
            }
        }

        bool replaceEng;
        /// <summary>
        /// Переименовывать латинские наименования?
        /// </summary>
        public bool ReplaceEng
        {
            get
            {
                return replaceEng;
            }
            set
            {
                replaceEng = value;
                OnPropertyChanged("ReplaceEng");
            }
        }

        void Check(ChangeImage item, ChangeParseData parse)
        {
            var info = new FileInfo(item.PathToImage);
            var name = info.Name.Remove(info.Name.Length - info.Extension.Length);
            var upperName = name.ToUpper();

            // Проверка на латинские буквы
            bool latinFound = false;
            var latinName = "";
            if (name.CheckReplace(out latinName))
            {
                latinFound = true;
            }

            if (latinFound)
            {
                if (ReplaceEng)
                {
                    //string newPath = string.Format("{0}\\{1}\\{2}{3}", info.Directory.Parent.FullName, latinDir, latinName, info.Extension);
                    //searchWorker.ReportProgress(5, string.Format("[Переименование латинских символов] {0} -> {1}", item.PathToImage, newPath));
                    //item.Move(newPath);
                    //ErrorCount++;
                    //Thread.Sleep(10);
                    //return;
                }
                else
                {
                    searchWorker.ReportProgress(3, string.Format("[Латинские символы в наименовании] {0}", item.PathToImage));
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
            }
            
            // Проверка формата
            string newName = "";
            bool hasExtraChars = false;

            if (name.Contains(" ."))
            {
                newName = name.Replace(" .", ".");
                hasExtraChars = true;
            }
            else if (name[name.Length - 1] == ' ')
            {
                newName = name.Remove(name.Length - 1);
                hasExtraChars = true;
            }
            else if (name[name.Length - 1] == '_')
            {
                newName = name.Remove(name.Length - 1);
                hasExtraChars = true;
            }
            else if (name.Contains(". "))
            {
                newName = name.Replace(". ", ".");
                hasExtraChars = true;
            }
            else
            {
                var spl = upperName.Split(new string[] { " Л" }, StringSplitOptions.RemoveEmptyEntries);
                if (spl.Length == 2)
                {
                    if (spl[1][0] != '.' && spl[1][0] != 'И')
                    {
                        newName = string.Format("{0} л.{1}", name.Remove(spl[0].Length), name.Remove(0, spl[0].Length + 2));
                        hasExtraChars = true;
                    }
                }
            }

            if (hasExtraChars)
            {
                if (ReplaceEmpty)
                {
                    //string newPath = string.Format("{0}\\{1}{2}", info.Directory.FullName, newName, info.Extension);
                    //searchWorker.ReportProgress(5, string.Format("[Исправление неправильного формата] {0} -> {1}", item.PathToImage, newPath));
                    //item.Move(newPath);
                    //ErrorCount++;
                    //Thread.Sleep(10);
                    //return;
                }
                else
                {
                    searchWorker.ReportProgress(3, string.Format("[Неправильный формат] {0}", item.PathToImage));
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
            }

            searchWorker.ReportProgress(2, string.Format("[Ошибка наименования] {0} {1}", item.PathToImage, parse.CheckString));
            ErrorCount++;
            Thread.Sleep(10);
        }

        bool replaceEmpty;
        /// <summary>
        /// Исправлять формат?
        /// </summary>
        public bool ReplaceEmpty
        {
            get
            {
                return replaceEmpty;
            }
            set
            {
                replaceEmpty = value;
                OnPropertyChanged("ReplaceEmpty");
            }
        }

        protected override void SearchErrors(object sender, DoWorkEventArgs e)
        {
            base.SearchErrors(sender, e);
            List<ChangeImage> toDelete = new List<ChangeImage>();
            List<string> ignored = new List<string>();

            foreach (var str in ignoredString.Split(new char[] { ',' }))
            {
                var toCheck = str.Trim();
                if (toCheck.Length > 0)
                {
                    ignored.Add(toCheck.ToUpper());
                }
            }

            foreach (ChangeImage item in ChangeImageManager.Instance.Items)
            {
                if (cancel)
                {
                    return;
                }
                if (!item.Parsed)
                {
                    bool mustIgnore = false;
                    foreach (var check in ignored)
                    {
                        if (item.PathToImage.ToUpper().Contains(check))
                        {
                            mustIgnore = true;
                            break;
                        }
                    }
                    if (mustIgnore)
                    {
                        IgnoredCount++;
                        continue;
                    }
                    var parse = ChangeParser.Instance.Parse(item.PathToImage);
                    item.Sync(parse);
                    item.Save();

                    if (!parse.IsParsed)
                    {
                        if (DeleteOnError)
                        {
                            Output.LogFormat("На удаление из БД: {0}", item.PathToImage);
                            toDelete.Add(item);
                        }
                        else
                        {
                            Check(item, parse);
                        }
                    }
                    else
                    {
                        Output.LogFormat("Исправлено изображение: {0}", item.PathToImage);

                        ChangeManager.Instance.AddImage(item);

                        RepairedCount++;
                    }
                }
            }

            while (toDelete.Count > 0)
            {
                toDelete[0].Delete();
                toDelete.RemoveAt(0);
            }
        }

        protected override void UpdateTemplate(object sender, RoutedEventArgs e)
        {
            base.UpdateTemplate(sender, e);
            ChangeParser.Instance.UpdateTemplate();
        }
    }
}
