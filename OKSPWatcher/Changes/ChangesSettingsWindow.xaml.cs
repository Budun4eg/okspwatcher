﻿using System;
using OKSPWatcher.Block;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows;

namespace OKSPWatcher.Changes
{
    /// <summary>
    /// Логика взаимодействия для ChangesSettingsWindow.xaml
    /// </summary>
    public partial class ChangesSettingsWindow : BasicWindow
    {
        public string PathToChanges
        {
            get
            {
                return ChangeImageManager.Instance.PathToChanges;
            }
            set
            {
                ChangeImageManager.Instance.PathToChanges = value;
            }
        }

        public ChangesSettingsWindow()
        {
            InitializeComponent();
            
            ChangeImageManager.Instance.CheckInstance();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_ChangesSettings");
        }
    }
}
