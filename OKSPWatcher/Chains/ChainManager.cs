﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Менеджер писем
    /// </summary>
    public class ChainManager : ManagerObject<Chain>
    {
        private static object root = new object();

        static ChainManager instance;

        protected ChainManager()
        {

        }

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ChainManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ChainManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Chains").Execute(@"DROP TABLE Chains");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Chains").Execute(@"CREATE TABLE IF NOT EXISTS Chains (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Products TEXT,
                                                    Dep INTEGER,
                                                    Dse TEXT,
                                                    Items TEXT,
                                                    Completed INTEGER,
                                                    User INTEGER,
                                                    Tags TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Загрузить все цепочки
        /// </summary>
        public void LoadAll(Dep dep = null)
        {
            if(dep == null)
            {
                base.LoadAll();
                var data = SQL.Get("Chains").ExecuteRead("SELECT * FROM Chains ORDER BY Id");
                foreach (SQLDataRow row in data)
                {
                    Add(new Chain(row));
                }
            }
            else
            {
                string request = string.Format("SELECT * FROM Chains WHERE Dep = {0} ORDER BY Id", dep.Id);
                var data = SQL.Get("Chains").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new Chain(row));
                }
            }            
        }

        /// <summary>
        /// Загрузить все невыполненные цепочки
        /// </summary>
        public void LoadIncompleted(Dep dep = null)
        {
            string request = string.Format("SELECT * FROM Chains WHERE Completed = 0 {0}ORDER BY Id", dep == null ? "" : string.Format("AND Dep = {0} ", dep.Id));
            var data = SQL.Get("Chains").ExecuteRead(request);
            foreach (SQLDataRow row in data)
            {
                Add(new Chain(row));
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
