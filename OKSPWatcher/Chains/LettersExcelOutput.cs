﻿//using Microsoft.Office.Interop.Excel;
//using System;
//using System.Diagnostics;
//using System.Runtime.InteropServices;

//namespace OKSPWatcher.Letters
//{
//    /// <summary>
//    /// Объект для подготовки отчета в Excel
//    /// </summary>
//    public static class LettersExcelOutput
//    {
//        [STAThread]
//        public static void Create()
//        {
//            var folderDialog = new System.Windows.Forms.FolderBrowserDialog();
//            folderDialog.Description = "Укажите путь до папки";
//            folderDialog.ShowNewFolderButton = true;
//            folderDialog.ShowDialog();
//            var path = folderDialog.SelectedPath;

//            var excelApp = new Application();
//            excelApp.DisplayAlerts = false;
//            var excelBook = excelApp.Workbooks.Add();
//            var excelSheet = (Worksheet)excelBook.Worksheets.Add();
//            excelSheet.Name = "Отчет по письмам";

//            var savePath = string.Format("{0}\\Отчет по письмам {1}.xlsx", path, DateTime.Now.ToShortDateString());

//            excelSheet.Cells[1, 1] = "№ п/п";
//            excelSheet.Cells[1, 2] = "Изделие";
//            excelSheet.Cells[1, 3] = "ДСЕ";
//            excelSheet.Cells[1, 4] = "Тип цепочки";

//            int maxOffset = 5;
//            int letterWidth = 7;
//            int maxLetterCount = 0;

//            for(int i = 0; i < LettersManager.Chains.Count; i++)
//            {
//                int currentOffset = 5;
//                var chain = LettersManager.Chains[i];
//                excelSheet.Cells[i + 2, 1] = i + 1;
//                excelSheet.Cells[i + 2, 2] = chain.ProductOutput != null ? chain.ProductOutput : "-";
//                excelSheet.Cells[i + 2, 3] = chain.DSEList != null ? chain.DSEList : "-";
//                excelSheet.Cells[i + 2, 4] = chain.ChainType != null ? chain.ChainType.Name : "-";
//                int letterInd = 0;
//                foreach(Letter letter in chain.LettersList)
//                {
//                    if(letter != null)
//                    {
//                        excelSheet.Cells[i + 2, 5 + letterInd * letterWidth] = letter.LinkedOrganization != null ? letter.LinkedOrganization.Name : "-";
//                        excelSheet.Cells[i + 2, 6 + letterInd * letterWidth] = letter.OutNumber;
//                        excelSheet.Cells[i + 2, 7 + letterInd * letterWidth] = letter.OutNumber != null && letter.OutNumber != "" ? letter.OutDate.ToShortDateString() : "";
//                        excelSheet.Cells[i + 2, 8 + letterInd * letterWidth] = letter.OkspNumber;
//                        excelSheet.Cells[i + 2, 9 + letterInd * letterWidth] = letter.OkspNumber != null && letter.OkspNumber != "" ? letter.OkspDate.ToShortDateString() : "";
//                        excelSheet.Cells[i + 2, 10 + letterInd * letterWidth] = letter.Description;
//                        excelSheet.Cells[i + 2, 11 + letterInd * letterWidth] = letter.LinkedUser != null ? letter.LinkedUser.Name : "-";
//                        currentOffset += letterWidth;
//                        if(currentOffset > maxOffset)
//                        {
//                            maxOffset = currentOffset;
//                        }
//                        letterInd++;
//                        if(letterInd > maxLetterCount)
//                        {
//                            maxLetterCount = letterInd;
//                        }
//                    }
//                }
//            }
            
//            for(int i = 0; i < maxLetterCount; i++)
//            {
//                excelSheet.Cells[1, 5 + i * letterWidth] = "Организация";
//                excelSheet.Cells[1, 6 + i * letterWidth] = "№ общий";
//                excelSheet.Cells[1, 7 + i * letterWidth] = "Дата общая";
//                excelSheet.Cells[1, 8 + i * letterWidth] = "№ ОКСП";
//                excelSheet.Cells[1, 9 + i * letterWidth] = "Дата ОКСП";
//                excelSheet.Cells[1, 10 + i * letterWidth] = "Описание";
//                excelSheet.Cells[1, 11 + i * letterWidth] = "Исполнитель";
//            }

//            for (int i = 0; i < LettersManager.Chains.Count; i++)
//            {
//                var chain = LettersManager.Chains[i];
//                excelSheet.Cells[i + 2, maxOffset] = chain.Correction;
//                excelSheet.Cells[i + 2, maxOffset + 1] = chain.ProjectsOutput;
//                excelSheet.Cells[i + 2, maxOffset + 2] = chain.ChangesOutput;
//                excelSheet.Cells[i + 2, maxOffset + 3] = chain.IsCompleted ? "Выполнено" : "";
//            }

//            excelSheet.Cells[1, maxOffset] = "Корректировка";
//            excelSheet.Cells[1, maxOffset + 1] = "Проекты извещений";
//            excelSheet.Cells[1, maxOffset + 2] = "Извещения";
//            excelSheet.Cells[1, maxOffset + 3] = "Статус выполнения";

//            excelSheet.Columns.AutoFit();
//            excelSheet.UsedRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
//            excelSheet.UsedRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
//            excelSheet.UsedRange.Borders.Weight = 2;
//            excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[1, excelSheet.UsedRange.Columns.Count]].Borders.Weight = 4;
//            excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[1, excelSheet.UsedRange.Columns.Count]].Font.Bold = true;

//            try {
//                excelBook.SaveAs(savePath);
//            }
//            catch
//            {
//#if IN_EDITOR
//                Debug.Print("[LettersExcelOutput] Ошибка при сохранении файла " + savePath);
//#endif
//            }
            
//            excelBook.Close();            
//            excelApp.Quit();

//            Marshal.ReleaseComObject(excelBook);
//            Marshal.ReleaseComObject(excelApp);
//        }
//    }
//}
