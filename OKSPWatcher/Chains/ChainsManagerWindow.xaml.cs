﻿using OKSPWatcher.Secondary;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using OKSPWatcher.Core;
using System.Windows.Input;
using OKSPWatcher.Letters;
using OKSPWatcher.Texts;
using OKSPWatcher.Rules;
using System;
using OKSPWatcher.Remarks;
using System.Diagnostics;
using OKSPWatcher.RequestModel;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Логика взаимодействия для ChainsManagerWindow.xaml
    /// </summary>
    public partial class ChainsManagerWindow : BasicWindow
    {
        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if(state == States.Load)
            {
                ChainHandler.Instance.CheckInstance();
            }else if(state == States.Save)
            {
                ChainItemManager.Instance.Save();
                ChainManager.Instance.Save();
            }            
        }

        public ChainsManagerWindow()
        {
            InitializeComponent();
            ChainItem.CheckAvailable();
            worker.RunWorkerCheck();
        }

        /// <summary>
        /// Варианты состояния
        /// </summary>
        enum States { Load, Save }

        /// <summary>
        /// Текущий вариант состояния
        /// </summary>
        States state = States.Load;

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            if(state == States.Load)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    DataContext = ChainHandler.Instance;
                }));
            }
            else if(state == States.Save)
            {
                Close();
            }                   
        }

        private void UpdateClick(object sender, RoutedEventArgs e)
        {
            ChainHandler.Instance.Reload();
        }

        private void NewChainClick(object sender, RoutedEventArgs e)
        {
            ChainHandler.Instance.New();           
        }

        private void RemoveChain(object sender, RoutedEventArgs e)
        {
            if (RulesManager.Instance.Can("DeleteChains"))
            {
                var chain = (sender as Control).DataContext as Chain;
                if (chain.LinkedUser == MainApp.Instance.CurrentUser || RulesManager.Instance.Can("ForcedDeleteChains"))
                {
                    RequestWindow.Show("Удалить цепочку?");
                    if (RequestWindow.Result == RequestWindow.ResultType.Yes)
                    {
                        foreach(var item in chain.Pack.Items)
                        {
                            if(item.ItemType == "Письмо" && item.Item != null)
                            {
                                (item.Item as Letter).Attached--;
                                item.Item.Save();
                            }
                        }
                        ChainHandler.Instance.Remove(chain);
                    }
                }
                else
                {
                    Output.Warning("Недостаточно прав для удаления цепочек другого пользователя");
                }
            }
            else
            {
                Output.Warning("Недостаточно прав для удаления цепочек");
            }
        }

        /// <summary>
        /// Данные были сохранены?
        /// </summary>
        bool saved = false;

        protected override void OnClosing(CancelEventArgs e)
        {
            if (!saved)
            {
                state = States.Save;
                saved = true;
                worker.RunWorkerCheck();
                e.Cancel = true;
            }
            base.OnClosing(e);
        }

        private void Search(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                ChainHandler.Instance.Search();
            }            
        }

        private void NewLetterClick(object sender, RoutedEventArgs e)
        {
            EditLetterWindow.Edit();
            ChainHandler.Instance.FreeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
        }

        private void AddItem(object sender, RoutedEventArgs e)
        {
            var chain = (sender as Control).DataContext as Chain;
            var item = new ChainItem();
            item.LinkedUser = MainApp.Instance.CurrentUser;
            item.Save();
            chain.Pack.Add(item);
            chain.Save();
        }

        private void CompleteChain(object sender, RoutedEventArgs e)
        {
            var chain = (sender as Control).DataContext as Chain;
            chain.Enabled = !chain.Enabled;
            chain.Save();
        }

        private void DeleteItem(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ChainItem;
            if (RulesManager.Instance.Can("DeleteChainItems"))
            {
                var chain = item.Parent;
                if (chain.LinkedUser == MainApp.Instance.CurrentUser || RulesManager.Instance.Can("ForcedDeleteChainItems"))
                {
                    RequestWindow.Show("Удалить элемент?");
                    if (RequestWindow.Result == RequestWindow.ResultType.Yes)
                    {
                        
                        if(item.ItemType == "Письмо" && item.Item != null)
                        {
                            (item.Item as Letter).Attached--;
                            item.Item.Save();
                        }
                        item.Delete();
                        chain.Pack.Remove(item);
                        chain.Save();
                        ChainHandler.Instance.FreeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
                    }
                }
                else
                {
                    Output.Warning("Недостаточно прав для удаления элементов другого пользователя");
                }
            }
            else
            {
                Output.Warning("Недостаточно прав для удаления элементов");
            }
        }

        private void PerformSearch(object sender, RoutedEventArgs e)
        {
            ChainHandler.Instance.Search();
        }

        private void EditData(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ChainItem;
            switch (item.ItemType)
            {
                case "Текстовая заметка":
                    var text = (item.Item as TextItem);
                    text.Text = GetTextWindow.Get("Текстовая заметка", "Введите текстовую заметку", text.Text);
                    text.Save();
                    break;
                case "Письмо":
                    EditLetterWindow.Edit(item.Item as Letter);
                    break;
                case "Отчет по замечаниям":
                    var rep = item.Item as RemarkReport;
                    try
                    {                        
                        Process.Start(rep.PathToReport);
                    }catch(Exception ex)
                    {
                        Output.Error(ex, "Ошибка при открытии отчета {0}", rep.PathToReport);
                    }
                    break;
                case "Ответ на запрос 3D-модели":
                    var req = item.Item as RequestModelItem;
                    try
                    {
                        Process.Start(req.PathToFolder);
                    }
                    catch (Exception ex)
                    {
                        Output.Error(ex, "Ошибка при открытии папки с заявкой {0}", req.PathToFolder);
                    }
                    break;
                    
            }
        }

        private void ChooseLetter(object sender, RoutedEventArgs e)
        {
            var chainItem = (sender as Control).DataContext as ChainItem;
            chainItem.ItemType = "Письмо";
            chainItem.Item = ChooseLetterWindow.Get();
            if(chainItem.Item != null)
            {
                (chainItem.Item as Letter).Attached++;
                chainItem.Item.Save();
            }
            ChainHandler.Instance.FreeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
        }

        private void ChooseText(object sender, RoutedEventArgs e)
        {
            var chainItem = (sender as Control).DataContext as ChainItem;
            var item = new TextItem();
            item.Text = GetTextWindow.Get("Текстовая заметка", "Введите текстовую заметку", item.Text);
            item.Save();
            chainItem.ItemType = "Текстовая заметка";
            chainItem.Item = item;
            chainItem.Save();
        }

        private void SetInfo(object sender, RoutedEventArgs e)
        {
            var chain = (sender as Control).DataContext as Chain;
            var window = new AddChainWindow();
            window.Load(chain);
            window.Show();
        }

        private void DeleteFreeLetter(object sender, RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("LettersRemove"))
            {
                Output.Warning("Удаление свободных писем запрещено");
                return;
            }
            RequestWindow.Show("Удалить письмо?");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                var item = (sender as Control).DataContext as Letter;
                item.Delete();
                ChainHandler.Instance.FreeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
            }            
        }

        private void EditLetter(object sender, RoutedEventArgs e)
        {
            var letter = (sender as Control).DataContext as Letter;
            EditLetterWindow.Edit(letter);
        }

        private Point startPoint;

        private void DragLeftButton(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        Letter dragged;

        private void DragMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                if (!RulesManager.Instance.Can("ChainAttachLetters"))
                {
                    Output.Warning("Привязка писем к цепочкам запрещена");
                    return;
                }
                var bt = (sender as Border);
                dragged = bt.DataContext as Letter;

                var listViewItem = (e.OriginalSource as DependencyObject).GetVisualParent<ListViewItem>();
                if (listViewItem == null) return;
                DragDrop.DoDragDrop(listViewItem, dragged, DragDropEffects.Move);
            }
        }

        private void ItemDrop(object sender, DragEventArgs e)
        {
            if (dragged == null) return;
            var gr = (sender as Grid);
            var target = gr.DataContext as ChainItem;
            target.AddLetter(dragged);
            ChainHandler.Instance.FreeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
        }

        private void ChooseRequest(object sender, RoutedEventArgs e)
        {
            var chainItem = (sender as Control).DataContext as ChainItem;
            var item = GenerateModelRequestWindow.Get();
            if (item == null) return;
            item.Save();
            chainItem.ItemType = "Ответ на запрос 3D-модели";
            chainItem.Item = item;
            chainItem.Save();
        }

        private void ChooseRemarks(object sender, RoutedEventArgs e)
        {
            var chainItem = (sender as Control).DataContext as ChainItem;
            var item = SelectReportWindow.Get();
            if (item == null) return;
            item.Save();
            chainItem.ItemType = "Отчет по замечаниям";
            chainItem.Item = item;
            chainItem.Save();
        }
    }
}
