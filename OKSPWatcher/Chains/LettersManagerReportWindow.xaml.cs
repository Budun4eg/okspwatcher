﻿using OKSPWatcher.Secondary;
using System.Windows;
using System.ComponentModel;
using OKSPWatcher.Core;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Логика взаимодействия для LettersManagerReport.xaml
    /// </summary>
    public partial class LettersManagerReportWindow : BasicWindow
    {
        public LettersManagerReportWindow()
        {
            InitializeComponent();
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            //SQL.Open();
            //LettersManager.Load();
            //SQL.Close();
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            //LettersExcelOutput.Create();
            NotifyWindow.ShowNote("Excel отчет сохранен");
            Close();
        }

        private void CreateReport(object sender, RoutedEventArgs e)
        {
            worker.RunWorkerCheck();            
        }
    }
}
