﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Набор элементов цепочки
    /// </summary>
    public class ChainItemPack : ItemPack<ChainItem>
    {
        /// <summary>
        /// Родительская цепочка
        /// </summary>
        Chain parent;

        public ChainItemPack(Chain _parent) : base()
        {
            parent = _parent;
            name = "ChainItemPack";
        }

        public override void Add(ChainItem item)
        {
            base.Add(item);
            item.Parent = parent;
        }

        public override void Remove(ChainItem item)
        {
            base.Remove(item);
            item.Parent = null;
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = ChainItemManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
