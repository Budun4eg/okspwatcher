﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Набор цепочек
    /// </summary>
    public class ChainPack : ItemPack<Chain>
    {
        /// <summary>
        /// Загрузить набор цепочек
        /// </summary>
        public void Load(bool withCompleted, bool onlyUser, bool onlyDep, bool onlyPrd)
        {
            if (withCompleted)
            {
                ChainManager.Instance.LoadAll(onlyDep ? MainApp.Instance.CurrentUser.LinkedDep : null);
            }
            else
            {
                ChainManager.Instance.LoadIncompleted(onlyDep ? MainApp.Instance.CurrentUser.LinkedDep : null);
            }
            items.Clear();
            foreach(var item in ChainManager.Instance.Items)
            {
                if (onlyDep && item.LinkedDep != MainApp.Instance.CurrentUser.LinkedDep) continue;
                if (onlyUser && item.LinkedUser != MainApp.Instance.CurrentUser) continue;
                if (onlyPrd)
                {
                    bool abort = true;

                    foreach(var prd in item.Products)
                    {
                        if (MainApp.Instance.CurrentUser.Products.Contains(prd))
                        {
                            abort = false;
                            break;
                        }
                    }

                    if (abort) continue;
                }
                if (withCompleted)
                {
                    items.Add(item);
                }
                else
                {
                    if (!item.Enabled)
                    {
                        items.Add(item);
                    }
                }
            }
        }
    }
}
