﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Deps;
using OKSPWatcher.Products;
using OKSPWatcher.Users;
using System;
using System.Text;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Цепочка писем
    /// </summary>
    public class Chain : DBObject
    {
        ProductPack products;
        /// <summary>
        /// Список привязанных изделий
        /// </summary>
        public ProductPack Products
        {
            get
            {
                if(products == null)
                {
                    products = new ProductPack();
                    products.Updated += PackUpdated;
                }
                return products;
            }
        }

        /// <summary>
        /// Информация о изделиях
        /// </summary>
        public string ProductInfo
        {
            get
            {
                if(Products.Count == 0)
                {
                    return "-";
                }
                StringBuilder builder = new StringBuilder();
                foreach(var prd in Products)
                {
                    builder.AppendFormat("{0} ,", prd.Name);
                }
                builder.Remove(builder.Length - 2, 2);
                return builder.ToString();
            }
        }

        /// <summary>
        /// Информация заполнена
        /// </summary>
        public bool HasInfo
        {
            get
            {
                if(products.Count > 0)
                {
                    return true;
                }
                if(Info != null && Info.Length > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public override void RefreshSearch()
        {
            base.RefreshSearch();
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} ", ProductInfo.ToUpper());
            if (LinkedDep != null)
            {
                builder.AppendFormat("{0} ", LinkedDep.Name.ToUpper());
            }
            if (LinkedUser != null)
            {
                builder.AppendFormat("{0} ", LinkedUser.Name.ToUpper());
            }

            builder.AppendFormat("{0} ", Tags.ToUpper());

            foreach (var item in Pack)
            {
                builder.AppendFormat("{0} ", item.SearchString);
            }

            searchString = builder.ToString();
        }

        private void PackUpdated(object sender, System.EventArgs e)
        {
            needUpdate = true;
            RefreshSearch();
            OnPropertyChanged("ProductInfo");
            OnPropertyChanged("HasInfo");
        }

        long depId = -1;
        /// <summary>
        /// Привязанный отдел
        /// </summary>
        public Dep LinkedDep
        {
            get
            {
                return DepManager.Instance.ById(depId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(depId != toSet)
                {
                    needUpdate = true;
                    RefreshSearch();
                }
                depId = toSet;
                OnPropertyChanged("LinkedDep");
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if(userId != toSet)
                {
                    needUpdate = true;
                    RefreshSearch();
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        string tags = "";
        /// <summary>
        /// Тэги
        /// </summary>
        public string Tags
        {
            get
            {
                return tags;
            }
            set
            {
                if(tags != value)
                {
                    needUpdate = true;
                    RefreshSearch();
                }
                tags = value;
                OnPropertyChanged("Tags");
            }
        }

        string info = "";
        /// <summary>
        /// Информация о цепочке
        /// </summary>
        public string Info
        {
            get
            {
                return info;
            }
            set
            {
                if(info != value)
                {
                    needUpdate = true;
                }
                info = value;
                OnPropertyChanged("Info");
                OnPropertyChanged("HasInfo");
            }
        }

        ChainItemPack pack;
        /// <summary>
        /// Набор элементов цепочки
        /// </summary>
        public ChainItemPack Pack
        {
            get
            {
                if(pack == null)
                {
                    pack = new ChainItemPack(this);
                    pack.Updated += PackUpdated;
                }
                return pack;
            }
        }

        bool enabled;
        /// <summary>
        /// Цепочка выполнена?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if(enabled != value)
                {
                    needUpdate = true;
                }
                enabled = value;
                OnPropertyChanged("Enabled");
            }
        }

        public Chain(SQLDataRow data) : base()
        {
            Load(data);
        }

        public Chain() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Products.Sync(data.Ids("Products"));
                foreach(var prd in Products)
                {
                    if(prd != null)
                    {
                        prd.CheckUpdate();
                    }                    
                }
                depId = data.Long("Dep");
                OnPropertyChanged("LinkedDep");
                if(LinkedDep != null)
                {
                    LinkedDep.CheckUpdate();
                }
                Info = data.String("Dse");
                Pack.Sync(data.Ids("Items"));
                foreach(var item in Pack.Items)
                {
                    if(item != null)
                    {
                        item.CheckUpdate();
                    }                    
                }
                Enabled = data.Bool("Completed");
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                Tags = data.String("Tags");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                Products.Sync(data.Ids("Products"));
                depId = data.Long("Dep");
                info = data.String("Dse");
                Pack.Sync(data.Ids("Items"));
                enabled = data.Bool("Completed");
                userId = data.Long("User");
                tags = data.String("Tags");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "Chains";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Chains (Products, Dep, Dse, Items, Completed, User, Tags, UpdateDate) VALUES ('{0}', {1}, '{2}', '{3}', {4}, {5}, '{6}', {7})", out id,
                Products.Serialize(), depId, info.Check(), Pack.Serialize(), enabled.ToLong(), userId, tags, UpdateDate.Ticks);
            base.CreateNew();
            ChainManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("UPDATE Chains SET Products = '{0}', Dep = {1}, Dse = '{2}', Items = '{3}', Completed = {4}, User = {5}, Tags = '{6}', UpdateDate = {7} WHERE Id = {8}",
                Products.Serialize(), depId, info.Check(), Pack.Serialize(), enabled.ToLong(), userId, tags, UpdateDate.Ticks, id);
            base.Update();
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM Chains WHERE Id = {0}", id);
            for(int i = Pack.Count-1; i >= 0; i--)
            {
                Pack.Items[i].Delete();
            }
            ChainManager.Instance.Remove(this);
        }

        public override bool Save()
        {
            Pack.Save();
            return base.Save();
        }
    }
}
