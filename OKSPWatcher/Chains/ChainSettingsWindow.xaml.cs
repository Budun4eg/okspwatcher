﻿using System;
using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.DepPaths;
using OKSPWatcher.Deps;
using System.Windows;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Логика взаимодействия для ChainSettingsWindow.xaml
    /// </summary>
    public partial class ChainSettingsWindow : BasicWindow
    {
        public ChainSettingsWindow()
        {
            InitializeComponent();
            DepManager.Instance.LoadAll();
            foreach(var dep in DepManager.Instance.Items)
            {
                items.Add(new DepPathItem(dep));
            }
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }
                
        SafeObservableCollection<DepPathItem> items = new SafeObservableCollection<DepPathItem>();
        /// <summary>
        /// Список путей
        /// </summary>
        public SafeObservableCollection<DepPathItem> Items
        {
            get
            {
                return items;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_ChainSettings");
        }
    }
}
