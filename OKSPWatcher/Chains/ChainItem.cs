﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Letters;
using OKSPWatcher.Models;
using OKSPWatcher.Remarks;
using OKSPWatcher.RequestModel;
using OKSPWatcher.Rules;
using OKSPWatcher.Texts;
using OKSPWatcher.Users;
using OKSPWatcher.Worked;
using System;
using System.Text;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Элемент цепочки
    /// </summary>
    public class ChainItem : DBObject
    {
        static ChainItem() {
            CheckAvailable();
        }

        /// <summary>
        /// Проверить доступные элементы
        /// </summary>
        public static void CheckAvailable()
        {
            attachLetter = RulesManager.Instance.Can("ChainAttachLetters");
            attachText = RulesManager.Instance.Can("ChainAttachText");
            attachRequest = RulesManager.Instance.Can("ChainAttachRequest");
            attachRemarks = RulesManager.Instance.Can("ChainAttachRemarks");
        }

        public override void RefreshSearch()
        {
            base.RefreshSearch();
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} ", Item.SearchString);
            builder.AppendFormat("{0} ", ItemType);
            searchString = builder.ToString();
        }

        Chain parent;
        /// <summary>
        /// Родительская цепочка
        /// </summary>
        public Chain Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
            }
        }

        static bool attachRequest;
        /// <summary>
        /// Добавление выполненных заявок по 3D-моделям
        /// </summary>
        public bool AttachRequest
        {
            get
            {
                if (Item != null)
                {
                    return false;
                }
                return attachRequest;
            }
        }

        static bool attachRemarks;
        /// <summary>
        /// Добавление отчетов по замечаниям
        /// </summary>
        public bool AttachRemarks
        {
            get
            {
                if (Item != null)
                {
                    return false;
                }
                return attachRemarks;
            }
        }

        static bool attachLetter;
        /// <summary>
        /// Добавление писем доступно
        /// </summary>
        public bool AttachLetter
        {
            get
            {
                if(Item != null)
                {
                    return false;
                }
                return attachLetter;
            }
        }

        static bool attachText;
        /// <summary>
        /// Добавление текстовых заметок доступно
        /// </summary>
        public bool AttachText
        {
            get
            {
                if (Item != null)
                {
                    return false;
                }
                return attachText;
            }
        }

        /// <summary>
        /// Присоединить письмо
        /// </summary>
        public void AddLetter(Letter letter)
        {
            if (letter == null) return;
            if(itemId >= 0 && itemType == "Письмо")
            {
                (Item as Letter).Attached--;
                Item.Save();
            }
            ItemType = "Письмо";
            Item = letter;
            letter.Attached++;
            letter.Save();            
        }

        long itemId = -1;
        /// <summary>
        /// Объект элемента
        /// </summary>
        public DBObject Item
        {
            get
            {
                switch (itemType)
                {
                    case "Модель":
                        return ModelManager.Instance.ById(itemId, true);
                    case "Проработанный чертеж":
                        return WorkedManager.Instance.ById(itemId, true);
                    case "Письмо":
                        return LetterManager.Instance.ById(itemId, true);
                    case "Текстовая заметка":
                        return TextItemManager.Instance.ById(itemId, true);
                    case "Отчет по замечаниям":
                        return RemarkManager.Instance.ById(itemId, true);
                    case "Ответ на запрос 3D-модели":
                        return RequestModelManager.Instance.ById(itemId, true);
                }
                return null;
            }
            set
            {
                long toSet = value != null ? value.Id : -1;            
                itemId = toSet;
                needUpdate = true;
                OnPropertyChanged("Item");
                OnPropertyChanged("AttachLetter");
                OnPropertyChanged("AttachText");
                OnPropertyChanged("AttachRemarks");
                OnPropertyChanged("AttachRequest");
                RefreshSearch();
                if(Parent != null)
                {
                    Parent.RefreshSearch();
                }
            }
        }

        long insertDate;
        /// <summary>
        /// Дата занесения элемента
        /// </summary>
        public DateTime InsertDate
        {
            get
            {
                return new DateTime(insertDate);
            }
            set
            {
                if(value.Ticks != insertDate)
                {
                    needUpdate = true;
                }
                insertDate = value.Ticks;
                OnPropertyChanged("InsertDate");
            }
        }

        long userId = -1;
        /// <summary>
        /// Привязанный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                return UserManager.Instance.ById(userId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if (userId != toSet)
                {
                    needUpdate = true;
                }
                userId = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        string itemType;
        /// <summary>
        /// Тип элемента
        /// </summary>
        public string ItemType
        {
            get
            {
                return itemType;
            }
            set
            {
                if(itemType != value)
                {
                    needUpdate = true;
                    RefreshSearch();
                    if (Parent != null)
                    {
                        Parent.RefreshSearch();
                    }
                }
                itemType = value;
                OnPropertyChanged("ItemType");
            }
        }

        public ChainItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public ChainItem() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                ItemType = data.String("ItemType");
                itemId = data.Long("Item");
                OnPropertyChanged("Item");
                if(Item != null)
                {
                    Item.CheckUpdate();
                }
                userId = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                InsertDate = new DateTime(data.Long("InsertDate"));
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                itemType = data.String("ItemType");
                itemId = data.Long("Item");                
                userId = data.Long("User");
                insertDate = data.Long("InsertDate");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "ChainItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO ChainItems (Item, ItemType, User, InsertDate, UpdateDate) VALUES ({0}, '{1}', {2}, {3}, {4})", out id,
                itemId, itemType, userId, insertDate, UpdateDate.Ticks);
            base.CreateNew();
            ChainItemManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE ChainItems SET Item = {0}, ItemType = '{1}', User = {2}, InsertDate = {3}, UpdateDate = {4} WHERE Id = {5}",
                itemId, itemType, userId, insertDate, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM ChainItems WHERE Id = {0}", id);
            ChainItemManager.Instance.Remove(this);
        }
    }
}
