﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Менеджер элементов цепочек
    /// </summary>
    public class ChainItemManager : ManagerObject<ChainItem>
    {
        private static object root = new object();

        static ChainItemManager instance;

        protected ChainItemManager() { }

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ChainItemManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ChainItemManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("ChainItems").Execute(@"DROP TABLE ChainItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("ChainItems").Execute(@"CREATE TABLE IF NOT EXISTS ChainItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Item INTEGER,
                                                    ItemType TEXT,
                                                    User INTEGER,
                                                    InsertDate INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }        

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override ChainItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM ChainItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("ChainItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new ChainItem(row));
                }

                return ById(id);
            }
            else
            {
                return ret;
            }
        }
    }
}
