﻿using OKSPWatcher.Core;
using OKSPWatcher.Letters;

namespace OKSPWatcher.Chains
{
    /// <summary>
    /// Менеджер цепочек
    /// </summary>
    public class ChainHandler : NotifyObject
    {
        private static object root = new object();

        static ChainHandler instance;

        protected ChainHandler() {
            Reload();            
        }

        /// <summary>
        /// Проверить наличие экземпляра
        /// </summary>
        public void CheckInstance() { }

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ChainHandler Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ChainHandler();
                        }
                    }                    
                }
                return instance;
            }
        }

        bool completedVisible;
        /// <summary>
        /// Показ выполненных цепочек включен?
        /// </summary>
        public bool CompletedVisible
        {
            get
            {
                return completedVisible;
            }
            set
            {
                completedVisible = value;
                Reload();
                OnPropertyChanged("CompletedVisible");
            }
        }

        bool onlyDep = true;
        /// <summary>
        /// Показ цепочек только своего отдела?
        /// </summary>
        public bool OnlyDep
        {
            get
            {
                return onlyDep;
            }
            set
            {
                onlyDep = value;
                Reload();
                OnPropertyChanged("OnlyDep");
            }
        }

        bool onlyLinkedProducts = false;
        /// <summary>
        /// Только привязанные изделия
        /// </summary>
        public bool OnlyLinkedProducts
        {
            get
            {
                return onlyLinkedProducts;
            }
            set
            {
                onlyLinkedProducts = value;
                Reload();
                OnPropertyChanged("OnlyLinkedProducts");
            }
        }

        bool onlyUser = false;
        /// <summary>
        /// Только объекты пользователя
        /// </summary>
        public bool OnlyUser
        {
            get
            {
                return onlyUser;
            }
            set
            {
                onlyUser = value;
                Reload();
                OnPropertyChanged("OnlyUser");
            }
        }

        string searchText;
        /// <summary>
        /// Текст для поиска
        /// </summary>
        public string SearchText
        {
            get
            {
                return searchText;
            }
            set
            {
                searchText = value;
                OnPropertyChanged("SearchText");
            }
        }

        /// <summary>
        /// Перезагрузить менеджер
        /// </summary>
        public void Reload()
        {
            ChainManager.Instance.CheckUpdate();
            LetterManager.Instance.CheckUpdate();
            Pack.Load(CompletedVisible, OnlyUser, OnlyDep, OnlyLinkedProducts);
            FreeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
        }

        ChainPack pack;
        /// <summary>
        /// Набор цепочек
        /// </summary>
        public ChainPack Pack
        {
            get
            {
                if(pack == null)
                {
                    pack = new ChainPack();
                }
                return pack;
            }
        }

        LetterPack freeLetters;
        /// <summary>
        /// Список свободных писем
        /// </summary>
        public LetterPack FreeLetters
        {
            get
            {
                if(freeLetters == null)
                {
                    freeLetters = new LetterPack();
                    freeLetters.Load(MainApp.Instance.CurrentUser.LinkedDep, true);
                }
                return freeLetters;
            }
        }

        /// <summary>
        /// Добавить новую цепочку
        /// </summary>
        public void New()
        {
            Chain item = new Chain();
            item.LinkedDep = MainApp.Instance.CurrentUser.LinkedDep;
            item.LinkedUser = MainApp.Instance.CurrentUser;
            item.Save();
            Pack.Add(item);
        }

        /// <summary>
        /// Удалить цепочку
        /// </summary>
        public void Remove(Chain item)
        {
            Pack.Remove(item);
            item.Delete();
        }

        /// <summary>
        /// Осуществить поиск
        /// </summary>
        public void Search()
        {
            ChainManager.Instance.LoadAll();
            Pack.Clear();
            var chk = searchText != null ? searchText.ToUpper() : "";
            foreach (var item in ChainManager.Instance.Items)
            {
                if (chk == "")
                {
                    Pack.Add(item);
                }
                else
                {
                    if (item.SearchString.Contains(chk))
                    {
                        Pack.Add(item);
                        continue;
                    }
                }
            }
        }
    }
}
