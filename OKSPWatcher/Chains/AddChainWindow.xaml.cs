﻿using OKSPWatcher.Chains;
using OKSPWatcher.Core;
using OKSPWatcher.Products;
using OKSPWatcher.SystemEditors;
using OKSPWatcher.SystemEditors.Editors;
using System;
using System.Windows;

namespace OKSPWatcher.Letters
{
    /// <summary>
    /// Логика взаимодействия для AddChainWindow.xaml
    /// </summary>
    public partial class AddChainWindow : BasicWindow
    {
        protected SafeObservableCollection<PackEditorItem<ProductPack, Product>> items = new SafeObservableCollection<PackEditorItem<ProductPack, Product>>();
        /// <summary>
        /// Элементы для редактирования
        /// </summary>
        public SafeObservableCollection<PackEditorItem<ProductPack, Product>> Items
        {
            get
            {
                return items;
            }
        }

        public AddChainWindow()
        {
            InitializeComponent();            
        }
                
        Chain edit;
        /// <summary>
        /// Цепочка для редактирования
        /// </summary>
        public Chain Edit
        {
            get
            {
                return edit;
            }
        }

        public void Load(Chain chain)
        {
            edit = chain;
            items.Clear();
            foreach (var prd in MainApp.Instance.CurrentUser.Products)
            {
                items.Add(new PackEditorItem<ProductPack, Product>(edit.Products, prd));
            }
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = chain;
            }));
        }

        private void SaveClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            edit.Save();
            base.OnClosed(e);
        }
    }
}
