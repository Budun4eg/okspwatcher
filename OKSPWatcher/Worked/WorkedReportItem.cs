﻿using OKSPWatcher.Products;
using OKSPWatcher.Reports;
using System.Collections.Generic;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Элемент отчета проработанных
    /// </summary>
    public class WorkedReportItem : ReportItem
    {
        public WorkedReportItem(Product product, List<WorkedPack> data, List<long> _intervals) : base(product, _intervals){
            foreach (var item in data)
            {
                var obj = item.Top;
                if (obj == null) continue;
                if (obj.Date.Ticks <= 0) continue;
                bool intervalFound = false;
                for (int i = 1; i < intervals.Count; i++)
                {
                    if (obj.Date.Ticks > intervals[i])
                    {
                        items[i - 1].SummaryCount++;
                        items[i - 1].Datas.Add(new ReportCellData(obj.LinkedUser, obj.Date, obj.Name));
                        if (obj.LinkedUser != null)
                        {                            
                            if (!items[i - 1].Users.ContainsKey(obj.LinkedUser))
                            {
                                items[i - 1].Users.Add(obj.LinkedUser, 0);
                            }
                            items[i - 1].Users[obj.LinkedUser]++;
                        }
                        intervalFound = true;
                        break;
                    }
                }
                if (!intervalFound)
                {
                    items[intervals.Count - 1].SummaryCount++;
                    items[intervals.Count - 1].Datas.Add(new ReportCellData(obj.LinkedUser, obj.Date, obj.Name));
                    if (obj.LinkedUser != null)
                    {                        
                        if (!items[intervals.Count - 1].Users.ContainsKey(obj.LinkedUser))
                        {
                            items[intervals.Count - 1].Users.Add(obj.LinkedUser, 0);
                        }
                        items[intervals.Count - 1].Users[obj.LinkedUser]++;
                    }
                }
            }
        }

        public WorkedReportItem(List<long> intervals, string _name) : base(intervals, _name)
        {

        }
    }
}
