﻿using System;
using OKSPWatcher.Block;
using OKSPWatcher.Core.SQLProcessor;
using System.Windows;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Логика взаимодействия для WorkedSettingsWindow.xaml
    /// </summary>
    public partial class WorkedSettingsWindow : BasicWindow
    {
        public string PathToWorked
        {
            get
            {
                return WorkedManager.Instance.PathToWorked;
            }
            set
            {
                WorkedManager.Instance.PathToWorked = value;
            }
        }

        public string ErrorFolderName
        {
            get
            {
                return WorkedManager.Instance.ErrorFolderName;
            }
            set
            {
                WorkedManager.Instance.ErrorFolderName = value;
            }
        }

        public WorkedSettingsWindow()
        {
            InitializeComponent();
            
            WorkedManager.Instance.CheckInstance();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_WorkedSettings");
        }
    }
}
