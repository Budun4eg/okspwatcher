﻿using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Fixers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Documents;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Логика взаимодействия для WorkedRepairWindow.xaml
    /// </summary>
    public partial class WorkedRepairWindow : FixerWindow
    {
        public override void StopUpdate()
        {
            base.StopUpdate();
            WorkedManager.Instance.CancelUpdate = true;
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            WorkedManager.Instance.LoadAll();
        }

        public WorkedRepairWindow()
        {
            InitializeComponent();
            LoadShown = true;
            UpdateShown = true;
            WorkedManager.Instance.DataUpdated += ProceedStructure;
            WorkedManager.Instance.Update();
        }

        private void ProceedStructure(object sender, EventArgs e)
        {
            UpdateShown = false;
            worker.RunWorkerCheck();
        }

        protected override void OnClosed(EventArgs e)
        {
            WorkedManager.Instance.CancelUpdate = true;
            WorkedManager.Instance.DataUpdated -= ProceedStructure;
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_WorkedRepair");
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        public override int TotalCount
        {
            get
            {
                int count = 0;
                for(int i = 0; i < WorkedManager.Instance.Items.Count; i++)
                {
                    var item = WorkedManager.Instance.Items[i];
                    if (!item.Parsed)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        protected override void StartCheck(object sender, RoutedEventArgs e)
        {
            OutputBox.Document.Blocks.Clear();
            base.StartCheck(sender, e);
        }

        protected override void OutputToGUI(object sender, EventArgs e)
        {
            base.OutputToGUI(sender, e);
            lock (toShow)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    while (toShow.Count > 0)
                    {
                        var item = toShow.Dequeue();
                        OutputBox.AddLine(item.Text);
                    }
                    if (AutoScroll)
                    {
                        OutputBox.ScrollToEnd();
                    }
                }));
            }
        }

        bool replaceEmpty = false;
        /// <summary>
        /// Убирать лишние пробелы
        /// </summary>
        public bool ReplaceEmpty
        {
            get
            {
                return replaceEmpty;
            }
            set
            {
                replaceEmpty = value;
                OnPropertyChanged("ReplaceEmpty");
            }
        }

        bool removeEmptyData;
        /// <summary>
        /// Удалять пустые данные
        /// </summary>
        public bool RemoveEmptyData
        {
            get
            {
                return removeEmptyData;
            }
            set
            {
                removeEmptyData = value;
                OnPropertyChanged("RemoveEmptyData");
            }
        }

        bool replaceEng;
        /// <summary>
        /// Заменять английские буквы
        /// </summary>
        public bool ReplaceEng
        {
            get
            {
                return replaceEng;
            }
            set
            {
                replaceEng = value;
                OnPropertyChanged("ReplaceEng");
            }
        }

        bool splitEntity;
        /// <summary>
        /// Разделять групповые чертежи
        /// </summary>
        public bool SplitEntity
        {
            get
            {
                return splitEntity;
            }
            set
            {
                splitEntity = value;
                OnPropertyChanged("SplitEntity");
            }
        }

        bool deleteOnError;
        /// <summary>
        /// Удалять из БД изображения с неправильным наименованием
        /// </summary>
        public bool DeleteOnError
        {
            get
            {
                return deleteOnError;
            }
            set
            {
                deleteOnError = value;
                OnPropertyChanged("DeleteOnError");
            }
        }

        /// <summary>
        /// Проверка проработанного чертежа при проблемах с разбором
        /// </summary>
        void Check(WorkedParseData parse, WorkedItem item)
        {
            var info = new FileInfo(item.PathToImage);
            var name = info.Name.Remove(info.Name.Length - info.Extension.Length);
            var upperName = name.ToUpper();

            // Проверка на латинские буквы
            bool latinFound = false;
            var latinName = "";            
            if (name.CheckReplace(out latinName))
            {
                latinFound = true;
            }
            var latinDir = "";
            if(info.Directory.Name.CheckReplace(out latinDir))
            {
                latinFound = true;
            }

            if (latinFound)
            {
                if (ReplaceEng)
                {
                    string newPath = string.Format("{0}\\{1}\\{2}{3}", info.Directory.Parent.FullName, latinDir, latinName, info.Extension);
                    searchWorker.ReportProgress(5, string.Format("[Переименование латинских символов] {0} -> {1}", item.PathToImage, newPath));
                    item.Move(newPath);
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
                else
                {
                    searchWorker.ReportProgress(3, string.Format("[Латинские символы в наименовании] {0}", item.PathToImage));
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }                
            }

            // Проверка групповых чертежей
            int groupFound = 0;
            foreach(var ch in name)
            {
                if(ch == '_')
                {
                    groupFound++;
                }
            }
            if(groupFound == 1)
            {
                var spl = name.Split(new char[] { ' ', '.', '-' }, StringSplitOptions.None);
                foreach (var splItem in spl)
                {
                    if (splItem.Contains("_"))
                    {
                        var find = splItem.Split(new char[] { '_' }, StringSplitOptions.None);
                        if (find.Length == 2)
                        {
                            var left = 0;
                            var right = 0;
                            if (find[0].ToInt(out left))
                            {
                                if (find[1].ToInt(out right))
                                {
                                    if (right > left)
                                    {
                                        if (SplitEntity)
                                        {
                                            int index = item.PathToImage.LastIndexOf('_');
                                            var leftName = item.PathToImage.Remove(index - find[0].Length);
                                            var rightName = item.PathToImage.Remove(0, leftName.Length + 1 + find[0].Length + find[1].Length);
                                            string template = string.Format("{0}{1}{2}", leftName, "{0}", rightName);
                                            searchWorker.ReportProgress(5, string.Format("[Разделение группового чертежа] {0}", item.PathToImage));
                                            for(int i = left; i <= right; i++)
                                            {
                                                string newPath = string.Format(template, i);
                                                searchWorker.ReportProgress(5, newPath);
                                                var newItem = item.Copy(newPath);
                                                if(newItem != null)
                                                {
                                                    toSave.Add(newItem);
                                                }                                                
                                            }
                                            File.Delete(item.PathToImage);
                                            toDelete.Add(item);
                                            ErrorCount++;
                                            Thread.Sleep(10);
                                            return;
                                        }
                                        else
                                        {
                                            searchWorker.ReportProgress(3, string.Format("[Групповой чертеж] {0}", item.PathToImage));
                                            ErrorCount++;
                                            Thread.Sleep(10);
                                            return;
                                        }                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Проверка потерянных данных
            bool hasLostData = false;
            string newName = "";
            if (upperName.Contains(" ИЗМ"))
            {
                var spl = upperName.Split(new string[] { " ИЗМ" }, StringSplitOptions.RemoveEmptyEntries);
                if(spl.Length == 1)
                {
                    newName = name.Remove(spl[0].Length);
                    hasLostData = true;
                }
            }

            if (hasLostData)
            {
                if (RemoveEmptyData)
                {
                    string newPath = string.Format("{0}\\{1}{2}", info.Directory.FullName, newName, info.Extension);
                    searchWorker.ReportProgress(5, string.Format("[Удаление потерянных данных] {0} -> {1}", item.PathToImage, newPath));
                    item.Move(newPath);
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
                else
                {
                    searchWorker.ReportProgress(3, string.Format("[Потерянные данные] {0}", item.PathToImage));
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
            }

            // Проверка формата
            bool hasExtraChars = false;

            if (name.Contains(" ."))
            {
                newName = name.Replace(" .", ".");
                hasExtraChars = true;
            }else if(name[name.Length-1] == ' ')
            {
                newName = name.Remove(name.Length - 1);
                hasExtraChars = true;
            }
            else if (name[name.Length - 1] == '_')
            {
                newName = name.Remove(name.Length - 1);
                hasExtraChars = true;
            }
            else if (name.Contains(". "))
            {
                newName = name.Replace(". ", ".");
                hasExtraChars = true;
            }
            else
            {
                var spl = upperName.Split(new string[] { " Л" }, StringSplitOptions.RemoveEmptyEntries);
                if(spl.Length == 2)
                {
                    if(spl[1][0] != '.' && spl[1][0] != 'И')
                    {
                        newName = string.Format("{0} л.{1}",name.Remove(spl[0].Length), name.Remove(0, spl[0].Length + 2));
                        hasExtraChars = true;
                    }
                }
                if (!hasExtraChars)
                {
                    var newSpl = upperName.Split(new string[] { " ИЗМ" }, StringSplitOptions.RemoveEmptyEntries);
                    if (newSpl.Length == 2)
                    {
                        if (newSpl[1][0] != '.')
                        {
                            newName = string.Format("{0} изм.{1}", name.Remove(newSpl[0].Length), name.Remove(0, newSpl[0].Length + 4));
                            hasExtraChars = true;
                        }
                    }
                }
            }

            if (hasExtraChars)
            {
                if (ReplaceEmpty)
                {
                    string newPath = string.Format("{0}\\{1}{2}", info.Directory.FullName, newName, info.Extension);
                    searchWorker.ReportProgress(5, string.Format("[Исправление неправильного формата] {0} -> {1}", item.PathToImage, newPath));
                    item.Move(newPath);
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
                else
                {
                    searchWorker.ReportProgress(3, string.Format("[Неправильный формат] {0}", item.PathToImage));
                    ErrorCount++;
                    Thread.Sleep(10);
                    return;
                }
            }

            searchWorker.ReportProgress(2, string.Format("[Ошибка наименования] {0} {1}", item.PathToImage, parse.CheckString));
            ErrorCount++;
            Thread.Sleep(10);
        }

        /// <summary>
        /// Элементы для сохранения
        /// </summary>
        List<WorkedItem> toSave = new List<WorkedItem>();

        /// <summary>
        /// Элементы для удаления
        /// </summary>
        List<WorkedItem> toDelete = new List<WorkedItem>();

        protected override void SearchErrors(object sender, DoWorkEventArgs e)
        {
            base.SearchErrors(sender, e);
            toDelete.Clear();
            toSave.Clear();

            foreach (WorkedItem item in WorkedManager.Instance.Items)
            {
                if (cancel)
                {
                    return;
                }
                if (!item.Parsed)
                {
                    var parse = WorkedParser.Instance.Parse(item.PathToImage);
                    item.Sync(parse);
                    item.Save();

                    if (!parse.IsParsed)
                    {
                        if (DeleteOnError)
                        {
                            Output.LogFormat("На удаление из БД: {0}", item.PathToImage);
                            toDelete.Add(item);
                        }
                        else
                        {
                            Check(parse, item);                            
                        }
                    }
                    else
                    {
                        Output.LogFormat("Исправлено изображение: {0}", item.PathToImage);
                        RepairedCount++;
                    }
                }
            }

            while (toDelete.Count > 0)
            {
                toDelete[0].Delete();
                toDelete.RemoveAt(0);
            }

            while (toSave.Count > 0)
            {
                try
                {
                    toSave[0].Save();
                    toSave.RemoveAt(0);
                }catch(Exception ex)
                {
                    Output.WriteError(ex);
                }                
            }
        }

        protected override void UpdateTemplate(object sender, RoutedEventArgs e)
        {
            base.UpdateTemplate(sender, e);
            WorkedParser.Instance.UpdateTemplate();
        }
    }
}
