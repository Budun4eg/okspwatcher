﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using OKSPWatcher.Core;
using System.ComponentModel;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Users;

namespace OKSPWatcher.Worked
{
    /// <summary>
    /// Логика взаимодействия для WorkedReportWindow.xaml
    /// </summary>
    public partial class WorkedReportWindow : WorkedReport
    {
        public override void StopUpdate()
        {
            base.StopUpdate();
            WorkedManager.Instance.CancelUpdate = true;
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            Total = 0;
            WorkedManager.Instance.LoadAll();
            var sorted = WorkedManager.Instance.Sorted;
            DateTime minimal = DateTime.Now;
            
            foreach (var prd in sorted)
            {
                foreach(var item in prd.Value)
                {
                    WorkedItem top = item.Top;
                    if (minimal > top.Date && top.Date.Ticks != 0)
                    {
                        minimal = top.Date;
                    }
                }                
            }
            DateTime current = DateTime.Now;
            var currentMonth = current.Day > 23 ? current.Month + 1 : current.Month;
            int currentYear = current.Year;
            if(currentMonth > 12)
            {
                currentYear++;
                currentMonth = 1;
            }
            DateTime start = new DateTime(currentYear, currentMonth, 23);
            while (start.Ticks > minimal.Ticks)
            {
                DateIntervals.Add(start.Ticks);
                int month = start.Month - 1;
                int year = start.Year;
                if (month < 1)
                {
                    month = 12;
                    year--;
                }
                start = new DateTime(year, month, 23);
            }
            Insert(HeaderPanel, null, "Изделия", "", true);
            foreach (long date in DateIntervals)
            {
                DateTime dateObj = new DateTime(date);
                int month = dateObj.Month - 1;
                int year = dateObj.Year;
                if (month < 1)
                {
                    month = 12;
                    year--;
                }
                DateTime left = new DateTime(year, month, 24);
                Insert(HeaderPanel, null, string.Format("{0} - {1}", left.ToShortDateString(), dateObj.ToShortDateString()), "", false, true);
            }

            foreach (var item in sorted)
            {
                ReportItems.Add(new WorkedReportItem(item.Key, item.Value, DateIntervals));
            }

            var summary = new WorkedReportItem(DateIntervals, "Всего");
            foreach (WorkedReportItem report in ReportItems)
            {
                for (int i = 0; i < report.Items.Count; i++)
                {
                    foreach(var item in report.Items[i].Datas)
                    {
                        summary.Items[i].Datas.Add(item);
                    }
                    summary.Items[i].SummaryCount += report.Items[i].SummaryCount;
                    foreach (KeyValuePair<User, int> userPair in report.Items[i].Users)
                    {
                        if (!summary.Items[i].Users.ContainsKey(userPair.Key))
                        {
                            summary.Items[i].Users.Add(userPair.Key, 0);
                        }
                        summary.Items[i].Users[userPair.Key] += userPair.Value;
                    }
                }
            }

            foreach (var cell in summary.Items)
            {
                Total += cell.SummaryCount;
            }
            ReportItems.Insert(0, summary);
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        public WorkedReportWindow()
        {
            InitializeComponent();
            LoadShown = true;
            UpdateShown = true;
            WorkedManager.Instance.DataUpdated += ProceedStructure;            
            WorkedManager.Instance.Update();
        }

        private void ProceedStructure(object sender, EventArgs e)
        {            
            UpdateShown = false;
            worker.RunWorkerCheck();
        }

        protected override void OnClosed(EventArgs e)
        {
            WorkedManager.Instance.CancelUpdate = true;
            WorkedManager.Instance.DataUpdated -= ProceedStructure;
            base.OnClosed(e);
        }

        private void LinkList(object sender, RoutedEventArgs e)
        {
            var scroll = OutputList.GetVisualChild<ScrollViewer>();

            scroll.ScrollChanged += ScrollChanged;
            header = HeaderList.GetVisualChild<ScrollViewer>();
        }
    }
}
