﻿using OKSPWatcher.Core;
using OKSPWatcher.Structure;

namespace OKSPWatcher.CompletedIn3D
{
    /// <summary>
    /// Набор готовности в 3D
    /// </summary>
    public class CompletedItemPack : ItemPack<CompletedItem>
    {
        public CompletedItemPack() { }

        public CompletedItemPack(StructureNodePack pack)
        {
            foreach (var item in pack)
            {
                if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                Add(CompletedItemManager.Instance.ByNode(item, true));
            }
        }
    }
}
