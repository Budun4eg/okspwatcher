﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Structure;
using System.Collections.Generic;

namespace OKSPWatcher.CompletedIn3D
{
    /// <summary>
    /// Менеджер готовности изделий в 3D
    /// </summary>
    public class CompletedItemManager : ManagerObject<CompletedItem>
    {
        static CompletedItemManager instance;

        protected CompletedItemManager() { }

        bool mustStop;
        /// <summary>
        /// Проверка готовности должна быть остановлена?
        /// </summary>
        public bool MustStop
        {
            get
            {
                return mustStop;
            }
            set
            {
                mustStop = value;
            }
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static CompletedItemManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if (instance == null) {
                            instance = new CompletedItemManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("CompletedItems").Execute(@"DROP TABLE CompletedItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("CompletedItems").Execute(@"CREATE TABLE IF NOT EXISTS CompletedItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    LinkedNode INTEGER,
                                                    LinkedModel TEXT,
                                                    LinkedWorked TEXT,
                                                    UsedInStructure INTEGER,
                                                    MainNode INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("CompletedItems").Execute(@"CREATE INDEX IF NOT EXISTS CompletedItems_LinkedNode ON CompletedItems(LinkedNode)");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        /// <summary>
        /// Список готовности по ячейке структуры
        /// </summary>
        Dictionary<StructureNode, CompletedItem> byNode = new Dictionary<StructureNode, CompletedItem>();

        /// <summary>
        /// Получить готовность по ячейке структуры
        /// </summary>
        public CompletedItem ByNode(StructureNode item, bool load = false)
        {
            if (byNode.ContainsKey(item))
            {
                return byNode[item];
            }
            else
            {
                var data = new CompletedItem(item);
                data.Save();
                return data;
            }
        }

        public override bool Add(CompletedItem item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.LinkedNode != null)
                {
                    if (byNode.ContainsKey(item.LinkedNode))
                    {
#if IN_EDITOR
                        Output.ErrorFormat("Дублирование данных по структуре {0}", item.LinkedNode.Name);
#endif
                    }
                    else
                    {
                        byNode.Add(item.LinkedNode, item);
                    }
                }
                return true;
            }            
        }

        public override void Remove(CompletedItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.LinkedNode != null && byNode.ContainsKey(item.LinkedNode))
                {
                    byNode.Remove(item.LinkedNode);
                }
            }            
        }
    }
}
