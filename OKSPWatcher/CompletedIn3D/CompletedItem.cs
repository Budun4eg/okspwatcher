﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Models;
using OKSPWatcher.Rules;
using OKSPWatcher.Structure;
using OKSPWatcher.Worked;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.CompletedIn3D
{
    /// <summary>
    /// Готовность ячейки в 3D
    /// </summary>
    public class CompletedItem : DBObject
    {
        /// <summary>
        /// Обновить показ ячейки
        /// </summary>
        public void UpdateView()
        {
            OnPropertyChanged("LinkedNode");
            OnPropertyChanged("Childs");
            OnPropertyChanged("Percent");
            OnPropertyChanged("LinkedModel");
            OnPropertyChanged("LinkedWorked");
            OnPropertyChanged("Completed");
        }

        long linkedNode = -1;
        /// <summary>
        /// Привязанная ячейка
        /// </summary>
        public StructureNode LinkedNode
        {
            get
            {
                Sync();
                return StructureNodeManager.Instance.ById(linkedNode, true);
            }
            set
            {
                Sync();
                var toSet = value != null ? value.Id : -1;
                if(toSet != linkedNode)
                {
                    needUpdate = true;
                }
                linkedNode = toSet;
                OnPropertyChanged("LinkedNode");
            }
        }

        CompletedItemPack childs;
        /// <summary>
        /// Набор дочерних ячеек
        /// </summary>
        public CompletedItemPack Childs
        {
            get
            {
                if(childs == null)
                {
                    if(LinkedNode != null && LinkedNode.Childs != null)
                    {
                        childs = new CompletedItemPack(LinkedNode.Childs);
                    }
                    else
                    {
                        childs = new CompletedItemPack();
                    }                    
                }
                return childs;
            }
        }

        CompletedItemPack _saved;
        /// <summary>
        /// Сохраненный набор
        /// </summary>
        CompletedItemPack saved
        {
            get
            {
                if(_saved == null)
                {
                    _saved = new CompletedItemPack();
                    _saved.InsertFrom(Childs);
                }
                return _saved;
            }
        }

        /// <summary>
        /// Хватает только модели?
        /// </summary>
        bool onlyModel = true;

        /// <summary>
        /// Поменять видимость ячеек
        /// </summary>
        public void ChangeVis(bool completedVisible, bool _onlyModel)
        {
            var chk = saved.Count;
            onlyModel = _onlyModel;
            if (completedVisible)
            {
                foreach(var item in saved)
                {
                    if (!Childs.Contains(item))
                    {
                        Childs.Insert(saved.IndexOf(item), item);
                    }
                }
            }
            else
            {
                for (int i = Childs.Count - 1; i >= 0; i--)
                {
                    var item = Childs.Items[i];
                    item.ChangeVis(completedVisible, _onlyModel);
                    if (item.canBeCollapsed)
                    {
                        Childs.Remove(item);
                    }
                }
            }
            foreach(var item in Childs)
            {
                item.ChangeVis(completedVisible, _onlyModel);
            }
            OnPropertyChanged("Completed");
            OnPropertyChanged("Percent");
        }

        /// <summary>
        /// Процент выполнения
        /// </summary>
        public string Percent
        {
            get
            {
                if(Childs.Count == 0)
                {
                    return "";
                }
                else
                {
                    if(percent < 0f)
                    {
                        return "-";
                    }
                    else
                    {
                        return string.Format("{0:P1}", percent);
                    }                    
                }                
            }
        }

        float percent
        {
            get
            {
                int total = TotalCount;
                if (total == 0) return -1f;
                return (float)CompletedCount / TotalCount;
            }
        }

        /// <summary>
        /// Суммарное количество ячеек
        /// </summary>
        public int TotalCount
        {
            get
            {
                int cnt = 0;
                if (Enabled)
                {
                    cnt++;
                }
                
                foreach(var item in saved)
                {
                    cnt += item.TotalCount;
                }
                return cnt;
            }
        }

        /// <summary>
        /// Суммарное количество выполненных
        /// </summary>
        public int CompletedCount
        {
            get
            {
                int cnt = 0;
                if(Enabled && Completed)
                {
                    cnt++;
                }
                foreach(var item in saved)
                {
                    cnt += item.CompletedCount;
                }
                return cnt;
            }
        }

        /// <summary>
        /// Общее уникальное количество
        /// </summary>
        public int TotalUniqueCount(List<CompletedItem> added = null)
        {
            if(added == null)
            {
                added = new List<CompletedItem>();
            }
            int cnt = 0;
            if (!added.Contains(this))
            {
                cnt++;
                added.Add(this);
            }
            foreach (var item in saved)
            {
                cnt += item.TotalUniqueCount(added);
            }
            return cnt;
        }

        /// <summary>
        /// Общее выполненное уникальное количество
        /// </summary>
        public int CompletedUniqueCount(List<CompletedItem> added = null)
        {
            if (added == null)
            {
                added = new List<CompletedItem>();
            }
            int cnt = 0;
            if (!added.Contains(this))
            {
                cnt += Completed ? 1 : 0;
                added.Add(this);
            }
            foreach (var item in saved)
            {
                cnt += item.CompletedUniqueCount(added);
            }
            return cnt;
        }

        bool canBeCollapsed
        {
            get
            {
                bool check = Completed;
                if (!check) return false;
                foreach(var item in Childs)
                {
                    if (!item.canBeCollapsed) return false;
                }
                return true;
            }
        }

        public override bool Save()
        {
            Childs.Save();
            return base.Save();
        }

        ModelPack linkedModel;
        /// <summary>
        /// Привязанная модель
        /// </summary>
        public ModelPack LinkedModel
        {
            get
            {
                if(linkedModel == null)
                {
                    linkedModel = new ModelPack();
                    linkedModel.Updated += PackUpdated;
                }
                Sync();
                return linkedModel;
            }
        }

        bool mainNode = false;
        /// <summary>
        /// Основная ячейка?
        /// </summary>
        public bool MainNode
        {
            get
            {
                return mainNode;
            }
            set
            {
                if (!RulesManager.Instance.Can("ChangeStructureState"))
                {
                    Output.TraceError("Изменение структуры запрещено");
                    return;
                }
                if (mainNode != value)
                {
                    needUpdate = true;
                }
                mainNode = value;
                OnPropertyChanged("MainNode");
            }
        }

        bool enabled = true;
        /// <summary>
        /// Элемент используется в структуре?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (!RulesManager.Instance.Can("ChangeStructureState"))
                {
                    Output.TraceError("Изменение структуры запрещено");
                    return;
                }
                if(enabled != value)
                {
                    needUpdate = true;
                }
                enabled = value;
                OnPropertyChanged("Enabled");
                foreach(var child in Childs)
                {
                    child.Enabled = enabled;
                }
            }
        }

        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        WorkedPack linkedWorked;
        /// <summary>
        /// Привязанное проработанное изображение
        /// </summary>
        public WorkedPack LinkedWorked
        {
            get
            {
                if(linkedWorked == null)
                {
                    linkedWorked = new WorkedPack();
                    linkedWorked.Updated += PackUpdated;
                }
                Sync();
                return linkedWorked;
            }
        }

        public CompletedItem(StructureNode node)
        {
            linkedNode = node.Id;
            needSync = true;
        }

        /// <summary>
        /// Ячейка выполнена?
        /// </summary>
        public bool Completed
        {
            get
            {
                if (!Enabled) return true;
                if(LinkedWorked.Count > 0 && LinkedModel.Count > 0)
                {
                    return true;
                }
                else
                {
                    if(LinkedNode != null && !(LinkedNode.NodeType == "Деталь") && !(LinkedNode.NodeType == "Сборка") && !(LinkedNode.NodeType == "Сборочная единица"))
                    {
                        return true;
                    }
                    else
                    {
                        if(onlyModel && LinkedModel.Count > 0)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Нужна синхронизация объекта?
        /// </summary>
        bool needSync = false;

        /// <summary>
        /// Синхронизация состояния объекта
        /// </summary>
        public void Sync(bool forced = false)
        {
            if (!needSync && !forced) return;
            needSync = false;
            var data = SQL.Get("CompletedItems").ExecuteRead("SELECT * FROM CompletedItems WHERE LinkedNode = {0} LIMIT 1", linkedNode);
            var row = data.Single;
            if (row != null)
            {
                Load(row);
                CompletedItemManager.Instance.Add(this);
            }
            else
            {
                needUpdate = true;
            }
        }

        public CompletedItem() : base()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                linkedNode = data.Long("LinkedNode");
                OnPropertyChanged("LinkedNode");
                if(LinkedNode != null)
                {
                    LinkedNode.CheckUpdate();
                }
                LinkedModel.Sync(data.Ids("LinkedModel"));
                foreach(var mod in LinkedModel.Items)
                {
                    if(mod != null)
                    {
                        mod.CheckUpdate();
                    }
                }
                LinkedWorked.Sync(data.Ids("LinkedWorked"));
                foreach(var wor in LinkedWorked.Items)
                {
                    if(wor != null)
                    {
                        wor.CheckUpdate();
                    }
                }
                Enabled = data.Bool("UsedInStructure");
                MainNode = data.Bool("MainNode");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                linkedNode = data.Long("LinkedNode");
                LinkedModel.Sync(data.Ids("LinkedModel"));
                LinkedWorked.Sync(data.Ids("LinkedWorked"));
                enabled = data.Bool("UsedInStructure");
                mainNode = data.Bool("MainNode");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "CompletedItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO CompletedItems (LinkedNode, LinkedModel, LinkedWorked, UsedInStructure, MainNode, UpdateDate) VALUES ({0}, '{1}', '{2}', {3}, {4}, {5})", out id,
                linkedNode, LinkedModel.Serialize(), LinkedWorked.Serialize(), enabled.ToLong(), mainNode.ToLong(), UpdateDate.Ticks);
            base.CreateNew();
            CompletedItemManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE CompletedItems SET LinkedNode = {0}, LinkedModel = '{1}', LinkedWorked = '{2}', UsedInStructure = {3}, MainNode = {4}, UpdateDate = {5} WHERE Id = {6}",
                linkedNode, LinkedModel.Serialize(), LinkedWorked.Serialize(), enabled.ToLong(), mainNode.ToLong(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM CompletedItems WHERE Id = {0}", id);
            CompletedItemManager.Instance.Remove(this);
        }
    }
}
