﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Models;
using OKSPWatcher.Products;
using OKSPWatcher.Structure;
using OKSPWatcher.Worked;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System;
using System.IO;
using System.Diagnostics;
using OKSPWatcher.SchedulePlan;
using System.Collections.Generic;
using OKSPWatcher.Secondary;
using OKSPWatcher.CompletedIn3D.StateManager;
using OKSPWatcher.Rules;

namespace OKSPWatcher.CompletedIn3D
{
    /// <summary>
    /// Логика взаимодействия для CompletedIn3DWindow.xaml
    /// </summary>
    public partial class CompletedIn3DWindow : BasicWindow
    {
        public CompletedIn3DWindow()
        {
            InitializeComponent();
            CompletedItemManager.Instance.MustStop = false;
            WorkedManager.Instance.DataUpdated += ProceedWorked;
            ModelManager.Instance.DataUpdated += ProceedModel;
            worker.RunWorkerCheck();
        }

        bool mustCancel = false;

        public override void StopUpdate()
        {
            base.StopUpdate();
            mustCancel = true;
            WorkedManager.Instance.CancelUpdate = true;
            ModelManager.Instance.CancelUpdate = true;
        }

        private void ProceedModel(object sender, EventArgs e)
        {
            UpdateShown = false;
            try
            {
                worker.RunWorkerCheck();
            }catch(Exception ex)
            {
                Output.WriteError("Фоновая задача не может быть выполнена {0}", worker.GetHashCode());
                Output.WriteError(ex);
            }            
        }

        private void ProceedWorked(object sender, EventArgs e)
        {            
            ModelManager.Instance.Update();
            if (mustCancel)
            {
                ModelManager.Instance.CancelUpdate = true;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            WorkedManager.Instance.DataUpdated -= ProceedWorked;
            ModelManager.Instance.DataUpdated -= ProceedModel;
            CompletedItemManager.Instance.MustStop = true;
        }

        /// <summary>
        /// Варианты режима
        /// </summary>
        enum States { Load, Update, Show }

        /// <summary>
        /// Текущий режим
        /// </summary>
        States state = States.Load;

        /// <summary>
        /// Список обновленных ячеек
        /// </summary>
        List<long> updated = new List<long>();

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if (state == States.Load)
            {
                ProductManager.Instance.LoadAll();
                ProductManager.Instance.Sort();
                foreach (var product in ProductManager.Instance.Items)
                {
                    if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                    structures.Add(ProductStructureManager.Instance.ByProduct(product));
                }
            }
            else if (state == States.Update)
            {
                CompletedItemManager.Instance.MustStop = false;
                updated.Clear();
                if (!updateAllMode)
                {
                    foreach (var node in toUpdate.TopNodes)
                    {
                        if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                        CheckNode(node);
                    }

                    double total = 0;
                    double completed = 0;
                    foreach (var item in toUpdate.TopNodes)
                    {
                        if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                        var comp = new CompletedItem(item);
                        total += comp.TotalCount;
                        completed += comp.CompletedCount;
                    }

                    double totalPerc;
                    if (total == 0 || completed == 0)
                    {
                        totalPerc = 0;
                    }
                    else
                    {
                        totalPerc = completed / total;
                    }
                    totalPerc *= 100;
                    toUpdate.ReadyPercent = totalPerc;
                    toUpdate.Save();
                }
                else
                {
                    foreach(var structure in Structures)
                    {
                        if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                        foreach (var node in structure.TopNodes)
                        {
                            if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                            CheckNode(node);
                        }

                        double total = 0;
                        double completed = 0;
                        foreach (var item in structure.TopNodes)
                        {
                            if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                            var comp = new CompletedItem(item);
                            total += comp.TotalCount;
                            completed += comp.CompletedCount;
                        }
                        double totalPerc;
                        if (total == 0 || completed == 0)
                        {
                            totalPerc = 0;
                        }
                        else
                        {
                            totalPerc = completed / total;
                        }
                        totalPerc *= 100;
                        structure.ReadyPercent = totalPerc;
                        structure.Save();
                    }
                }
                
            }
        }

        bool enabled = true;
        /// <summary>
        /// Показывать выполненные ячейки?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                OnPropertyChanged("Enabled");
                foreach (var node in TopNodes)
                {
                    node.ChangeVis(Enabled, completeState == "Модель");
                }
            }
        }

        /// <summary>
        /// Проверить готовность ячейки
        /// </summary>
        void CheckNode(StructureNode node)
        {
            if (node == null) return;
            if (updated.Contains(node.Id)) return;
            Output.TraceLog("Проверка ячейки {0}", node.Name);
            var check = CompletedItemManager.Instance.ByNode(node);
            if(check.LinkedModel.Count == 0)
            {
                check.LinkedModel.Clear();
                check.LinkedModel.InsertFrom(ModelManager.Instance.ByNameCertain(node.Name.ToUpper(), true));
            }
            if(check.LinkedWorked.Count == 0)
            {
                check.LinkedWorked.Clear();
                check.LinkedWorked.InsertFrom(WorkedManager.Instance.ByNameCertain(node.Name.ToUpper(), true));
            }
            check.Save();
            check.UpdateView();

            updated.Add(node.Id);
            
            foreach(var inner in check.Childs)
            {
                if (BackgroundHandler.AllClose || CompletedItemManager.Instance.MustStop) return;
                CheckNode(inner.LinkedNode);
            }
        }

        CompletedItemPack topNodes;
        /// <summary>
        /// Верхние ячейки
        /// </summary>
        public CompletedItemPack TopNodes
        {
            get
            {
                if(topNodes == null)
                {
                    topNodes = new CompletedItemPack();
                }
                return topNodes;
            }
        }

        SafeObservableCollection<string> completeStates = new SafeObservableCollection<string>() {
            "Модель и чертеж",
            "Модель"
        };

        /// <summary>
        /// Варианты готовности
        /// </summary>
        public SafeObservableCollection<string> CompleteStates
        {
            get
            {
                return completeStates;
            }
        }

        string completeState = "Модель";
        /// <summary>
        /// Текущий вариант готовности
        /// </summary>
        public string CompleteState
        {
            get
            {
                return completeState;
            }
            set
            {
                completeState = value;
                foreach (var node in TopNodes)
                {
                    node.ChangeVis(Enabled, completeState == "Модель");
                }
                OnPropertyChanged("CompleteState");
            }
        }

        SafeObservableCollection<ProductStructure> structures = new SafeObservableCollection<ProductStructure>();
        /// <summary>
        /// Список структур
        /// </summary>
        public SafeObservableCollection<ProductStructure> Structures
        {
            get
            {
                return structures;
            }
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            if (state == States.Load)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    DataContext = this;
                }));

            }
            else if(state == States.Show)
            {
                
            }else if(state == States.Update)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    TopNodes.Save();
                    TopNodes.Clear();
                    foreach (var child in toShow.TopNodes)
                    {
                        var item = new CompletedItem(child);
                        item.MainNode = true;
                        TopNodes.Add(item);
                    }
                    OnPropertyChanged("TopNodes");
                    StructurePack = StructureStateManager.Instance.ByProduct(toShow.LinkedProduct, true);
                    CheckPercent();
                }));                
            }
        }

        /// <summary>
        /// Структура для показа
        /// </summary>
        ProductStructure toShow;

        private void StructureClicked(object sender, System.Windows.RoutedEventArgs e)
        {           
            var item = (sender as Control).DataContext as ProductStructure;
            if(item != toShow)
            {
                if(toShow != null)
                {
                    toShow.IsSelected = false;
                }
                toShow = item;
                toShow.IsSelected = true;
                updateAllMode = false;
                e.Handled = true;
                toUpdate = (sender as Control).DataContext as ProductStructure;
                state = States.Update;
                mustCancel = false;
                LoadShown = true;
                UpdateShown = true;
                WorkedManager.Instance.Update();
            }
        }

        string searchString;
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString
        {
            get
            {
                return searchString;
            }
            set
            {
                searchString = value;
                OnPropertyChanged("SearchString");
            }
        }

        private void PerformSearch(object sender, RoutedEventArgs e)
        {
            
        }

        private void EnterPressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PerformSearch(null, null);
            }
        }

        /// <summary>
        /// Структура для обновления
        /// </summary>
        ProductStructure toUpdate;

        private void OpenWorked(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as CompletedItem;
            if(item.LinkedWorked == null)
            {
                Output.TraceWarning("Чертеж отсутствует");
                return;
            }
            var top = item.LinkedWorked.Top;
            if(top == null)
            {
                Output.ErrorFormat("Не определена верхняя ячейка чертежей {0}", item.Name);
                return;
            }
            if (!File.Exists(top.PathToImage))
            {
                Output.WarningFormat("Файл чертежа отсутствует {0}", top.PathToImage);
                return;
            }
            try
            {
                Process.Start(top.PathToImage);
            }catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии чертежа {0}", top.PathToImage);
            }
        }

        private void OpenWorkedFolder(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as CompletedItem;
            if (item.LinkedWorked == null)
            {
                Output.TraceWarning("Чертеж отсутствует");
                return;
            }
            var top = item.LinkedWorked.Top;
            if (top == null)
            {
                Output.ErrorFormat("Не определена верхняя ячейка чертежей {0}", item.Name);
                return;
            }
            if (!File.Exists(top.PathToImage))
            {
                Output.WarningFormat("Файл чертежа отсутствует {0}", top.PathToImage);
                return;
            }
            try
            {
                Process.Start(new FileInfo(top.PathToImage).DirectoryName);
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии чертежа {0}", top.PathToImage);
            }
        }

        private void OpenModel(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as CompletedItem;
            if (item.LinkedModel == null)
            {
                Output.TraceWarning("Модель отсутствует");
                return;
            }
            var top = item.LinkedModel.Top;
            if (top == null)
            {
                Output.ErrorFormat("Не определена верхняя ячейка моделей {0}", item.Name);
                return;
            }
            if (!File.Exists(top.PathToModel))
            {
                Output.WarningFormat("Файл модели отсутствует {0}", top.PathToModel);
                return;
            }
            try
            {
                Process.Start(top.PathToModel);
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии модели {0}", top.PathToModel);
            }
        }

        private void OpenModelFolder(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as CompletedItem;
            if (item.LinkedModel == null)
            {
                Output.TraceWarning("Модель отсутствует");
                return;
            }
            var top = item.LinkedModel.Top;
            if (top == null)
            {
                Output.ErrorFormat("Не определена верхняя ячейка моделей {0}", item.Name);
                return;
            }
            if (!File.Exists(top.PathToModel))
            {
                Output.WarningFormat("Файл модели отсутствует {0}", top.PathToModel);
                return;
            }
            try
            {
                Process.Start(new FileInfo(top.PathToModel).DirectoryName);
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии модели {0}", top.PathToModel);
            }
        }

        private void PreparePlan(object sender, RoutedEventArgs e)
        {
            if(toShow == null)
            {
                Output.TraceWarning("Изделие не выбрано");
                return;
            }
            if(!RulesManager.Instance.Can("CreateSchedule"))
            {
                Output.TraceError("Построение план-графика / списка ДСЕ запрещено");
                return;
            }
            var window = new CreateScheduleWindow();
            window.Load(toShow.LinkedProduct, TopNodes);
            window.Show();
        }

        private void UpdateAll(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Обновить готовность всех изделий? Это займет много времени.");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                UpdateAllProceed();
            }
        }

        /// <summary>
        /// Обновление всех изделий?
        /// </summary>
        bool updateAllMode;

        /// <summary>
        /// Запустить обновление всех изделий
        /// </summary>
        void UpdateAllProceed()
        {
            updateAllMode = true;
            state = States.Update;
            mustCancel = false;
            LoadShown = true;
            UpdateShown = true;
            WorkedManager.Instance.Update();
        }

        StructureStatePack structurePack;
        /// <summary>
        /// Набор структур
        /// </summary>
        public StructureStatePack StructurePack
        {
            get
            {
                return structurePack;
            }
            set
            {
                structurePack = value;
                OnPropertyChanged("StructurePack");
            }
        }

        StructureStateItem structureItem;
        /// <summary>
        /// Элемент структуры
        /// </summary>
        public StructureStateItem StructureItem
        {
            get
            {
                return structureItem;
            }
            set
            {
                structureItem = value;
                OnPropertyChanged("StructureItem");
            }
        }

        /// <summary>
        /// Добавить новое состояние
        /// </summary>
        public void AddNewState()
        {
            if (toShow == null) {
                Output.TraceWarning("Изделие не выбрано");
                return;
            }
            var name = GetTextWindow.Get("Новое состояние", "Введите наименование нового состояния", "");
            var structure = new StructureStateItem();
            structure.Name = name;
            structure.LinkedProduct = toShow.LinkedProduct;
            structure.Save();
            StructureItem = structure;
            Output.LogFormat("Состояние {0} добавлено", name);
        }

        /// <summary>
        /// Импортировать состояние из Excel
        /// </summary>
        public void ImportFromExcel()
        {
            if (StructureItem == null)
            {
                Output.TraceWarning("Состояние не выбрано");
                return;
            }
            StructureItem.ImportFromExcel(TopNodes);
        }

        /// <summary>
        /// Загрузить состояние
        /// </summary>
        public void LoadState()
        {
            if (StructureItem == null)
            {
                Output.TraceWarning("Состояние не выбрано");
                return;
            }
            StructureItem.LoadStructure(TopNodes);
            Output.TraceLog("Состояние загружено");
        }

        /// <summary>
        /// Сохранить состояние
        /// </summary>
        public void SaveState()
        {
            if (StructureItem == null)
            {
                Output.TraceWarning("Состояние не выбрано");
                return;
            }
            StructureItem.SaveStructure(TopNodes);
            Output.TraceLog("Состояние сохранено");
        }

        private void ToggleEnable(object sender, System.Windows.RoutedEventArgs e)
        {
            CheckPercent();
        }

        private void AddState(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("AddNewStructureState"))
            {
                Output.TraceError("Добавление новых состояний запрещено");
                return;
            }
            AddNewState();
        }

        private void SaveState(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("AddNewStructureState"))
            {
                Output.TraceError("Сохранение состояний запрещено");
                return;
            }
            SaveState();
        }

        private void LoadState(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadState();
            CheckPercent();
        }

        private void ImportFromExcel(object sender, System.Windows.RoutedEventArgs e)
        {
            ImportFromExcel();
            CheckPercent();
        }

        bool currentState = false;

        private void ToggleState(object sender, System.Windows.RoutedEventArgs e)
        {
            currentState = !currentState;
            foreach (var item in TopNodes)
            {
                item.Enabled = currentState;
            }
            CheckPercent();
        }

        /// <summary>
        /// Проверить процент готовности изделия
        /// </summary>
        void CheckPercent()
        {
            if(toShow == null)
            {
                return;
            }
            double total = 0;
            double completed = 0;
            foreach (var item in TopNodes)
            {
                CheckNodePercent(item);                
                total += item.TotalCount;
                completed += item.CompletedCount;
                item.Save();
            }

            double totalPerc;
            if (total == 0 || completed == 0)
            {
                totalPerc = 0;
            }
            else
            {
                totalPerc = completed / total;
            }
            totalPerc *= 100;
            toShow.ReadyPercent = totalPerc;
            toShow.Save();
        }

        void CheckNodePercent(CompletedItem item)
        {
            item.OnPropertyChanged("Percent");
            foreach(var child in item.Childs)
            {
                CheckNodePercent(child);
            }
        }

        private void RenameState(object sender, RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("AddNewStructureState"))
            {
                Output.TraceError("Переименование состояний запрещено");
                return;
            }
            var item = (sender as MenuItem).DataContext as StructureStateItem;
            item.Name = GetTextWindow.Get("Наименование состояния", "Введите новое наименование", item.Name);
            item.Save();
        }

        private void DeleteState(object sender, RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("DeleteStructureState"))
            {
                Output.TraceError("Удаление состояний запрещено");
                return;
            }
            RequestWindow.Show("Удалить состояние?");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                var item = (sender as MenuItem).DataContext as StructureStateItem;
                item.Delete();
            }
        }
    }
}
