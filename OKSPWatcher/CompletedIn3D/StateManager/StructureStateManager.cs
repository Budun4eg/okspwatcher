﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System.Collections.Generic;

namespace OKSPWatcher.CompletedIn3D.StateManager
{
    /// <summary>
    /// Менеджер состояний структур
    /// </summary>
    public class StructureStateManager : ManagerObject<StructureStateItem>
    {
        static StructureStateManager instance;

        protected StructureStateManager() : base()
        {
            
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static StructureStateManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new StructureStateManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Список состояний по изделию
        /// </summary>
        Dictionary<Product, StructureStatePack> byProduct = new Dictionary<Product, StructureStatePack>();

        /// <summary>
        /// Получить набор состояний по изделию
        /// </summary>
        public StructureStatePack ByProduct(Product key, bool load = false)
        {
            if (byProduct.ContainsKey(key))
            {
                return byProduct[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM StructureStateItems WHERE Product = {0} ORDER BY Name", key.Id);
                    var data = SQL.Get("StructureStateItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new StructureStateItem(row));
                    }
                    return ByProduct(key);
                }
                else
                {
                    var pack = new StructureStatePack();
                    byProduct.Add(key, pack);
                    return pack;
                }
            }
        }

        /// <summary>
        /// Получить состояние по идентификатору
        /// </summary>
        public override StructureStateItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM StructureStateItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("StructureStateItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new StructureStateItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Добавить состояние
        /// </summary>
        public override bool Add(StructureStateItem item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.LinkedProduct != null)
                {
                    if (byProduct.ContainsKey(item.LinkedProduct))
                    {
                        byProduct[item.LinkedProduct].Add(item);
                    }
                    else
                    {
                        byProduct.Add(item.LinkedProduct, new StructureStatePack() { item });
                    }
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить состояние
        /// </summary>
        public override void Remove(StructureStateItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.LinkedProduct != null && byProduct.ContainsKey(item.LinkedProduct) && byProduct[item.LinkedProduct].Contains(item))
                {
                    byProduct[item.LinkedProduct].Remove(item);
                    if (byProduct[item.LinkedProduct].Count == 0)
                    {
                        byProduct.Remove(item.LinkedProduct);
                    }
                }
            }            
        }
        
        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("StructureStateItems").Execute(@"DROP TABLE StructureStateItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("StructureStateItems").Execute(@"CREATE TABLE IF NOT EXISTS StructureStateItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    Product INTEGER,
                                                    Items TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Загрузить все состояния
        /// </summary>
        public override void LoadAll()
        {
            base.LoadAll();
            var data = SQL.Get("StructureStateItems").ExecuteRead("SELECT * FROM StructureStateItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new StructureStateItem(row));
            }
        }
    }
}
