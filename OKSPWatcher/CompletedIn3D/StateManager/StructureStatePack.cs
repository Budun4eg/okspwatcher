﻿using OKSPWatcher.Core;

namespace OKSPWatcher.CompletedIn3D.StateManager
{
    /// <summary>
    /// Набор состояний
    /// </summary>
    public class StructureStatePack : ItemPack<StructureStateItem>
    {
        public StructureStatePack() : base()
        {
            name = "StructureStatePack";
        }
        
        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = StructureStateManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
