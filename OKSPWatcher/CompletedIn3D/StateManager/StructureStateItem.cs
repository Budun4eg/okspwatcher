﻿using Microsoft.Office.Interop.Excel;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OKSPWatcher.CompletedIn3D.StateManager
{
    /// <summary>
    /// Отдельное состояние
    /// </summary>
    public class StructureStateItem : DBObject
    {
        /// <summary>
        /// Наименование состояния
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        long productId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(productId, true);
            }
            set
            {
                long toSet = value != null ? value.Id : -1;
                if (productId != toSet)
                {
                    needUpdate = true;
                }
                productId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        /// <summary>
        /// Список включенных элементов
        /// </summary>
        List<long> enabledItems = new List<long>();

        /// <summary>
        /// Сохранить структуру в состояние
        /// </summary>
        public void SaveStructure(CompletedItemPack pack)
        {
            enabledItems.Clear();
            foreach(var item in pack)
            {
                SaveNode(item);
            }
            needUpdate = true;
            Save();
        }

        /// <summary>
        /// Сохранить отдельную ячейку
        /// </summary>
        void SaveNode(CompletedItem item)
        {
            if (item.Enabled)
            {
                enabledItems.Add(item.Id);
            }
            
            foreach(var child in item.Childs)
            {
                SaveNode(child);
            }
        }

        /// <summary>
        /// Загрузить состояние в структуру
        /// </summary>
        public void LoadStructure(CompletedItemPack pack)
        {
            foreach (var item in pack)
            {
                LoadNode(item);
            }
        }

        /// <summary>
        /// Загрузить отдельную ячейку
        /// </summary>
        void LoadNode(CompletedItem item)
        {
            item.Enabled = enabledItems.Contains(item.Id);

            foreach (var child in item.Childs)
            {
                LoadNode(child);
            }
        }

        /// <summary>
        /// Список использованных наименований
        /// </summary>
        List<string> usedNames = new List<string>();

        /// <summary>
        /// Импортировать данные из Excel
        /// </summary>
        public void ImportFromExcel(CompletedItemPack pack)
        {
            usedNames.Clear();
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Title = "Путь до файла";
            try
            {
                var res = dialog.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }
            catch
            {

            }
            
            var excelApp = new Application();
            var excelBook = excelApp.Workbooks.Open(dialog.FileName);
            var excelSheet = (Worksheet)excelBook.Worksheets[1];

            var range = excelSheet.UsedRange;

            for (int row = 1; row <= range.Rows.Count; row++)
            {
                var name = (range.Cells[row, 1] as Range).Value2.ToString().ToUpper();
                usedNames.Add(name);
            }

            excelBook.Close();
            excelApp.Quit();

            Marshal.ReleaseComObject(excelBook);
            Marshal.ReleaseComObject(excelApp);

            foreach (var item in pack)
            {
                LoadExcel(item);
            }
        }

        /// <summary>
        /// Загрузить отдельную ячейку
        /// </summary>
        void LoadExcel(CompletedItem item)
        {
            item.Enabled = usedNames.Contains(item.LinkedNode.Name.ToUpper());

            if (!item.Enabled)
            {
                foreach (var child in item.Childs)
                {
                    LoadExcel(child);
                }
            }
        }

        public StructureStateItem() : base() {
            needUpdate = true;
        }

        public StructureStateItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Name = data.String("Name");
                productId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                enabledItems = data.String("Items").DeserializeLong();
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                name = data.String("Name");
                productId = data.Long("Product");
                enabledItems = data.String("Items").DeserializeLong();
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "StructureStateItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO StructureStateItems (Name, Product, Items, UpdateDate) VALUES ('{0}', {1}, '{2}', {3})", out id,
                name.Check(), productId, enabledItems.Serialize(), UpdateDate.Ticks);
            base.CreateNew();
            StructureStateManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE StructureStateItems SET Name = '{0}', Product = {1}, Items = '{2}', UpdateDate = {3} WHERE Id = {4}",
                name.Check(), productId, enabledItems.Serialize(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM StructureStateItems WHERE Id = {0}", id);
            StructureStateManager.Instance.Remove(this);
        }

        public override void SaveState()
        {
            base.SaveState();
            AddStateItem("Name", name);
            AddStateItem("LinkedProduct", productId);
        }

        public override void LoadState()
        {
            if (hasSavedState)
            {
                name = (string)savedState["Name"];
                productId = (long)savedState["LinkedProduct"];
                base.LoadState();
            }
        }        
    }
}
