﻿namespace OKSPWatcher.Help
{
    /// <summary>
    /// Логика взаимодействия для StartWindowHelp.xaml
    /// </summary>
    public partial class StartWindowHelp : BasicWindow
    {
        public StartWindowHelp()
        {
            InitializeComponent();
            HelpShown = false;
        }
    }
}
