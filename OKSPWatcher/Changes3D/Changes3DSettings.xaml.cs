﻿using System;
using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.SystemEditors.Windows;
using System.Windows;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Логика взаимодействия для Changes3DSettings.xaml
    /// </summary>
    public partial class Changes3DSettings : BasicWindow
    {
        public Changes3DSettings()
        {
            InitializeComponent();
            Changes3DManager.Instance.CheckInstance();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        public string DaysToWarn
        {
            get
            {
                return Changes3DManager.Instance.DaysToWarn.ToString();
            }
            set
            {
                Changes3DManager.Instance.DaysToWarn = value.ToLong();
            }
        }

        private void ActualClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ProductPackEditorWindow.Show(Changes3DManager.Instance.Available);
            Changes3DManager.Instance.Save();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_Changes3DSettings");
        }
    }
}
