﻿using OKSPWatcher.Core;
using OKSPWatcher.Products;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Набор изделий
    /// </summary>
    public class Changes3DProductPack : NotifyPack<Changes3DProduct>
    {
        /// <summary>
        /// Загрузить изделия
        /// </summary>
        public void Load(ProductPack pack)
        {
            items.Clear();
            foreach(Product item in pack)
            {
                Add(new Changes3DProduct(item));
            }
        }
    }
}
