﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Secondary;
using System;
using System.ComponentModel;
using System.Windows.Controls;
using OKSPWatcher.Core;
using OKSPWatcher.Rules;
using OKSPWatcher.Block;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Логика взаимодействия для Changes3DWindow.xaml
    /// </summary>
    public partial class Changes3DWindow : BasicWindow
    {
        /// <summary>
        /// Состояния страницы
        /// </summary>
        enum States { Load, Open }

        /// <summary>
        /// Текущее состояние страницы
        /// </summary>
        States state = States.Load;

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if (state == States.Load)
            {
                Changes3DManager.Instance.CheckInstance();
            }else if(state == States.Open)
            {
                Changes3DManager.Instance.Load(toOpen);
            }
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            if(state == States.Load)
            {
                DataContext = Changes3DManager.Instance;
            }else if(state == States.Open)
            {
                var scroll = ListOutput.GetVisualChild<ScrollViewer>();
                scroll.ScrollToTop();
            }
        }

        public Changes3DWindow()
        {
            InitializeComponent();
            LoadShown = true;
            UpdateShown = true;
            ChangeImageManager.Instance.DataUpdated += ProceedStructure;
            ChangeImageManager.Instance.Update();
        }

        private void ProceedStructure(object sender, EventArgs e)
        {
            UpdateShown = false;
            worker.RunWorkerCheck();
        }

        public override void StopUpdate()
        {
            base.StopUpdate();
            ChangeImageManager.Instance.CancelUpdate = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            ChangeImageManager.Instance.CancelUpdate = true;
            ChangeImageManager.Instance.DataUpdated -= ProceedStructure;
            base.OnClosed(e);
        }

        private void ActualClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as Changes3DProduct;
            if(Changes3DManager.Instance.Product != null && Changes3DManager.Instance.Product != item)
            {
                Changes3DManager.Instance.Product.IsSelected = false;
            }
            if(Changes3DManager.Instance.Product == item)
            {
                return;
            }
            OpenProduct(item);
        }

        /// <summary>
        /// Изделие для открытия
        /// </summary>
        Changes3DProduct toOpen;

        /// <summary>
        /// Показать извещения по изделию
        /// </summary>
        void OpenProduct(Changes3DProduct prd)
        {
            toOpen = prd;
            state = States.Open;
            worker.RunWorkerCheck();
        }

        string searchString = "";
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString
        {
            get
            {
                return searchString;
            }
            set
            {
                searchString = value;
                OnPropertyChanged("SearchString");
            }
        }

        private void DeleteChange(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("DeleteChangeItem"))
            {
                Output.TraceError("Удаление извещений запрещено");
                return;
            }
            var item = (sender as Control).DataContext as Changes3DItem;
            RequestWindow.Show("Удалить извещение?");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                item.LinkedChange.Delete();
                item.Delete();
            }
        }

        private void ChangeClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as Changes3DItem;
            Changes3DManager.Instance.Load(item);
            var window = new Changes3DWorkbench();
            window.Show();
            window.IsLocked = true;
        }

        private void PerformSearch(object sender, System.Windows.RoutedEventArgs e)
        {
            Changes3DManager.Instance.ItemPack.Search(searchString);
        }

        private void EnterPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Enter)
            {
                PerformSearch(null, null);
            }
        }

        private void ClearChangeState(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!RulesManager.Instance.Can("ClearChangeState"))
            {
                Output.TraceError("Снятие выполнения запрещено");
                return;
            }
            var item = (sender as Control).DataContext as Changes3DItem;
            RequestWindow.Show("Снять выполнение извещения?");
            if (RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                item.IsSelected = false;
                item.Save();
            }
        }
    }
}
