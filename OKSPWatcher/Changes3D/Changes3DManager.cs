﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.ImageNotation;
using OKSPWatcher.Products;
using System.Collections.Generic;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Менеджер проведения извещений в 3D
    /// </summary>
    public class Changes3DManager : ManagerObject<Changes3DItem>
    {
        static Changes3DManager instance;

        protected Changes3DManager()
        {
            daysToWarn = AppSettings.GetLong("DaysToWarn");
            Available.Sync(AppSettings.GetIds("Changes3DAvailable"));
        }

        bool onlyActual = true;
        /// <summary>
        /// Показывать только актуальные изделия?
        /// </summary>
        public bool OnlyActual
        {
            get
            {
                return onlyActual;
            }
            set
            {
                onlyActual = value;
                OnPropertyChanged("OnlyActual");
                OnPropertyChanged("Pack");
                OnPropertyChanged("TotalCount");
            }
        }

        /// <summary>
        /// Общее количество извещений
        /// </summary>
        public long TotalCount
        {
            get
            {
                long cnt = 0;

                foreach(Changes3DProduct item in Pack)
                {
                    cnt += item.ChangesCount;
                }

                return cnt;
            }
        }

        bool onlyUnfinished = true;
        /// <summary>
        /// Показывать только незавершенные извещения?
        /// </summary>
        public bool OnlyUnfinished
        {
            get
            {
                return onlyUnfinished;
            }
            set
            {
                onlyUnfinished = value;
                OnPropertyChanged("OnlyUnfinished");
                ItemPack.Search(null);
            }
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static Changes3DManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if (instance == null) {
                            instance = new Changes3DManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        long daysToWarn;
        /// <summary>
        /// Количество дней до показа предупреждения
        /// </summary>
        public long DaysToWarn
        {
            get
            {
                return daysToWarn;
            }
            set
            {
                if (daysToWarn != value)
                {
                    AppSettings.SetLong("DaysToWarn", value);
                    daysToWarn = value;
                    OnPropertyChanged("DaysToWarn");
                }
            }
        }

        Changes3DProductPack pack;
        /// <summary>
        /// Набор изделий для вывода
        /// </summary>
        public Changes3DProductPack Pack
        {
            get
            {
                if(pack == null)
                {
                    pack = new Changes3DProductPack();
                }
                pack.Load(OnlyActual ? Available : ProductManager.Instance.All);
                return pack;
            }
        }

        ProductPack available;
        /// <summary>
        /// Список актуальных изделий
        /// </summary>
        public ProductPack Available
        {
            get
            {
                if(available == null)
                {
                    available = new ProductPack();
                }
                return available;
            }
        }

        Changes3DItemPack itemPack;
        /// <summary>
        /// Набор извещений для изделия
        /// </summary>
        public Changes3DItemPack ItemPack
        {
            get
            {
                if(itemPack == null)
                {
                    itemPack = new Changes3DItemPack();
                }
                return itemPack;
            }
        }

        Changes3DProduct product;
        /// <summary>
        /// Текущее изделие менеджера
        /// </summary>
        public Changes3DProduct Product
        {
            get
            {
                return product;
            }
        }

        /// <summary>
        /// Количество изображений без якорей
        /// </summary>
        public int WithoutAnchors
        {
            get
            {
                int cnt = 0;
                foreach(Changes3DImage item in Images)
                {
                    if(item.Anchors.Count == 0)
                    {
                        cnt++;
                    }
                }
                return cnt;
            }
        }

        /// <summary>
        /// Количество непроработанных якорей
        /// </summary>
        public int NeedWorkAnchors
        {
            get
            {
                int cnt = 0;
                foreach (Changes3DImage item in Images)
                {
                    foreach(ImageAnchor anchor in item.Anchors) {
                        if(anchor.Code == "NeedWork")
                        {
                            cnt++;
                        }
                    }
                } 
                return cnt;
            }
        }

        /// <summary>
        /// Синхронизировать якоря
        /// </summary>
        public void SyncAnchors()
        {
            OnPropertyChanged("NeedWorkAnchors");
            OnPropertyChanged("WithoutAnchors");
        }

        /// <summary>
        /// Загрузить изделие в менеджер
        /// </summary>
        public void Load(Changes3DProduct _product)
        {
            product = _product;
            product.IsSelected = true;
            ItemPack.Load(ChangeManager.Instance.ByProduct(product.LinkedProduct, true));
        }

        /// <summary>
        /// Список дополнительных данных по извещению
        /// </summary>
        Dictionary<ChangeItem, Changes3DItem> byChange = new Dictionary<ChangeItem, Changes3DItem>();

        /// <summary>
        /// Получить дополнительные данные по извещению
        /// </summary>
        public Changes3DItem ByChange(ChangeItem item)
        {
            if (byChange.ContainsKey(item))
            {
                return byChange[item];
            }
            else
            {
                var data = new Changes3DItem(item);
                data.Save();
                return data;
            }
        }

        public override bool Add(Changes3DItem item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.LinkedChange != null)
                {
                    if (byChange.ContainsKey(item.LinkedChange))
                    {
#if IN_EDITOR
                        Output.ErrorFormat("Дублирование данных по извещению {0}", item.LinkedChange.Name);
#endif
                    }
                    else
                    {
                        byChange.Add(item.LinkedChange, item);
                    }
                }
                return true;
            }            
        }

        public override void Remove(Changes3DItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.LinkedChange != null && byChange.ContainsKey(item.LinkedChange))
                {
                    byChange.Remove(item.LinkedChange);
                }
            }            
        }

        Changes3DImagePack images;
        /// <summary>
        /// Набор изображений
        /// </summary>
        public Changes3DImagePack Images
        {
            get
            {
                if(images == null)
                {
                    images = new Changes3DImagePack();
                }
                return images;
            }
        }

        Changes3DItem change;
        /// <summary>
        /// Текущее извещение
        /// </summary>
        public Changes3DItem Change
        {
            get
            {
                return change;
            }
        }

        /// <summary>
        /// Загрузить извещение в менеджер
        /// </summary>
        public void Load(Changes3DItem _item)
        {
            change = _item;
            Images.Load(_item.LinkedChange.Images);
        }

        public override void Save()
        {
            AppSettings.SetString("Changes3DAvailable", Available.Serialize());
            base.Save();
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("Changes3DItems").Execute(@"DROP TABLE Changes3DItems");
            SQL.Get("Changes3DImages").Execute(@"DROP TABLE Changes3DImages");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("Changes3DItems").Execute(@"CREATE TABLE IF NOT EXISTS Changes3DItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Change INTEGER,
                                                    User INTEGER,
                                                    Date INTEGER,
                                                    UpdateDate INTEGER                          
                                                    )");
            SQL.Get("Changes3DImages").Execute(@"CREATE TABLE IF NOT EXISTS Changes3DImages (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Image INTEGER,
                                                    Anchors TEXT,
                                                    UpdateDate INTEGER                         
                                                    )");
            SQL.Get("Changes3DItems").Execute(@"CREATE INDEX IF NOT EXISTS Changes3DItems_Change ON Changes3DItems(Change)");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
