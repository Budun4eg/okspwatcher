﻿using OKSPWatcher.Core;
using OKSPWatcher.ImageNotation;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.ComponentModel;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Block;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Логика взаимодействия для Changes3DWorkbench.xaml
    /// </summary>
    public partial class Changes3DWorkbench : BasicWindow
    {
        public Changes3DWorkbench()
        {
            Changes3DManager.Instance.Change.Block();
            InitializeComponent();
            DataContext = Changes3DManager.Instance;
        }

        private void ToggleComplete(object sender, RoutedEventArgs e)
        {
            if (!Changes3DManager.Instance.Change.Available())
            {
                return;
            }
            if (Changes3DManager.Instance.Change.LinkedChange.WorkedIn3D) return;
            if (Changes3DManager.Instance.Images.HasErrors) return;
            Changes3DManager.Instance.Change.IsSelected = true;
            Changes3DManager.Instance.Change.LinkedUser = MainApp.Instance.CurrentUser;
            Changes3DManager.Instance.Change.Date = DateTime.Now;
            Close();
        }

        private void SignChange(object sender, RoutedEventArgs e)
        {
            if (!Changes3DManager.Instance.Change.Available())
            {
                return;
            }
            Changes3DManager.Instance.Change.LinkedUser = MainApp.Instance.CurrentUser;
        }

        bool isSaved = false;

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!Changes3DManager.Instance.Change.Available())
            {
                return;
            }
            if (!isSaved)
            {
                e.Cancel = true;

                worker.RunWorkerCheck();
            }    
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Changes3DManager.Instance.Change.UnBlock();
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            foreach (Changes3DImage item in Changes3DManager.Instance.Images)
            {
                item.Save();
            }
            Changes3DManager.Instance.Change.Save();
            Changes3DManager.Instance.Product.OnPropertyChanged("ChangesCount");
            isSaved = true;
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Close();
        }

        float anchorSize = 0.04f;

        Vector textResize = new Vector(3, 1.3);

        Label AddAnchor(object sender, string variant, string tooltip)
        {
            return AddAnchor(sender, variant, tooltip, new Rect(mousePosition.X, mousePosition.Y, anchorSize, anchorSize));
        }

        Label AddAnchor(object sender, string variant, string tooltip, Rect size)
        {
            var panel = ((ContextMenu)(sender as MenuItem).Parent).PlacementTarget as RelativeLayoutPanel;
            return AddAnchor(panel, variant, tooltip, size);
        }

        Label AddAnchor(RelativeLayoutPanel panel, string variant, string tooltip, Rect size)
        {
            if (!Changes3DManager.Instance.Change.Available())
            {
                return null;
            }
            var newAnchor = new Label();
            newAnchor.Style = FindResource(variant) as Style;
            panel.Children.Add(newAnchor);
            RelativeLayoutPanel.SetRelativeRect(newAnchor, size);
            var image = panel.DataContext as Changes3DImage;
            var anchor = new ImageAnchor(size, variant);
            anchor.Text = tooltip;
            image.Anchors.Add(anchor);
            newAnchor.DataContext = anchor;
            image.Save();

            Changes3DManager.Instance.SyncAnchors();
            return newAnchor;
        }

        private void AddNotShownAnchor(object sender, RoutedEventArgs e)
        {
            AddAnchor(sender, "NotShown", "На 3D-модели не отражается");
        }

        private void AddNoModelAnchor(object sender, RoutedEventArgs e)
        {
            AddAnchor(sender, "NoModel", "3D-модели нет");
        }

        private void AddAlreadyHasAnchor(object sender, RoutedEventArgs e)
        {
            AddAnchor(sender, "AlreadyHas", "В 3D-модели учтено");
        }

        private void AddNeedWorkAnchor(object sender, RoutedEventArgs e)
        {
            AddAnchor(sender, "NeedWork", "3D-модель требует доработки");
        }

        Point mousePosition;

        private void SavePosition(object sender, MouseButtonEventArgs e)
        {
            var panel = sender as RelativeLayoutPanel;
            var tempPosition = e.GetPosition(panel);
            mousePosition = new Point(tempPosition.X / panel.ActualWidth, tempPosition.Y / panel.ActualHeight);
        }

        private void DeleteAnchor(object sender, RoutedEventArgs e)
        {
            if (!Changes3DManager.Instance.Change.Available())
            {
                return;
            }
            DependencyObject check = ((ContextMenu)(sender as MenuItem).Parent).PlacementTarget as Border;
            if (check == null)
            {
                check = ((ContextMenu)(sender as MenuItem).Parent).PlacementTarget as Grid;
            }
            var obj = VisualTreeHelper.GetParent(check) as Label;
            var index = obj.DataContext as ImageAnchor;
            var parent = VisualTreeHelper.GetParent(obj) as RelativeLayoutPanel;
            parent.Children.Remove(obj);
            var image = parent.DataContext as Changes3DImage;
            image.Anchors.Remove(index);
            image.Save();
            Changes3DManager.Instance.SyncAnchors();
        }

        private void SetWorkedAnchor(object sender, RoutedEventArgs e)
        {
            if (!Changes3DManager.Instance.Change.Available())
            {
                return;
            }
            var obj = VisualTreeHelper.GetParent(((ContextMenu)(sender as MenuItem).Parent).PlacementTarget as Border) as Label;
            var panel = VisualTreeHelper.GetParent(obj) as RelativeLayoutPanel;
            AddAnchor(panel, "Worked", "Модель доработана", RelativeLayoutPanel.GetRelativeRect(obj));
            DeleteAnchor(sender, null);
        }

        private void PanelInit(object sender, EventArgs e)
        {
            var panel = sender as RelativeLayoutPanel;
            var image = panel.DataContext as Changes3DImage;
            foreach (ImageAnchor anchor in image.Anchors)
            {
                var newAnchor = new Label();
                newAnchor.Style = FindResource(anchor.Code) as Style;
                panel.Children.Add(newAnchor);
                RelativeLayoutPanel.SetRelativeRect(newAnchor, anchor.Position);
                newAnchor.DataContext = anchor;
                if (anchor.Code == "Text")
                {
                    RelativeLayoutPanel.SetRelativeRect(newAnchor, new Rect(anchor.Position.X, anchor.Position.Y, anchorSize * textResize.X, anchorSize * textResize.Y));
                }
            }
            Changes3DManager.Instance.SyncAnchors();
            ClearFocus.Focus();
        }

        private void AddTextAnchor(object sender, RoutedEventArgs e)
        {
            if (!Changes3DManager.Instance.Change.Available())
            {
                return;
            }
            var label = AddAnchor(sender, "Text", "");
            RelativeLayoutPanel.SetRelativeRect(label, new Rect(mousePosition.X, mousePosition.Y, anchorSize * textResize.X, anchorSize * textResize.Y));
        }

        private void TextLabelInit(object sender, RoutedEventArgs e)
        {
            var box = sender as TextBox;
            box.Focus();
        }

        private void RemoveFocus(object sender, MouseButtonEventArgs e)
        {
            ClearFocus.Focus();
        }
    }
}
