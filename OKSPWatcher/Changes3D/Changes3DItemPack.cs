﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using System.Collections.Generic;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Набор извещений для проработки в 3D-модели
    /// </summary>
    public class Changes3DItemPack : ItemPack<Changes3DItem>
    {
        /// <summary>
        /// Список всех извещений
        /// </summary>
        List<Changes3DItem> allItems = new List<Changes3DItem>();

        /// <summary>
        /// Загрузить извещения
        /// </summary>
        public void Load(ChangePack pack)
        {
            allItems.Clear();
            foreach (ChangeItem item in pack)
            {
                var chk = Changes3DManager.Instance.ByChange(item);
                if(chk != null)
                {
                    allItems.Add(chk);
                }                
            }

            Search(null);
        }

        /// <summary>
        /// Осуществить поиск по набору
        /// </summary>
        public void Search(string toSearch)
        {
            Clear();            
            if(toSearch == null || toSearch == "")
            {
                foreach (var item in allItems)
                {
                    if(!Changes3DManager.Instance.OnlyUnfinished || !item.LinkedChange.WorkedIn3D)
                    {
                        Add(item);
                    }                    
                }
            }
            else
            {
                toSearch = toSearch.ToUpper();
                foreach (var item in allItems)
                {
                    if (item.LinkedChange.Name.ToUpper().Contains(toSearch))
                    {
                        Add(item);
                    }                    
                }
            }
        }
    }
}
