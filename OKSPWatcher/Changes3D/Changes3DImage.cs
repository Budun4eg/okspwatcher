﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.ImageNotation;
using System;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Изображение извещения для проработки в 3D-модели
    /// </summary>
    public class Changes3DImage : DBObject
    {
        long linkedImage = -1;

        public ChangeImage LinkedImage
        {
            get
            {
                Sync();
                return ChangeImageManager.Instance.ById(linkedImage, true);
            }
            set
            {
                Sync();
                var toSet = value != null ? value.Id : -1;
                if(linkedImage != toSet)
                {
                    needUpdate = true;
                }
                linkedImage = toSet;
                OnPropertyChanged("LinkedImage");
            }
        }

        ImageAnchorPack anchors;
        /// <summary>
        /// Набор меток на изображении
        /// </summary>
        public ImageAnchorPack Anchors
        {
            get
            {
                Sync();
                if(anchors == null)
                {
                    anchors = new ImageAnchorPack();
                    anchors.Updated += PackUpdated;
                }
                return anchors;
            }
        }

        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        /// <summary>
        /// Нужна синхронизация объекта?
        /// </summary>
        bool needSync = false;

        public Changes3DImage(ChangeImage item)
        {
            linkedImage = item.Id;
            needSync = true;
        }

        /// <summary>
        /// Синхронизировать состояние объекта
        /// </summary>
        public void Sync(bool forced = false)
        {
            if (!needSync && !forced) return;
            needSync = false;
            var data = SQL.Get("Changes3DImages").ExecuteRead("SELECT * FROM Changes3DImages WHERE Image = {0} LIMIT 1", linkedImage);
            var row = data.Single;
            if (row != null)
            {
                Load(row);
            }
            else
            {
                needUpdate = true;
            }
        }

        public Changes3DImage()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                linkedImage = data.Long("Image");
                OnPropertyChanged("LinkedImage");
                if(LinkedImage != null)
                {
                    LinkedImage.CheckUpdate();
                }
                Anchors.Sync(data.Ids("Anchors"));
                foreach(var anc in Anchors.Items)
                {
                    if(anc != null)
                    {
                        anc.CheckUpdate();
                    }
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                linkedImage = data.Long("Image");
                Anchors.Sync(data.Ids("Anchors"));
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "Changes3DImages";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Changes3DImages (Image, Anchors, UpdateDate) VALUES ({0}, '{1}', {2})", out id, linkedImage, Anchors.Serialize(), UpdateDate.Ticks);
            base.CreateNew();
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Changes3DImages SET Image = {0}, Anchors = '{1}', UpdateDate = {2} WHERE Id = {3}", linkedImage, Anchors.Serialize(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM Changes3DImages WHERE Id = {0}", id);
        }

        public override bool Save()
        {
            Anchors.Save();
            LinkedImage.Save();
            return base.Save();
        }
    }
}
