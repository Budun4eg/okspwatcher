﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Объект представления изделия при проработке извещений в 3D-модели
    /// </summary>
    public class Changes3DProduct : NotifyObject
    {
        Product linkedProduct;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return linkedProduct;
            }
        }

        /// <summary>
        /// Необходимо показать предупреждение?
        /// </summary>
        public bool Warn
        {
            get
            {                
                return false;
            }
        }

        /// <summary>
        /// Количество непроведенных извещений
        /// </summary>
        public long ChangesCount
        {
            get
            {
                var res = SQL.Get("ChangeItems").ExecuteScalar("SELECT COUNT(Id) FROM ChangeItems WHERE WorkedIn3D = 0 AND Product = {0}", linkedProduct.Id);
                return (long)res;
            }
        }

        public Changes3DProduct(Product product)
        {
            linkedProduct = product;
        }
    }
}
