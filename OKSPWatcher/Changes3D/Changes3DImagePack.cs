﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using System.Collections.Generic;
using System.Linq;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Набор изображений извещения для проработки в 3D-модели
    /// </summary>
    public class Changes3DImagePack : ItemPack<Changes3DImage>
    {
        bool hasErrors;
        /// <summary>
        /// Есть ошибки в наборе?
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return hasErrors;
            }
            set
            {
                hasErrors = value;
                OnPropertyChanged("HasErrors");
            }
        }

        /// <summary>
        /// Загрузить изображения
        /// </summary>
        public void Load(ChangeImagePack pack)
        {
            HasErrors = false;
            items.Clear();
            List<Changes3DImage> toSort = new List<Changes3DImage>();
            foreach (ChangeImage item in pack)
            {
                toSort.Add(new Changes3DImage(item));
            }

            toSort = toSort.OrderBy(o => o.LinkedImage.PageIndex).ToList();
            int check = 0;

            foreach(var item in toSort)
            {
                if(item.LinkedImage.PageIndex != check)
                {
                    while(item.LinkedImage.PageIndex > check)
                    {
                        Output.ErrorFormat("Отсутствует лист извещения {0} с номером {1}", item.LinkedImage.Name, check + 1);
                        check++;
                    }                    
                    HasErrors = true;
                }
                Add(item);
                check++;

#if !IN_EDITOR
                if(item.LinkedImage.PathToImage == AppSettings.NoImagePath)
                {
                    Output.ErrorFormat("Отсутствует лист извещения {0} с номером {1}", item.LinkedImage.Name, item.LinkedImage.PageIndex + 1);
                    HasErrors = true;
                }
#endif
            }
        }
    }
}
