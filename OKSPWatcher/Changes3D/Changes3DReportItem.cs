﻿using OKSPWatcher.Changes;
using OKSPWatcher.Products;
using OKSPWatcher.Reports;
using System.Collections.Generic;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Элемент отчета по извещениям
    /// </summary>
    public class Changes3DReportItem : ReportItem
    {
        public Changes3DReportItem(Product product, List<ChangePack> data, List<long> _intervals) : base(product, _intervals){
            foreach (var item in data)
            {
                var single = item.Single;
                if (single == null) continue;
                if (!single.WorkedIn3D)
                {
                    continue;
                }
                var obj = Changes3DManager.Instance.ByChange(single);
                if (obj == null) continue;
                if (obj.Date.Ticks <= 0) continue;
                bool intervalFound = false;
                for (int i = 1; i < intervals.Count; i++)
                {
                    if (obj.Date.Ticks > intervals[i])
                    {
                        items[i - 1].Datas.Add(new ReportCellData(obj.LinkedUser, obj.Date, obj.LinkedChange.Name));
                        items[i - 1].SummaryCount++;
                        if (obj.LinkedUser != null)
                        {
                            if (!items[i - 1].Users.ContainsKey(obj.LinkedUser))
                            {
                                items[i - 1].Users.Add(obj.LinkedUser, 0);
                            }
                            items[i - 1].Users[obj.LinkedUser]++;
                        }
                        intervalFound = true;
                        break;
                    }
                }
                if (!intervalFound)
                {
                    items[intervals.Count - 1].Datas.Add(new ReportCellData(obj.LinkedUser, obj.Date, obj.LinkedChange.Name));
                    items[intervals.Count - 1].SummaryCount++;
                    if (obj.LinkedUser != null)
                    {
                        if (!items[intervals.Count - 1].Users.ContainsKey(obj.LinkedUser))
                        {
                            items[intervals.Count - 1].Users.Add(obj.LinkedUser, 0);
                        }
                        items[intervals.Count - 1].Users[obj.LinkedUser]++;
                    }
                }
            }
        }

        public Changes3DReportItem(List<long> intervals, string _name) : base(intervals, _name)
        {

        }
    }
}
