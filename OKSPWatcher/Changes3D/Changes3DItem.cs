﻿using OKSPWatcher.Changes;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Users;
using System;

namespace OKSPWatcher.Changes3D
{
    /// <summary>
    /// Отдельное извещение для проработки в 3D-модели
    /// </summary>
    public class Changes3DItem : DBObject
    {
        long linkedChange = -1;
        /// <summary>
        /// Присоединенное извещение
        /// </summary>
        public ChangeItem LinkedChange
        {
            get
            {
                Sync();
                return ChangeManager.Instance.ById(linkedChange, true);
            }
            set
            {
                Sync();
                var toSet = value != null ? value.Id : -1;
                if(linkedChange != toSet)
                {
                    needUpdate = true;
                }
                linkedChange = toSet;
                OnPropertyChanged("LinkedChange");
            }
        }

        public override string BlockCode
        {
            get
            {
                return string.Format("Change3D_{0}", Id);
            }
        }

        /// <summary>
        /// Извещение проведено?
        /// </summary>
        public override bool IsSelected
        {
            get
            {
                return LinkedChange.WorkedIn3D;
            }

            set
            {
                LinkedChange.WorkedIn3D = value;
                OnPropertyChanged("IsSelected");
            }
        }

        long linkedUser = -1;
        /// <summary>
        /// Присоединенный пользователь
        /// </summary>
        public User LinkedUser
        {
            get
            {
                Sync();
                return UserManager.Instance.ById(linkedUser, true);
            }
            set
            {
                Sync();
                var toSet = value != null ? value.Id : -1;
                if(linkedUser != toSet)
                {
                    needUpdate = true;
                }
                linkedUser = toSet;
                OnPropertyChanged("LinkedUser");
            }
        }

        long date;
        /// <summary>
        /// Дата проработки
        /// </summary>
        public DateTime Date
        {
            get
            {
                Sync();
                return new DateTime(date);
            }
            set
            {
                Sync();
                if(date != value.Ticks)
                {
                    needUpdate = true;
                }
                date = value.Ticks;
                OnPropertyChanged("Date");
            }
        }

        /// <summary>
        /// Необходимо показать предупреждение?
        /// </summary>
        public bool Warn
        {
            get
            {
                if (!LinkedChange.WorkedIn3D)
                {
                    var span = new TimeSpan((int)Changes3DManager.Instance.DaysToWarn, 0, 0, 0);
                    if((DateTime.Now - LinkedChange.ScanDate) > span)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public Changes3DItem(ChangeItem item)
        {
            linkedChange = item.Id;
            needSync = true;
        }

        /// <summary>
        /// Нужна синхронизация объекта?
        /// </summary>
        bool needSync = false;

        /// <summary>
        /// Синхронизировать состояние объекта
        /// </summary>
        public void Sync(bool forced = false)
        {
            if (!needSync && !forced) return;
            needSync = false;
            var data = SQL.Get("Changes3DItems").ExecuteRead("SELECT * FROM Changes3DItems WHERE Change = {0} LIMIT 1", linkedChange);
            var row = data.Single;
            if (row != null)
            {
                Load(row);
                Changes3DManager.Instance.Add(this);
            }
            else
            {
                needUpdate = true;
            }
        }

        public Changes3DItem()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                linkedUser = data.Long("User");
                OnPropertyChanged("LinkedUser");
                if(LinkedUser != null)
                {
                    LinkedUser.CheckUpdate();
                }
                Date = new DateTime(data.Long("Date"));
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                linkedUser = data.Long("User");
                date = data.Long("Date");
            }catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "Changes3DItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO Changes3DItems (Change, User, Date, UpdateDate) VALUES ({0}, {1}, {2}, {3})", out id, linkedChange, linkedUser, date, UpdateDate.Ticks);
            base.CreateNew();
            Changes3DManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE Changes3DItems SET Change = {0}, User = {1}, Date = {2}, UpdateDate = {3} WHERE Id = {4}", linkedChange, linkedUser, date, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            Sync();
            base.Delete();
            sql.Execute("DELETE FROM Changes3DItems WHERE Id = {0}", id);
            Changes3DManager.Instance.Remove(this);
        }

        public override bool Save()
        {
            Sync();
            LinkedChange.Save();
            return base.Save();
        }
    }
}
