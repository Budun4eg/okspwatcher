﻿using OKSPWatcher.Core;
using OKSPWatcher.Deps;
using System.Collections.Generic;

namespace OKSPWatcher.DepPaths
{
    /// <summary>
    /// Пути для отдела
    /// </summary>
    public class DepPathItem : NotifyObject
    {
        Dep dep;
        /// <summary>
        /// Привязанный отдел
        /// </summary>
        public Dep LinkedDep
        {
            get
            {
                return dep;
            }
        }

        public DepPathItem(Dep _dep)
        {
            dep = _dep;
        }

        /// <summary>
        /// Пути
        /// </summary>
        public string Paths
        {
            get
            {
                return AppSettings.GetString(string.Format("{0}_DepPaths", dep.Id));
            }
            set
            {
                AppSettings.SetString(string.Format("{0}_DepPaths", dep.Id), value);
                OnPropertyChanged("Paths");
            }
        }

        /// <summary>
        /// Список разобранных путей
        /// </summary>
        public List<string> Parsed
        {
            get
            {
                var ret = new List<string>();

                foreach(var spl in Paths.Split(new char[] { ',' }))
                {
                    ret.Add(spl.Trim());
                }

                return ret;
            }
        }
    }
}
