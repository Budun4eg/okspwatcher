﻿using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Utilities.Apps;
using System;
using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher.Utilities
{
    /// <summary>
    /// Логика взаимодействия для UtilitiesWindow.xaml
    /// </summary>
    public partial class UtilitiesWindow : BasicWindow
    {        
        SafeObservableCollection<Utility> utilities = new SafeObservableCollection<Utility>() {
            new Utility() { Name = "Замена даты изменения файлов", Code = "ChangeDatePack" },
            new Utility() { Name = "Разделение наименования файлов", Code = "SplitNamePack" },
            new Utility() { Name = "Случайная дата изменения файлов", Code = "RandomDatePack" },
            new Utility() { Name = "Бэкап БД", Code = "DBBackup" },
            //new Utility() { Name = "Проверка расположения моделей", Code = "ModelPathCheck" },
            new Utility() { Name = "Создание файла поиска", Code = "CreateSearch" }
            //new Utility() { Name = "Генератор шаблонов", Code = "TemplateGenerator" }
        };

        /// <summary>
        /// Список утилит
        /// </summary>
        public SafeObservableCollection<Utility> Utilities
        {
            get
            {
                return utilities;
            }
        }

        public UtilitiesWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        private void UtilityClicked(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as Utility;
            if (!item.Available)
            {
                Output.TraceError("Утилита недоступна");
                return;
            }
            BasicWindow window;
            try
            {
                switch (item.Code)
                {
                    case "ChangeDatePack":
                        window = new ChangeDatePackWindow();
                        window.Show();
                        break;
                    case "SplitNamePack":
                        window = new SplitNamePackWindow();
                        window.Show();
                        break;
                    case "RandomDatePack":
                        window = new RandomDatePackWindow();
                        window.Show();
                        break;
                    case "TemplateGenerator":
                        window = new TemplateGeneratorWindow();
                        window.Show();
                        break;
                    case "DBBackup":
                        if (BlockManager.Instance.Block("Utility_CreateBackup"))
                        {
                            window = new DBBackupWindow();
                            window.Show();
                            window.IsLocked = true;
                        }
                        
                        break;
                    case "ModelPathCheck":
                        if (BlockManager.Instance.Block("Utility_PathCheck"))
                        {
                            window = new ModelPathCheckWindow();
                            window.Show();
                            window.IsLocked = true;
                        }                        
                        break;
                    case "CreateSearch":
                        if (BlockManager.Instance.Block("Utility_CreateSearch"))
                        {
                            window = new CreateSearchWindow();
                            window.Show();
                            window.IsLocked = true;
                        }
                        
                        break;
                }
            }catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии утилиты");
            }            
        }
    }
}
