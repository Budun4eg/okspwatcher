﻿using OKSPWatcher.Core;
using OKSPWatcher.Secondary;
using System;
using System.IO;
using System.Windows;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для RandomDatePackWindow.xaml
    /// </summary>
    public partial class RandomDatePackWindow : BasicWindow
    {
        public RandomDatePackWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        DateTime startDate = DateTime.Now;
        /// <summary>
        /// Начальная дата
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
                OnPropertyChanged("StartDate");
            }
        }

        DateTime endDate = DateTime.Now;
        /// <summary>
        /// Начальная дата
        /// </summary>
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
                OnPropertyChanged("EndDate");
            }
        }

        string pathToFolder;
        /// <summary>
        /// Путь до папки
        /// </summary>
        public string PathToFolder
        {
            get
            {
                return pathToFolder;
            }
            set
            {
                pathToFolder = value;
                OnPropertyChanged("PathToFolder");
            }
        }

        private void Proceed(object sender, System.Windows.RoutedEventArgs e)
        {
            long leftTick = startDate.Ticks;
            long rightTick = endDate.Ticks;
            if(leftTick == rightTick)
            {
                Output.Warning("Начальные и конечные даты должны быть разными");
                return;
            }
            if (leftTick > rightTick)
            {
                Output.Warning("Начальная дата должна быть меньше конечной");
                return;
            }
            long offset = rightTick - leftTick;
            var rand = new Random();
            foreach (string pathToFile in Directory.GetFiles(PathToFolder, "*.*", SearchOption.AllDirectories))
            {
                try
                {
                    var info = new FileInfo(pathToFile);
                    var dif = rand.NextDouble();
                    long tick = leftTick + (long)Math.Ceiling(dif * offset);
                    info.LastWriteTime = new DateTime(tick);
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                }
            }
            NotifyWindow.ShowNote("Изменение даты выполнено");
        }
    }
}
