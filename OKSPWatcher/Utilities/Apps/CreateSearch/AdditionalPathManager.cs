﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Utilities.Apps.CreateSearch
{
    /// <summary>
    /// Менеджер дополнительных путей
    /// </summary>
    public class AdditionalPathManager : ManagerObject<AdditionalPathItem>
    {
        private static object root = new object();

        static AdditionalPathManager instance;

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static AdditionalPathManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new AdditionalPathManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override bool Add(AdditionalPathItem item)
        {
            lock (changeLock)
            {
                item.PropertyChanged += Item_PropertyChanged;
                return base.Add(item);
            }                       
        }

        public override void Remove(AdditionalPathItem item)
        {
            item.PropertyChanged -= Item_PropertyChanged;
            base.Remove(item);
        }

        private void Item_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged("");            
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("AdditionalPathItems").Execute(@"DROP TABLE AdditionalPathItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("AdditionalPathItems").Execute(@"CREATE TABLE IF NOT EXISTS AdditionalPathItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Path TEXT,
                                                    PathType TEXT,
                                                    AddChilds INTEGER,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Загрузить все элементы
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("AdditionalPathItems").ExecuteRead("SELECT * FROM AdditionalPathItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new AdditionalPathItem(row));
            }
        }
    }
}
