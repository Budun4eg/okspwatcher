﻿using OKSPWatcher.CompletedIn3D;
using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Models;
using OKSPWatcher.Products;
using OKSPWatcher.Structure;
using System;
using System.Collections.Generic;
using System.IO;

namespace OKSPWatcher.Utilities.Apps.CreateSearch
{
    /// <summary>
    /// Объект поиска отдельного изделия
    /// </summary>
    public class ProductSearchItem : DBObject
    {
        long linkedProduct = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return ProductManager.Instance.ById(linkedProduct, true);
            }
            set
            {
                var toSet = value != null ? value.Id : -1;
                if(toSet != linkedProduct)
                {
                    needUpdate = true;
                }
                linkedProduct = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        string additionalStrings;
        /// <summary>
        /// Дополнительные строки
        /// </summary>
        public string AdditionalStrings
        {
            get
            {
                return additionalStrings;
            }
            set
            {
                if(additionalStrings != value)
                {
                    needUpdate = true;
                }
                additionalStrings = value;
                OnPropertyChanged("AdditionalStrings");
            }
        }

        /// <summary>
        /// Список дополнительных путей
        /// </summary>
        public List<string> Additional
        {
            get
            {
                var ret = new List<string>();

                if(additionalStrings != null)
                {
                    foreach (var item in additionalStrings.Split(new char[] { ',' }, StringSplitOptions.None))
                    {
                        var toAdd = item.Trim();
                        ret.Add(toAdd);
                    }
                }                

                return ret;
            }
        }

        public override string Name
        {
            get
            {
                return LinkedProduct != null ? LinkedProduct.Name : "Не определено";
            }
        }

        bool useInSearch = true;
        /// <summary>
        /// Изделие используется в файле?
        /// </summary>
        public bool UseInSearch
        {
            get
            {
                return useInSearch;
            }
            set
            {
                if(value != useInSearch)
                {
                    needUpdate = true;
                }
                useInSearch = value;
                OnPropertyChanged("UseInSearch");
            }
        }

        int order = -1;
        /// <summary>
        /// Место в списке
        /// </summary>
        public int Order
        {
            get
            {
                return order;
            }
            set
            {
                if(value != order)
                {
                    needUpdate = true;
                }
                order = value;
                OnPropertyChanged("Order");
            }
        }

        Dictionary<string, int> paths = new Dictionary<string, int>();

        /// <summary>
        /// Список путей изделия
        /// </summary>
        public Dictionary<string, int> Paths
        {
            get
            {
                return paths;
            }
        }

        /// <summary>
        /// Добавить путь
        /// </summary>
        public void AddPath(string path)
        {
            var info = new FileInfo(path);
            if (!paths.ContainsKey(info.DirectoryName))
            {
                paths.Add(info.DirectoryName, 1);
            }
            else
            {
                paths[info.DirectoryName] += 1;
            }
        }

        /// <summary>
        /// Синхронизировать пути со структурой?
        /// </summary>
        bool synced;

        /// <summary>
        /// Синхронизация
        /// </summary>
        void Sync()
        {
            synced = true;
            var structure = ProductStructureManager.Instance.ByProduct(LinkedProduct);
            if (structure == null) return;
            foreach(var node in structure.TopNodes)
            {
                var comp = new CompletedItem(node);
                CheckNode(comp);
            }
        }

        /// <summary>
        /// Проверить ячейку
        /// </summary>
        void CheckNode(CompletedItem item)
        {
            try
            {
                if (item.LinkedModel.Count > 0)
                {
                    item.LinkedModel.ForceRefresh();
                    var ind = item.LinkedModel.Count - 1;
                    if(ind >= 0)
                    {
                        var model = item.LinkedModel.Items[ind];
                        var dir = new FileInfo(model.PathToModel).DirectoryName;
                        if (!paths.ContainsKey(dir))
                        {
                            if (model.LinkedProduct != null && model.LinkedProduct != LinkedProduct)
                            {

                                var nextPrd = ProductSearchManager.Instance.ByProduct(model.LinkedProduct);

                                if (nextPrd.Paths.ContainsKey(dir))
                                {
                                    paths.Add(dir, nextPrd.Paths[dir]);
                                }
                            }
                        }
                    }                    
                }
            }catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при определении готовности ячейки {0}", item.LinkedNode.Name);
            }            

            foreach(var child in item.Childs)
            {
                CheckNode(child);
            }
        }

        /// <summary>
        /// Сортированный список путей
        /// </summary>
        public List<string> Sorted
        {
            get
            {
                if (!synced)
                {
                    Sync();
                }
                var ret = new List<string>();
                var cnt = new List<int>();

                foreach(var item in paths)
                {
                    if(ret.Count == 0)
                    {
                        ret.Add(item.Key);
                        cnt.Add(item.Value);
                        continue;
                    }
                    int index = 0;
                    for(int i = 0; i < cnt.Count; i++)
                    {
                        if(item.Value > cnt[i])
                        {
                            break;
                        }
                        index++;
                    }
                    ret.Insert(index, item.Key);
                    cnt.Insert(index, item.Value);
                }

                return ret;
            }
        }

        public ProductSearchItem() : base() {
            needUpdate = true;
        }

        public ProductSearchItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                linkedProduct = data.Long("LinkedProduct");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                UseInSearch = data.Bool("UseInSearch");
                Order = data.Int("OrderVal");
                AdditionalStrings = data.String("AdditionalStrings");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                linkedProduct = data.Long("LinkedProduct");
                useInSearch = data.Bool("UseInSearch");
                order = data.Int("OrderVal");
                additionalStrings = data.String("AdditionalStrings");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "ProductSearchItems";
            }
        }

        protected override bool CreateNew()
        {
            if (LinkedProduct == null) return false;
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO ProductSearchItems (LinkedProduct, UseInSearch, OrderVal, AdditionalStrings, UpdateDate) VALUES ({0}, {1}, {2}, '{3}', {4})", out id,
                linkedProduct, useInSearch.ToLong(), order, additionalStrings.Check(), UpdateDate.Ticks);
            base.CreateNew();
            ProductSearchManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            if (LinkedProduct == null) return false;
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE ProductSearchItems SET LinkedProduct = {0}, UseInSearch = {1}, OrderVal = {2}, AdditionalStrings = '{3}', UpdateDate = {4} WHERE Id = {5}",
                linkedProduct, useInSearch.ToLong(), order, additionalStrings.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            if (LinkedProduct == null) return;
            base.Delete();
            sql.Execute("DELETE FROM ProductSearchItems WHERE Id = {0}", id);
            ProductSearchManager.Instance.Remove(this);
        }
    }
}
