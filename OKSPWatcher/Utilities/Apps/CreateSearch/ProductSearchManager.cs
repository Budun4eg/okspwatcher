﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;
using System.Collections.Generic;
using System.IO;

namespace OKSPWatcher.Utilities.Apps.CreateSearch
{
    public class ProductSearchManager : ManagerObject<ProductSearchItem>
    {
        private static object root = new object();

        static ProductSearchManager instance;

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static ProductSearchManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ProductSearchManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        protected ProductSearchManager() : base()
        {
            translitedPrd = ProductManager.Instance.Translited;
        }

        Dictionary<Product, ProductSearchItem> byProduct = new Dictionary<Product, ProductSearchItem>();

        /// <summary>
        /// Получить объект по изделию
        /// </summary>
        public ProductSearchItem ByProduct(Product prd)
        {
            if (byProduct.ContainsKey(prd))
            {
                return byProduct[prd];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Изделие используется?
        /// </summary>
        public bool Used(Product prd)
        {
            return byProduct.ContainsKey(prd);
        }

        ProductSearchItem unknown;

        public override bool Add(ProductSearchItem item)
        {
            lock (changeLock)
            {
                item.PropertyChanged += Item_PropertyChanged;
                var ret = base.Add(item);
                if (item.Order >= 0)
                {
                    items.Move(items.IndexOf(item), item.Order);
                }
                byProduct.Add(item.LinkedProduct, item);
                return ret;
            }            
        }        

        Dictionary<string, ProductPack> translitedPrd;

        /// <summary>
        /// Удалить пустые изделия
        /// </summary>
        public void DeleteEmpty()
        {
            for(int i = Items.Count - 1; i >= 0; i--)
            {
                if(Items[i].Paths.Count == 0)
                {
                    Items.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Добавить путь
        /// </summary>
        public void AddPath(DirectoryInfo product, string path)
        {
            var model = new FileInfo(path);
            string ext = model.Name.ToLower();
            if (ext.Contains(".prt.") || ext.Contains(".asm.")) {
                string toCheck = product.Name.ToUpper();
                var prd = translitedPrd.ContainsKey(toCheck) ? translitedPrd[toCheck].Single : null;
                if(prd == null)
                {
                    toCheck = model.Name.Split(new char[] { '_' }, StringSplitOptions.None)[0].ToUpper();
                    prd = translitedPrd.ContainsKey(toCheck) ? translitedPrd[toCheck].Single : null;
                }
                if (prd != null)
                {
                    byProduct[prd].AddPath(path);
                }
                else
                {
                    if(unknown == null)
                    {
                        unknown = new ProductSearchItem();
                        items.Add(unknown);
                    }
                    unknown.AddPath(path);
                }
            }
        }

        /// <summary>
        /// Обновить последовательность
        /// </summary>
        void Link()
        {
            foreach(var item in items)
            {
                item.Order = items.IndexOf(item);
            }
        }

        public override void Save()
        {
            Link();
            base.Save();
        }

        public override void Remove(ProductSearchItem item)
        {
            lock (changeLock)
            {
                byProduct.Remove(item.LinkedProduct);
                item.PropertyChanged -= Item_PropertyChanged;
                base.Remove(item);
            }            
        }

        private void Item_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged("");
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("ProductSearchItems").Execute(@"DROP TABLE ProductSearchItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("ProductSearchItems").Execute(@"CREATE TABLE IF NOT EXISTS ProductSearchItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    LinkedProduct INTEGER,
                                                    UseInSearch INTEGER,
                                                    OrderVal INTEGER,
                                                    AdditionalStrings TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Загрузить все элементы
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("ProductSearchItems").ExecuteRead("SELECT * FROM ProductSearchItems ORDER BY OrderVal");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new ProductSearchItem(row));
            }
        }

        /// <summary>
        /// Поменять объекты местами
        /// </summary>
        public void Move(List<ProductSearchItem> from, ProductSearchItem to)
        {
            try
            {
                var toI = items.IndexOf(to);
                for(int i = from.Count - 1; i >= 0; i--)
                {
                    var fromI = items.IndexOf(from[i]);
                    items.Move(fromI, toI);
                }
                
            }catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при перемещении объектов");
            }            
        }

        /// <summary>
        /// Передвинуть наверх
        /// </summary>
        public void Top(ProductSearchItem item)
        {
            try
            {
                var fromI = items.IndexOf(item);
                items.Move(fromI, 0);

            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при перемещении объектов");
            }
        }

        /// <summary>
        /// Передвинуть вниз
        /// </summary>
        public void Bottom(ProductSearchItem item)
        {
            try
            {
                var fromI = items.IndexOf(item);
                items.Move(fromI, items.Count - 1);
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при перемещении объектов");
            }
        }
    }
}
