﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Collections.Generic;
using System.IO;

namespace OKSPWatcher.Utilities.Apps.CreateSearch
{
    /// <summary>
    /// Дополнительные пути
    /// </summary>
    public class AdditionalPathItem : DBObject
    {
        string path;
        /// <summary>
        /// Путь
        /// </summary>
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                if (path != value)
                {
                    needUpdate = true;
                }
                path = value;
                OnPropertyChanged("Path");
            }
        }

        /// <summary>
        /// Дополнительные пути
        /// </summary>
        public List<string> Additional
        {
            get
            {
                var ret = new List<string>();
                if (AddChilds)
                {
                    try
                    {
                        var dir = new DirectoryInfo(path);
                        foreach (var child in dir.GetDirectories("*", SearchOption.AllDirectories))
                        {
                            ret.Add(child.FullName);
                        }
                    }catch(Exception ex)
                    {
                        Output.Error(ex, "Ошибка при добавлении дочерних папок {0}", Path);
                    }                    
                }
                return ret;
            }
        }

        string pathType;
        /// <summary>
        /// Тип пути
        /// </summary>
        public string PathType
        {
            get
            {
                return pathType;
            }
            set
            {
                if (pathType != value)
                {
                    needUpdate = true;
                }
                pathType = value;
                OnPropertyChanged("PathType");
            }
        }

        bool addChilds;
        /// <summary>
        /// Добавлять дочерние папки?
        /// </summary>
        public bool AddChilds
        {
            get
            {
                return addChilds;
            }
            set
            {
                if(addChilds != value)
                {
                    needUpdate = true;
                }
                addChilds = value;
                OnPropertyChanged("AddChilds");
            }
        }

        public AdditionalPathItem() : base() {
            pathType = "Начало";
            needUpdate = true;
        }

        public AdditionalPathItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Path = data.String("Path");
                PathType = data.String("PathType");
                AddChilds = data.Bool("AddChilds");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                path = data.String("Path");
                pathType = data.String("PathType");
                addChilds = data.Bool("AddChilds");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "AdditionalPathItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO AdditionalPathItems (Path, PathType, AddChilds, UpdateDate) VALUES ('{0}', '{1}', {2}, {3})", out id,
                path.Check(), pathType, addChilds.ToLong(), UpdateDate.Ticks);
            base.CreateNew();
            AdditionalPathManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE AdditionalPathItems SET Path = '{0}', PathType = '{1}', AddChilds = {2}, UpdateDate = {3} WHERE Id = {4}",
                path.Check(), pathType, addChilds.ToLong(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM AdditionalPathItems WHERE Id = {0}", id);
            AdditionalPathManager.Instance.Remove(this);
        }
    }
}
