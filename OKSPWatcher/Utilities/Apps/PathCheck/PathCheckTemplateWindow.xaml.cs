﻿namespace OKSPWatcher.Utilities.Apps.PathCheck
{
    /// <summary>
    /// Логика взаимодействия для PathCheckTemplateWindow.xaml
    /// </summary>
    public partial class PathCheckTemplateWindow : BasicWindow
    {
        public PathCheckTemplateWindow()
        {
            InitializeComponent();
            DataContext = PathCheckManager.Instance;
        }
    }
}
