﻿using OKSPWatcher.Core;
using OKSPWatcher.Products;

namespace OKSPWatcher.Utilities.Apps.PathCheck
{
    /// <summary>
    /// Объект шаблона отдельного изделия
    /// </summary>
    public class PathCheckItem : NotifyObject
    {
        Product linkedProduct;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                return linkedProduct;
            }
        }

        int templateIndex;
        /// <summary>
        /// Индекс шаблона
        /// </summary>
        public int TemplateIndex
        {
            get
            {
                return templateIndex;
            }
            set
            {
                templateIndex = value;
                AppSettings.SetInt(string.Format("{0}_CheckTemplate", linkedProduct.Id), value);
            }
        }

        public PathCheckItem(Product product)
        {
            linkedProduct = product;
            templateIndex = AppSettings.GetInt(string.Format("{0}_CheckTemplate", linkedProduct.Id));
        }
    }
}
