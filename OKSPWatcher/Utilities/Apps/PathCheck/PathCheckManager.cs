﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System.Collections.Generic;

namespace OKSPWatcher.Utilities.Apps.PathCheck
{
    /// <summary>
    /// Менеджер шаблонов
    /// </summary>
    public class PathCheckManager : NotifyObject
    {
        private static object root = new object();

        static PathCheckManager instance;
        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static PathCheckManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new PathCheckManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        SafeObservableCollection<PathCheckItem> items = new SafeObservableCollection<PathCheckItem>();
        /// <summary>
        /// Список шаблонов
        /// </summary>
        public SafeObservableCollection<PathCheckItem> Items
        {
            get
            {
                return items;
            }
        }

        /// <summary>
        /// Список шаблонов по изделию
        /// </summary>
        Dictionary<Product, PathCheckItem> byProduct = new Dictionary<Product, PathCheck.PathCheckItem>();

        /// <summary>
        /// Получить шаблон по изделию
        /// </summary>
        public PathCheckItem ByProduct(Product prd)
        {
            if (byProduct.ContainsKey(prd))
            {
                return byProduct[prd];
            }
            else
            {
                var item = new PathCheckItem(prd);
                items.Add(item);
                byProduct.Add(prd, item);
                return item;
            }
        }
                
        /// <summary>
        /// Список игнорированных путей
        /// </summary>
        List<string> ignoredPaths = new List<string>();
        
        /// <summary>
        /// Проверить путь на игнорирование
        /// </summary>
        public bool IsIgnored(string path)
        {
            return ignoredPaths.Contains(path);
        }

        /// <summary>
        /// Список проверенных путей
        /// </summary>
        List<string> checkedPaths = new List<string>();
        
        /// <summary>
        /// Проверить путь на правильность
        /// </summary>
        public bool IsChecked(string path)
        {
            return checkedPaths.Contains(path);
        }

        /// <summary>
        /// Добавить строку в игнорированные
        /// </summary>
        public void AddIgnored(string path)
        {
            ignoredPaths.Add(path);
            SQL.Get("PathIgnoredItems").Execute("INSERT INTO PathIgnoredItems (Path) VALUES ('{0}')", path.Check());
        }

        /// <summary>
        /// Добавить строку в проверенные
        /// </summary>
        public void AddChecked(string path)
        {
            checkedPaths.Add(path);
            SQL.Get("PathCheckedItems").Execute("INSERT INTO PathCheckedItems (Path) VALUES ('{0}')", path.Check());
        }

        PathCheckManager()
        {
            LoadAll();
            items.Clear();
            foreach (var item in ProductManager.Instance.All)
            {
                var newItem = new PathCheckItem(item);
                items.Add(newItem);
                byProduct.Add(item, newItem);
            }
        }

        /// <summary>
        /// Загрузить данные из БД
        /// </summary>
        void LoadAll()
        {
            checkedPaths.Clear();
            var req = SQL.Get("PathCheckedItems").ExecuteRead("SELECT Path FROM PathCheckedItems");
            foreach(var item in req)
            {
                checkedPaths.Add(item.String("Path"));
            }

            ignoredPaths.Clear();
            req = SQL.Get("PathIgnoredItems").ExecuteRead("SELECT Path FROM PathIgnoredItems");
            foreach (var item in req)
            {
                ignoredPaths.Add(item.String("Path"));
            }
        }

        /// <summary>
        /// Удалить кэш игнорированных
        /// </summary>
        public void ClearCacheIgnored()
        {
            SQL.Get("PathIgnoredItems").Execute("DELETE FROM PathIgnoredItems");
            ignoredPaths.Clear();
        }

        /// <summary>
        /// Удалить кэш проверенных
        /// </summary>
        public void ClearCacheNormal()
        {
            SQL.Get("PathCheckedItems").Execute("DELETE FROM PathCheckedItems");
            checkedPaths.Clear();
        }

        /// <summary>
        /// Сгенерировать БД
        /// </summary>
        public void GenerateDBStructure()
        {
            SQL.Get("PathCheckedItems").Execute(@"CREATE TABLE IF NOT EXISTS PathCheckedItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Path TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("PathCheckedItems").Execute("CREATE INDEX IF NOT EXISTS PathCheckedItems_Path ON PathCheckedItems(Path)");

            SQL.Get("PathIgnoredItems").Execute(@"CREATE TABLE IF NOT EXISTS PathIgnoredItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Path TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("PathIgnoredItems").Execute("CREATE INDEX IF NOT EXISTS PathIgnoredItems_Path ON PathIgnoredItems(Path)");
        }

        /// <summary>
        /// Удалить структуру
        /// </summary>
        public void DeleteStructure()
        {
            SQL.Get("PathCheckedItems").Execute(@"DROP TABLE PathCheckedItems");
            SQL.Get("PathIgnoredItems").Execute(@"DROP TABLE PathIgnoredItems");
        }

        /// <summary>
        /// Выгрузить менеджер
        /// </summary>
        public void Unload()
        {
            instance = null;
        }
    }
}
