﻿using OKSPWatcher.Core;
using OKSPWatcher.Models;
using System;

namespace OKSPWatcher.Utilities.Apps.PathCheck
{
    /// <summary>
    /// Объект для определения требуемого пути для модели
    /// </summary>
    public static class PathCheckTemplate
    {
        /// <summary>
        /// Проверить путь до модели
        /// </summary>
        public static PathCheckData Check(ModelParseData data)
        {
            PathCheckData toReturn = new PathCheckData();
            var templateInd = PathCheckManager.Instance.ByProduct(data.ProductItem);

            if(templateInd == null || templateInd.TemplateIndex <= 0)
            {
                toReturn.HasTemplate = false;
                toReturn.Error = "Нет шаблона для изделия";
                return toReturn;
            }
            else
            {
                toReturn.HasTemplate = true;
            }

            if(templateInd.TemplateIndex == 1)
            {
                Template_1(data, toReturn);
            }else if(templateInd.TemplateIndex == 2)
            {
                Template_2(data, toReturn);
            }else if(templateInd.TemplateIndex == 3)
            {
                Template_3(data, toReturn);
            }

            return toReturn;
        }

        /// <summary>
        /// Единая папка
        /// </summary>
        static void Template_2(ModelParseData data, PathCheckData chk)
        {
            try
            {
                var path = data.FilePath.ToLower();
                var cutPath = path.Remove(0, ModelManager.Instance.PathToModel.Length + 1);

                var fSpl = cutPath.Split(new char[] { '.' });
                if (fSpl.Length != 3) return;
                cutPath = fSpl[0];

                var spl = cutPath.Split(new char[] { '\\' });

                var prdTrans = data.ProductName.Translit().ToLower();

                var top = spl[spl.Length - 2];
                if (top != prdTrans)
                {
                    chk.Error = "Неверное наименование папки изделия";
                    return;
                }

                chk.IsRight = true;
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при проверке пути модели {0}", data.FilePath);
            }
        }

        /// <summary>
        /// Стандартный шаблон
        /// </summary>
        static void Template_1(ModelParseData data, PathCheckData chk)
        {
            try
            {
                var path = data.FilePath.ToLower();

                if((path.Contains(".prt") && data.Name.Contains("СБ")) ||
                    (path.Contains(".asm") && !data.Name.Contains("СБ")))
                {
                    chk.Error = "Неверный формат файла";
                    return;
                }

                var cutPath = path.Remove(0, ModelManager.Instance.PathToModel.Length + 1);

                var fSpl = cutPath.Split(new char[] { '.' });
                if (fSpl.Length != 3) return;
                cutPath = fSpl[0];

                var spl = cutPath.Split(new char[] { '\\' });

                var prdTrans = data.ProductName.Translit().ToLower();

                var name = spl[spl.Length - 1].Remove(0, prdTrans.Length + 1);

                var nameSpl = name.Split(new char[] { '-' });

                var group = nameSpl[0];

                if (group.Contains("_"))
                {
                    chk.Error = "Наличие _ в наименовании";
                    return;
                }

                if (group.StartsWith("sb"))
                {
                    group = group.Remove(0, 2);
                }

                string checkedGroup = "";

                foreach(char c in group)
                {
                    if (char.IsDigit(c))
                    {
                        checkedGroup += c;
                    }
                }

                int offset = 2;

                var upper = spl[spl.Length - offset];
                if(upper.Length <= prdTrans.Length)
                {
                    chk.Error = "Неверное наименование папки уровнем выше";
                    return;
                }
                upper = upper.Remove(0, prdTrans.Length + 1);

                var upperChk = "";
                foreach(var c in upper)
                {
                    if (char.IsDigit(c))
                    {
                        upperChk += c;
                    }
                }

                if(upperChk.Length > checkedGroup.Length)
                {
                    chk.Error = "Неверное наименование папки уровнем выше";
                    return;
                }

                if (!upper.Contains(string.Format("sb{0}", checkedGroup)))
                {
                    chk.Error = "Неверное наименование папки уровнем выше";
                    return;
                }
                offset++;

                if(checkedGroup.Length >= 4)
                {
                    var upperGroup = checkedGroup.Remove(checkedGroup.Length - 2, 2);
                    var upperNext = spl[spl.Length - offset];
                    if(upperNext.Length <= prdTrans.Length)
                    {
                        chk.Error = "Неверное наименование папки на 2 уровня выше";
                        return;
                    }
                    upperNext = upperNext.Remove(0, prdTrans.Length + 1);
                    if (!upperNext.Contains(string.Format("sb{0}", upperGroup)))
                    {
                        chk.Error = "Неверное наименование папки на 2 уровня выше";
                        return;
                    }
                    offset++;
                }

                var top = spl[spl.Length - offset];
                if (top != prdTrans)
                {
                    chk.Error = "Неверное наименование папки изделия";
                    return;
                }

                chk.IsRight = true;
            }
            catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при проверке пути модели {0}", data.FilePath);
            }            
        }

        /// <summary>
        /// Начало наименования
        /// </summary>
        static void Template_3(ModelParseData data, PathCheckData chk)
        {
            try
            {
                var path = data.FilePath.ToLower();
                var cutPath = path.Remove(0, ModelManager.Instance.PathToModel.Length + 1);

                var fSpl = cutPath.Split(new char[] { '.' });
                if (fSpl.Length != 3) return;
                cutPath = fSpl[0];

                var spl = cutPath.Split(new char[] { '\\' });

                var prdTrans = data.ProductName.Translit().ToLower();

                var name = spl[spl.Length - 1].Remove(0, prdTrans.Length + 1);

                int offset = 2;

                var upper = spl[spl.Length - offset];
                if (upper.Length <= prdTrans.Length)
                {
                    chk.Error = "Неверное наименование папки уровнем выше";
                    return;
                }
                upper = upper.Remove(0, prdTrans.Length + 1);
                if (!name.StartsWith(upper))
                {
                    chk.Error = "Неверное наименование папки уровнем выше";
                    return;
                }
                offset++;

                var top = spl[spl.Length - offset];
                if (top != prdTrans)
                {
                    chk.Error = "Неверное наименование папки изделия";
                    return;
                }

                chk.IsRight = true;
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при проверке пути модели {0}", data.FilePath);
            }
        }
    }
}
