﻿namespace OKSPWatcher.Utilities.Apps.PathCheck
{
    /// <summary>
    /// Объект данных проверки пути
    /// </summary>
    public class PathCheckData
    {
        bool isRight;
        /// <summary>
        /// Путь правильный?
        /// </summary>
        public bool IsRight
        {
            get
            {
                return isRight;
            }
            set
            {
                isRight = value;
            }
        }

        bool hasTemplate;
        /// <summary>
        /// Есть шаблон?
        /// </summary>
        public bool HasTemplate
        {
            get
            {
                return hasTemplate;
            }
            set
            {
                hasTemplate = value;
            }
        }

        string rightPath;
        /// <summary>
        /// Правильный путь для модели
        /// </summary>
        public string RightPath
        {
            get
            {
                return rightPath;
            }
            set
            {
                rightPath = value;
            }
        }

        string error;
        /// <summary>
        /// Ошибка
        /// </summary>
        public string Error
        {
            get
            {
                return error;
            }
            set
            {
                error = value;
            }
        }
    }
}
