﻿using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Fixers;
using OKSPWatcher.Models;
using OKSPWatcher.Secondary;
using OKSPWatcher.Utilities.Apps.PathCheck;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Threading;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для ModelPathCheckWindow.xaml
    /// </summary>
    public partial class ModelPathCheckWindow : BasicWindow
    {
        public ModelPathCheckWindow()
        {
            InitializeComponent();
            worker.RunWorkerCheck();
        }

        int deleteCount = 0;
        /// <summary>
        /// Количество файлов на удаление
        /// </summary>
        public int DeleteCount
        {
            get
            {
                return deleteCount;
            }
            set
            {
                deleteCount = value;
                OnPropertyChanged("DeleteCount");
            }
        }

        int moveCount;
        /// <summary>
        /// Количество файлов на перемещение
        /// </summary>
        public int MoveCount
        {
            get
            {
                return moveCount;
            }
            set
            {
                moveCount = value;
                OnPropertyChanged("MoveCount");
            }
        }

        int errorCount;
        /// <summary>
        /// Количество файлов на перемещение
        /// </summary>
        public int ErrorCount
        {
            get
            {
                return errorCount;
            }
            set
            {
                errorCount = value;
                OnPropertyChanged("ErrorCount");
            }
        }

        int ignoredCount;
        /// <summary>
        /// Количество игнорированных файлов
        /// </summary>
        public int IgnoredCount
        {
            get
            {
                return ignoredCount;
            }
            set
            {
                ignoredCount = value;
                OnPropertyChanged("IgnoredCount");
            }
        }

        int normalCount;
        /// <summary>
        /// Количество правильных файлов
        /// </summary>
        public int NormalCount
        {
            get
            {
                return normalCount;
            }
            set
            {
                normalCount = value;
                OnPropertyChanged("NormalCount");
            }
        }

        int totalCount;
        /// <summary>
        /// Общее количество элементов для проверки
        /// </summary>
        public int TotalCount
        {
            get
            {
                return totalCount;
            }
            set
            {
                totalCount = value;
                OnPropertyChanged("TotalCount");
            }
        }

        bool autoScroll = true;
        /// <summary>
        /// Автоматическая прокрутка списка
        /// </summary>
        public bool AutoScroll
        {
            get
            {
                return autoScroll;
            }
            set
            {
                autoScroll = value;
                OnPropertyChanged("AutoScroll");
            }
        }

        BackgroundWorker _searchWorker;
        /// <summary>
        /// Объект для фоновой работы
        /// </summary>
        protected BackgroundWorker searchWorker
        {
            get
            {
                if (_searchWorker == null)
                {
                    _searchWorker = new BackgroundWorker() { WorkerReportsProgress = true };
                    _searchWorker.DoWork += SearchErrors;
                    _searchWorker.ProgressChanged += ReportSearchProgress;
                    _searchWorker.RunWorkerCompleted += SearchComplete;
                }
                return _searchWorker;
            }
        }

        /// <summary>
        /// Задержка потока на каждую из моделей
        /// </summary>
        int threadWait = 5;        

#if IN_EDITOR
        List<string> debugDelete = new List<string>();
#endif

        void ParseModel(FileInfo model, DirectoryInfo product)
        {
            TotalCount++;
            if (PathCheckManager.Instance.IsChecked(model.FullName))
            {
                NormalCount++;
                return;
            }
            if (PathCheckManager.Instance.IsIgnored(model.FullName))
            {
                IgnoredCount++;
                return;
            }
            var parse = ModelParser.Instance.Parse(model, product);

            foreach (var check in ignoredStr)
            {
                if (parse.FilePath.ToUpper().Contains(check))
                {
#if IN_EDITOR
                    debugDelete.Add(parse.FilePath);
#endif

                    IgnoredCount++;
                    searchWorker.ReportProgress(3, string.Format("{0}", parse.FilePath));
                    PathCheckManager.Instance.AddIgnored(model.FullName);
                    Thread.Sleep(threadWait);
                    return;
                }
            }

            foreach (var check in deleteStr)
            {
                if (parse.FilePath.ToUpper().Contains(check))
                {
#if IN_EDITOR
                    debugDelete.Add(parse.FilePath);
#endif
                    modelToDelete.Add(parse.FilePath);
                    DeleteCount++;
                    searchWorker.ReportProgress(0, string.Format("{0}", parse.FilePath));
                    Thread.Sleep(threadWait);
                    return;
                }
            }

            if (parse.IsParsed)
            {
                var chk = PathCheckTemplate.Check(parse);
                if (chk.HasTemplate)
                {
                    if (chk.IsRight)
                    {
#if IN_EDITOR
                        debugDelete.Add(parse.FilePath);
#endif
                        NormalCount++;
                        searchWorker.ReportProgress(4, string.Format("{0}", parse.FilePath));
                        PathCheckManager.Instance.AddChecked(model.FullName);
                        Thread.Sleep(threadWait);
                        return;
                    }
                    else
                    {
                        MoveCount++;
                        searchWorker.ReportProgress(1, string.Format("[{0}] {1}", chk.Error, parse.FilePath ));
                        Thread.Sleep(threadWait);
                        return;
                    }
                }
                else
                {
                    ErrorCount++;
                    searchWorker.ReportProgress(2, string.Format("[{0}] {1}", chk.Error, parse.FilePath ));
                    Thread.Sleep(threadWait);
                    return;
                }
            }

            ErrorCount++;
            if(parse.ProductItem == null)
            {
                searchWorker.ReportProgress(2, string.Format("[Изделие отсутствует] {0}", parse.FilePath));
            }
            else
            {
                searchWorker.ReportProgress(2, string.Format("[Нет шаблона для модели] {0}", parse.FilePath));
            }            

            Thread.Sleep(threadWait);
        }

        /// <summary>
        /// Список игнорируемых строк
        /// </summary>
        List<string> ignoredStr = new List<string>();

        /// <summary>
        /// Список удаляемых строк
        /// </summary>
        List<string> deleteStr = new List<string>();

        /// <summary>
        /// Функция фоновой работы
        /// </summary>
        protected void SearchErrors(object sender, DoWorkEventArgs e)
        {
            ignoredStr.Clear();
            deleteStr.Clear();
            foreach (var str in ignoredString.Split(new char[] { ',' }))
            {
                var toCheck = str.Trim();
                if (toCheck.Length > 0)
                {
                    ignoredStr.Add(toCheck.ToUpper());
                }
            }

            foreach (var str in deleteString.Split(new char[] { ',' }))
            {
                var toCheck = str.Trim();
                if (toCheck.Length > 0)
                {
                    deleteStr.Add(toCheck.ToUpper());
                }
            }

            int cnt = Directory.GetFiles(ModelManager.Instance.PathToModel, "*.*", SearchOption.AllDirectories).Length;
            var modelCount = cnt != 0 ? cnt : 1;
            float current = 0f;

            foreach (string depDirectoryPath in Directory.GetDirectories(ModelManager.Instance.PathToModel))
            {
                foreach (string productDirectoryPath in Directory.GetDirectories(depDirectoryPath))
                {
                    DirectoryInfo productInfo = new DirectoryInfo(productDirectoryPath);
                    string[] files = Directory.GetFiles(productDirectoryPath, "*.*", SearchOption.AllDirectories);
                    foreach (string modelPath in files)
                    {
                        current++;
                        if (cancel)
                        {
                            return;
                        }
                        FileInfo model = new FileInfo(modelPath);
                        ParseModel(model, productInfo);
                        Output.TraceLog("[{0:p2}] Проверка файлов...", current / cnt);
                    }
                }
            }
        }

        /// <summary>
        /// Список сообщений на удаление
        /// </summary>
        protected Queue<FixerItem> toDelete = new Queue<FixerItem>();

        /// <summary>
        /// Список сообщений на перемещение
        /// </summary>
        protected Queue<FixerItem> toMove = new Queue<FixerItem>();

        /// <summary>
        /// Список ошибок
        /// </summary>
        protected Queue<FixerItem> errors = new Queue<FixerItem>();

        /// <summary>
        /// Список игнорирования
        /// </summary>
        protected Queue<FixerItem> ignored = new Queue<FixerItem>();

        /// <summary>
        /// Список пройденных проверок
        /// </summary>
        protected Queue<FixerItem> normal = new Queue<FixerItem>();

        private void ReportSearchProgress(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage == 0)
            {
                toDelete.Enqueue(new FixerItem(e.UserState.ToString(), e.ProgressPercentage));
            }
            else if(e.ProgressPercentage == 1)
            {
                toMove.Enqueue(new FixerItem(e.UserState.ToString(), e.ProgressPercentage));
            }
            else if(e.ProgressPercentage == 2)
            {
                errors.Enqueue(new FixerItem(e.UserState.ToString(), e.ProgressPercentage));
            }else if(e.ProgressPercentage == 3)
            {
                ignored.Enqueue(new FixerItem(e.UserState.ToString(), e.ProgressPercentage));
            }
            else
            {
                normal.Enqueue(new FixerItem(e.UserState.ToString(), e.ProgressPercentage));
            }            
        }

        private void SearchComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            StartEnabled = true;
            timer.Stop();
            BackgroundHandler.Unregister(_searchWorker);
            Output.Log("Проверка выполнена");
            NotifyWindow.ShowNote("Проверка выполнена");
        }

        DispatcherTimer _timer;
        /// <summary>
        /// Таймер для проверки вывода
        /// </summary>
        protected DispatcherTimer timer
        {
            get
            {
                if (_timer == null)
                {
                    _timer = new DispatcherTimer();
                    _timer.Interval = new TimeSpan(1000);
                    _timer.Tick += OutputToGUI;
                }
                return _timer;
            }
        }

        protected void OutputToGUI(object sender, EventArgs e)
        {
            lock (toDelete)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    while (toDelete.Count > 0)
                    {
                        var item = toDelete.Dequeue();
                        DeleteBox.AddLine(item.Text);
                    }

                    while (toMove.Count > 0)
                    {
                        var item = toMove.Dequeue();
                        MoveBox.AddLine(item.Text);
                    }

                    while (errors.Count > 0)
                    {
                        var item = errors.Dequeue();
                        ErrorBox.AddLine(item.Text);
                    }

                    while (ignored.Count > 0)
                    {
                        var item = ignored.Dequeue();
                        IgnoredBox.AddLine(item.Text);
                    }

                    while (normal.Count > 0)
                    {
                        var item = normal.Dequeue();
                        NormalBox.AddLine(item.Text);
                    }
                    if (AutoScroll)
                    {
                        DeleteBox.ScrollToEnd();
                        MoveBox.ScrollToEnd();
                        ErrorBox.ScrollToEnd();
                        IgnoredBox.ScrollToEnd();
                        NormalBox.ScrollToEnd();
                    }
                }));
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            cancel = true;
            timer.Stop();
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("Utility_PathCheck");
        }

        /// <summary>
        /// Отмена фоновой операции
        /// </summary>
        protected bool cancel = false;

        bool startEnabled = true;
        /// <summary>
        /// Включен показ начала проверки?
        /// </summary>
        public bool StartEnabled
        {
            get
            {
                return startEnabled;
            }
            set
            {
                startEnabled = value;
                OnPropertyChanged("StartEnabled");
            }
        }

        /// <summary>
        /// Начать проверку
        /// </summary>
        protected void StartCheck(object sender, RoutedEventArgs e)
        {
            toDelete.Clear();
            toMove.Clear();
            errors.Clear();
            ignored.Clear();
            normal.Clear();
#if IN_EDITOR
            debugDelete.Clear();
#endif
            DeleteBox.Document.Blocks.Clear();
            MoveBox.Document.Blocks.Clear();
            ErrorBox.Document.Blocks.Clear();
            IgnoredBox.Document.Blocks.Clear();
            NormalBox.Document.Blocks.Clear();

            modelToDelete.Clear();

            StartEnabled = false;
            TotalCount = 0;
            DeleteCount = 0;
            MoveCount = 0;
            ErrorCount = 0;
            IgnoredCount = 0;
            NormalCount = 0;
            cancel = false;
            timer.Start();
            Output.Log("Загрузка сохраненных данных...");
            searchWorker.RunWorkerCheck();
        }

        /// <summary>
        /// Закончить проверку
        /// </summary>
        protected void StopCheck(object sender, RoutedEventArgs e)
        {
            cancel = true;
            StartEnabled = true;
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            ignoredString = AppSettings.GetString("PathCheckIgnore");
            deleteString = AppSettings.GetString("PathCheckDelete");
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        string ignoredString;
        /// <summary>
        /// Игнорируемые модели
        /// </summary>
        public string IgnoredString
        {
            get
            {
                return ignoredString;
            }
            set
            {
                ignoredString = value;
                AppSettings.SetString("PathCheckIgnore", value);
                OnPropertyChanged("IgnoredString");
            }
        }

        string deleteString;
        /// <summary>
        /// Удаляемые файлы
        /// </summary>
        public string DeleteString
        {
            get
            {
                return deleteString;
            }
            set
            {
                deleteString = value;
                AppSettings.SetString("PathCheckDelete", value);
                OnPropertyChanged("DeleteString");
            }
        }

        /// <summary>
        /// Список моделей для удаления
        /// </summary>
        List<string> modelToDelete = new List<string>();

        /// <summary>
        /// Удалить все файлы для удаления
        /// </summary>
        private void DeleteAll(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Удалить все модели, предназначенные к удалению?");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    DeleteBox.Document.Blocks.Clear();
                    foreach (var item in modelToDelete)
                    {
                        try
                        {
                            File.Delete(item);
                            DeleteCount--;
                        }
                        catch (Exception ex)
                        {
                            DeleteBox.AppendText(string.Format("[Ошибка]{1} {2}", item, ex.Message));
                            Output.WriteError(ex);
                        }
                    }
                    NotifyWindow.ShowNote("Удаление завершено");
                    modelToDelete.Clear();
                }));                                            
            }
        }

        /// <summary>
        /// Изменить правила удаления
        /// </summary>
        private void DeleteRule(object sender, RoutedEventArgs e)
        {
            DeleteString = GetTextWindow.Get("Правила удаления", "Укажите правила удаления файлов через запятую", DeleteString);
        }

        /// <summary>
        /// Изменить правила игнорирования
        /// </summary>
        private void IgnoredRule(object sender, RoutedEventArgs e)
        {
            IgnoredString = GetTextWindow.Get("Правила игнорирования", "Укажите правила игнорирования файлов через запятую", IgnoredString);
        }

        private void ToggleTemplates(object sender, RoutedEventArgs e)
        {
            var window = new PathCheckTemplateWindow();
            window.ShowDialog();
        }

        private void DebugDelete(object sender, RoutedEventArgs e)
        {
#if IN_EDITOR
            foreach(var item in debugDelete)
            {
                File.Delete(item);
            }
#endif
        }

        private void DropCache(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Удалить кэш игнорированных?");
            if (RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                PathCheckManager.Instance.ClearCacheIgnored();
            }

            RequestWindow.Show("Удалить кэш проверенных?");
            if (RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                PathCheckManager.Instance.ClearCacheNormal();
            }
        }
    }
}
