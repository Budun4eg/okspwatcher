﻿using OKSPWatcher.Core;
using OKSPWatcher.Utilities.Apps.TemplateGen;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для TemplateGeneratorWindow.xaml
    /// </summary>
    public partial class TemplateGeneratorWindow : BasicWindow
    {
        public TemplateGeneratorWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        SafeObservableCollection<string> modes = new SafeObservableCollection<string>()
        {
            "Чертеж",
            "Модель",
            "Извещение"
        };

        /// <summary>
        /// Доступные режимы
        /// </summary>
        public SafeObservableCollection<string> Modes
        {
            get
            {
                return modes;
            }
        }

        string mode = "Чертеж";
        /// <summary>
        /// Текущий режим
        /// </summary>
        public string Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
                OnPropertyChanged("Mode");
                if (modeInfos.ContainsKey(mode))
                {
                    ModeInfo = modeInfos[mode];
                }
            }
        }

        Dictionary<string, string> modeInfos = new Dictionary<string, string>() {
            { "Чертеж", @"0 - группа
1 - номер
2 - вариант
3 - лист
4 - изменение
5 - добавочная буква к номеру листа" }
        };

        string modeInfo;
        /// <summary>
        /// Информация о текущем режиме
        /// </summary>
        public string ModeInfo
        {
            get
            {
                return modeInfo;
            }
            set
            {
                modeInfo = value;
                OnPropertyChanged("ModeInfo");
            }
        }

        string fileName;
        /// <summary>
        /// Наименование
        /// </summary>
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
                OnPropertyChanged("FileName");
                Regenerate();
            }
        }

        string generated;
        /// <summary>
        /// Сгенерированный шаблон
        /// </summary>
        public string Generated
        {
            get
            {
                return generated;
            }
            set
            {
                generated = value;
                OnPropertyChanged("Generated");
            }
        }

        /// <summary>
        /// Текущие правила
        /// </summary>
        Dictionary<string, TemplateRule> rules = new Dictionary<string, TemplateRule>();

        private void Toggle(object sender, RoutedEventArgs e)
        {
            var item = sender as MenuItem;
            AddRule(item.Header.ToString(), StartText.SelectionStart, StartText.SelectionLength);
        }

        /// <summary>
        /// Добавить правило
        /// </summary>
        void AddRule(string key, int start, int length)
        {
            if (!rules.ContainsKey(key))
            {
                //rules.Add(key, new KeyValuePair<int, int>(start, length));
            }
            else
            {
                //rules[key] = new KeyValuePair<int, int>(start, length);
            }
            
            Regenerate();
        }

        /// <summary>
        /// Пересоздать шаблон
        /// </summary>
        void Regenerate()
        {
            var tmp = FileName.ToUpper();
            foreach(var rule in rules)
            {
                //var left = tmp.Substring(0, rule.Value.Key);
                //var right = tmp.Substring(rule.Value.Key);
                //right = right.Substring(rule.Value.Value);
                //right = string.Format("{0}{1}{2}{3}", '{', new string(rule.Key[0], rule.Value.Value), '}', right);
                //tmp = left + right;
            }
            Generated = tmp;
        }
    }
}
