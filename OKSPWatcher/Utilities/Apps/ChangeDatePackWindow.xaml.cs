﻿using OKSPWatcher.Core;
using OKSPWatcher.Secondary;
using System;
using System.IO;
using System.Windows;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для ChangeDatePackWindow.xaml
    /// </summary>
    public partial class ChangeDatePackWindow : BasicWindow
    {
        public ChangeDatePackWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        DateTime date = DateTime.Now;
        /// <summary>
        /// Требуемая дата
        /// </summary>
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }

        string pathToFolder;
        /// <summary>
        /// Путь до папки
        /// </summary>
        public string PathToFolder
        {
            get
            {
                return pathToFolder;
            }
            set
            {
                pathToFolder = value;
                OnPropertyChanged("PathToFolder");
            }
        }

        private void Proceed(object sender, System.Windows.RoutedEventArgs e)
        {
            foreach (string pathToFile in Directory.GetFiles(PathToFolder, "*.*", SearchOption.AllDirectories))
            {
                try
                {
                    var info = new FileInfo(pathToFile);
                    info.LastWriteTime = Date;
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                }                
            }
            NotifyWindow.ShowNote("Изменение даты выполнено");                       
        }
    }
}
