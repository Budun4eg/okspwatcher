﻿using OKSPWatcher.Core;
using OKSPWatcher.Secondary;
using System;
using System.IO;
using System.Windows;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для SplitNamePackWindow.xaml
    /// </summary>
    public partial class SplitNamePackWindow : BasicWindow
    {
        public SplitNamePackWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        string textTemp;
        /// <summary>
        /// Шаблон для поиска
        /// </summary>
        public string TextTemp
        {
            get
            {
                return textTemp;
            }
            set
            {
                textTemp = value;
                OnPropertyChanged("TextTemp");
            }
        }

        string pathToFolder;
        /// <summary>
        /// Путь до папки
        /// </summary>
        public string PathToFolder
        {
            get
            {
                return pathToFolder;
            }
            set
            {
                pathToFolder = value;
                OnPropertyChanged("PathToFolder");
            }
        }

        private void Proceed(object sender, System.Windows.RoutedEventArgs e)
        {
            foreach (string pathToFile in Directory.GetFiles(PathToFolder, "*.*", SearchOption.AllDirectories))
            {
                try
                {
                    var info = new FileInfo(pathToFile);
                    if (info.Name.Contains(textTemp))
                    {
                        string name = info.Name.Substring(0, info.Name.Length - info.Extension.Length);
                        var spl = name.Split(new string[] { textTemp }, StringSplitOptions.None);
                        if(spl.Length == 2)
                        {
                            if(spl[1][0] != ' ')
                            {
                                string right = string.Format(" {0}", spl[1]);
                                string newName = string.Format("{0}{1}{2}", spl[0], textTemp, right);
                                string newPath = string.Format("{0}\\{1}{2}", info.DirectoryName, newName, info.Extension);
                                File.Move(pathToFile, newPath);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Output.WriteError(ex);
                }
            }
            NotifyWindow.ShowNote("Разделение наименования выполнено");
        }
    }
}
