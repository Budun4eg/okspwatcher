﻿using Ionic.Zip;
using OKSPWatcher.Block;
using OKSPWatcher.Core;
using OKSPWatcher.Secondary;
using OKSPWatcher.Utilities.Apps.BDBackup;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows;
using OKSPWatcher.Rules;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для DBBackupWindow.xaml
    /// </summary>
    public partial class DBBackupWindow : BasicWindow
    {
        public DBBackupWindow()
        {
            InitializeComponent();
            BackupPath = AppSettings.GetString("BackupPath");
            DaysToWarn = AppSettings.GetLong("BackupDaysToWarn");
            LastBackup = new DateTime(AppSettings.GetLong("LastBackup"));
            AdditionalBackupManager.Instance.LoadAll();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        /// <summary>
        /// Пользователь должен сделать бэкап?
        /// </summary>
        public static bool MustBackup()
        {
            var enabled = RulesManager.Instance.Can("Utility_DBBackup");
            if (!enabled) return false;
            string path = AppSettings.GetString("BackupPath");
            if (path[path.Length - 1] != '\\' || path[path.Length - 1] != '/')
            {
                path += '/';
            }
            path += "Check";
            try
            {
                using (var stream = File.Create(path)) { }
                File.Delete(path);
            }
            catch
            {
                return false;
            }
            long daysToWarn = AppSettings.GetLong("BackupDaysToWarn");
            DateTime lastBackup = new DateTime(AppSettings.GetLong("LastBackup"));
            if((DateTime.Now - lastBackup).TotalDays >= daysToWarn)
            {
                return true;
            }
            return false;
        }

        long lastBackup;
        /// <summary>
        /// Дата последнего бэкапа
        /// </summary>
        public DateTime LastBackup
        {
            get
            {
                return new DateTime(lastBackup);
            }
            set
            {
                if(lastBackup != value.Ticks)
                {
                    AppSettings.SetLong("LastBackup", value.Ticks);
                }
                lastBackup = value.Ticks;
                OnPropertyChanged("LastBackup");
            }
        }

        long daysToWarn;
        /// <summary>
        /// Количество дней до предупреждения
        /// </summary>
        public long DaysToWarn
        {
            get
            {
                return daysToWarn;
            }
            set
            {
                if(daysToWarn != value)
                {
                    AppSettings.SetLong("BackupDaysToWarn", value);
                }
                daysToWarn = value;
                OnPropertyChanged("DaysToWarn");
            }
        }        

        string backupPath;
        /// <summary>
        /// Путь до папки с бэкапами
        /// </summary>
        public string BackupPath
        {
            get
            {
                return backupPath;
            }
            set
            {
                if(backupPath != value)
                {
                    AppSettings.SetString("BackupPath", value);
                }
                backupPath = value;
                OnPropertyChanged("BackupPath");
            }
        }

        string modPath;

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            string path = BackupPath;
            if (path[path.Length - 1] != '\\' || path[path.Length - 1] != '/')
            {
                path += '/';
            }
            modPath = path;
            path += string.Format("Watcher_{0}", DateTime.Now.ToString().Replace(':', '_'));
            try
            {
                using (ZipFile zip = new ZipFile(path + ".zip"))
                {
                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                    string dbPath = AppSettings.BaseFolder + "\\data\\";
                    foreach(var file in Directory.GetFiles(dbPath, "*.*"))
                    {
                        try
                        {
                            zip.AddFile(file, "Data");
                        }
                        catch (Exception ex)
                        {
                            Output.Error(ex, "Ошибка при добавлении файла {0}", file);
                        }
                    }
                    
                    zip.Save();
                }

                foreach (var item in Additional)
                {
                    if (!Directory.Exists(item.Path))
                    {
                        Output.ErrorFormat("Папка отсутствует {0}", item.Path);
                        continue;
                    }

                    AdditionalBackup(item.Path);                   
                }

                Output.Log("Бэкап создан успешно");
                LastBackup = DateTime.Now;
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при бэкапе");
            }
        }

        void AdditionalBackup(string path, string add = "") {
            var info = new DirectoryInfo(path);

            var to = string.Format("{0}\\{1}{2}", modPath, add, info.Name);

            if (!Directory.Exists(to))
            {
                Directory.CreateDirectory(to);
            }

            foreach (var file in Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly))
            {
                var fileInfo = new FileInfo(file);
                var toName = string.Format("{0}\\{1}", to, fileInfo.Name);
                int offset = 0;
                bool mustCopy = true;
                var chkName = toName;
                while (File.Exists(chkName))
                {
                    var toInfo = new FileInfo(chkName);
                    if (toInfo.LastWriteTime == fileInfo.LastWriteTime)
                    {
                        mustCopy = false;
                        break;
                    }
                    chkName = string.Format("{0}.{1}", toName, offset);
                    offset++;
                }
                if (mustCopy)
                {
                    try
                    {
                        File.Copy(file, chkName);
                    }
                    catch (Exception ex)
                    {
                        Output.Error(ex, "Ошибка при добавлении файла {0} -> {1}", file, chkName);
                    }
                }
            }

            add += info.Name+"\\";

            foreach(var folder in info.GetDirectories())
            {
                AdditionalBackup(folder.FullName, add);
            }
        }

        private void Proceed(object sender, System.Windows.RoutedEventArgs e)
        {
            worker.RunWorkerCheck();                         
        }

        /// <summary>
        /// Дополнительные пути
        /// </summary>
        public SafeObservableCollection<AdditionalBackupPath> Additional
        {
            get
            {
                return AdditionalBackupManager.Instance.Items;
            }
        }

        private void OpenBackupFolder(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Process.Start(BackupPath);
            }catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии папки с бэкапами");
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            AdditionalBackupManager.Instance.Save();
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("Utility_CreateBackup");
        }

        private void AddAdditional(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = new AdditionalBackupPath();
            item.Save();
        }

        private void DeleteAdditional(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as FrameworkElement).DataContext as AdditionalBackupPath;
            RequestWindow.Show("Удалить путь {0}?", item.Path);
            if (RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                item.Delete();
            }
        }
    }
}
