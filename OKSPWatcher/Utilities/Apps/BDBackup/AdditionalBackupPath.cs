﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;

namespace OKSPWatcher.Utilities.Apps.BDBackup
{
    /// <summary>
    /// Дополнительный путь при производстве бэкапа
    /// </summary>
    public class AdditionalBackupPath : DBObject
    {
        string path;
        /// <summary>
        /// Путь
        /// </summary>
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                if (path != value)
                {
                    needUpdate = true;
                }
                path = value;
                OnPropertyChanged("Path");
            }
        }

        public AdditionalBackupPath() : base() {
            needUpdate = true;
        }

        public AdditionalBackupPath(SQLDataRow data) : base()
        {
            Load(data);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                Path = data.String("Path");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                path = data.String("Path");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "AdditionalBackupItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO AdditionalBackupItems (Path, UpdateDate) VALUES ('{0}', {1})", out id,
                path.Check(), UpdateDate.Ticks);
            base.CreateNew();
            AdditionalBackupManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE AdditionalBackupItems SET Path = '{0}', UpdateDate = {1} WHERE Id = {2}",
                path.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM AdditionalBackupItems WHERE Id = {0}", id);
            AdditionalBackupManager.Instance.Remove(this);
        }
    }
}
