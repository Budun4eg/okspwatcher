﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;

namespace OKSPWatcher.Utilities.Apps.BDBackup
{
    public class AdditionalBackupManager : ManagerObject<AdditionalBackupPath>
    {
        private static object root = new object();

        static AdditionalBackupManager instance;

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static AdditionalBackupManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if (instance == null)
                        {
                            instance = new AdditionalBackupManager();
                        }
                    }
                }
                return instance;
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("AdditionalBackupItems").Execute(@"DROP TABLE AdditionalBackupItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("AdditionalBackupItems").Execute(@"CREATE TABLE IF NOT EXISTS AdditionalBackupItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Path TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            instance = null;
        }

        /// <summary>
        /// Загрузить все элементы
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("AdditionalBackupItems").ExecuteRead("SELECT * FROM AdditionalBackupItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new AdditionalBackupPath(row));
            }
        }
    }
}
