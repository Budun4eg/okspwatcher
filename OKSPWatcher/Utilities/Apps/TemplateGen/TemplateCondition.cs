﻿namespace OKSPWatcher.Utilities.Apps.TemplateGen
{
    /// <summary>
    /// Дополнительное условие шаблона
    /// </summary>
    public class TemplateCondition
    {
        /// <summary>
        /// Смещение символа
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Требуемый символ
        /// </summary>
        public char Symbol { get; set; }
    }
}
