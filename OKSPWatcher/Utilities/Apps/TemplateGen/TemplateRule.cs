﻿using System.Collections.Generic;

namespace OKSPWatcher.Utilities.Apps.TemplateGen
{
    /// <summary>
    /// Правило шаблона
    /// </summary>
    public class TemplateRule
    {
        /// <summary>
        /// Ключ правила
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Начало правила
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Длина правила
        /// </summary>
        public int Length { get; set; }
                
        List<TemplateCondition> conditions = new List<TemplateCondition>();
        /// <summary>
        /// Список дополнительных условий
        /// </summary>
        public List<TemplateCondition> Conditions
        {
            get
            {
                return conditions;
            }
        }

        public TemplateRule(string key, int start, int length, string text)
        {

        }
    }
}
