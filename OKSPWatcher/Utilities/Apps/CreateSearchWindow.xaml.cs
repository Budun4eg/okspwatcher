﻿using System;
using System.ComponentModel;
using OKSPWatcher.Core;
using OKSPWatcher.Utilities.Apps.CreateSearch;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using OKSPWatcher.Secondary;
using System.Windows.Documents;
using OKSPWatcher.Products;
using System.IO;
using OKSPWatcher.Models;
using System.Windows;
using OKSPWatcher.Block;
using System.Linq;
using System.Diagnostics;

namespace OKSPWatcher.Utilities.Apps
{
    /// <summary>
    /// Логика взаимодействия для CreateSearchWindow.xaml
    /// </summary>
    public partial class CreateSearchWindow : BasicWindow
    {
        bool cancel = false;

        public override void StopUpdate()
        {
            cancel = true;
            base.StopUpdate();
        }

        string pathToSearch;

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            cancel = false;
            base.BackgroundWork(sender, e);
            pathToSearch = AppSettings.GetString("CreateSearchPath");
            AdditionalPathManager.Instance.LoadAll();            
            ProductSearchManager.Instance.LoadAll();            
            ProductManager.Instance.LoadAll();
            foreach (var chk in ProductManager.Instance.Items)
            {
                if (!ProductSearchManager.Instance.Used(chk)) {
                    var item = new ProductSearchItem();
                    item.LinkedProduct = chk;
                    item.Save();
                }
            }

            foreach (string depDirectoryPath in Directory.GetDirectories(ModelManager.Instance.PathToModel))
            {
                foreach (string productDirectoryPath in Directory.GetDirectories(depDirectoryPath))
                {
                    DirectoryInfo productInfo = new DirectoryInfo(productDirectoryPath);
                    string[] files = Directory.GetFiles(productDirectoryPath, "*.*", SearchOption.AllDirectories);
                    foreach (string modelPath in files)
                    {
                        if (cancel || BackgroundHandler.AllClose)
                        {
                            return;
                        }
                        ProductSearchManager.Instance.AddPath(productInfo, modelPath);
                    }
                }
            }
        }

        private void Instance_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Generate();
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            ProductSearchManager.Instance.DeleteEmpty();
            AdditionalPathManager.Instance.PropertyChanged += Instance_PropertyChanged;
            ProductSearchManager.Instance.PropertyChanged += Instance_PropertyChanged;

            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
            Generate();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            AdditionalPathManager.Instance.PropertyChanged -= Instance_PropertyChanged;
            ProductSearchManager.Instance.PropertyChanged -= Instance_PropertyChanged;
            AdditionalPathManager.Instance.Save();
            ProductSearchManager.Instance.Save();
        }

        public CreateSearchWindow()
        {            
            InitializeComponent();
            worker.RunWorkerCheck();            
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("Utility_CreateSearch");
        }

        /// <summary>
        /// Дополнительные пути
        /// </summary>
        public SafeObservableCollection<AdditionalPathItem> Additional
        {
            get
            {
                return AdditionalPathManager.Instance.Items;
            }
        }

        SafeObservableCollection<string> additionalTypes = new SafeObservableCollection<string>() {
            "Начало",
            "Конец"
        };

        public SafeObservableCollection<string> AdditionalTypes
        {
            get
            {
                return additionalTypes;
            }
        }

        /// <summary>
        /// Изделия
        /// </summary>
        public SafeObservableCollection<ProductSearchItem> Products
        {
            get
            {
                return ProductSearchManager.Instance.Items;
            }
        }

        /// <summary>
        /// Сгенерированная строка
        /// </summary>
        string generated;

        /// <summary>
        /// Сгенерировать список
        /// </summary>
        void Generate()
        {
            List<string> added = new List<string>();
            StringBuilder builder = new StringBuilder();
            OutputBox.Document.Blocks.Clear();
            List<string> top = new List<string>();
            List<string> bottom = new List<string>();

            foreach(var item in Additional)
            {
                if(item.PathType == "Начало")
                {
                    top.Add(item.Path);
                    foreach(var child in item.Additional)
                    {
                        top.Add(child);
                    }
                }else if(item.PathType == "Конец")
                {
                    bottom.Add(item.Path);
                    foreach (var child in item.Additional)
                    {
                        bottom.Add(child);
                    }
                }
            }

            foreach(var item in top)
            {
                if (added.Contains(item))
                {
                    continue;
                }
                OutputBox.AddLine(item);
                builder.AppendLine(item);
                added.Add(item);
            }

            foreach(var prd in Products)
            {
                if (!prd.UseInSearch) continue;
                if (prd.Paths.Count == 0) continue;
                int totalLines = 0;
                foreach (var line in prd.Additional)
                {
                    if (added.Contains(line))
                    {
                        continue;
                    }
                    totalLines++;
                }
                foreach (var line in prd.Sorted)
                {
                    if (added.Contains(line))
                    {
                        continue;
                    }
                    totalLines++;
                }
                if (totalLines == 0) continue;
                string toAdd = string.Format("!---- {0} ----", prd.LinkedProduct != null ? prd.LinkedProduct.Name : "Не определено");
                OutputBox.AddLine(toAdd);
                builder.AppendLine(toAdd);
                foreach(var line in prd.Additional)
                {
                    if (added.Contains(line))
                    {
                        continue;
                    }
                    OutputBox.AddLine(line);
                    builder.AppendLine(line);
                    added.Add(line);
                }
                foreach (var line in prd.Sorted)
                {
                    if (added.Contains(line))
                    {
                        continue;
                    }
                    OutputBox.AddLine(line);
                    builder.AppendLine(line);
                    added.Add(line);
                }
            }

            foreach(var item in bottom)
            {
                if (added.Contains(item))
                {
                    continue;
                }
                OutputBox.AddLine(item);
                builder.AppendLine(item);
                added.Add(item);
            }

            generated = builder.ToString();
        }

        private void SaveFile(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                File.WriteAllText(pathToSearch, generated);
                NotifyWindow.ShowNote("Сохранение файла завершено");
            }catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при сохранении файла");
            }
        }

        private void AddAdditional(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = new AdditionalPathItem();
            item.Save();
        }

        private void DeleteAdditional(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as AdditionalPathItem;
            RequestWindow.Show("Удалить путь {0}?", item.Path);
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                item.Delete();
            }
        }

        private Point startPoint;

        private void DragLeftButton(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {                       
            startPoint = e.GetPosition(null);            
        }

        List<ProductSearchItem> dragged = new List<ProductSearchItem>();

        private void DragMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if(e.LeftButton == System.Windows.Input.MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)){
                dragged.Clear();
                var list = (sender as ListView);
                foreach (var item in list.SelectedItems)
                {
                    var container = (item as ProductSearchItem);
                    if (!dragged.Contains(container))
                    {
                        dragged.Add(container);
                    }
                }

                var listViewItem = (e.OriginalSource as DependencyObject).GetVisualParent<ListViewItem>();
                if (listViewItem == null) return;
                var dragItem = listViewItem.DataContext as ProductSearchItem;
                if (!dragged.Contains(dragItem))
                {
                    dragged.Add(dragItem);
                }
                dragged = dragged.OrderBy(x => ProductSearchManager.Instance.Items.IndexOf(x)).ToList();
                DragDrop.DoDragDrop(listViewItem, dragged, DragDropEffects.Move);
            }            
        }

        private void ItemDrop(object sender, DragEventArgs e)
        {
            var target = (e.OriginalSource as DependencyObject).GetVisualParent<ListViewItem>().DataContext as ProductSearchItem;

            ProductSearchManager.Instance.Move(dragged, target);
        }

        private void ChangePath(object sender, RoutedEventArgs e)
        {
            pathToSearch = GetTextWindow.Get("Путь до файла search", "Введите путь до файла search", pathToSearch);
            AppSettings.SetString("CreateSearchPath", pathToSearch);
        }

        private void ChangeAdditional(object sender, RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as ProductSearchItem;
            item.AdditionalStrings = GetTextWindow.Get("Дополнительные пути", "Введите дополнительные пути для изделия через запятую", item.AdditionalStrings);
        }

        private void MoveToTop(object sender, RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as ProductSearchItem;

            ProductSearchManager.Instance.Top(item);
        }

        private void MoveToBottom(object sender, RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as ProductSearchItem;

            ProductSearchManager.Instance.Bottom(item);
        }

        private void OpenFile(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(pathToSearch);
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии файла");
            }
        }
    }
}
