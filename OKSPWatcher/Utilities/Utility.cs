﻿using OKSPWatcher.Core;
using OKSPWatcher.Rules;

namespace OKSPWatcher.Utilities
{
    /// <summary>
    /// Объект утилиты
    /// </summary>
    public class Utility : NotifyObject
    {

        string code;
        /// <summary>
        /// Код утилиты
        /// </summary>
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                code = value;
                OnPropertyChanged("Code");
            }
        }

        /// <summary>
        /// Утилита доступна?
        /// </summary>
        public bool Available
        {
            get
            {
                return RulesManager.Instance.Can("Utility_" + Code);
            }
        }
    }
}
