﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Diagnostics;
using System.Windows.Controls;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Ячейка структуры
    /// </summary>
    public class StructureNode : DBObject
    {
        #region VisualStructure
        public enum AlignTypes { Default, Child, Parent }

        AlignTypes alignType;
        /// <summary>
        /// Тип расположения элемента
        /// </summary>
        public AlignTypes AlignType
        {
            get
            {
                return alignType;
            }
            set
            {
                alignType = value;
                OnPropertyChanged("AlignType");
            }
        }

        int nodeIndex = 1;
        /// <summary>
        /// Номер ячейки
        /// </summary>
        public int NodeIndex
        {
            get
            {
                return nodeIndex;
            }
            set
            {
                nodeIndex = value;
                OnPropertyChanged("NodeIndex");
            }
        }

        int nodeCount;
        /// <summary>
        /// Количество доступных ячеек
        /// </summary>
        public int NodeCount
        {
            get
            {
                return nodeCount;
            }
            set
            {
                nodeCount = value;
                OnPropertyChanged("NodeCount");
                OnPropertyChanged("CanChangeIndex");
            }
        }

        /// <summary>
        /// Можно менять индекс ячейки?
        /// </summary>
        public bool CanChangeIndex
        {
            get
            {
                return nodeCount > 1;
            }
        }

        bool canBeExtended;
        /// <summary>
        /// Ячейка может быть развернута
        /// </summary>
        public bool CanBeExtended
        {
            get
            {
                return canBeExtended;
            }
            set
            {
                canBeExtended = value;
                OnPropertyChanged("CanBeExtended");
            }
        }

        StructureNode parent;
        /// <summary>
        /// Родительская ячейка
        /// </summary>
        public StructureNode Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
            }
        }

        int x = -1;
        /// <summary>
        /// Смещение по X
        /// </summary>
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
#if LAYOUT_OUTPUT
                Debug.Print("{0} X:{1}", Name, value);
#endif
            }
        }

        int y = -1;
        /// <summary>
        /// Смещение по Y
        /// </summary>
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
#if LAYOUT_OUTPUT
                Debug.Print("{0} Y:{1}", Name, value);
#endif
            }
        }

        bool collapsed = true;
        /// <summary>
        /// Ячейка свернута?
        /// </summary>
        public bool IsCollapsed
        {
            get
            {
                return collapsed;
            }
            set
            {
                collapsed = value;
                OnPropertyChanged("IsCollapsed");
            }
        }

        bool extended;
        /// <summary>
        /// Ячейка развернута?
        /// </summary>
        public bool IsExtended
        {
            get
            {
                return extended;
            }
            set
            {
                extended = value;
                if (extended)
                {
                    foreach(var child in Childs)
                    {
                        child.IsCollapsed = false;
                    }
                }
                else
                {
                    foreach(var child in Childs)
                    {
                        child.IsCollapsed = true;
                        child.IsExtended = false;
                    }
                }
                OnPropertyChanged("IsExtended");
            }
        }

        double xOffset = -1;
        /// <summary>
        /// Смещение по x при визуальном построении
        /// </summary>
        public double XOffset
        {
            get
            {
                return xOffset;
            }
            set
            {
                xOffset = value;
#if LAYOUT_OUTPUT
                Debug.Print("{0} XOffset:{1}", Name, value);
#endif
            }
        }

        double yOffset = -1;
        /// <summary>
        /// Смещение по y при визуальном построении
        /// </summary>
        public double YOffset
        {
            get
            {
                return yOffset;
            }
            set
            {
                yOffset = value;
#if LAYOUT_OUTPUT
                Debug.Print("{0} YOffset:{1}", Name, value);
#endif
            }
        }

        Label structureLabel;
        /// <summary>
        /// Объект визуального построения
        /// </summary>
        public Label StructureLabel
        {
            get
            {
                return structureLabel;
            }
            set
            {
                structureLabel = value;
            }
        }
        #endregion

        

        string nodeType = "";
        /// <summary>
        /// Тип ячейки
        /// </summary>
        public string NodeType
        {
            get
            {
                return nodeType;
            }
            set
            {
                if(nodeType != value)
                {
                    needUpdate = true;
                }
                nodeType = value;
                OnPropertyChanged("NodeType");
            }
        }

        /// <summary>
        /// Обозначение
        /// </summary>
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                if(name != value)
                {
                    needUpdate = true;
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Наименование
        /// </summary>
        public override string Description
        {
            get
            {
                return base.Description;
            }
            set
            {
                if(description != value)
                {
                    needUpdate = true;
                }
                description = value;
                OnPropertyChanged("Description");
            }
        }

        long loodsmanId = -1;
        /// <summary>
        /// Идентификатор в ЛОЦМАНе
        /// </summary>
        public long LoodsmanId
        {
            get
            {
                return loodsmanId;
            }
            set
            {
                if(loodsmanId != value)
                {
                    needUpdate = true;
                }
                loodsmanId = value;
                OnPropertyChanged("LoodsmanId");
            }
        }

        /// <summary>
        /// Суммарное количество ячеек
        /// </summary>
        public int TotalCount
        {
            get
            {
                int totalCount = Childs.Count;
                foreach(var item in Childs)
                {
                    totalCount += item.TotalCount;
                }

                return totalCount;
            }
        }

        StructureNodePack childs;
        /// <summary>
        /// Дочерние элементы
        /// </summary>
        public StructureNodePack Childs
        {
            get
            {
                if(childs == null)
                {
                    childs = new StructureNodePack();
                    childs.Updated += PackUpdated;
                }
                return childs;
            }
        }

        private void PackUpdated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        public StructureNode() : base()
        {
            needUpdate = true;
        }

        public StructureNode(SQLDataRow data) : base()
        {
            Load(data);
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                NodeType = data.String("NodeType");
                Name = data.String("Name");
                Description = data.String("Description");
                LoodsmanId = data.Long("LoodsmanId");
                Childs.Sync(data.Ids("Childs"));
                foreach(var child in Childs.Items)
                {
                    if(child != null)
                    {
                        child.CheckUpdate();
                    }
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                nodeType = data.String("NodeType");
                name = data.String("Name");
                description = data.String("Description");
                loodsmanId = data.Long("LoodsmanId");
                Childs.Sync(data.Ids("Childs"));
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "StructureNodes";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO StructureNodes (NodeType, Name, Description, LoodsmanId, Childs, UpdateDate) VALUES ('{0}', '{1}', '{2}', {3}, '{4}', {5})", out id,
                nodeType.Check(), name.Check(), description.Check(), loodsmanId, Childs.Serialize(), UpdateDate.Ticks);
            base.CreateNew();
            StructureNodeManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE StructureNodes SET NodeType = '{0}', Name = '{1}', Description = '{2}', LoodsmanId = {3}, Childs = '{4}', UpdateDate = {5} WHERE Id = {6}",
                nodeType.Check(), name.Check(), description.Check(), loodsmanId, Childs.Serialize(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM StructureNodes WHERE Id = {0}", id);
            StructureNodeManager.Instance.Remove(this);
        }

        public override bool Save()
        {
            Childs.Save();
            return base.Save();
        }
    }
}
