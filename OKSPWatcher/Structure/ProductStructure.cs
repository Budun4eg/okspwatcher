﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Структура изделия
    /// </summary>
    public class ProductStructure : DBObject
    {
        long prdId = -1;
        /// <summary>
        /// Привязанное изделие
        /// </summary>
        public Product LinkedProduct
        {
            get
            {
                Sync();
                return ProductManager.Instance.ById(prdId, true);
            }
            set
            {
                Sync();
                long toSet = value != null ? value.Id : -1;
                if(prdId != toSet)
                {
                    needUpdate = true;
                }
                prdId = toSet;
                OnPropertyChanged("LinkedProduct");
            }
        }

        double readyPercent;
        /// <summary>
        /// Процент готовности изделия
        /// </summary>
        public double ReadyPercent
        {
            get
            {
                Sync();
                return readyPercent;
            }
            set
            {
                Sync();
                if(value != readyPercent)
                {
                    needUpdate = true;
                }
                readyPercent = value;
                OnPropertyChanged("ReadyPercent");
            }
        }

        StructureNodePack topNodes;
        /// <summary>
        /// Верхние ячейки
        /// </summary>
        public StructureNodePack TopNodes
        {
            get
            {
                if(topNodes == null)
                {
                    topNodes = new StructureNodePack();
                    topNodes.Updated += TopNodes_Updated;
                }
                Sync();
                return topNodes;
            }
        }

        private void TopNodes_Updated(object sender, EventArgs e)
        {
            needUpdate = true;
        }

        public ProductStructure(Product item)
        {
            LinkedProduct = item;
            needSync = true;
            needUpdate = true;
        }

        /// <summary>
        /// Нужна синхронизация объекта?
        /// </summary>
        bool needSync = false;

        /// <summary>
        /// Синхронизировать состояние объекта
        /// </summary>
        public void Sync(bool forced = false)
        {
            if (!needSync && !forced) return;
            needSync = false;
            var data = SQL.Get("StructureItems").ExecuteRead("SELECT * FROM StructureItems WHERE Product = {0} LIMIT 1", prdId);
            var row = data.Single;
            if (row != null)
            {
                Load(row);
                ProductStructureManager.Instance.Add(this);
            }
            else
            {
                needUpdate = true;
            }
        }

        public ProductStructure()
        {
            needUpdate = true;
        }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                prdId = data.Long("Product");
                OnPropertyChanged("LinkedProduct");
                if(LinkedProduct != null)
                {
                    LinkedProduct.CheckUpdate();
                }
                ReadyPercent = data.Double("ReadyPercent");
                TopNodes.Sync(data.Ids("TopNodes"));
                foreach(var node in TopNodes.Items)
                {
                    if(node != null)
                    {
                        node.CheckUpdate();
                    }
                }
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                prdId = data.Long("Product");
                readyPercent = data.Double("ReadyPercent");
                TopNodes.Sync(data.Ids("TopNodes"));
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "StructureItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO StructureItems (Product, TopNodes, ReadyPercent, UpdateDate) VALUES ({0}, '{1}', '{2}', {3})", out id, prdId, TopNodes.Serialize(), readyPercent, UpdateDate.Ticks);
            base.CreateNew();
            ProductStructureManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE StructureItems SET Product = {0}, TopNodes = '{1}', ReadyPercent = '{2}', UpdateDate = {3} WHERE Id = {4}", prdId, TopNodes.Serialize(), readyPercent, UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            Sync();
            base.Delete();
            sql.Execute("DELETE FROM StructureItems WHERE Id = {0}", id);
            ProductStructureManager.Instance.Remove(this);
        }

        public override bool Save()
        {
            Sync();
            TopNodes.Save();
            return base.Save();
        }
    }
}
