﻿using System;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Данные события структуры изделия
    /// </summary>
    public class StructureEventArgs : EventArgs
    {
        string info;
        /// <summary>
        /// Информация
        /// </summary>
        public string Info
        {
            get
            {
                return info;
            }
        }

        public StructureEventArgs(string _info)
        {
            info = _info;
        }
    }
}
