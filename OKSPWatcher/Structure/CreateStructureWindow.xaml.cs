﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System.ComponentModel;
using System.Windows.Controls;
using System;
using System.Windows;
using OKSPWatcher.Block;
using OKSPWatcher.Secondary;
using System.IO;
using OKSPWatcher.Structure.LoodsmanData;
using System.Collections.Generic;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Diagnostics;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Логика взаимодействия для CreateStructureWindow.xaml
    /// </summary>
    public partial class CreateStructureWindow : BasicWindow
    {
        bool useStandart;
        /// <summary>
        /// Использовать стандартные изделия
        /// </summary>
        public bool UseStandart
        {
            get
            {
                return useStandart;
            }
            set
            {
                useStandart = value;
                OnPropertyChanged("UseStandart");
            }
        }

        bool useUnnamed;
        /// <summary>
        /// Использовать обезличенные
        /// </summary>
        public bool UseUnnamed
        {
            get
            {
                return useUnnamed;
            }
            set
            {
                useUnnamed = value;
                OnPropertyChanged("UseUnnamed");
            }
        }

        bool useOthers;
        /// <summary>
        /// Использовать прочее
        /// </summary>
        public bool UseOthers
        {
            get
            {
                return useOthers;
            }
            set
            {
                useOthers = value;
                OnPropertyChanged("UseOthers");
            }
        }

        bool useChildFolders;
        /// <summary>
        /// Использовать дочерние папки
        /// </summary>
        public bool UseChildFolders
        {
            get
            {
                return useChildFolders;
            }
            set
            {
                useChildFolders = value;
                OnPropertyChanged("UseChildFolders");
            }
        }

        public CreateStructureWindow()
        {
            InitializeComponent();
            lastUpdate = AppSettings.GetLong("LoodsmanLastUpdate");
            BlockManager.Instance.Block("CreateStructure");
            mainNodeStyle = FindResource("MainNode") as Style;
            childNodeStyle = FindResource("ChildNode") as Style;
            worker.RunWorkerCheck();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("CreateStructure");
        }

        /// <summary>
        /// Варианты режима
        /// </summary>
        enum States { Load, Update }

        /// <summary>
        /// Текущий режим
        /// </summary>
        States state = States.Load;

        long lastUpdate;
        /// <summary>
        /// Последняя дата обновления данных из ЛОЦМАНа
        /// </summary>
        public DateTime LastUpdate
        {
            get
            {
                return new DateTime(lastUpdate);
            }
            set
            {
                if(lastUpdate != value.Ticks)
                {
                    AppSettings.SetLong("LoodsmanLastUpdate", lastUpdate);
                }
                lastUpdate = value.Ticks;
                OnPropertyChanged("LastUpdate");
                OnPropertyChanged("ObjectCount");
                OnPropertyChanged("TreeCount");
            }
        }

        /// <summary>
        /// Количество объектов
        /// </summary>
        public long ObjectCount
        {
            get
            {
                return LoodsmanObjectManager.Instance.Items.Count;
            }
        }

        /// <summary>
        /// Количество элементов в дереве
        /// </summary>
        public long TreeCount
        {
            get
            {
                int cnt = 0;
                foreach(var item in LoodsmanTreeManager.Instance.Items)
                {
                    cnt += item.ParentIds.Count;
                }
                return cnt;
            }
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            if(state == States.Load)
            {
                ProductManager.Instance.LoadAll();
                foreach (var product in ProductManager.Instance.Items)
                {
                    if (BackgroundHandler.AllClose) return;
                    structures.Add(ProductStructureManager.Instance.ByProduct(product));
                }
                LoodsmanObjectManager.Instance.LoadAll();
                LoodsmanTreeManager.Instance.LoadAll();
            }else if(state == States.Update)
            {
                string objectRequest = "";
                string treeRequest = "";
                string objectUnnamed = "";
                string treeUnnamed = "";

                try
                {
                    using (StreamReader reader = new StreamReader(AppSettings.TemplatesFolder + "/LoodsmanObjCall.txt"))
                    {
                        objectRequest = reader.ReadToEnd();
                    }

                    using (StreamReader reader = new StreamReader(AppSettings.TemplatesFolder + "/LoodsmanTreeCall.txt"))
                    {
                        treeRequest = reader.ReadToEnd();
                    }

                    using (StreamReader reader = new StreamReader(AppSettings.TemplatesFolder + "/LoodsmanObjCallUnnamed.txt"))
                    {
                        objectUnnamed = reader.ReadToEnd();
                    }

                    using (StreamReader reader = new StreamReader(AppSettings.TemplatesFolder + "/LoodsmanTreeCallUnnamed.txt"))
                    {
                        treeUnnamed = reader.ReadToEnd();
                    }

                    Output.Log("Обновление данных из ЛОЦМАНа...");
                    Loodsman.Instance.Open();

                    if (BackgroundHandler.AllClose || stopUpdate) return;

                    var obj = Loodsman.Instance.ExecuteRead(objectRequest);

                    foreach (SQLDataRow row in obj)
                    {
                        if (BackgroundHandler.AllClose || stopUpdate) return;
                        long objectId = row.Int("ObjectId");
                        if (LoodsmanObjectManager.Instance.HasObjectId(objectId)) continue;
                        string objectType = row.String("ObjectType");
                        string name = row.String("ObjectName");
                        string description = row.String("ValStr");
                        var newObj = new LoodsmanObjectItem(objectId, objectType, name, description);
                        newObj.Save();
                        Output.LogFormat("Загружен объект {0} '{1}'", name, description);
                    }

                    var objUnnamed = Loodsman.Instance.ExecuteRead(objectUnnamed);

                    foreach (SQLDataRow row in objUnnamed)
                    {
                        if (BackgroundHandler.AllClose || stopUpdate) return;
                        long objectId = row.Int("ObjectId");
                        if (LoodsmanObjectManager.Instance.HasObjectId(objectId)) continue;
                        string objectType = row.String("ObjectType");
                        string name = row.String("ObjectName");
                        var newObj = new LoodsmanObjectItem(objectId, objectType, name, "");
                        newObj.Save();
                        Output.LogFormat("Загружен объект {0} '{1}'", name, "");
                    }

                    var tree = Loodsman.Instance.ExecuteRead(treeRequest);

                    foreach (SQLDataRow row in tree)
                    {
                        if (BackgroundHandler.AllClose || stopUpdate) return;
                        long parentId = row.Int("ParID");
                        long objectId = row.Int("ChID");
                        if (LoodsmanTreeManager.Instance.HasObjectId(objectId)) {
                            var pr = LoodsmanTreeManager.Instance.ByObjectId(objectId).Single;
                            pr.AddParent(parentId);
                            pr.Save();
                            continue;
                        }
                        string objectType = row.String("ChType");
                        string name = row.String("ChName");
                        string description = row.String("ValStr");
                        var newObj = new LoodsmanTreeItem(parentId, objectId, objectType, name, description);
                        newObj.Save();
                        Output.LogFormat("Загружен элемент дерева {0} '{1}'", name, description);
                    }

                    var treeUn = Loodsman.Instance.ExecuteRead(treeUnnamed);

                    foreach (SQLDataRow row in treeUn)
                    {
                        if (BackgroundHandler.AllClose || stopUpdate) return;
                        long parentId = row.Int("ParID");
                        long objectId = row.Int("ChID");
                        if (LoodsmanTreeManager.Instance.HasObjectId(objectId))
                        {
                            var pr = LoodsmanTreeManager.Instance.ByObjectId(objectId).Single;
                            pr.AddParent(parentId);
                            pr.Save();
                            continue;
                        }
                        string objectType = row.String("ChType");
                        string name = row.String("ChName");
                        var newObj = new LoodsmanTreeItem(parentId, objectId, objectType, name, "");
                        newObj.Save();
                        Output.LogFormat("Загружен элемент дерева {0} '{1}'", name, "");
                    }

                    LastUpdate = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Output.Error(ex, "Ошибка при обновлении данных из ЛОЦМАНа");
                }
                Loodsman.Instance.Close();
            }
        }

        SafeObservableCollection<ProductStructure> structures = new SafeObservableCollection<ProductStructure>();
        /// <summary>
        /// Список структур
        /// </summary>
        public SafeObservableCollection<ProductStructure> Structures
        {
            get
            {
                return structures;
            }
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            if(state == States.Load)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    DataContext = this;
                }));
            }else if(state == States.Update)
            {
                NotifyWindow.ShowNote("Обновление данных из ЛОЦМАНа завершено");
            }
        }

        ProductStructure selected;
        /// <summary>
        /// Выбранная структура
        /// </summary>
        public ProductStructure Selected
        {
            get
            {
                return selected;
            }
            set
            {
                if (selected != null)
                {
                    selected.Save();
                    selected.IsSelected = false;
                }
                selected = value;
                selected.IsSelected = true;
                OnPropertyChanged("Selected");
                structureName = selected.LinkedProduct.Name;
                OnPropertyChanged("StructureName");
                UpdateView();
            }
        }

        string structureName = "-";
        /// <summary>
        /// Наименование структуры
        /// </summary>
        public string StructureName
        {
            get
            {
                return structureName;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if(selected != null)
            {
                selected.Save();
            }
            if(panel != null)
            {
                panel.Children.Clear();
            }
            foreach (var node in labeled)
            {
                node.StructureLabel = null;
            }
            stopUpdate = true;
        }

        private void StructureClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ProductStructure;
            if(panel != null)
            {
                panel.Children.Clear();
            }
            foreach(var node in labeled)
            {
                node.StructureLabel = null;
            }
            Selected = item;
        }

        /// <summary>
        /// Необходимо отменить выполнение?
        /// </summary>
        bool stopUpdate = false;

        public override void StopUpdate()
        {
            base.StopUpdate();
            stopUpdate = true;
        }

        private void UpdateLoodsmanData(object sender, RoutedEventArgs e)
        {
            RequestWindow.Show("Обновить данные из ЛОЦМАНа?");
            if(RequestWindow.Result == RequestWindow.ResultType.Yes)
            {
                state = States.Update;
                stopUpdate = false;
                worker.RunWorkerCheck();
            }
        }

        /// <summary>
        /// Панель вывода
        /// </summary>
        RelativeLayoutPanel panel;
        
        /// <summary>
        /// Стандартный размер элемента
        /// </summary>
        Vector defaultItemSize = new Vector(350,70);

        private void PanelInit(object sender, EventArgs e)
        {
            panel = sender as RelativeLayoutPanel;
        }

        /// <summary>
        /// Список добавленных ячеек
        /// </summary>
        List<StructureNode> added = new List<StructureNode>();

        /// <summary>
        /// Максимальное значение элементов
        /// </summary>
        int maxCount;

        /// <summary>
        /// Уровень с максимальным количеством элементов
        /// </summary>
        int maxLevel;

        /// <summary>
        /// Список выровненных ячеек
        /// </summary>
        List<StructureNode> aligned = new List<StructureNode>();

        int defaultOffset;

        /// <summary>
        /// Обновить вывод
        /// </summary>
        void UpdateView()
        {
            added.Clear();
            if (selected != null)
            {
                selected.Save();
            }
            levels.Clear();

            for(int i = 0; i < 99; i++)
            {
                levels.Add(new List<StructureNode>());
            }

            foreach (var top in selected.TopNodes)
            {
                top.IsCollapsed = false;
            }

            foreach(var line in lines)
            {
                panel.Children.Remove(line);
            }
            lines.Clear();

            foreach(var item in selected.TopNodes)
            {
                AddItem(item, 0);
            }

            maxCount = 0;
            maxLevel = 0;
            foreach(var item in levels)
            {
                if(item.Count > maxCount)
                {
                    maxLevel = levels.IndexOf(item);
                    maxCount = item.Count;
                }
            }

            for(int i = panel.Children.Count - 1; i >= 0; i--)
            {
                var data = (panel.Children[i] as Label).DataContext as StructureNode;
                if (!added.Contains(data))
                {
                    panel.Children.RemoveAt(i);
                }
            }

            aligned.Clear();
            defaultOffset = 0;

#if LAYOUT_OUTPUT
            Debug.Print("==== ALIGN ====");
#endif
            foreach(var item in selected.TopNodes)
            {
                AlignItem(item);
            }

            foreach (var item in aligned)
            {
                CheckAlignItem(item);
            }
            CheckOverlap();

            if (levels.Count == 0)
            {
                return;
            }
            double xSize = 1 / (double)levels.Count;

            double maxY = 0;
            foreach(var cnt in levels)
            {
                if (cnt.Count > 0)
                {
                    var hei = cnt[cnt.Count - 1].YOffset + 1;
                    if(hei > maxY)
                    {
                        maxY = hei;
                    }
                }
            }
            double ySize = 1 / (double)maxY;

            panel.Width = levels.Count * defaultItemSize.X;
            panel.Height = maxY * defaultItemSize.Y;

            itemSize = new Vector(xSize, ySize);
            foreach (var item in selected.TopNodes)
            {
                ShowItem(item);
            }
            Output.Log("Представление обновлено");
        }
        
        /// <summary>
        /// Количество объектов на каждом уровне
        /// </summary>
        List<List<StructureNode>> levels = new List<List<StructureNode>>();

        /// <summary>
        /// Размер элемента
        /// </summary>
        Vector itemSize;

        /// <summary>
        /// Проверить ячейку на необходимость вывода
        /// </summary>
        bool NodeCheck(StructureNode node)
        {
            if ((node.NodeType == "Изделие" || node.NodeType == "Комплекс" || node.NodeType == "Комплект" || node.NodeType == "Папка" || node.NodeType == "Сборка" || node.NodeType == "Сборочная единица" || node.NodeType == "") || selected.TopNodes.Contains(node)) return true;
            return false;
        }

        Style mainNodeStyle;
        Style childNodeStyle;

        /// <summary>
        /// Проверить перекрытия ячеек
        /// </summary>
        void CheckOverlap()
        {
            foreach(var level in levels)
            {
                double minY = -1;
                foreach(var node in level)
                {
                    if(minY < 0)
                    {
                        minY = node.YOffset;
                    }
                    if (node.YOffset < minY)
                    {
                        node.YOffset = minY;
                        minY++;
                    }
                    else
                    {
                        minY = node.YOffset + 1;
                    }
                }
            }
        }

        /// <summary>
        /// Определить отношение элементов
        /// </summary>
        void AlignItem(StructureNode node)
        {
            if (node.IsCollapsed || !NodeCheck(node))
            {
                return;
            }

            if(node.X == maxLevel)
            {
                node.AlignType = StructureNode.AlignTypes.Default;
                if (!node.IsCollapsed && NodeCheck(node))
                {
                    node.YOffset = defaultOffset;
                    defaultOffset++;
                }                
            }
            else
            {
                if(node.Parent == null)
                {
                    node.AlignType = StructureNode.AlignTypes.Child;
                }
                else
                {
                    if(node.Parent.AlignType == StructureNode.AlignTypes.Parent || node.Parent.AlignType == StructureNode.AlignTypes.Default)
                    {
                        node.AlignType = StructureNode.AlignTypes.Parent;
                    }
                    else
                    {
                        node.AlignType = StructureNode.AlignTypes.Child;
                    }
                }
            }

            aligned.Add(node);

            foreach (var child in node.Childs)
            {
                AlignItem(child);
            }          
        }

        /// <summary>
        /// Проверить расположение ячеек
        /// </summary>
        void CheckAlignItem(StructureNode node)
        {
            try
            {
                if(node.AlignType == StructureNode.AlignTypes.Parent)
                {
                    List<StructureNode> available = new List<StructureNode>();
                    foreach(var child in node.Parent.Childs)
                    {
                        if (child.IsCollapsed || !NodeCheck(child)) continue;
                        available.Add(child);
                    }
                    if (available.Count == 0) return;
                    var childCount = (double)available.Count;
                    var childIndex = available.IndexOf(node);
                    var parentY = node.Parent.YOffset;

                    var offset = (-childCount / 2) + childIndex;
                    node.YOffset = parentY + offset + 0.5;
                }else if(node.AlignType == StructureNode.AlignTypes.Child && node.Childs.Count > 0)
                {
                    List<StructureNode> available = new List<StructureNode>();
                    foreach (var child in node.Childs)
                    {
                        if (child.IsCollapsed || !NodeCheck(child)) continue;
                        available.Add(child);
                    }
                    if (available.Count == 0) return;
                    var childCount = (double)available.Count;
                    var childPos = available[0].YOffset;

                    var offset = (childPos + childCount / 2);
                    node.YOffset = offset + 0.5;
                }
                if(node.YOffset < 0)
                {
                    node.YOffset = 0;
                }
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при выравнивании ячейки {0}", node.Name);
            }
        }

        List<StructureNode> labeled = new List<StructureNode>();

        /// <summary>
        /// Добавить элемент
        /// </summary>
        void AddItem(StructureNode node, int offset)
        {
            if (node.IsCollapsed || !NodeCheck(node))
            {
                return;
            }

            added.Add(node);

            var newAnchor = node.StructureLabel;
            if(newAnchor == null)
            {
                newAnchor = new Label();
                if (selected.TopNodes.Contains(node))
                {
                    newAnchor.Style = mainNodeStyle;
                }
                else
                {
                    newAnchor.Style = childNodeStyle;
                }
                labeled.Add(node);
            }

            if (!panel.Children.Contains(newAnchor))
            {
                panel.Children.Add(newAnchor);
            }

            node.X = offset;
            node.XOffset = offset;
            node.StructureLabel = newAnchor;
            node.Y = levels[offset].Count;
            node.YOffset = levels[offset].Count;
            levels[offset].Add(node);
            newAnchor.DataContext = node;

            foreach(var child in node.Childs)
            {
                child.Parent = node;
                AddItem(child, offset + 1);
            }
        }

        /// <summary>
        /// Список линий
        /// </summary>
        List<Line> lines = new List<Line>();

        /// <summary>
        /// Смещение объекта
        /// </summary>
        double margin = 47;

        /// <summary>
        /// Показать объект
        /// </summary>
        void ShowItem(StructureNode node, StructureNode parent = null)
        {
            if (node.IsCollapsed || !NodeCheck(node))
            {
                return;
            }

            bool canExtend = false;
            foreach (var child in node.Childs)
            {
                if (NodeCheck(child))
                {
                    canExtend = true;
                    break;
                }
            }
            node.CanBeExtended = canExtend;

            var position = new Rect(itemSize.X * node.XOffset, itemSize.Y * node.YOffset, itemSize.X, itemSize.Y);
            RelativeLayoutPanel.SetRelativeRect(node.StructureLabel, position);

            if(parent != null)
            {
                try
                {
                    var line = new Line();

                    line.X1 = parent.XOffset * panel.Width * itemSize.X + defaultItemSize.X - margin;
                    line.X2 = node.XOffset * panel.Width * itemSize.X + margin;
                    line.Y1 = parent.YOffset * panel.Height * itemSize.Y + defaultItemSize.Y / 2;
                    line.Y2 = node.YOffset * panel.Height * itemSize.Y + defaultItemSize.Y / 2;

                    panel.Children.Add(line);
                    Panel.SetZIndex(line, -1);

                    RelativeLayoutPanel.SetRelativeRect(line, new Rect(0,0,1,1));
                    lines.Add(line);
                }catch(Exception ex)
                {
                    Output.Error(ex, "Ошибка при соединении объектов линией");
                }                
            }

            foreach(var child in node.Childs)
            {
                ShowItem(child, node);
            }
        }

        private void LinkNode(object sender, RoutedEventArgs e)
        {
            var item = (sender as Button).DataContext as StructureNode;
            item.LoodsmanId = -1;
            item.IsCollapsed = true;
            item.IsExtended = false;
            item.Description = "";
            LinkCheck(item);
            UpdateView();
        }

        /// <summary>
        /// Данная ячейка может иметь дочерние ячейки?
        /// </summary>
        bool CanHaveChildren(StructureNode node)
        {
            if (node.NodeType == "Изделие" || node.NodeType == "Комплекс" || node.NodeType == "Комплект" || node.NodeType == "Комплектное изделие" || node.NodeType == "Обезличенная СЕ" || node.NodeType == "Папка" || node.NodeType == "Сборка" || node.NodeType == "Сборочная единица" || node.NodeType == "") return true;
            return false;
        }

        /// <summary>
        /// Проверить на отсутствие циклических ссылок
        /// </summary>
        bool CheckRecursive(StructureNode check, StructureNode current)
        {
            if(current == null)
            {
                return false;
            }
            if(check == current)
            {
                return true;
            }
            return CheckRecursive(check, current.Parent);
        }

        void LinkCheck(StructureNode item)
        {
            if (CheckRecursive(item, item.Parent))
            {
                Output.ErrorFormat("Циклическая ссылка на объекте {0}", item.Name);
                return;
            }
            item.Childs.Clear();
            LoodsmanObjectPack objCheck = null;
            if(item.LoodsmanId >= 0)
            {
                objCheck = LoodsmanObjectManager.Instance.ByObjectId(item.LoodsmanId);
            }
            if(objCheck == null || objCheck.Count == 0)
            {
                objCheck = LoodsmanObjectManager.Instance.ByName(item.Name);
            }
            if (objCheck.Count > 0)
            {
                if (selected.TopNodes.Contains(item))
                {
                    item.NodeCount = objCheck.Count;
                }
                var ind = item.NodeIndex - 1;
                if(ind >= objCheck.Count)
                {
                    ind = 0;
                }
                LoodsmanObjectItem choosed = objCheck.Items[ind];

                if (choosed != null)
                {
                    var type = choosed.ObjectType;
                    if(type == null || type.Length == 0)
                    {
                        return;
                    }
                    if (!UseStandart && (type == "Стандартное изделие покупное" || type == "Стандартное изделие"))
                    {
                        return;
                    }
                    if(!UseUnnamed && (type == "Обезличенная Деталь" || type == "Обезличенная СЕ"))
                    {
                        return;
                    }
                    if(!UseOthers && type == "Прочее изделие")
                    {
                        return;
                    }
                    if(!UseChildFolders && type == "Папка" && item.Parent != null && item.Parent.NodeType != "Папка")
                    {
                        return;
                    }
                    if(item.Parent != null)
                    {
                        item.Parent.Childs.Add(item);
                    }
                    string desc = choosed.Description;
                    if(desc == null || desc.Length == 0)
                    {
                        desc = "-";
                    }
                    item.Description = desc;
                    item.NodeType = choosed.ObjectType;
                    item.LoodsmanId = choosed.ObjectId;
                    item.Name = choosed.Name;

                    if (CanHaveChildren(item))
                    {
                        var childsToAdd = LoodsmanTreeManager.Instance.ByParentId(item.LoodsmanId);
                        foreach (var child in childsToAdd)
                        {
                            StructureNode node = null;
                            var haveChk = StructureNodeManager.Instance.ByLoodsman(child.ObjectId, true);
                            if(haveChk != null)
                            {
                                node = haveChk;
                            }
                            else
                            {
                                node = new StructureNode();
                                node.LoodsmanId = child.ObjectId;
                                node.Save();
                            }

                            node.Parent = item;

                            LinkCheck(node);
                        }
                    }                    
                }
                else
                {
                    Output.WarningFormat("Объект {0} отсутствует", item.Name);
                }
            }
            else
            {
                Output.WarningFormat("Объект {0} отсутствует", item.Name);
            }
        }

        private void AddTopNode(object sender, RoutedEventArgs e)
        {
            if(selected == null)
            {
                Output.Warning("Изделие не выбрано");
                return;
            }
            var item = new StructureNode();
            item.Save();
            selected.TopNodes.Add(item);
            UpdateView();
        }

        private void RemoveNode(object sender, RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as StructureNode;
            selected.TopNodes.Remove(item);
            UpdateView();
        }

        private void ExtendNode(object sender, RoutedEventArgs e)
        {
            var node = (sender as Button).DataContext as StructureNode;
            node.IsExtended = true;
            UpdateView();
        }

        private void CollapseNode(object sender, RoutedEventArgs e)
        {
            var node = (sender as Button).DataContext as StructureNode;
            node.IsExtended = false;
            UpdateView();
        }

        bool isExtended = true;

        private void ToggleAll(object sender, RoutedEventArgs e)
        {
            if(selected == null)
            {
                Output.Warning("Изделие не выбрано");
                return;
            }
            isExtended = !isExtended;
            foreach(var node in selected.TopNodes)
            {
                ToggleAddRec(node);
            }
            UpdateView();
        }

        void ToggleAddRec(StructureNode node)
        {
            node.IsExtended = isExtended;
            node.IsCollapsed = !isExtended;
            foreach(var child in node.Childs)
            {
                ToggleAddRec(child);
            }
        }

        private void MoveLeft(object sender, RoutedEventArgs e)
        {
            var item = (sender as Button).DataContext as StructureNode;
            item.NodeIndex--;
            if(item.NodeIndex < 1)
            {
                item.NodeIndex = item.NodeCount;
            }
            LinkNode(sender, e);
        }

        private void MoveRight(object sender, RoutedEventArgs e)
        {
            var item = (sender as Button).DataContext as StructureNode;
            item.NodeIndex++;
            if (item.NodeIndex > item.NodeCount)
            {
                item.NodeIndex = 1;
            }
            LinkNode(sender, e);
        }

        private void Deattach(object sender, RoutedEventArgs e)
        {
            var item = (sender as MenuItem).DataContext as StructureNode;
            if (item == null) return;
            item.Parent.Childs.Remove(item);
            UpdateView();
        }
    }
}
