﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Data.SqlClient;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Объект для соединения с ЛОЦМАНом
    /// </summary>
    public class Loodsman : NotifyObject
    {
        static Loodsman instance;

        /// <summary>
        /// Проверить наличие экземпляра
        /// </summary>
        public void CheckInstance() { }

        protected Loodsman()
        {
            pathToLoodsman = AppSettings.GetString("PathToLoodsman");
            con = new SqlConnection(string.Format("Data Source={0}; Initial Catalog=product; Integrated Security=Yes", pathToLoodsman));
        }

        string pathToLoodsman;
        /// <summary>
        /// Путь до сервера ЛОЦМАНа
        /// </summary>
        public string PathToLoodsman
        {
            get
            {
                return pathToLoodsman;
            }
            set
            {
                if (pathToLoodsman != value)
                {
                    AppSettings.SetString("PathToLoodsman", value);
                    pathToLoodsman = value;
                    OnPropertyChanged("PathToLoodsman");
                }
            }
        }

        /// <summary>
        /// СОединение с ЛОЦМАНом
        /// </summary>
        SqlConnection con;

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static Loodsman Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new Loodsman();                            
                        }
                    }                    
                }
                return instance;
            }
        }

        /// <summary>
        /// Выполнить команду с ответом от базы
        /// </summary>
        public SQLData ExecuteRead(string command, params object[] args)
        {
            return ExecuteRead(string.Format(command, args));
        }

        /// <summary>
        /// Блокировщик
        /// </summary>
        object lockObj = new object();

        /// <summary>
        /// Открыть соединение
        /// </summary>
        public void Open()
        {
            con.Open();
        }

        /// <summary>
        /// Закрыть соединение
        /// </summary>
        public void Close()
        {
            con.Close();
        }

        /// <summary>
        /// Выполнить команду с ответом от базы
        /// </summary>
        public SQLData ExecuteRead(string command)
        {
            lock (lockObj)
            {
                var req = new SqlCommand(command, con);
                SQLData output = new SQLData();

                try
                {
                    var outData = req.ExecuteReader();

                    while (outData.Read())
                    {
                        SQLDataRow row = new SQLDataRow();
                        for (int i = 0; i < outData.FieldCount; i++)
                        {
                            row.Add(outData.GetName(i), outData[i]);
                        }
                        output.Add(row);
                    }
                    outData.Close();
                }
                catch (Exception ex)
                {
                    Output.Error(ex, "Ошибка при соединении с ЛОЦМАНом");
                }
                return output;
            }            
        }
    }
}
