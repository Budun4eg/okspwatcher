﻿using OKSPWatcher.Block;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Windows;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Логика взаимодействия для StructureSettingsWindow.xaml
    /// </summary>
    public partial class StructureSettingsWindow : BasicWindow
    {
        public StructureSettingsWindow()
        {
            
            InitializeComponent();
            
            Loodsman.Instance.CheckInstance();
            Application.Current.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                DataContext = this;
            }));
        }

        /// <summary>
        /// Путь до сервера ЛОЦМАНа
        /// </summary>
        public string PathToLoodsman
        {
            get
            {
                return Loodsman.Instance.PathToLoodsman;
            }
            set
            {
                Loodsman.Instance.PathToLoodsman = value;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BlockManager.Instance.UnBlock("App_StructureSettings");
        }
    }
}
