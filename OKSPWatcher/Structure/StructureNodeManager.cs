﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Менеджер ячеек структуры
    /// </summary>
    public class StructureNodeManager : ManagerObject<StructureNode>
    {
        static StructureNodeManager instance;

        private static object root = new object();

        protected StructureNodeManager() { }
        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static StructureNodeManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new StructureNodeManager();
                        }
                    }                    
                }
                return instance;
            }
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("StructureNodes").Execute(@"DROP TABLE StructureNodes");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("StructureNodes").Execute(@"CREATE TABLE IF NOT EXISTS StructureNodes (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    NodeType TEXT,
                                                    Name TEXT,
                                                    Description TEXT,
                                                    LoodsmanId INTEGER,
                                                    Childs TEXT,
                                                    UpdateDate INTEGER                        
                                                    )");
            SQL.Get("StructureNodes").Execute(@"CREATE INDEX IF NOT EXISTS StructureNodes_Name ON StructureNodes(Name)");
            SQL.Get("StructureNodes").Execute(@"CREATE INDEX IF NOT EXISTS StructureNodes_LoodsmanId ON StructureNodes(LoodsmanId)");

            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        /// <summary>
        /// Получить ячейку по идентификатору
        /// </summary>
        public override StructureNode ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM StructureNodes WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("StructureNodes").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new StructureNode(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Список ячеек по идентификатору ЛОЦМАНа
        /// </summary>
        Dictionary<long, StructureNode> byLoodsman = new Dictionary<long, StructureNode>();

        /// <summary>
        /// Получить ячейку по идентификатору ЛОЦМАНа
        /// </summary>
        public StructureNode ByLoodsman(long id, bool load = false)
        {
            if (byLoodsman.ContainsKey(id))
            {
                return byLoodsman[id];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM StructureNodes WHERE LoodsmanId = {0} LIMIT 1", id);
                    var data = SQL.Get("StructureNodes").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new StructureNode(row));
                    }
                    return ByLoodsman(id);
                }
                else
                {
                    return null;
                }
            }
        }        

        /// <summary>
        /// Список ячеек по наименованию
        /// </summary>
        Dictionary<string, StructureNodePack> byName = new Dictionary<string, StructureNodePack>();

        /// <summary>
        /// Получить ячейку по наименованию
        /// </summary>
        public StructureNodePack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM StructureNodes WHERE Name = '{0}' ORDER BY Id", key);
                    var data = SQL.Get("StructureNodes").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new StructureNode(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new StructureNodePack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Получить ячейку по наименованию
        /// </summary>
        public StructureNodePack ByNameCertain(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM StructureNodes WHERE Name = '{0}' LIMIT 1", key);
                    var data = SQL.Get("StructureNodes").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new StructureNode(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new StructureNodePack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Добавить ячейку
        /// </summary>
        public override bool Add(StructureNode item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new StructureNodePack() { item });
                    }
                }
                if (item.LoodsmanId < 0) return true;
                if (byLoodsman.ContainsKey(item.LoodsmanId))
                {
#if IN_EDITOR
                    Output.ErrorFormat("Дублирование ячейки с идентификатором ЛОЦМАНа {0}", item.LoodsmanId);
#endif
                }
                else
                {
                    byLoodsman.Add(item.LoodsmanId, item);
                }
                return true;
            }            
        }

        /// <summary>
        /// Удалить ячейку
        /// </summary>
        public override void Remove(StructureNode item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if (byLoodsman.ContainsKey(item.LoodsmanId))
                {
                    byLoodsman.Remove(item.LoodsmanId);
                }
            }            
        }
    }
}
