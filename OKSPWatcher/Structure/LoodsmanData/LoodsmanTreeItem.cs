﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;
using System.Collections.Generic;

namespace OKSPWatcher.Structure.LoodsmanData
{
    /// <summary>
    /// Объект дерева ЛОЦМАНа
    /// </summary>
    public class LoodsmanTreeItem : DBObject
    {
        List<long> parentIds = new List<long>();
        /// <summary>
        /// Идентификатор родительского объекта
        /// </summary>
        public List<long> ParentIds
        {
            get
            {
                return parentIds;
            }
        }

        /// <summary>
        /// Добавить нового родителя
        /// </summary>
        public void AddParent(long id)
        {
            if (!ParentIds.Contains(id))
            {
                Output.LogFormat("Добавлен новый родитель для ячейки {0}", name);
                parentIds.Add(id);
                LoodsmanTreeManager.Instance.UpdateParents(this, id);
                needUpdate = true;
            }            
        }

        long objectId;
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public long ObjectId
        {
            get
            {
                return objectId;
            }
        }

        string objectType;
        /// <summary>
        /// Тип объекта
        /// </summary>
        public string ObjectType
        {
            get
            {
                return objectType;
            }
        }

        public LoodsmanTreeItem(long _parentId, long _objectId, string _objectType, string _name, string _description) : base(){
            parentIds.Add(_parentId);
            objectId = _objectId;
            objectType = _objectType;
            name = _name;
            description = _description;
            needUpdate = true;
        }

        public LoodsmanTreeItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public LoodsmanTreeItem() : base() { }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                parentIds = data.Ids("ParentId");
                OnPropertyChanged("ParentIds");
                objectId = data.Long("ObjectId");
                OnPropertyChanged("ObjectId");
                objectType = data.String("ObjectType");
                OnPropertyChanged("ObjectType");
                Name = data.String("Name");
                Description = data.String("Description");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                parentIds = data.Ids("ParentId");
                objectId = data.Long("ObjectId");
                objectType = data.String("ObjectType");
                name = data.String("Name");
                description = data.String("Description");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "LoodsmanTreeItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO LoodsmanTreeItems (Name, Description, ParentId, ObjectId, ObjectType, UpdateDate) VALUES ('{0}', '{1}', '{2}', {3}, '{4}', {5})", out id,
                name.Check(), description.Check(), parentIds.Serialize(), objectId, objectType.Check(), UpdateDate.Ticks);
            base.CreateNew();
            LoodsmanTreeManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE LoodsmanTreeItems SET Name = '{0}', Description = '{1}', ParentId = '{2}', ObjectId = {3}, ObjectType = '{4}', UpdateDate = {5} WHERE Id = {6}",
                name.Check(), description.Check(), parentIds.Serialize(), objectId, objectType.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM LoodsmanTreeItems WHERE Id = {0}", id);
            LoodsmanTreeManager.Instance.Remove(this);
        }
    }
}
