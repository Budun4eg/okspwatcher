﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Structure.LoodsmanData
{
    /// <summary>
    /// Набор объектов из ЛОЦМАНа
    /// </summary>
    public class LoodsmanObjectPack : ItemPack<LoodsmanObjectItem>
    {
        public LoodsmanObjectPack() : base()
        {
            name = "LoodsmanObjectPack";
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = LoodsmanObjectManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
