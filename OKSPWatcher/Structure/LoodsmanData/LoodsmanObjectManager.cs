﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;

namespace OKSPWatcher.Structure.LoodsmanData
{
    /// <summary>
    /// Менеджер объектов ЛОЦМАНа
    /// </summary>
    public class LoodsmanObjectManager : ManagerObject<LoodsmanObjectItem>
    {
        private static object root = new object();

        static LoodsmanObjectManager instance;

        protected LoodsmanObjectManager() : base()
        {
            
        }

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static LoodsmanObjectManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if (instance == null)
                        {
                            instance = new LoodsmanObjectManager();
                        }
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// Список объектов по наименованию
        /// </summary>
        Dictionary<string, LoodsmanObjectPack> byName = new Dictionary<string, LoodsmanObjectPack>();

        /// <summary>
        /// Получить набор объектов по наименованию
        /// </summary>
        public LoodsmanObjectPack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanObjectItems WHERE Name = '{0}' ORDER BY Id", key);
                    var data = SQL.Get("LoodsmanObjectItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanObjectItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new LoodsmanObjectPack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Список объектов по идентификатору ЛОЦМАНа
        /// </summary>
        Dictionary<long, LoodsmanObjectPack> byObjectId = new Dictionary<long, LoodsmanObjectPack>();

        /// <summary>
        /// Есть объект с определенным идентификатором?
        /// </summary>
        /// <returns></returns>
        public bool HasObjectId(long id, bool load = false)
        {
            if (byObjectId.ContainsKey(id))
            {
                return true;
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanObjectItems WHERE ObjectId = {0} ORDER BY Id", id);
                    var data = SQL.Get("LoodsmanObjectItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanObjectItem(row));
                    }
                    return HasObjectId(id);
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Получить набор объектов по идентификатору ЛОЦМАНа
        /// </summary>
        public LoodsmanObjectPack ByObjectId(long id, bool load = false)
        {
            if (byObjectId.ContainsKey(id))
            {
                return byObjectId[id];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanObjectItems WHERE ObjectId = {0} ORDER BY Id", id);
                    var data = SQL.Get("LoodsmanObjectItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanObjectItem(row));
                    }
                    return ByObjectId(id);
                }
                else
                {
                    var item = new LoodsmanObjectPack();
                    byObjectId.Add(id, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Получить объект по идентификатору
        /// </summary>
        public override LoodsmanObjectItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM LoodsmanObjectItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("LoodsmanObjectItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new LoodsmanObjectItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }
        
        /// <summary>
        /// Добавить объект
        /// </summary>
        public override bool Add(LoodsmanObjectItem item)
        {
            lock (changeLock)
            {
                if (byObjectId.ContainsKey(item.ObjectId)) return false;
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new LoodsmanObjectPack() { item });
                    }
                }
                if (byObjectId.ContainsKey(item.ObjectId))
                {
                    byObjectId[item.ObjectId].Add(item);
                }
                else
                {
                    byObjectId.Add(item.ObjectId, new LoodsmanObjectPack() { item });
                }
                return true;
            }
        }

        /// <summary>
        /// Удалить объект
        /// </summary>
        public override void Remove(LoodsmanObjectItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if(byObjectId.ContainsKey(item.ObjectId) && byObjectId[item.ObjectId].Contains(item))
                {
                    byObjectId[item.ObjectId].Remove(item);
                    if(byObjectId[item.ObjectId].Count == 0)
                    {
                        byObjectId.Remove(item.ObjectId);
                    }
                }
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("LoodsmanObjectItems").Execute(@"DROP TABLE LoodsmanObjectItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("LoodsmanObjectItems").Execute(@"CREATE TABLE IF NOT EXISTS LoodsmanObjectItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    Description TEXT,
                                                    ObjectId INTEGER,
                                                    ObjectType TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("LoodsmanObjectItems").Execute(@"CREATE INDEX IF NOT EXISTS LoodsmanObjectItems_Name ON LoodsmanObjectItems(Name)");
            SQL.Get("LoodsmanObjectItems").Execute(@"CREATE INDEX IF NOT EXISTS LoodsmanObjectItems_ObjectId ON LoodsmanObjectItems(ObjectId)");
            instance = null;
        }

        /// <summary>
        /// Загрузить все объекты
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("LoodsmanObjectItems").ExecuteRead("SELECT * FROM LoodsmanObjectItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new LoodsmanObjectItem(row));
            }
        }
    }
}
