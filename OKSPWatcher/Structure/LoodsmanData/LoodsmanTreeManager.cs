﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System.Collections.Generic;

namespace OKSPWatcher.Structure.LoodsmanData
{
    class LoodsmanTreeManager : ManagerObject<LoodsmanTreeItem>
    {
        private static object root = new object();

        static LoodsmanTreeManager instance;

        protected LoodsmanTreeManager() : base()
        {

        }

        /// <summary>
        /// Текущий экземпляр менеджера
        /// </summary>
        public static LoodsmanTreeManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (root)
                    {
                        if (instance == null)
                        {
                            instance = new LoodsmanTreeManager();
                        }
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// Список объектов по наименованию
        /// </summary>
        Dictionary<string, LoodsmanTreePack> byName = new Dictionary<string, LoodsmanTreePack>();

        /// <summary>
        /// Получить набор объектов по наименованию
        /// </summary>
        public LoodsmanTreePack ByName(string key, bool load = false)
        {
            if (byName.ContainsKey(key))
            {
                return byName[key];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanTreeItems WHERE Name = '{0}' ORDER BY Id", key);
                    var data = SQL.Get("LoodsmanTreeItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanTreeItem(row));
                    }
                    return ByName(key);
                }
                else
                {
                    var item = new LoodsmanTreePack();
                    byName.Add(key, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Список объектов по идентификатору ЛОЦМАНа
        /// </summary>
        Dictionary<long, LoodsmanTreePack> byObjectId = new Dictionary<long, LoodsmanTreePack>();

        /// <summary>
        /// Есть объект с определенным идентификатором?
        /// </summary>
        /// <returns></returns>
        public bool HasObjectId(long id, bool load = false)
        {
            if (byObjectId.ContainsKey(id))
            {
                return true;
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanObjectItems WHERE ObjectId = {0} ORDER BY Id", id);
                    var data = SQL.Get("LoodsmanTreeItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanTreeItem(row));
                    }
                    return HasObjectId(id);
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Получить набор объектов по идентификатору ЛОЦМАНа
        /// </summary>
        public LoodsmanTreePack ByObjectId(long id, bool load = false)
        {
            if (byObjectId.ContainsKey(id))
            {
                return byObjectId[id];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanTreeItems WHERE ObjectId = {0} ORDER BY Id", id);
                    var data = SQL.Get("LoodsmanTreeItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanTreeItem(row));
                    }
                    return ByObjectId(id);
                }
                else
                {
                    var item = new LoodsmanTreePack();
                    byObjectId.Add(id, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Список объектов по родительскому идентификатору ЛОЦМАНа
        /// </summary>
        Dictionary<long, LoodsmanTreePack> byParentId = new Dictionary<long, LoodsmanTreePack>();

        /// <summary>
        /// Обновить список родителей объекта
        /// </summary>
        public void UpdateParents(LoodsmanTreeItem item, long newParent)
        {
            if (byParentId.ContainsKey(newParent))
            {
                byParentId[newParent].Add(item);
            }
            else
            {
                byParentId.Add(newParent, new LoodsmanTreePack() { item });
            }
        }

        /// <summary>
        /// Получить набор объектов по родительскому идентификатору ЛОЦМАНа
        /// </summary>
        public LoodsmanTreePack ByParentId(long id, bool load = false)
        {
            if (byParentId.ContainsKey(id))
            {
                return byParentId[id];
            }
            else
            {
                if (load)
                {
                    string request = string.Format("SELECT * FROM LoodsmanTreeItems WHERE ParentId LIKE '%{0}%' ORDER BY Id", id);
                    var data = SQL.Get("LoodsmanTreeItems").ExecuteRead(request);
                    foreach (SQLDataRow row in data)
                    {
                        Add(new LoodsmanTreeItem(row));
                    }
                    return ByParentId(id);
                }
                else
                {
                    var item = new LoodsmanTreePack();
                    byParentId.Add(id, item);
                    return item;
                }
            }
        }

        /// <summary>
        /// Получить объект по идентификатору
        /// </summary>
        public override LoodsmanTreeItem ById(long id, bool load = false)
        {
            var ret = base.ById(id, load);
            if (ret == null && load)
            {
                string request = string.Format("SELECT * FROM LoodsmanTreeItems WHERE Id = {0} LIMIT 1", id);
                var data = SQL.Get("LoodsmanTreeItems").ExecuteRead(request);
                foreach (SQLDataRow row in data)
                {
                    Add(new LoodsmanTreeItem(row));
                }
                return ById(id);
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Добавить объект
        /// </summary>
        public override bool Add(LoodsmanTreeItem item)
        {
            lock (changeLock)
            {
                if (byObjectId.ContainsKey(item.ObjectId)) return false;
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.Name != null)
                {
                    if (byName.ContainsKey(item.Name))
                    {
                        byName[item.Name].Add(item);
                    }
                    else
                    {
                        byName.Add(item.Name, new LoodsmanTreePack() { item });
                    }
                }
                if (byObjectId.ContainsKey(item.ObjectId))
                {
                    byObjectId[item.ObjectId].Add(item);
                }
                else
                {
                    byObjectId.Add(item.ObjectId, new LoodsmanTreePack() { item });
                }
                foreach(var parId in item.ParentIds)
                {
                    if (byParentId.ContainsKey(parId))
                    {
                        byParentId[parId].Add(item);
                    }
                    else
                    {
                        byParentId.Add(parId, new LoodsmanTreePack() { item });
                    }
                }                
                return true;
            }
        }

        /// <summary>
        /// Удалить объект
        /// </summary>
        public override void Remove(LoodsmanTreeItem item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.Name != null && byName.ContainsKey(item.Name) && byName[item.Name].Contains(item))
                {
                    byName[item.Name].Remove(item);
                    if (byName[item.Name].Count == 0)
                    {
                        byName.Remove(item.Name);
                    }
                }
                if (byObjectId.ContainsKey(item.ObjectId) && byObjectId[item.ObjectId].Contains(item))
                {
                    byObjectId[item.ObjectId].Remove(item);
                    if (byObjectId[item.ObjectId].Count == 0)
                    {
                        byObjectId.Remove(item.ObjectId);
                    }
                }
                foreach(var parId in item.ParentIds)
                {
                    if (byParentId.ContainsKey(parId) && byParentId[parId].Contains(item))
                    {
                        byParentId[parId].Remove(item);
                        if (byParentId[parId].Count == 0)
                        {
                            byParentId.Remove(parId);
                        }
                    }
                }                
            }
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("LoodsmanTreeItems").Execute(@"DROP TABLE LoodsmanTreeItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("LoodsmanTreeItems").Execute(@"CREATE TABLE IF NOT EXISTS LoodsmanTreeItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Name TEXT,
                                                    Description TEXT,
                                                    ParentId TEXT,
                                                    ObjectId INTEGER,
                                                    ObjectType TEXT,
                                                    UpdateDate INTEGER
                                                    )");
            SQL.Get("LoodsmanTreeItems").Execute(@"CREATE INDEX IF NOT EXISTS LoodsmanTreeItems_Name ON LoodsmanTreeItems(Name)");
            SQL.Get("LoodsmanTreeItems").Execute(@"CREATE INDEX IF NOT EXISTS LoodsmanTreeItems_ObjectId ON LoodsmanTreeItems(ObjectId)");
            SQL.Get("LoodsmanTreeItems").Execute(@"CREATE INDEX IF NOT EXISTS LoodsmanTreeItems_ParentId ON LoodsmanTreeItems(ParentId)");
            instance = null;
        }

        /// <summary>
        /// Загрузить все объекты
        /// </summary>
        public override void LoadAll()
        {
            var data = SQL.Get("LoodsmanTreeItems").ExecuteRead("SELECT * FROM LoodsmanTreeItems ORDER BY Id");
            foreach (SQLDataRow row in data)
            {
                if (BackgroundHandler.AllClose) return;
                Add(new LoodsmanTreeItem(row));
            }
        }
    }
}
