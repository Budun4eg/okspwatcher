﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using System;

namespace OKSPWatcher.Structure.LoodsmanData
{
    /// <summary>
    /// Данные об объекте из ЛОЦМАНа
    /// </summary>
    public class LoodsmanObjectItem : DBObject
    {
        long objectId;
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public long ObjectId
        {
            get
            {
                return objectId;
            }
        }

        string objectType;
        /// <summary>
        /// Тип объекта
        /// </summary>
        public string ObjectType
        {
            get
            {
                return objectType;
            }
        }

        public LoodsmanObjectItem(long _objectId, string _objectType, string _name, string _description) : base(){
            objectId = _objectId;
            objectType = _objectType;
            name = _name;
            description = _description;
            needUpdate = true;
        }

        public LoodsmanObjectItem(SQLDataRow data) : base()
        {
            Load(data);
        }

        public LoodsmanObjectItem() : base() { }

        protected override void UpdateState(SQLDataRow data)
        {
            base.UpdateState(data);
            try
            {
                objectId = data.Long("ObjectId");
                OnPropertyChanged("ObjectId");
                objectType = data.String("ObjectType");
                OnPropertyChanged("ObjectType");
                Name = data.String("Name");
                Description = data.String("Description");
            }
            catch(Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        public override void Load(SQLDataRow data)
        {
            base.Load(data);
            try
            {
                objectId = data.Long("ObjectId");
                objectType = data.String("ObjectType");
                name = data.String("Name");
                description = data.String("Description");
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }
        }

        protected override string sqlPath
        {
            get
            {
                return "LoodsmanObjectItems";
            }
        }

        protected override bool CreateNew()
        {
            UpdateDate = DateTime.Now;
            sql.Execute("INSERT INTO LoodsmanObjectItems (Name, Description, ObjectId, ObjectType, UpdateDate) VALUES ('{0}', '{1}', {2}, '{3}', {4})", out id,
                name.Check(), description.Check(), objectId, objectType.Check(), UpdateDate.Ticks);
            base.CreateNew();
            LoodsmanObjectManager.Instance.Add(this);
            return true;
        }

        protected override bool Update()
        {
            UpdateDate = DateTime.Now;
            base.Update();
            sql.Execute("UPDATE LoodsmanObjectItems SET Name = '{0}', Description = '{1}', ObjectId = {2}, ObjectType = '{3}', UpdateDate = {4} WHERE Id = {5}",
                name.Check(), description.Check(), objectId, objectType.Check(), UpdateDate.Ticks, id);
            return true;
        }

        public override void Delete()
        {
            base.Delete();
            sql.Execute("DELETE FROM LoodsmanObjectItems WHERE Id = {0}", id);
            LoodsmanObjectManager.Instance.Remove(this);
        }
    }
}
