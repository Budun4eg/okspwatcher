﻿using OKSPWatcher.Core;

namespace OKSPWatcher.Structure.LoodsmanData
{
    class LoodsmanTreePack : ItemPack<LoodsmanTreeItem>
    {
        public LoodsmanTreePack() : base()
        {
            name = "LoodsmanTreePack";
        }

        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = LoodsmanTreeManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
