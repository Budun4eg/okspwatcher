﻿using System;
using OKSPWatcher.Core;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Набор ячеек
    /// </summary>
    public class StructureNodePack : ItemPack<StructureNode>
    {
        protected override void Refresh()
        {
            base.Refresh();
            items.Clear();
            foreach (var id in idsToSync)
            {
                var item = StructureNodeManager.Instance.ById(id, true);
                if (item != null)
                {
                    Add(item);
                }
            }
        }
    }
}
