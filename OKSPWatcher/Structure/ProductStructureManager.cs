﻿using OKSPWatcher.Core;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Products;
using System.Collections.Generic;

namespace OKSPWatcher.Structure
{
    /// <summary>
    /// Менеджер структур изделий
    /// </summary>
    public class ProductStructureManager : ManagerObject<ProductStructure>
    {
        static ProductStructureManager instance;

        protected ProductStructureManager() {
            
        }

        private static object root = new object();

        /// <summary>
        /// Текущий экземпляр
        /// </summary>
        public static ProductStructureManager Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (root)
                    {
                        if(instance == null)
                        {
                            instance = new ProductStructureManager();
                        }
                    }                    
                }
                return instance;
            }
        }        

        /// <summary>
        /// Список структур по изделию
        /// </summary>
        Dictionary<Product, ProductStructure> byProduct = new Dictionary<Product, ProductStructure>();

        /// <summary>
        /// Получить структуру по изделию
        /// </summary>
        public ProductStructure ByProduct(Product item)
        {
            if(item == null)
            {
                return null;
            }
            if (byProduct.ContainsKey(item))
            {
                return byProduct[item];
            }
            else
            {
                var data = new ProductStructure(item);
                data.Save();
                return data;
            }
        }

        public override bool Add(ProductStructure item)
        {
            lock (changeLock)
            {
                var ret = base.Add(item);
                if (!ret) return false;
                if (item.LinkedProduct != null)
                {
                    if (byProduct.ContainsKey(item.LinkedProduct))
                    {
#if IN_EDITOR
                        Output.ErrorFormat("Дублирование данных по изделию {0}", item.LinkedProduct.Name);
#endif
                    }
                    else
                    {
                        byProduct.Add(item.LinkedProduct, item);
                    }
                }
                return true;
            }            
        }

        public override void Remove(ProductStructure item)
        {
            lock (changeLock)
            {
                base.Remove(item);
                if (item.LinkedProduct != null && byProduct.ContainsKey(item.LinkedProduct))
                {
                    byProduct.Remove(item.LinkedProduct);
                }
            }            
        }

        public override void DeleteStructure()
        {
            base.DeleteStructure();
            SQL.Get("StructureItems").Execute(@"DROP TABLE StructureItems");
        }

        public override void GenerateDBStructure()
        {
            base.GenerateDBStructure();
            SQL.Get("StructureItems").Execute(@"CREATE TABLE IF NOT EXISTS StructureItems (
                                                    Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    Product INTEGER,
                                                    TopNodes TEXT,
                                                    ReadyPercent TEXT,
                                                    UpdateDate INTEGER                      
                                                    )");
            instance = null;
        }

        public override void Unload()
        {
            base.Unload();
            instance = null;
        }
    }
}
