﻿using OKSPWatcher.Core;
using OKSPWatcher.Help;
using System;
using System.Windows;
using OKSPWatcher.Apps;
using System.Windows.Controls;
using OKSPWatcher.Admin;
using OKSPWatcher.AppSetting;
using OKSPWatcher.Core.SQLProcessor;
using OKSPWatcher.Worked;
using OKSPWatcher.Models;
using OKSPWatcher.Changes;
using OKSPWatcher.Changes3D;
using OKSPWatcher.Remarks;
using OKSPWatcher.DepMgr;
using OKSPWatcher.Chains;
using OKSPWatcher.Utilities;
using OKSPWatcher.Structure;
using OKSPWatcher.CompletedIn3D;
using OKSPWatcher.InfoApp;
using System.ComponentModel;
using System.Threading;
using OKSPWatcher.Block;
using System.Linq;
using OKSPWatcher.NotifyRequest;
using OKSPWatcher.Utilities.Apps;
using OKSPWatcher.Secondary;

namespace OKSPWatcher
{
    /// <summary>
    /// Логика взаимодействия для StartWindow.xaml
    /// </summary>
    public partial class StartWindow : BasicWindow
    {
        static StartWindow instance;

        public static StartWindow Instance
        {
            get
            {
                return instance;
            }
        }

        public StartWindow()
        {
            instance = this;            
            TopMenuEnabled = false;
            NotifyRequestManager.Init();
            InitializeComponent();            
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.DoubleClick += NotifyClick;
            notifyIcon.Icon = new System.Drawing.Icon("Images/Icon.ico");
            notifyIcon.BalloonTipText = "Watcher";

            System.Windows.Forms.MenuItem closeApp = new System.Windows.Forms.MenuItem();
            closeApp.Text = "Закрыть";
            closeApp.Click += CloseApp;

            System.Windows.Forms.ContextMenu menu = new System.Windows.Forms.ContextMenu();
            menu.MenuItems.AddRange(
                new System.Windows.Forms.MenuItem[] { closeApp }
                );

            notifyIcon.ContextMenu = menu;

            try
            {
                HelpShown = true;
                DataContext = MainApp.Instance;
                MainApp.Instance.Load();
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
            }

            if (DBBackupWindow.MustBackup())
            {
                NotifyRequestWindow.ShowNote("Backup", "Необходимо создать новый бэкап данных. Для создания нажмите на кнопку ниже.", "Перейти к созданию");
            }
        }

        private void CloseApp(object sender, EventArgs e)
        {
            RequestWindow.Show("Выйти из приложения?");
            if(RequestWindow.Result != RequestWindow.ResultType.Yes)
            {
                return;
            }
            try
            {
                var wind = windows.Values.ToList();
                for(int i = wind.Count - 1; i >= 0; i--)
                {
                    if (wind[i] != this)
                    {
                        wind[i].Close();
                    }
                }
                Close();
            }
            catch (Exception ex)
            {
                Output.WriteError(ex);
                Close();
            }
        }

        private void AppClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                var app = (sender as Control).DataContext as AppItem;
                OpenApp(app);
            }
            catch (Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии приложения");
            }
        }

        public override void ShowHelp()
        {
            try {
                base.ShowHelp();
                var helpWindow = new StartWindowHelp();
                helpWindow.Show();
            }
            catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии помощи");
            }            
        }

        /// <summary>
        /// Открыть приложение
        /// </summary>
        void OpenApp(AppItem app)
        {
            BasicWindow window;
            switch (app.Code)
            {
                case "Admin":
                    window = new AdminMainWindow();
                    window.Show();
                    break;
                case "WorkedReport":
                    window = new WorkedReportWindow();
                    window.Show();
                    break;
                case "WorkedRepair":
                    if (BlockManager.Instance.Block("App_WorkedRepair"))
                    {
                        window = new WorkedRepairWindow();
                        window.Show();
                        window.IsLocked = true;
                    }                    
                    break;
                case "WorkedSettings":
                    if (BlockManager.Instance.Block("App_WorkedSettings"))
                    {
                        window = new WorkedSettingsWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "ModelSettings":
                    if (BlockManager.Instance.Block("App_ModelSettings"))
                    {
                        window = new ModelSettingsWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "ModelReport":
                    window = new ModelReportWindow();
                    window.Show();
                    break;
                case "ModelRepair":
                    if (BlockManager.Instance.Block("App_ModelRepair"))
                    {
                        window = new ModelRepairWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "ChangesSettings":
                    if (BlockManager.Instance.Block("App_ChangesSettings"))
                    {
                        window = new ChangesSettingsWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "ChangesRepair":
                    if (BlockManager.Instance.Block("App_ChangesRepair"))
                    {
                        window = new ChangesRepairWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "Changes3DSettings":
                    if (BlockManager.Instance.Block("App_Changes3DSettings"))
                    {
                        window = new Changes3DSettings();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "Changes3D":
                    window = new Changes3DWindow();
                    window.Show();
                    break;
                case "Changes3DReport":
                    window = new Changes3DReportWindow();
                    window.Show();
                    break;
                case "RemarksSettings":
                    if (BlockManager.Instance.Block("App_RemarksSettings"))
                    {
                        window = new RemarkReportSettingsWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    
                    break;
                case "Remarks":
                    window = new CreateRemarkReportWindow();
                    window.Show();
                    break;
                case "RemarksReport":
                    window = new RemarkReportWindow();
                    window.Show();
                    break;
                case "DepManager":
                    string code = string.Format("App_DepManager_{0}", MainApp.Instance.CurrentUser.LinkedDep.Id);
                    if (BlockManager.Instance.Block(code))
                    {
                        window = new DepManagerWindow();
                        window.Show();
                        window.IsLocked = true;
                    }                    
                    break;
                case "Chains":
                    window = new ChainsManagerWindow();
                    window.Show();
                    break;
                case "ChainSettings":
                    if (BlockManager.Instance.Block("App_ChainSettings"))
                    {
                        window = new ChainSettingsWindow();
                        window.Show();
                        window.IsLocked = true;
                    }
                    break;
                case "Utilities":
                    window = new UtilitiesWindow();
                    window.Show();
                    break;
                case "CreateStructure":
                    window = new CreateStructureWindow();
                    window.Show();
                    break;
                case "StructureSettings":
                    if (BlockManager.Instance.Block("App_StructureSettings"))
                    {
                        window = new StructureSettingsWindow();
                        window.Show();
                        window.IsLocked = true;
                    }                    
                    break;
                case "CompletedIn3D":
                    window = new CompletedIn3DWindow();
                    window.Show();
                    break;
                case "InfoApp":
                    window = new InfoAppWindow();
                    window.Show();

                    break;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            
            int count = 0;
            BackgroundHandler.StopAll();
            while (!BackgroundHandler.CanBeClosed)
            {
                Thread.Sleep(100);
                count++;
                if (count >= BackgroundHandler.TimesToWait)
                {
                    break;
                }
            }
            SQL.Instance.Dispose();
        }

        private void OpenSettings(object sender, RoutedEventArgs e)
        {
            try
            {
                e.Handled = true;
                var app = (sender as Control).DataContext as AppItem;
                OpenApp(app.Settings);
            }
            catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии настроек");
            }                        
        }

        private void OpenReport(object sender, RoutedEventArgs e)
        {
            try {
                e.Handled = true;
                var app = (sender as Control).DataContext as AppItem;
                OpenApp(app.Report);
            }
            catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии отчета");
            }            
        }

        private void AppSettings(object sender, RoutedEventArgs e)
        {
            try {
                var window = new AppSettingsWindow();
                window.ShowDialog();
            }
            catch(Exception ex)
            {
                Output.Error(ex, "Ошибка при открытии настроек");
            }            
        }

        public override void Minimize()
        {
            base.Minimize();
            Visibility = Visibility.Collapsed;
        }

        public void Toggle()
        {
            worker.RunWorkerCheck();
        }

        private void NotifyClick(object sender, EventArgs e)
        {
            Visibility = Visibility.Visible;
            WindowState = WindowState.Minimized;
            Toggle();
        }

        protected override void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            base.BackgroundWork(sender, e);
            Thread.Sleep(1);
        }

        protected override void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.BackgroundCompleted(sender, e);
            WindowState = WindowState.Normal;
            Topmost = true;
            Topmost = false;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var workingArea = SystemParameters.WorkArea;
            Left = workingArea.Right - Width;
            Top = 0;
            var bottom = workingArea.Bottom - 250;
            Height = bottom;
            notifyIcon.Visible = true;
        }

        /// <summary>
        /// Иконка в трее
        /// </summary>
        System.Windows.Forms.NotifyIcon notifyIcon;

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            try
            {
                notifyIcon.Dispose();
                notifyIcon = null;
            }
            catch { }
            instance = null;  
        }
    }
}
